<?php

use App\Events\ReceiptCreated;
use App\Events\ReceiptNotification;
use Illuminate\Support\Facades\Route;
use App\User;
use App\Models\Roles;
use App\Models\Client;
/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Auth::routes();
Route::get('login/access/{token}', 'API\UserController@loginConfirm');
Route::post('/login/inter', 'Auth\LoginController@login_back')->name('logininter');;
Route::group(['middleware' => ['guest']], function () {
    Route::post('password/reset', 'Auth\ConfirmPasswordController@confirm');
});


Route::group(['middleware' => ['auth']], function () {

    Route::get('/ver_ContratoPdf', 'ContratoController@ContratoPdf')->name('ContratoPdf');
    
    Route::post('/logout', 'Auth\LoginController@logout')->name('logout');
    Route::get('/', 'HomeController@authtentication');
    Route::get('/configuracion/notificaciones', 'configuracionController@notificaciones')->name('notificaciones');
    Route::post('/configuracion/update_notificaciones', 'configuracionController@updateNotifications')->name('update_notificaciones');

    Route::get('/configuracion/password', 'configuracionController@password')->name('password');
    Route::post('/configuracion/update_password', 'configuracionController@updatePassword')->name('update_password');
    
    
    Route::group(['middleware' => ['Administrador']], function () {
        
        Route::get('/admin', function () {
            return view('admin.admin');//view('inicio');
        });
        

        /************************************************************ */
        /** Rutas para Back office de Cliente                         */
        /************************************************************ */
        /** Depositos */
        Route::get('/admin/clientes/clientes','Admin\adminController@clientes')->name('admin.clientes.clientes');
        Route::get('/admin/clientes/comprobante/{id}', 'Admin\adminController@comprobante')->name('admin.clientes.comprobante');
        Route::get('/admin/clientes/movimientoscliente/{id}', 'Admin\adminController@movimientoscliente')->name('admin.clientes.movimientoscliente');
        Route::post('/admin/clientes/subir_comprobante', 'Admin\adminController@updateComprobante')->name('admin.clientes.subir_comprobante');
        Route::get('/admin/clientes/editar_movimiento_cliente/{id}', 'Admin\adminController@editarmovimientocliente')->name('admin.clientes.editar_movimientos');
        Route::post('/admin/clientes/actualizar_movimiento/{id}', 'Admin\adminController@updateMovimientoCliente')->name('admin.clientes.actualizar_movimiento');
        /************************************************************ */
        /** Rutas para Back office de Socio                           */
        /************************************************************ */
        /** Depositos */
        Route::get('/admin/socios/socios','Admin\adminController@socios')->name('admin.socios.socios');
        Route::get('/admin/socios/comprobante/{partnerid}', 'Admin\adminController@socioComprobante')->name('admin.socios.comprobante');
        Route::get('/admin/socios/movimientossocio/{id}', 'Admin\adminController@movimientossocio')->name('admin.socios.movimientossocio');
        Route::post('/admin/socios/subir_comprobante', 'Admin\adminController@updateComprobantesocio')->name('admin.socios.subir_comprobante');
        Route::get('/admin/socios/editar_movimiento_socio/{id}', 'Admin\adminController@editarmovimientosocio')->name('admin.socios.editar_movimientos');
        Route::post('/admin/socios/actualizar_movimiento/{id}', 'Admin\adminController@updateMovimientoSocio')->name('admin.socios.actualizar_movimiento');
    
        /** Registro */

        Route::get('/admin/registro/usuario','Admin\adminController@usuario')->name('admin.registro.usuario');
        Route::post('/admin/registro/usuario_store','Admin\adminController@userStore')->name('admin.registro.usuario_store');
       
      
       
    
    });
    Route::get('banca/depositar_fondos', 'bancaController@depositarFondos')->name('depositar_fondos');


    /** Seccion de banca */
    Route::get('banca/depositar_fondos', 'bancaController@depositarFondos')->name('depositar_fondos');

    Route::get('banca/comprobante', 'bancaController@comprobante')->name('comprobante');
    Route::post('banca/update_comprobante', 'bancaController@updateComprobante')->name('update_comprobante');
    
    Route::get('banca/retirar_fondos', 'bancaController@retirarFondos')->name('retirar_fondos');
    Route::post('banca/update_retirar_fondos', 'bancaController@ClientWithdrawFunds')->name('update_retirar_fondos');

    Route::get('banca/movimientos', 'bancaController@movimientos')->name('movimientos');
    /************************************************************ */
    /** Rutas para el perfil de legal                             */
    /************************************************************ */
    Route::group(['middleware' => ['Legal']], function () {

        /**************************************************************************** */
        /** Rutas para mensajes de notificaciones sobre aprobacion de documentos      */
        /**************************************************************************** */
        Route::get('/notificaciones/documentos/mensajeDocs/{client}', 'Notificaciones\MessagesController@mensajeDocs')->name('notificaciones.documentos.mensajeDocs');
        Route::get('/notificaciones/documentos/mensajeDocsSocios/{partner}', 'Notificaciones\MessagesController@mensajeDocsSocio')->name('notificaciones.documentos.mensajeDocsSocio');
        Route::post('/notificaciones/documentos/mensajeDocs_store', 'Notificaciones\MessagesController@mensajeDocsStore')->name('notificaciones.documentos.mensajeDocs_store');

        /************************************************************ */
        /** Rutas para Back office de Cliente                         */
        /************************************************************ */
        /** Todos los Perfiles */
        Route::get('/legal/clientes/perfiles', 'Legal\legalController@perfilesCliente')->name('legal.clientes.perfiles');
        /** Aprobar documentos de clientes y actualizar porcentaje de registro */
        Route::post('/legal/clientes/perfiles_store', 'Legal\legalController@perfilesClienteStore')->name('legal.clientes.perfiles_store');
        /** Nuevos Perfiles (ultimos 7 dias) */
        Route::get('/legal/clientes/nuevos', 'Legal\legalController@clientesNuevos')->name('legal.clientes.nuevos');
         /** Perfiles no activados */
        Route::get('/legal/clientes/noActivados', 'Legal\legalController@clientesNoActivados')->name('legal.clientes.noActivados');
        /** Perfiles incompletos (con 1, 2 o 3 documentos) */
        Route::get('/legal/clientes/incompletos', 'Legal\legalController@clientesIncompletos')->name('legal.clientes.incompletos');
        /** Perfiles de clientes verificados */
        Route::get('/legal/clientes/verificados', 'Legal\legalController@clientesVerificados')->name('legal.clientes.verificados');
        /** Información del perfil del cliente */
        Route::get('/legal/clientes/infoCliente/{id}', 'Legal\legalController@infoPerfilCliente')->name('legal.clientes.infoCliente');
        Route::post('/legal/clientes/infoCliente_gains', 'Legal\legalController@gainsCliente')->name('legal.clientes.infoCliente.gains');
        /** Visualizar los documentos subidos por el cliente */
        Route::get('/legal/detail/{idClient}/{nameImage}/{image}/{value}','Legal\legalController@detail')->name('detail');
        
        /************************************************************ */
        /** Rutas para Back office de Socio                           */
        /************************************************************ */
        /** Todos los Perfiles */
        Route::get('/legal/socios/perfiles', 'Legal\legalController@perfilesSocio')->name('legal.socios.perfiles');
        /** Aprobar documentos de socios y actualizar porcentaje de registro */
        Route::post('/legal/socios/perfiles_store', 'Legal\legalController@perfilesSocioStore')->name('legal.socios.perfiles_store');
        /** Nuevos Perfiles (ultimos 7 dias) */
        Route::get('/legal/socios/nuevos', 'Legal\legalController@sociosNuevos')->name('legal.socios.nuevos');
         /** Perfiles no activados */
        Route::get('/legal/socios/noActivados', 'Legal\legalController@sociosNoActivados')->name('legal.socios.noActivados');
        /** Perfiles incompletos (con 1, 2 o 3 documentos) */
        Route::get('/legal/socios/incompletos', 'Legal\legalController@sociosIncompletos')->name('legal.socios.incompletos');
        /** Perfiles de socio verificados */
        Route::get('/legal/socios/verificados', 'Legal\legalController@sociosVerificados')->name('legal.socios.verificados');
        /** Información del perfil del socio */
        Route::get('/legal/socios/infoSocio/{id}', 'Legal\legalController@infoPerfilSocio')->name('legal.socios.infoSocio');
        Route::post('/legal/socios/infoSocio_gains', 'Legal\legalController@gainsSocio')->name('legal.socios.infoSocio.gains');
        /** Visualizar los documentos subidos por el socio */
        Route::get('/legal/detailSocio/{idSocio}/{nameImage}/{image}/{value}','Legal\legalController@detailSocio')->name('detailSocio');
    });

    /************************************************************ */
    /** Rutas para el perfil de Rendimiento                       */
    /************************************************************ */
    Route::group(['middleware' => ['Rendimiento']], function () {

        /************************************************************ */
        /** Rutas para Back office de Cliente                           */
        /************************************************************ */
        Route::get('/backoffice/socios/perfiles', 'Admin\adminController@perfilesSocio')->name('admin.socios.perfiles');
        /**************************************************************************** */
        /** Rutas para mensajes de notificaciones sobre depositos y retiros           */
        /**************************************************************************** */
        Route::get('/notificaciones/transacciones/mensajeDeposito/{clientReceipt}', 'Notificaciones\MessagesController@mensajeTransaccionDeposito')->name('notificaciones.transacciones.mensajeDeposito');
        Route::get('/notificaciones/transacciones/mensajeDepositoSocio/{partnerReceipt}', 'Notificaciones\MessagesController@mensajeTransaccionDepositoSocio')->name('notificaciones.transacciones.mensajeDepositoSocio');
        Route::get('/notificaciones/transacciones/mensajeRetiro/{clientWithDrawalRequest}', 'Notificaciones\MessagesController@mensajeTransaccionRetiro')->name('notificaciones.transacciones.mensajeRetiro');
        Route::get('/notificaciones/transacciones/mensajeRetiroSocio/{partnerWithDrawalRequest}', 'Notificaciones\MessagesController@mensajeTransaccionRetiroSocio')->name('notificaciones.transacciones.mensajeRetiroSocio');
        Route::post('/notificaciones/transacciones/mensaje_transaccion_store', 'Notificaciones\MessagesController@mensajeTransaccionStore')->name('notifications.transacciones.mensajeTransaccionStore');

        /************************************************************ */
        /** Rutas para Back office de Cliente                           */
        /************************************************************ */
        /** Depositos */
        Route::get('/rendimiento/clientes/depositos','Rendimiento\rendimientoController@DepositosCliente')->name('rendimiento.clientes.depositos');
        Route::post('/rendimiento/clientes/depositos_cliente_reviewed','Rendimiento\rendimientoController@RevisarComprobranteCliente')->name('rendimiento.clientes.depositos_cliente_reviewed');
        Route::get('/rendimiento/clientes/comprobante_cliente/{idClient}/{nameImage}/{image}','Rendimiento\rendimientoController@ComprobanteCliente')->name('rendimiento.clientes.comprobante_cliente');
        /** Retiros */
        Route::get('/rendimiento/clientes/retirarfondos','Rendimiento\rendimientoController@retirarFondosCliente')->name('rendimiento.clientes.retirarFondos');
        Route::post('/rendimiento/clientes/retiros_cliente_reviewed','Rendimiento\rendimientoController@RevisarRetiroCliente')->name('rendimiento.clientes.retiros_cliente_reviewed');
        /** Movimientos */
        Route::get('rendimiento/clientes/movimientos', 'Rendimiento\rendimientoController@clientes')->name('rendimiento.clientes.movimientos');
        Route::get('rendimiento/clientes/movimientos_cliente/{client}', 'Rendimiento\rendimientoController@movimientoscliente')->name('rendimiento.clientes.movimientos_Cliente');
    
        /** Operaciones */
        Route::get('/rendimiento/clientes/operaciones','Rendimiento\rendimientoController@operaciones')->name('rendimiento.clientes.operaciones');
        Route::get('/rendimiento/clientes/operacionesForm/{client}','Rendimiento\rendimientoController@operacionesForm')->name('operacionesForm');
        Route::get('/rendimiento/clientes/historyOperationsClientEdit/{idAcumulated}','Rendimiento\rendimientoController@historyOperationsClientEdit')->name('historyOperationsClientEdit');

        Route::get('/rendimiento/clientes/historyOperations/{client}','Rendimiento\rendimientoController@historyOperations')->name('historyOperations');
        Route::get('/rendimiento/operaciones/getComisionStatus/{account}/{comision}/{client_id}/{date}','Rendimiento\rendimientoController@getComisionStatus');
        Route::post('/rendimiento/operaciones/update','Rendimiento\rendimientoController@updateOperation')->name('operations_update');
        Route::post('rendimiento/updateComission', 'Rendimiento\rendimientoController@updateComission')->name('updateComission');
        Route::post('/rendimiento/clientes/operaciones/update/edit','Rendimiento\rendimientoController@updateOperationClientEdit')->name('operations_update_client_edit');

        /************************************************************ */
        /** Rutas para Back office de Socio                           */
        /************************************************************ */
        /** Depositos */
        Route::get('/rendimiento/socios/depositos','Rendimiento\rendimientoController@DepositosSocio')->name('rendimiento.socios.depositos');
        Route::post('/rendimiento/socios/depositos_socio_reviewed','Rendimiento\rendimientoController@RevisarComprobranteSocio')->name('rendimiento.socios.depositos_socio_reviewed');
        Route::get('/rendimiento/socios/comprobante_socio/{idPartner}/{nameImage}/{image}','Rendimiento\rendimientoController@ComprobanteSocio')->name('rendimiento.socios.comprobante_socio');
        /** Retiros */
        Route::get('/rendimiento/socios/retirarfondos','Rendimiento\rendimientoController@retirarFondosSocio')->name('rendimiento.socios.retirarFondos');
        Route::post('/rendimiento/socios/retiros_socio_reviewed','Rendimiento\rendimientoController@RevisarRetiroSocio')->name('rendimiento.socios.retiros_socio_reviewed');
        /** Movimientos */
        Route::get('rendimiento/socios/movimientos', 'Rendimiento\rendimientoController@socios')->name('rendimiento.socios.movimientos');
        Route::get('rendimiento/socios/movimientos_socio/{partner}', 'Rendimiento\rendimientoController@movimientossocio')->name('rendimiento.socios.movimientos_socio');
         /** Operaciones */
        Route::get('/rendimiento/socios/operaciones','Rendimiento\rendimientoController@OperacionesSocios')->name('rendimiento.socios.operaciones');
        Route::get('/rendimiento/socios/operacionesForm/{partner}','Rendimiento\rendimientoController@OperacionesSociosForm')->name('rendimiento.socios.operaciones_form');

        Route::get('/rendimiento/socios/historyOperationsPartner/{partner}','Rendimiento\rendimientoController@historyOperationsPartner')->name('historyOperationsPartner');
        Route::get('/rendimiento/socios/historyOperationsPartnerEdit/{idAcumulated}','Rendimiento\rendimientoController@historyOperationsPartnerEdit')->name('historyOperationsPartnerEdit');
        
        Route::get('/rendimiento/socios/operaciones/getComisionStatus/{account}/{comision}/{partner_id}/{date}','Rendimiento\rendimientoController@getComisionStatusPartner');
        Route::post('/rendimiento/socios/operaciones/update','Rendimiento\rendimientoController@updateOperationPartner')->name('operations_update_partner');
        Route::post('/rendimiento/socios/operaciones/update/edit','Rendimiento\rendimientoController@updateOperationPartnerEdit')->name('operations_update_partner_edit');
        Route::post('rendimiento/socios/updateComission', 'Rendimiento\rendimientoController@updateComissionPartner')->name('updateComissionPartner');
    });

    /************************************************************ */
    /** Rutas para el perfil de cliente                           */
    /************************************************************ */
    Route::group(['middleware' => ['Cliente']], function () {
        Route::get('/home', 'inicioController@index')->name('home');
    
        Route::get('/portafolio', 'portafolioController@index')->name('portafolio');
        Route::get('/inicio', 'inicioController@index')->name('inicio');
        Route::get('/soporte', 'inicioController@soporte')->name('soporte');
        Route::get('cliente/stadistics/{id}', 'inicioController@estadisticsHomeAccount');
        //congiguraciones de cuenta cliente
        Route::get('/personal', 'configuracionController@researchPersonal');

        Route::get('/personal/datosp', 'configuracionController@datosp')->name('datosp');
        Route::post('personal/update_datosp', 'configuracionController@updateDatosp')->name('update_datosp');

        Route::get('/personal/actividade', 'configuracionController@actividade')->name('actividade');
        Route::post('personal/update_actividade', 'configuracionController@updateDatosActividade')->name('update_actividade');
        Route::get('personal/editar_actividade', 'configuracionController@editActividade')->name('editar_actividade');

        Route::get('/personal/cargard', 'configuracionController@cargard')->name('cargard');
        Route::post('personal/update_cargard', 'configuracionController@updateDatosCargard')->name('update_cargard');
        Route::get('personal/editar_cargard', 'configuracionController@editCargard')->name('editar_cargard');
        Route::post('personal/update_cargard2', 'configuracionController@updateFilesClient')->name('update_cargard2');
        
        Route::get('/personal/firmac', 'configuracionController@firmac')->name('firmac');
        Route::post('/personal/update_firmac', 'configuracionController@updateFirmar')->name('update_firmac');

        
        Route::post('/updateTranslate', 'inicioController@updateTranslate');

        Route::get('/ver_contrato', 'ContratoController@verContrato')->name('ver_contrato');
        Route::post('personal/update_firm', 'configuracionController@uploadFirm')->name('update_firm');

        
    });


    /************************************************************ */
    /** Rutas para el perfil de socio                             */
    /************************************************************ */
    Route::group(['middleware' => ['Socio']], function () {
        Route::get('/socio', 'Socio\perfilController@researchPersonal');
        /** Seccion Inicio */
        Route::get('/socio/inicio', 'Socio\inicioController@inicioSocio')->name('socio.inicio');
        Route::get('socio/stadistics/{id}', 'Socio\inicioController@estadisticsHomeAccount');
        /*********************************************************************** */
        /** Rutas para el registro de socios                                     */
        /*********************************************************************** */
        /** Seccion Perfil (Registro datos de la empresa) */
        Route::get('/socio/registro_datos_empresa', 'Socio\perfilController@datosEmpresaForm')->name('socio.datos_empresa');
        Route::post('/socio/registro_datos_empresa/store', 'Socio\perfilController@DatosEmpresaStore')->name('socio.datos_empresa_store');
        /** Seccion Perfil (Registro datos del representante legal) */
        Route::get('/socio/registro_representante_legal', 'Socio\perfilController@DatosRepresentanteLegalForm')->name('socio.representante_legal');
        Route::post('/socio/registro_representante_legal/store', 'Socio\perfilController@DatosRepresentanteLegalStore')->name('socio.representante_legal_store');
        /** Seccion Perfil (Registro documentos del representante legal) */
        Route::get('/socio/registro_documentos_representante', 'Socio\perfilController@DocumentosRepresentanteForm')->name('socio.documentos_representante');
        Route::post('/socio/registro_documentos_representante/store', 'Socio\perfilController@DocumentosRepresentanteLegalStore')->name('socio.documentos_representante_store');
        /** Seccion Perfil (Registro firma del contrato) */
        Route::get('/socio/registro_firma_contrato', 'Socio\perfilController@FirmarContratoForm')->name('socio.firma_contrato');
        Route::post('/socio/registro_firma_contrato/store', 'Socio\perfilController@FirmaContratoStore')->name('socio.firma_contrato_store');
        Route::get('/ver_contrato', 'ContratoController@verContrato')->name('ver_contrato');
      
        Route::get('socio/editar_empresa', 'Socio\perfilController@editDatosEmpresa')->name('editar_datos_empresa');
        Route::get('socio/editar_documentos', 'Socio\perfilController@editCargard')->name('editar_cargard');
        Route::post('socio/update_documentos_post', 'Socio\perfilController@updateFilesClient')->name('update_cargard2');
    });
     Route::group(['middleware' => ['Legal']], function () {
        
        Route::get('/legal/contact', function () {
            return 'estamos en legal';
        });
    });    
    /*********************************************************************** */
    /** Rutas para notificaciones (para perfiles logueados)                  */
    /*********************************************************************** */
    /** Ver mensaje de las notificaciones sobre depositos y retiros (Solo para el perfil de cliente y socio) */
    Route::get('/messages/{id}', 'Notificaciones\MessagesController@show')->name('messages.show');
    /** Enviar notificaciones sobre novedades (Solo para el perfil legal y rendimiento) */
    Route::get('/novedad_axin', 'Notificaciones\MessagesController@NovedadAxinForm')->name('notificaciones.novedades.novedad');
    Route::post('/novedad_axin_store', 'Notificaciones\MessagesController@NovedadAxinStore')->name('notificaciones.novedades.novedad_store');
    /** Ver mensaje de las notificaciones sobre novedades de axin (Solo para el perfil de cliente y socio) */
    Route::get('/ver_novedad_axin/{id}', 'Notificaciones\MessagesController@verNovedadAxin')->name('notificaciones.novedades.verNovedad');
    /** Marcar Notificaciones como leidas (para los perfiles cliente, socio, legal y rendimiento) */
    Route::get('maskAsRead', function() {
        auth()->user()->unreadNotifications->markAsRead();
        return redirect()->back();
    })->name('maskAsRead');
    Route::get('/notificaciones', 'Notificaciones\NotificationController@index');

});


Route::get('/page/not/found',function(){
    abort(404,'Page not found');
    abort(403); 
});

Route::get('/notificaciones', 'Notificaciones\NotificationController@index');