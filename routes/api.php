<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|2 cuentas, no permitido
*/

/*Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});*/
//contact empresa
Route::post('contact', 'API\ContactController@contact');
//api
Route::post('login', 'API\UserController@login');
Route::post('register', 'API\UserController@register');
Route::post('register_socio', 'API\UserController@registerSocio');
Route::post('confirm/password','Auth\ConfirmPasswordController@confirmPassword');
//Route::get('confirm/token/{token}','Auth\ConfirmPasswordController@confirm_token');



Route::group(['middleware' => 'auth:api'], function(){
    Route::post('details', 'API\UserController@details');
    Route::get('logout', 'API\UserController@logout');
    Route::post('register_contact', 'legalController\@saveContact');

});
Route::get('auth/signup/activate/{token}', 'API\UserController@signupActivate');
//Route::get('detail', 'API\UserController@detail');
/*
Route::group(['prefix' => 'auth'], function () {
    Route::post('login', 'AuthController@login');
    Route::post('signup', 'AuthController@signup');
});*/

Route::post('/save-token', 'FCMController@index');


