<?php

use Illuminate\Database\Seeder;
use App\User;
use App\Models\Roles;
use App\Models\Client;
use App\Models\Partner;
use App\Models\AccountClient;
use App\Models\AccountPartner;
use Carbon\Carbon;
use App\Models\Accumulated;
class DatabaseSeeder extends Seeder
{
    /**
     *php artisan db:seed
     *php artisan passport:install
     *php artisan make:seeder UserSeeder
     *php artisan config:cache 
     *composer dump-autoload
     mailysanmar@th3code.com
     * @return void
     */
    public function run()
    {
        //cliente de prueba
        $user1 = User::create(['email'=>'cliente@axin.com','password'=>bcrypt('cliente12345')]);
        $use1 = User::find($user1->id);
        $use1->roles()->sync([4]);
        //$user1->rol_id = 4;
        $user1->is_active = true;
        $user1->save();
        $client1 = Client::create(['user_id' => $user1->id]);
        $client1->complete_profile = 0;
        $client1->save();
        $accountClient1 = AccountClient::create([
            'client_id'=>$client1->id,
            'type_account_id'=>'1',
            'account_number'=>'100001',
            'amount_money'=>'0',
            'is_active'=>true,
            'date_contract_init' => NULL,
            'date_contract_fin' => NULL
        ]);
        $accountClient2 = AccountClient::create([
            'client_id'=>$client1->id,
            'type_account_id'=>'2',
            'account_number'=>'100002',
            'amount_money'=>'0',
            'is_active'=>false,
            'date_contract_init' => NULL,
            'date_contract_fin' => NULL
        ]);
        $accountClient3 = AccountClient::create([
            'client_id'=>$client1->id,
            'type_account_id'=>'3',
            'account_number'=>'100003',
            'amount_money'=>'0',
            'is_active'=>false,
            'date_contract_init' => NULL,
            'date_contract_fin' => NULL
        ]);
        $accountClient1->save();
        $accountClient2->save();
        $accountClient3->save();
        //amalia cuenta
        $user12 = User::create(['email'=>'mailysanmar@gmail.com','password'=>bcrypt('12345678')]);
        $use12 = User::find($user12->id);
        $use12->roles()->sync([4]);
        //$user12->rol_id = 4;
        $user12->is_active = true;
        $user12->save();
        $client12 = Client::create(['user_id' => $user12->id]);
        $client12->complete_profile = 0;
        $client12->is_politically_exposed = false;
        $client12->is_declared_bankrupt = false;
        $client12->save();
        $accountClient12 = AccountClient::create([
            'client_id'=>$client12->id,
            'type_account_id'=>'1',
            'account_number'=>'100004',
            'amount_money'=>'0',
            'is_active'=>true,
            'date_contract_init' => NULL,
            'date_contract_fin' => NULL
        ]);
        $accountClient22 = AccountClient::create([
            'client_id'=>$client12->id,
            'type_account_id'=>'2',
            'account_number'=>'100005',
            'amount_money'=>'0',
            'is_active'=>false,
            'date_contract_init' => NULL,
            'date_contract_fin' => NULL
        ]);
        $accountClient32 = AccountClient::create([
            'client_id'=>$client12->id,
            'type_account_id'=>'3',
            'account_number'=>'100006',
            'amount_money'=>'0',
            'is_active'=>false,
            'date_contract_init' => NULL,
            'date_contract_fin' => NULL
        ]);
        $accountClient12->save();
        $accountClient22->save();
        $accountClient32->save();
        //cuenta para nestor nquibau@gmail.com
        $user321 = User::create(['email'=>'nquibau@gmail.com','password'=>bcrypt('12345678')]);
        $user321 = User::find($user321->id);
        $user321->roles()->sync([4]);
        //$user321->rol_id = 4;
        $user321->is_active = true;
        $user321->save();
        $client221 = Client::create(['user_id' => $user321->id]);
        $client221->complete_profile = 0;
        $client221->save();
        $accountClient121 = AccountClient::create([
            'client_id'=>$client221->id,
            'type_account_id'=>'1',
            'account_number'=>'100007',
            'amount_money'=>'0',
            'is_active'=>true,
            'date_contract_init' => NULL,
            'date_contract_fin' => NULL
        ]);
        $accountClient221 = AccountClient::create([
            'client_id'=>$client221->id,
            'type_account_id'=>'2',
            'account_number'=>'100008',
            'amount_money'=>'0',
            'is_active'=>false,
            'date_contract_init' => NULL,
            'date_contract_fin' => NULL
        ]);
        $accountClient321 = AccountClient::create([
            'client_id'=>$client221->id,
            'type_account_id'=>'3',
            'account_number'=>'100009',
            'amount_money'=>'0',
            'is_active'=>false,
            'date_contract_init' => NULL,
            'date_contract_fin' => NULL
        ]);
        $accountClient121->save();
        $accountClient221->save();
        $accountClient321->save();

        //cuenta para nestor contacto@axin.mx
        $user3211 = User::create(['email'=>'contacto@axin.mx','password'=>bcrypt('12345678')]);
        $user3211 = User::find($user3211->id);
        $user3211->roles()->sync([4]);
        //$user3211->rol_id = 4;
        $user3211->is_active = true;
        $user3211->save();
        $client2211 = Client::create(['user_id' => $user3211->id]);
        $client2211->complete_profile = 0;
        $client2211->save();
        $accountClient1211 = AccountClient::create([
            'client_id'=>$client2211->id,
            'type_account_id'=>'1',
            'account_number'=>'100010',
            'amount_money'=>'0',
            'is_active'=>true,
            'date_contract_init' => NULL,
            'date_contract_fin' => NULL
        ]);
        $accountClient2211 = AccountClient::create([
            'client_id'=>$client2211->id,
            'type_account_id'=>'2',
            'account_number'=>'100011',
            'amount_money'=>'0',
            'is_active'=>false,
            'date_contract_init' => NULL,
            'date_contract_fin' => NULL
        ]);
        $accountClient3211 = AccountClient::create([
            'client_id'=>$client2211->id,
            'type_account_id'=>'3',
            'account_number'=>'100012',
            'amount_money'=>'0',
            'is_active'=>false,
            'date_contract_init' => NULL,
            'date_contract_fin' => NULL
        ]);
        $accountClient1211->save();
        $accountClient2211->save();
        $accountClient3211->save();
        //socio 1
        $user_socio = User::create(['email'=>'socio@axin.com','password'=>bcrypt('socioaxin')]);
        $user_socio1 = User::find($user_socio->id);
        $user_socio1->roles()->sync([5]);
        //$user_socio1->rol_id = 5;
        $user_socio1->is_active = true;
        $user_socio1->save();
        $parther1 = Partner::create(['user_id' => $user_socio1->id]);
        $parther1->name_business = 'Socio Axin S.A';
        $parther1->complete_profile = 5;
        $parther1->performance_fee = '0.30';
        $parther1->management_fee = '0.20';
        $parther1->name_business = 'AXIN DEMO';
        $parther1->code_postal_business = '35345';
        $parther1->rfc_business = '9876543244567';
        $parther1->country_business = 'MEXICO';
        $parther1->direction_company = 'AMEROICA Y PERU ENTE 12321323';
        $parther1->city_business = 'MEXI';
        $parther1->number_phone = '8765435';
        $parther1->code_postal = '76543';
        $parther1->city = 'CERCADO';
        $parther1->birth_date_country = '2002-12-12';
        $parther1->country_code = '12';
        $parther1->type_identity_oficial = 'Pasaporte';
        $parther1->birth_date = '2002-12-12';
        $parther1->save();
        $accountPartner_s1 = AccountPartner::create([
            'partner_id'=>$parther1->id,
            'type_account_id'=>'4',
            'account_number'=>'50001',
            'amount_money'=>'0',
            'is_active'=>true,
            'date_contract_init' => NULL,
            'date_contract_fin' => NULL
        ]);
        $accountPartner_s1->save();

        
        //socio 2
        $user_socio = User::create(['email'=>'socio2@axin.com','password'=>bcrypt('socioaxin')]);
        $user_socio1 = User::find($user_socio->id);
        //$user_socio1->rol_id = 5;
        $user_socio1->roles()->sync([5]);
        $user_socio1->is_active = true;
        $user_socio1->save();
        $parther1 = Partner::create(['user_id' => $user_socio1->id]);
        $parther1->name_business = 'Socio Axin S.A';
        $parther1->complete_profile = 5;
        $parther1->performance_fee = '0.30';
        $parther1->management_fee = '0.20';
        $parther1->name_business = 'AXIN DEMO';
        $parther1->code_postal_business = '35345';
        $parther1->rfc_business = '9876543244567';
        $parther1->country_business = 'MEXICO';
        $parther1->direction_company = 'AMEROICA Y PERU ENTE 12321323';
        $parther1->city_business = 'MEXI';
        $parther1->number_phone = '8765435';
        $parther1->code_postal = '76543';
        $parther1->city = 'CERCADO';
        $parther1->birth_date_country = '12-12-12';
        $parther1->country_code = '12';
        $parther1->type_identity_oficial = 'Pasaporte';
        $parther1->birth_date = '12-12-12';
        $parther1->save();
        $accountPartner_s1 = AccountPartner::create([
            'partner_id'=>$parther1->id,
            'type_account_id'=>'4',
            'account_number'=>'50051',
            'amount_money'=>'10000',
            'is_active'=>true,
            'date_contract_init' => '2012-12-12',
            'date_contract_fin' => '2022-12-12'
        ]);
        $accountPartner_s1->save();
        //add operations
        for ($i=2019; $i < 2021; $i++) {
            for ($j=1; $j <= 12; $j++) {
                for ($k=1; $k <= 30; $k++) {
                        $date = Carbon::create($i, $j, $k, 0, 0, 0, 'America/Toronto');//Carbon::now();
                        $date2 = Carbon::create($i, $j, $k+1, 0, 0, 0, 'America/Toronto');//Carbon::now();
                        $rango = rand(-1000,1000);
                        $rango2 = rand(-10,10);
                        $comsion = $rango2>3?true:false;
                        $end = $rango2>3?$date2:NULL;
                        $accumulated = Accumulated::create([
                            'partner_id'=>$parther1->id,
                            'account_partner_id'=>$accountPartner_s1->id,
                            'amount'=>$rango,
                            'is_commision'=>$comsion,
                            'date'=>$date,
                            'date_end' => NULL
                        ]);
                }
            }
        }
        
        $date = new Carbon($accumulated->date);
        //socio 3
        $user_socio = User::create(['email'=>'socio3@axin.com','password'=>bcrypt('socioaxin')]);
        $user_socio1 = User::find($user_socio->id);
        $user_socio1->roles()->sync([5]);
        //$user_socio1->rol_id = 5;
        $user_socio1->is_active = true;
        $user_socio1->save();
        $parther1 = Partner::create(['user_id' => $user_socio1->id]);
        $parther1->name_business = 'Socio Axin S.A';
        $parther1->complete_profile = 0;
        $parther1->save();
        $accountPartner_s1 = AccountPartner::create([
            'partner_id'=>$parther1->id,
            'type_account_id'=>'4',
            'account_number'=>'50101',
            'amount_money'=>'0',
            'is_active'=>true,
            'date_contract_init' => NULL,
            'date_contract_fin' => NULL
        ]);
        $accountPartner_s1->save();
        //socio 4 amalia
        $user_socio = User::create(['email'=>'mailysanmar@th3code.com','password'=>bcrypt('12345678')]);
        $user_socio1 = User::find($user_socio->id);
        $user_socio1->roles()->sync([5]);
        //$user_socio1->rol_id = 5;
        $user_socio1->is_active = true;
        $user_socio1->save();
        $parther1 = Partner::create(['user_id' => $user_socio1->id]);
        $parther1->name_business = 'Socio Axin S.A';
        $parther1->complete_profile = 0;
        $parther1->save();
        $accountPartner_s1 = AccountPartner::create([
            'partner_id'=>$parther1->id,
            'type_account_id'=>'4',
            'account_number'=>'50151',
            'amount_money'=>'0',
            'is_active'=>true,
            'date_contract_init' => NULL,
            'date_contract_fin' => NULL
        ]);
        $accountPartner_s1->save();
        //administradores
        $user2 = User::create(['email'=>'administrador@axin.com','password'=>bcrypt('administrador12345')]);
        $use2 = User::find($user2->id);
        $use2->roles()->sync([1,2,3]);
        //$user2->rol_id = 1;
        $user2->is_active = true;
        $user2->save();

        $user3 = User::create(['email'=>'legal@axin.com','password'=>bcrypt('legal12345')]);
        $use3 = User::find($user3->id);
        $use3->roles()->sync([2]);
        //$user3->rol_id = 2;
        $user3->is_active = true;
        $user3->save();

        $user4 = User::create(['email'=>'banca@axin.com','password'=>bcrypt('banca12345')]);
        $use4 = User::find($user4->id);
        $use4->roles()->sync([3,2]);
        //$user4->rol_id = 3;
        $user4->is_active = true;
        $user4->save();

    }
}
