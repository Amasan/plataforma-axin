<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateUsersRolesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('roles_user', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->foreignId('user_id')->constrained('users');
            $table->foreignId('roles_id')->constrained('roles');
            $table->timestamps();
        });
        DB::statement('ALTER TABLE `roles_user` CHANGE `user_id` `user_id` BIGINT(20) UNSIGNED NULL DEFAULT NULL');
        DB::statement('ALTER TABLE `roles_user` CHANGE `roles_id` `roles_id` BIGINT(20) UNSIGNED NULL DEFAULT NULL');
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('users_roles');
    }
}
