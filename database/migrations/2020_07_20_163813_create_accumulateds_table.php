<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateAccumulatedsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('accumulateds', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->foreignId('client_id')->constrained('clients');
            $table->foreignId('account_client_id')->constrained('account_clients');
            $table->boolean('is_commision')->default(false);//valido para socio
            //partner_id
            //account_partner_id
            $table->decimal('amount', 10, 2)->default(0);
            $table->decimal('commissions', 10, 2)->default(0);
            $table->date('date');
            $table->date('date_end')->nullable();
            $table->timestamps();
        });
        DB::statement('ALTER TABLE `accumulateds` CHANGE `client_id` `client_id` BIGINT(20) UNSIGNED NULL DEFAULT NULL');
        DB::statement('ALTER TABLE `accumulateds` CHANGE `account_client_id` `account_client_id` BIGINT(20) UNSIGNED NULL DEFAULT NULL');
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('accumulateds');
    }
}
