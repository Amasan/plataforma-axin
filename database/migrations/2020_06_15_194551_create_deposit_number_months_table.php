<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateDepositNumberMonthsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('deposit_number_months', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('description',200)->nullable();
            $table->timestamps();
        });
        DB::table('deposit_number_months')->insert(array('id'=>'1','description'=>'Cada semana'));
        DB::table('deposit_number_months')->insert(array('id'=>'2','description'=>'Cada mes'));
        DB::table('deposit_number_months')->insert(array('id'=>'3','description'=>'Cada anio'));
        
        Schema::table('clients',function (Blueprint $table){
            $table->foreignId('deposit_number_month_id')->constrained('deposit_number_months')->nullable();
           });
        DB::statement('ALTER TABLE `clients` CHANGE `deposit_number_month_id` `deposit_number_month_id` BIGINT(20) UNSIGNED NULL DEFAULT NULL');
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('deposit_number_months');
    }
}
