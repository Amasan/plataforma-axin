<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class TypeAccountsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('type_accounts', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('name',50);
            $table->string('description',100)->nullable();
            $table->boolean('is_socio')->default(0);
            $table->timestamps();
        });
        DB::table('type_accounts')->insert(array('id'=>'1','name'=>'Ahorro','description'=>'Tipo Ahorro'));
        DB::table('type_accounts')->insert(array('id'=>'2','name'=>'Inversionista','description'=>'Tipo Inversionista'));
        DB::table('type_accounts')->insert(array('id'=>'3','name'=>'Mercado','description'=>'Tipo Mercado'));
        
        //DB::table('type_accounts')->insert(array('id'=>'4','name'=>'Afiliado','description'=>'Afiliado','is_socio'=>1));
        //DB::table('type_accounts')->insert(array('id'=>'5','name'=>'Gestor Archivos','description'=>'Gestor Archivos','is_socio'=>1));
        //DB::table('type_accounts')->insert(array('id'=>'6','name'=>'Empresas','description'=>'Empresas','is_socio'=>1));
        DB::table('type_accounts')->insert(array('id'=>'4','name'=>'Portafolio','description'=>'Portafolio personal','is_socio'=>1));
        /*Schema::table('clients',function (Blueprint $table){
            $table->foreignId('type_account_id')->constrained('type_accounts')->nullable();
        });
        DB::statement('ALTER TABLE `clients` CHANGE `type_account_id` `type_account_id` BIGINT(20) UNSIGNED NULL DEFAULT NULL');
        */
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
         Schema::dropIfExists('type_accounts');
    }
}
