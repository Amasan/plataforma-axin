<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateTotalFundsInvestTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('total_funds_invests', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('description',200)->nullable();
            $table->timestamps();
        });
        DB::table('total_funds_invests')->insert(array('id'=>'1','description'=>'0-500'));
        DB::table('total_funds_invests')->insert(array('id'=>'2','description'=>'500-2500'));
        DB::table('total_funds_invests')->insert(array('id'=>'3','description'=>'5K-10K'));
        DB::table('total_funds_invests')->insert(array('id'=>'4','description'=>'10K-25K'));
        DB::table('total_funds_invests')->insert(array('id'=>'5','description'=>'25K-50K'));
        DB::table('total_funds_invests')->insert(array('id'=>'6','description'=>'Más de 100K'));
        Schema::table('clients',function (Blueprint $table){
            $table->foreignId('total_funds_invest_id')->constrained('total_funds_invests')->nullable();
           });
        DB::statement('ALTER TABLE `clients` CHANGE `total_funds_invest_id` `total_funds_invest_id` BIGINT(20) UNSIGNED NULL DEFAULT NULL');
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('total_funds_invest');
    }
}
