<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateStrategiesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('strategies', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('number')->nullable();
            $table->string('strategy')->nullable();
            $table->date('date_opened');
            $table->date('date_closed');
            $table->string('instrument')->nullable();
            $table->string('action')->nullable();
            $table->decimal('lotes', 10, 2)->default(0);
            $table->string('sl_price', 10, 2)->default(0);
            $table->string('pt_price', 10, 2)->default(0);
            $table->string('open_price', 10, 2)->default(0);
            $table->string('close_price', 10, 2)->default(0);
            $table->string('pips', 10, 2)->default(0);
            $table->string('net_profit', 10, 2)->default(0);
            $table->string('gain', 10, 2)->default(0);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('strategies');
    }
}
