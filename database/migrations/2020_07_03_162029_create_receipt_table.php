<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateReceiptTable extends Migration
{
    /**
     * Run the migrations.
     *nquibau@gmail.com
     * @return void
     */
    public function up()
    {
        Schema::create('receipts', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->foreignId('client_id')->constrained('clients');
            //partner_id
            $table->string('file_name')->nullable();
            $table->string('file_url')->nullable();
            $table->foreignId('account_client_id')->constrained('account_clients');
            //account_partner_id
            $table->string('reviewed')->default('NULL');
            $table->timestamps();
        });
        DB::statement('ALTER TABLE `receipts` CHANGE `client_id` `client_id` BIGINT(20) UNSIGNED NULL DEFAULT NULL');
        DB::statement('ALTER TABLE `receipts` CHANGE `account_client_id` `account_client_id` BIGINT(20) UNSIGNED NULL DEFAULT NULL');
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('receipt');
    }
}
