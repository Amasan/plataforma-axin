<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateClientsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('clients', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->integer('complete_profile')->default(1);
            $table->string('country_code',3)->nullable();
            $table->string('number_phone',10)->nullable();
            $table->decimal('performance_fee', 20, 2)->default(0);
            $table->decimal('management_fee', 20, 2)->default(0);
            $table->date('birth_date')->nullable();
            $table->string('birth_date_country',20)->nullable();
            $table->string('code_postal',10)->nullable();
            $table->string('city',50)->nullable();
            $table->string('state_employee',50)->nullable();
            $table->boolean('is_politically_exposed')->nullable();
            $table->boolean('is_declared_bankrupt')->nullable();
            $table->boolean('is_active')->default(true);
            $table->enum('type_identity_oficial', ['IFE/INE', 'Pasaporte'])->nullable();
            $table->string('file_document_identity_oficial_V1')->nullable();
            $table->string('file_document_identity_oficial_V1_name')->nullable();
            $table->string('file_document_identity_oficial_V1_status')->nullable('NULL');
            $table->string('file_document_identity_oficial_V2')->nullable();
            $table->string('file_document_identity_oficial_V2_name')->nullable();
            $table->string('file_document_identity_oficial_V2_status')->nullable('NULL');
            $table->string('file_document_home')->nullable();
            $table->string('file_document_home_name')->nullable();
            $table->string('file_document_home_status')->default('NULL');
            $table->string('file_document_contract')->nullable();
            $table->string('file_document_contract_status')->nullable('NULL');
            
            $table->string('signature')->nullable();
            $table->boolean('is_active_newsletter')->default(false);
            $table->boolean('is_active_movements')->default(false);
            $table->foreignId('user_id')->constrained('users');
            $table->timestamps();
        });
        //cantH int COMMENT 'Campo para la cantidad de horas trabajadas',
        DB::statement('ALTER TABLE `clients` CHANGE `user_id` `user_id` BIGINT(20) UNSIGNED NULL DEFAULT NULL');
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('clients');
    }
}
