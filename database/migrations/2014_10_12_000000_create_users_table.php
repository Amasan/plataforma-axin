<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateUsersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('users', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('name',80)->nullable();
            $table->string('first_last_name',80)->nullable();
            $table->string('second_last_name',80)->nullable();
            $table->longText('fcm_token')->nullable();
            $table->string('direction',100)->nullable();
            $table->string('email')->unique();
            $table->string('username',40)->nullable();//deprecado
            $table->string('password',300);
            $table->string('type_document',30)->nullable();//deprecado
            $table->string('num_document',20)->nullable();//deprecado
            $table->string('telephone',10)->nullable();//deprecado
            $table->string('gender',16)->nullable();//deprecado
            $table->boolean('is_active')->default(1);
            $table->rememberToken();
            $table->longText('activation_token')->nullable();
            $table->softDeletes();
            $table->timestamps();
        });
        
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('users');
    }
}
