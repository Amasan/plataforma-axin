<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateRolesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('roles', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('name',30)->unique();
            $table->string('description',100)->nullable();
            $table->boolean('is_active')->default(false);
        });
        Schema::table('users',function (Blueprint $table){
            //$table->foreignId('rol_id')->constrained('roles')->nullable();
        });
        //DB::statement('ALTER TABLE `users` CHANGE `rol_id` `rol_id` BIGINT(20) UNSIGNED NULL DEFAULT NULL');
        DB::table('roles')->insert(array('id'=>'1','name'=>'Administrador','description'=>'Super Administrador'));
        DB::table('roles')->insert(array('id'=>'2','name'=>'Legal','description'=>'Area legal'));
        DB::table('roles')->insert(array('id'=>'3','name'=>'Rendimiento','description'=>'Area de rendimiento'));
        DB::table('roles')->insert(array('id'=>'4','name'=>'Cliente','description'=>'Cliente'));
        DB::table('roles')->insert(array('id'=>'5','name'=>'Socio','description'=>'Socio'));
        
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('roles');
        
    }
}
