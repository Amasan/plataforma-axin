<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateFamilyMemberHoldsPublicOfficesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('family_member_holds_public_offices', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('description',200)->nullable();
            $table->timestamps();
        });

        DB::table('family_member_holds_public_offices')->insert(array('id'=>'1','description'=>'Salario'));
        DB::table('family_member_holds_public_offices')->insert(array('id'=>'2','description'=>'Pension'));
        DB::table('family_member_holds_public_offices')->insert(array('id'=>'3','description'=>'Herencia'));
        DB::table('family_member_holds_public_offices')->insert(array('id'=>'4','description'=>'Ahorro'));
        
        DB::table('family_member_holds_public_offices')->insert(array('id'=>'5','description'=>'Ganancias del trading'));
        Schema::table('clients',function (Blueprint $table){
            $table->foreignId('family_member_holds_public_office_id')->constrained('family_member_holds_public_offices')->nullable();
        });
        DB::statement('ALTER TABLE `clients` CHANGE `family_member_holds_public_office_id` `family_member_holds_public_office_id` BIGINT(20) UNSIGNED NULL DEFAULT NULL');
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('family_member_holds_public_offices');
    }
}
