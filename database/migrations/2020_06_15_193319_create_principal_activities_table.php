<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreatePrincipalActivitiesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('principal_activities', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('description',200)->nullable();
            $table->timestamps();
        });
        DB::table('principal_activities')->insert(array('id'=>'1','description'=>'actividad 1'));
        DB::table('principal_activities')->insert(array('id'=>'2','description'=>'actividad 2'));
        DB::table('principal_activities')->insert(array('id'=>'3','description'=>'actividad 3'));
        
        Schema::table('clients',function (Blueprint $table){
            $table->foreignId('principal_activity_id')->constrained('principal_activities')->nullable();
        });
        DB::statement('ALTER TABLE `clients` CHANGE `principal_activity_id` `principal_activity_id` BIGINT(20) UNSIGNED NULL DEFAULT NULL');
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('principal_activities');
    }
}
