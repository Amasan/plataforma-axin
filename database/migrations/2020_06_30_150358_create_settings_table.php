<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateSettingsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('settings', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('key')->nullable();
            $table->string('value')->nullable();
            $table->boolean('copy')->default(0);
            $table->boolean('is_payment')->default(1);
            $table->timestamps();
        });

        DB::table('settings')->insert(array('id'=>'1','key'=>'Banco','value'=>'BBVA, Bancomer'));
        DB::table('settings')->insert(array('id'=>'2','key'=>'Moneda','value'=>'Dólares americanos'));
        DB::table('settings')->insert(array('id'=>'3','key'=>'Beneficiario','value'=>'Axin valores A.A.P.I. de C.V.'));
        DB::table('settings')->insert(array('id'=>'4','key'=>'SWIFT','value'=>'BBRCMZCT','copy'=>1));
        DB::table('settings')->insert(array('id'=>'5','key'=>'Numero de cuenta','value'=>'255010080','copy'=>1));
        DB::table('settings')->insert(array('id'=>'6','key'=>'CLAVE','value'=>'110201244','copy'=>1));
        DB::table('settings')->insert(array('id'=>'7','key'=>'Referencia','value'=>'Tu número de cuenta Axin'));
        DB::table('settings')->insert(array('id'=>'8','key'=>'generate_account_client','value'=>'100012','is_payment'=>0));
        DB::table('settings')->insert(array('id'=>'9','key'=>'generate_account_socio','value'=>'50201','is_payment'=>0));
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('settings');
    }
}
