<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateWithdrawalrequestTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('withdrawalrequests', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('name');
            $table->string('email');
            $table->enum('concept', ['Retiro', 'Deposito'])->default('Retiro');
            //foreignId account-client id
            //accountpather_id
            $table->decimal('amount', 10, 2)->default(0);
            $table->enum('method', ['Bitcoin', 'Transferencia'])->nullable();
            $table->string('bitcoin_address')->nullable();
            $table->string('bank_name',150)->nullable();
            $table->string('bank_number',100)->nullable();
            $table->string('swift_code',50)->nullable();
            $table->string('ivan_code',50)->nullable();
            $table->enum('status', ['Pendiente', 'Realizado','Rechazado'])->default('Pendiente');
            $table->string('date_deposite')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('withdrawalrequest');
    }
}
