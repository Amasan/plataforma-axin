<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateAccountpathersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('accountpathers', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->foreignId('partner_id')->constrained('partners');
            $table->foreignId('type_account_id')->constrained('type_accounts');
            $table->string('account_number',10)->nullable();//nro cuenta
            $table->decimal('amount_money', 20, 2)->default(0);//inversion acumulada
            $table->decimal('withdrawals', 20, 2)->default(0);//retiros
            $table->boolean('is_active')->default(false);
            $table->date('date_contract_init')->nullable();
            $table->date('date_contract_fin')->nullable();
            $table->timestamps();
        });
        DB::statement('ALTER TABLE `accountpathers` CHANGE `type_account_id` `type_account_id` BIGINT(20) UNSIGNED NULL DEFAULT NULL');
        DB::statement('ALTER TABLE `accountpathers` CHANGE `partner_id` `partner_id` BIGINT(20) UNSIGNED NULL DEFAULT NULL');
        
        Schema::table('receipts',function (Blueprint $table){
            $table->foreignId('partner_id')->constrained('partners');
            $table->foreignId('account_partner_id')->constrained('accountpathers');
        });
        DB::statement('ALTER TABLE `receipts` CHANGE `partner_id` `partner_id` BIGINT(20) UNSIGNED NULL DEFAULT NULL');
        DB::statement('ALTER TABLE `receipts` CHANGE `account_partner_id` `account_partner_id` BIGINT(20) UNSIGNED NULL DEFAULT NULL');
        Schema::table('accumulateds',function (Blueprint $table){
            $table->foreignId('partner_id')->constrained('partners');
            $table->foreignId('account_partner_id')->constrained('accountpathers');
        });
        DB::statement('ALTER TABLE `accumulateds` CHANGE `partner_id` `partner_id` BIGINT(20) UNSIGNED NULL DEFAULT NULL');
        DB::statement('ALTER TABLE `accumulateds` CHANGE `account_partner_id` `account_partner_id` BIGINT(20) UNSIGNED NULL DEFAULT NULL');
        Schema::table('withdrawalrequests',function (Blueprint $table){
            $table->foreignId('account_partner_id')->constrained('accountpathers')->nullable();
        });
        DB::statement('ALTER TABLE `withdrawalrequests` CHANGE `account_partner_id` `account_partner_id` BIGINT(20) UNSIGNED NULL DEFAULT NULL');
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('accountpathers');
    }
}
