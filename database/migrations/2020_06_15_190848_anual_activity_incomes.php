<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AnualActivityIncomes extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('anual_activity_incomes', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('description',200)->nullable();
            $table->timestamps();
        });
        DB::table('anual_activity_incomes')->insert(array('id'=>'1','description'=>'0-50,000'));
        DB::table('anual_activity_incomes')->insert(array('id'=>'2','description'=>'50,000-100,000'));
        DB::table('anual_activity_incomes')->insert(array('id'=>'3','description'=>'100,000-250,000'));
        DB::table('anual_activity_incomes')->insert(array('id'=>'4','description'=>'250,000-500,000'));
        DB::table('anual_activity_incomes')->insert(array('id'=>'5','description'=>'Más de 500,000'));
        
        
        Schema::table('clients',function (Blueprint $table){
            $table->foreignId('anual_activity_income_id')->constrained('anual_activity_incomes')->nullable();
        });
        DB::statement('ALTER TABLE `clients` CHANGE `anual_activity_income_id` `anual_activity_income_id` BIGINT(20) UNSIGNED NULL DEFAULT NULL');
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('anual_activity_incomes');
    }
}
