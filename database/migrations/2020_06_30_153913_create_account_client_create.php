<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateAccountClientCreate extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('account_clients', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->foreignId('client_id')->constrained('clients');
            $table->foreignId('type_account_id')->constrained('type_accounts');
            $table->string('account_number',10)->nullable();//nro cuenta
            $table->decimal('amount_money', 20, 2)->default(0);//inversion acumulada
            $table->decimal('withdrawals', 20, 2)->default(0);//retiros
            $table->boolean('is_active')->default(false);
            $table->date('date_contract_init')->nullable();
            $table->date('date_contract_fin')->nullable();
            //$table->date('date_deposite');
            $table->timestamps();
        });

        DB::statement('ALTER TABLE `account_clients` CHANGE `type_account_id` `type_account_id` BIGINT(20) UNSIGNED NULL DEFAULT NULL');
        DB::statement('ALTER TABLE `account_clients` CHANGE `client_id` `client_id` BIGINT(20) UNSIGNED NULL DEFAULT NULL');
        
        Schema::table('withdrawalrequests',function (Blueprint $table){
            $table->foreignId('account_client_id')->constrained('account_clients')->nullable();
        });
        DB::statement('ALTER TABLE `withdrawalrequests` CHANGE `account_client_id` `account_client_id` BIGINT(20) UNSIGNED NULL DEFAULT NULL');
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('account_client_create');
    }
}
