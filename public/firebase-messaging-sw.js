importScripts('https://www.gstatic.com/firebasejs/7.17.2/firebase-app.js');
importScripts('https://www.gstatic.com/firebasejs/7.17.2/firebase-messaging.js');

// Initialize the Firebase app in the service worker by passing in
// your app's Firebase config object.
// https://firebase.google.com/docs/web/setup#config-object
 var firebaseConfig = {
        apiKey: "AIzaSyBCFlUMyv9cOB1v4qz98VphHB7Ubxvar6E",
        authDomain: "axin-ebb8f.firebaseapp.com",
        databaseURL: "https://axin-ebb8f.firebaseio.com",
        projectId: "axin-ebb8f",
        storageBucket: "axin-ebb8f.appspot.com",
        messagingSenderId: "496139219905",
        appId: "1:496139219905:web:5c6d96e585bcc150b63540",
        measurementId: "G-QLYQX9DJ5D"
};
        // Initialize Firebase
firebase.initializeApp(firebaseConfig);

// Retrieve an instance of Firebase Messaging so that it can handle background
// messages.
const messaging = firebase.messaging();

messaging.setBackgroundMessageHandler(function(payload) {
  console.log('[firebase-messaging-sw.js] Received background message ', payload);
  // Customize notification here
  const {title, body} = payload.notification;
  const notificationOptions = {
    body,
  };

  return self.registration.showNotification(title,
    notificationOptions);
});
