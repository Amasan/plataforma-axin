/******/ (function(modules) { // webpackBootstrap
/******/ 	// The module cache
/******/ 	var installedModules = {};
/******/
/******/ 	// The require function
/******/ 	function __webpack_require__(moduleId) {
/******/
/******/ 		// Check if module is in cache
/******/ 		if(installedModules[moduleId]) {
/******/ 			return installedModules[moduleId].exports;
/******/ 		}
/******/ 		// Create a new module (and put it into the cache)
/******/ 		var module = installedModules[moduleId] = {
/******/ 			i: moduleId,
/******/ 			l: false,
/******/ 			exports: {}
/******/ 		};
/******/
/******/ 		// Execute the module function
/******/ 		modules[moduleId].call(module.exports, module, module.exports, __webpack_require__);
/******/
/******/ 		// Flag the module as loaded
/******/ 		module.l = true;
/******/
/******/ 		// Return the exports of the module
/******/ 		return module.exports;
/******/ 	}
/******/
/******/
/******/ 	// expose the modules object (__webpack_modules__)
/******/ 	__webpack_require__.m = modules;
/******/
/******/ 	// expose the module cache
/******/ 	__webpack_require__.c = installedModules;
/******/
/******/ 	// define getter function for harmony exports
/******/ 	__webpack_require__.d = function(exports, name, getter) {
/******/ 		if(!__webpack_require__.o(exports, name)) {
/******/ 			Object.defineProperty(exports, name, { enumerable: true, get: getter });
/******/ 		}
/******/ 	};
/******/
/******/ 	// define __esModule on exports
/******/ 	__webpack_require__.r = function(exports) {
/******/ 		if(typeof Symbol !== 'undefined' && Symbol.toStringTag) {
/******/ 			Object.defineProperty(exports, Symbol.toStringTag, { value: 'Module' });
/******/ 		}
/******/ 		Object.defineProperty(exports, '__esModule', { value: true });
/******/ 	};
/******/
/******/ 	// create a fake namespace object
/******/ 	// mode & 1: value is a module id, require it
/******/ 	// mode & 2: merge all properties of value into the ns
/******/ 	// mode & 4: return value when already ns object
/******/ 	// mode & 8|1: behave like require
/******/ 	__webpack_require__.t = function(value, mode) {
/******/ 		if(mode & 1) value = __webpack_require__(value);
/******/ 		if(mode & 8) return value;
/******/ 		if((mode & 4) && typeof value === 'object' && value && value.__esModule) return value;
/******/ 		var ns = Object.create(null);
/******/ 		__webpack_require__.r(ns);
/******/ 		Object.defineProperty(ns, 'default', { enumerable: true, value: value });
/******/ 		if(mode & 2 && typeof value != 'string') for(var key in value) __webpack_require__.d(ns, key, function(key) { return value[key]; }.bind(null, key));
/******/ 		return ns;
/******/ 	};
/******/
/******/ 	// getDefaultExport function for compatibility with non-harmony modules
/******/ 	__webpack_require__.n = function(module) {
/******/ 		var getter = module && module.__esModule ?
/******/ 			function getDefault() { return module['default']; } :
/******/ 			function getModuleExports() { return module; };
/******/ 		__webpack_require__.d(getter, 'a', getter);
/******/ 		return getter;
/******/ 	};
/******/
/******/ 	// Object.prototype.hasOwnProperty.call
/******/ 	__webpack_require__.o = function(object, property) { return Object.prototype.hasOwnProperty.call(object, property); };
/******/
/******/ 	// __webpack_public_path__
/******/ 	__webpack_require__.p = "/";
/******/
/******/
/******/ 	// Load entry module and return exports
/******/ 	return __webpack_require__(__webpack_require__.s = 1);
/******/ })
/************************************************************************/
/******/ ({

/***/ "./node_modules/ui-huma/js/app.js":
/*!****************************************!*\
  !*** ./node_modules/ui-huma/js/app.js ***!
  \****************************************/
/*! no exports provided */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _main__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./main */ "./node_modules/ui-huma/js/main.js");
/* harmony import */ var _main__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(_main__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var _perfect_scrollbar__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./perfect-scrollbar */ "./node_modules/ui-huma/js/perfect-scrollbar.js");
/* harmony import */ var _perfect_scrollbar__WEBPACK_IMPORTED_MODULE_1___default = /*#__PURE__*/__webpack_require__.n(_perfect_scrollbar__WEBPACK_IMPORTED_MODULE_1__);
/* harmony import */ var _preloader__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ./preloader */ "./node_modules/ui-huma/js/preloader.js");
/* harmony import */ var _preloader__WEBPACK_IMPORTED_MODULE_2___default = /*#__PURE__*/__webpack_require__.n(_preloader__WEBPACK_IMPORTED_MODULE_2__);
/* harmony import */ var _sidebar__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ./sidebar */ "./node_modules/ui-huma/js/sidebar.js");
/* harmony import */ var _sidebar__WEBPACK_IMPORTED_MODULE_3___default = /*#__PURE__*/__webpack_require__.n(_sidebar__WEBPACK_IMPORTED_MODULE_3__);



 // import './popover'
// import './mdk-carousel-control'
// import './read-more'

(function () {
  'use strict';

  $('[data-toggle="tab"]').on('hide.bs.tab', function (e) {
    $(e.target).removeClass('active');
  }); ///////////////////////////////////
  // Custom JavaScript can go here //
  ///////////////////////////////////
})();

/***/ }),

/***/ "./node_modules/ui-huma/js/main.js":
/*!*****************************************!*\
  !*** ./node_modules/ui-huma/js/main.js ***!
  \*****************************************/
/*! no static exports found */
/***/ (function(module, exports) {

(function () {
  'use strict'; // Self Initialize DOM Factory Components

  domFactory.handler.autoInit(); // ENABLE TOOLTIPS

  $('[data-toggle="tooltip"]').tooltip();
})();

/***/ }),

/***/ "./node_modules/ui-huma/js/perfect-scrollbar.js":
/*!******************************************************!*\
  !*** ./node_modules/ui-huma/js/perfect-scrollbar.js ***!
  \******************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

(function () {
  'use strict';

  $('[data-perfect-scrollbar]').each(function () {
    var $element = $(this);
    var element = this;
    var ps = new PerfectScrollbar(element, {
      wheelPropagation: void 0 !== $element.data('perfect-scrollbar-wheel-propagation') ? $element.data('perfect-scrollbar-wheel-propagation') : false,
      suppressScrollY: void 0 !== $element.data('perfect-scrollbar-suppress-scroll-y') ? $element.data('perfect-scrollbar-suppress-scroll-y') : false,
      swipeEasing: false
    });
    Object.defineProperty(element, 'PerfectScrollbar', {
      configurable: true,
      writable: false,
      value: ps
    });
  });
})();

/***/ }),

/***/ "./node_modules/ui-huma/js/preloader.js":
/*!**********************************************!*\
  !*** ./node_modules/ui-huma/js/preloader.js ***!
  \**********************************************/
/*! no static exports found */
/***/ (function(module, exports) {

(function () {
  'use strict'; // PRELOADER

  window.addEventListener('load', function () {
    $('.preloader').fadeOut();
    domFactory.handler.upgradeAll();
  });
})();

/***/ }),

/***/ "./node_modules/ui-huma/js/sidebar.js":
/*!********************************************!*\
  !*** ./node_modules/ui-huma/js/sidebar.js ***!
  \********************************************/
/*! no static exports found */
/***/ (function(module, exports) {

(function () {
  'use strict'; // Connect button(s) to drawer(s)

  var sidebarToggle = document.querySelectorAll('[data-toggle="sidebar"]');
  sidebarToggle = Array.prototype.slice.call(sidebarToggle);
  sidebarToggle.forEach(function (toggle) {
    toggle.addEventListener('click', function (e) {
      var selector = e.currentTarget.getAttribute('data-target') || '#default-drawer';
      var drawer = document.querySelector(selector);

      if (drawer) {
        drawer.mdkDrawer.toggle();
      }
    });
  });
  var drawers = document.querySelectorAll('.mdk-drawer');
  drawers = Array.prototype.slice.call(drawers);
  drawers.forEach(function (drawer) {
    drawer.addEventListener('mdk-drawer-change', function (e) {
      if (!e.target.mdkDrawer) {
        return;
      }

      document.querySelector('body').classList[e.target.mdkDrawer.opened ? 'add' : 'remove']('has-drawer-opened');
      var button = document.querySelector('[data-target="#' + e.target.id + '"]');

      if (button) {
        button.classList[e.target.mdkDrawer.opened ? 'add' : 'remove']('active');
      }
    });
  }); // SIDEBAR COLLAPSE MENUS

  $('.sidebar .collapse').on('show.bs.collapse', function (e) {
    e.stopPropagation();
    var parent = $(this).parent().closest('ul');
    parent.find('.open').find('.collapse').not(this).collapse('hide');
    $(this).closest('li').addClass('open');
  });
  $('.sidebar .collapse').on('hidden.bs.collapse', function (e) {
    e.stopPropagation();
    $(this).closest('li').removeClass('open');
  });
})();

/***/ }),

/***/ "./src/js/app.js":
/*!***********************!*\
  !*** ./src/js/app.js ***!
  \***********************/
/*! no exports provided */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var ui_huma_js_app__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ui-huma/js/app */ "./node_modules/ui-huma/js/app.js");


/***/ }),

/***/ 1:
/*!*****************************!*\
  !*** multi ./src/js/app.js ***!
  \*****************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

module.exports = __webpack_require__(/*! C:\Users\Marco Antonio\Downloads\themeforest-dCQaTvCY-huma-bootstrap-business-admin-template\huma-v1.3.1\huma-v1.3.1\src\js\app.js */"./src/js/app.js");


/***/ })

/******/ });
//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbIndlYnBhY2s6Ly8vd2VicGFjay9ib290c3RyYXAiLCJ3ZWJwYWNrOi8vLy4vbm9kZV9tb2R1bGVzL3VpLWh1bWEvanMvYXBwLmpzIiwid2VicGFjazovLy8uL25vZGVfbW9kdWxlcy91aS1odW1hL2pzL21haW4uanMiLCJ3ZWJwYWNrOi8vLy4vbm9kZV9tb2R1bGVzL3VpLWh1bWEvanMvcGVyZmVjdC1zY3JvbGxiYXIuanMiLCJ3ZWJwYWNrOi8vLy4vbm9kZV9tb2R1bGVzL3VpLWh1bWEvanMvcHJlbG9hZGVyLmpzIiwid2VicGFjazovLy8uL25vZGVfbW9kdWxlcy91aS1odW1hL2pzL3NpZGViYXIuanMiLCJ3ZWJwYWNrOi8vLy4vc3JjL2pzL2FwcC5qcyJdLCJuYW1lcyI6WyIkIiwib24iLCJlIiwidGFyZ2V0IiwicmVtb3ZlQ2xhc3MiLCJkb21GYWN0b3J5IiwiaGFuZGxlciIsImF1dG9Jbml0IiwidG9vbHRpcCIsImVhY2giLCIkZWxlbWVudCIsImVsZW1lbnQiLCJwcyIsIlBlcmZlY3RTY3JvbGxiYXIiLCJ3aGVlbFByb3BhZ2F0aW9uIiwiZGF0YSIsInN1cHByZXNzU2Nyb2xsWSIsInN3aXBlRWFzaW5nIiwiT2JqZWN0IiwiZGVmaW5lUHJvcGVydHkiLCJjb25maWd1cmFibGUiLCJ3cml0YWJsZSIsInZhbHVlIiwid2luZG93IiwiYWRkRXZlbnRMaXN0ZW5lciIsImZhZGVPdXQiLCJ1cGdyYWRlQWxsIiwic2lkZWJhclRvZ2dsZSIsImRvY3VtZW50IiwicXVlcnlTZWxlY3RvckFsbCIsIkFycmF5IiwicHJvdG90eXBlIiwic2xpY2UiLCJjYWxsIiwiZm9yRWFjaCIsInRvZ2dsZSIsInNlbGVjdG9yIiwiY3VycmVudFRhcmdldCIsImdldEF0dHJpYnV0ZSIsImRyYXdlciIsInF1ZXJ5U2VsZWN0b3IiLCJtZGtEcmF3ZXIiLCJkcmF3ZXJzIiwiY2xhc3NMaXN0Iiwib3BlbmVkIiwiYnV0dG9uIiwiaWQiLCJzdG9wUHJvcGFnYXRpb24iLCJwYXJlbnQiLCJjbG9zZXN0IiwiZmluZCIsIm5vdCIsImNvbGxhcHNlIiwiYWRkQ2xhc3MiXSwibWFwcGluZ3MiOiI7UUFBQTtRQUNBOztRQUVBO1FBQ0E7O1FBRUE7UUFDQTtRQUNBO1FBQ0E7UUFDQTtRQUNBO1FBQ0E7UUFDQTtRQUNBO1FBQ0E7O1FBRUE7UUFDQTs7UUFFQTtRQUNBOztRQUVBO1FBQ0E7UUFDQTs7O1FBR0E7UUFDQTs7UUFFQTtRQUNBOztRQUVBO1FBQ0E7UUFDQTtRQUNBLDBDQUEwQyxnQ0FBZ0M7UUFDMUU7UUFDQTs7UUFFQTtRQUNBO1FBQ0E7UUFDQSx3REFBd0Qsa0JBQWtCO1FBQzFFO1FBQ0EsaURBQWlELGNBQWM7UUFDL0Q7O1FBRUE7UUFDQTtRQUNBO1FBQ0E7UUFDQTtRQUNBO1FBQ0E7UUFDQTtRQUNBO1FBQ0E7UUFDQTtRQUNBLHlDQUF5QyxpQ0FBaUM7UUFDMUUsZ0hBQWdILG1CQUFtQixFQUFFO1FBQ3JJO1FBQ0E7O1FBRUE7UUFDQTtRQUNBO1FBQ0EsMkJBQTJCLDBCQUEwQixFQUFFO1FBQ3ZELGlDQUFpQyxlQUFlO1FBQ2hEO1FBQ0E7UUFDQTs7UUFFQTtRQUNBLHNEQUFzRCwrREFBK0Q7O1FBRXJIO1FBQ0E7OztRQUdBO1FBQ0E7Ozs7Ozs7Ozs7Ozs7QUNsRkE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFDQTtBQUNBO0NBRUE7QUFDQTtBQUNBOztBQUVBLENBQUMsWUFBVztBQUNWOztBQUVBQSxHQUFDLENBQUMscUJBQUQsQ0FBRCxDQUF5QkMsRUFBekIsQ0FBNEIsYUFBNUIsRUFBMkMsVUFBVUMsQ0FBVixFQUFhO0FBQ3RERixLQUFDLENBQUNFLENBQUMsQ0FBQ0MsTUFBSCxDQUFELENBQVlDLFdBQVosQ0FBd0IsUUFBeEI7QUFDRCxHQUZELEVBSFUsQ0FPVjtBQUNBO0FBQ0E7QUFFRCxDQVhELEk7Ozs7Ozs7Ozs7O0FDUkEsQ0FBQyxZQUFXO0FBQ1YsZUFEVSxDQUdWOztBQUNBQyxZQUFVLENBQUNDLE9BQVgsQ0FBbUJDLFFBQW5CLEdBSlUsQ0FNVjs7QUFDQVAsR0FBQyxDQUFDLHlCQUFELENBQUQsQ0FBNkJRLE9BQTdCO0FBRUQsQ0FURCxJOzs7Ozs7Ozs7OztBQ0FBLENBQUMsWUFBVztBQUNWOztBQUVBUixHQUFDLENBQUMsMEJBQUQsQ0FBRCxDQUE4QlMsSUFBOUIsQ0FBbUMsWUFBVztBQUM1QyxRQUFNQyxRQUFRLEdBQUdWLENBQUMsQ0FBQyxJQUFELENBQWxCO0FBQ0EsUUFBTVcsT0FBTyxHQUFHLElBQWhCO0FBQ0EsUUFBTUMsRUFBRSxHQUFHLElBQUlDLGdCQUFKLENBQXFCRixPQUFyQixFQUE4QjtBQUN2Q0csc0JBQWdCLEVBQUUsS0FBSyxDQUFMLEtBQVdKLFFBQVEsQ0FBQ0ssSUFBVCxDQUFjLHFDQUFkLENBQVgsR0FDZEwsUUFBUSxDQUFDSyxJQUFULENBQWMscUNBQWQsQ0FEYyxHQUVkLEtBSG1DO0FBSXZDQyxxQkFBZSxFQUFFLEtBQUssQ0FBTCxLQUFXTixRQUFRLENBQUNLLElBQVQsQ0FBYyxxQ0FBZCxDQUFYLEdBQ2JMLFFBQVEsQ0FBQ0ssSUFBVCxDQUFjLHFDQUFkLENBRGEsR0FFYixLQU5tQztBQU92Q0UsaUJBQVcsRUFBRTtBQVAwQixLQUE5QixDQUFYO0FBU0FDLFVBQU0sQ0FBQ0MsY0FBUCxDQUFzQlIsT0FBdEIsRUFBK0Isa0JBQS9CLEVBQW1EO0FBQ2pEUyxrQkFBWSxFQUFFLElBRG1DO0FBRWpEQyxjQUFRLEVBQUUsS0FGdUM7QUFHakRDLFdBQUssRUFBRVY7QUFIMEMsS0FBbkQ7QUFLRCxHQWpCRDtBQW1CRCxDQXRCRCxJOzs7Ozs7Ozs7OztBQ0FBLENBQUMsWUFBVztBQUNWLGVBRFUsQ0FHVjs7QUFDQVcsUUFBTSxDQUFDQyxnQkFBUCxDQUF3QixNQUF4QixFQUFnQyxZQUFXO0FBQ3pDeEIsS0FBQyxDQUFDLFlBQUQsQ0FBRCxDQUFnQnlCLE9BQWhCO0FBQ0FwQixjQUFVLENBQUNDLE9BQVgsQ0FBbUJvQixVQUFuQjtBQUNELEdBSEQ7QUFLRCxDQVRELEk7Ozs7Ozs7Ozs7O0FDQUEsQ0FBQyxZQUFXO0FBQ1YsZUFEVSxDQUdWOztBQUNBLE1BQUlDLGFBQWEsR0FBR0MsUUFBUSxDQUFDQyxnQkFBVCxDQUEwQix5QkFBMUIsQ0FBcEI7QUFDQUYsZUFBYSxHQUFHRyxLQUFLLENBQUNDLFNBQU4sQ0FBZ0JDLEtBQWhCLENBQXNCQyxJQUF0QixDQUEyQk4sYUFBM0IsQ0FBaEI7QUFFQUEsZUFBYSxDQUFDTyxPQUFkLENBQXNCLFVBQVVDLE1BQVYsRUFBa0I7QUFDdENBLFVBQU0sQ0FBQ1gsZ0JBQVAsQ0FBd0IsT0FBeEIsRUFBaUMsVUFBVXRCLENBQVYsRUFBYTtBQUM1QyxVQUFJa0MsUUFBUSxHQUFHbEMsQ0FBQyxDQUFDbUMsYUFBRixDQUFnQkMsWUFBaEIsQ0FBNkIsYUFBN0IsS0FBK0MsaUJBQTlEO0FBQ0EsVUFBSUMsTUFBTSxHQUFHWCxRQUFRLENBQUNZLGFBQVQsQ0FBdUJKLFFBQXZCLENBQWI7O0FBQ0EsVUFBSUcsTUFBSixFQUFZO0FBQ1ZBLGNBQU0sQ0FBQ0UsU0FBUCxDQUFpQk4sTUFBakI7QUFDRDtBQUNGLEtBTkQ7QUFPRCxHQVJEO0FBVUEsTUFBSU8sT0FBTyxHQUFHZCxRQUFRLENBQUNDLGdCQUFULENBQTBCLGFBQTFCLENBQWQ7QUFDQWEsU0FBTyxHQUFHWixLQUFLLENBQUNDLFNBQU4sQ0FBZ0JDLEtBQWhCLENBQXNCQyxJQUF0QixDQUEyQlMsT0FBM0IsQ0FBVjtBQUNBQSxTQUFPLENBQUNSLE9BQVIsQ0FBZ0IsVUFBQ0ssTUFBRCxFQUFZO0FBQzFCQSxVQUFNLENBQUNmLGdCQUFQLENBQXdCLG1CQUF4QixFQUE2QyxVQUFDdEIsQ0FBRCxFQUFPO0FBQ2xELFVBQUksQ0FBQ0EsQ0FBQyxDQUFDQyxNQUFGLENBQVNzQyxTQUFkLEVBQXlCO0FBQ3ZCO0FBQ0Q7O0FBQ0RiLGNBQVEsQ0FBQ1ksYUFBVCxDQUF1QixNQUF2QixFQUErQkcsU0FBL0IsQ0FBeUN6QyxDQUFDLENBQUNDLE1BQUYsQ0FBU3NDLFNBQVQsQ0FBbUJHLE1BQW5CLEdBQTRCLEtBQTVCLEdBQW9DLFFBQTdFLEVBQXVGLG1CQUF2RjtBQUNBLFVBQUlDLE1BQU0sR0FBR2pCLFFBQVEsQ0FBQ1ksYUFBVCxDQUF1QixvQkFBb0J0QyxDQUFDLENBQUNDLE1BQUYsQ0FBUzJDLEVBQTdCLEdBQWtDLElBQXpELENBQWI7O0FBQ0EsVUFBSUQsTUFBSixFQUFZO0FBQ1ZBLGNBQU0sQ0FBQ0YsU0FBUCxDQUFpQnpDLENBQUMsQ0FBQ0MsTUFBRixDQUFTc0MsU0FBVCxDQUFtQkcsTUFBbkIsR0FBNEIsS0FBNUIsR0FBb0MsUUFBckQsRUFBK0QsUUFBL0Q7QUFDRDtBQUNGLEtBVEQ7QUFVRCxHQVhELEVBbkJVLENBZ0NWOztBQUVBNUMsR0FBQyxDQUFDLG9CQUFELENBQUQsQ0FBd0JDLEVBQXhCLENBQTJCLGtCQUEzQixFQUErQyxVQUFVQyxDQUFWLEVBQWE7QUFDMURBLEtBQUMsQ0FBQzZDLGVBQUY7QUFDQSxRQUFJQyxNQUFNLEdBQUdoRCxDQUFDLENBQUMsSUFBRCxDQUFELENBQVFnRCxNQUFSLEdBQWlCQyxPQUFqQixDQUF5QixJQUF6QixDQUFiO0FBQ0FELFVBQU0sQ0FBQ0UsSUFBUCxDQUFZLE9BQVosRUFBcUJBLElBQXJCLENBQTBCLFdBQTFCLEVBQXVDQyxHQUF2QyxDQUEyQyxJQUEzQyxFQUFpREMsUUFBakQsQ0FBMEQsTUFBMUQ7QUFDQXBELEtBQUMsQ0FBQyxJQUFELENBQUQsQ0FBUWlELE9BQVIsQ0FBZ0IsSUFBaEIsRUFBc0JJLFFBQXRCLENBQStCLE1BQS9CO0FBQ0QsR0FMRDtBQU9BckQsR0FBQyxDQUFDLG9CQUFELENBQUQsQ0FBd0JDLEVBQXhCLENBQTJCLG9CQUEzQixFQUFpRCxVQUFVQyxDQUFWLEVBQWE7QUFDNURBLEtBQUMsQ0FBQzZDLGVBQUY7QUFDQS9DLEtBQUMsQ0FBQyxJQUFELENBQUQsQ0FBUWlELE9BQVIsQ0FBZ0IsSUFBaEIsRUFBc0I3QyxXQUF0QixDQUFrQyxNQUFsQztBQUNELEdBSEQ7QUFLRCxDQTlDRCxJOzs7Ozs7Ozs7Ozs7QUNBQTtBQUFBIiwiZmlsZSI6Ii9kaXN0L2Fzc2V0cy9qcy9hcHAuanMiLCJzb3VyY2VzQ29udGVudCI6WyIgXHQvLyBUaGUgbW9kdWxlIGNhY2hlXG4gXHR2YXIgaW5zdGFsbGVkTW9kdWxlcyA9IHt9O1xuXG4gXHQvLyBUaGUgcmVxdWlyZSBmdW5jdGlvblxuIFx0ZnVuY3Rpb24gX193ZWJwYWNrX3JlcXVpcmVfXyhtb2R1bGVJZCkge1xuXG4gXHRcdC8vIENoZWNrIGlmIG1vZHVsZSBpcyBpbiBjYWNoZVxuIFx0XHRpZihpbnN0YWxsZWRNb2R1bGVzW21vZHVsZUlkXSkge1xuIFx0XHRcdHJldHVybiBpbnN0YWxsZWRNb2R1bGVzW21vZHVsZUlkXS5leHBvcnRzO1xuIFx0XHR9XG4gXHRcdC8vIENyZWF0ZSBhIG5ldyBtb2R1bGUgKGFuZCBwdXQgaXQgaW50byB0aGUgY2FjaGUpXG4gXHRcdHZhciBtb2R1bGUgPSBpbnN0YWxsZWRNb2R1bGVzW21vZHVsZUlkXSA9IHtcbiBcdFx0XHRpOiBtb2R1bGVJZCxcbiBcdFx0XHRsOiBmYWxzZSxcbiBcdFx0XHRleHBvcnRzOiB7fVxuIFx0XHR9O1xuXG4gXHRcdC8vIEV4ZWN1dGUgdGhlIG1vZHVsZSBmdW5jdGlvblxuIFx0XHRtb2R1bGVzW21vZHVsZUlkXS5jYWxsKG1vZHVsZS5leHBvcnRzLCBtb2R1bGUsIG1vZHVsZS5leHBvcnRzLCBfX3dlYnBhY2tfcmVxdWlyZV9fKTtcblxuIFx0XHQvLyBGbGFnIHRoZSBtb2R1bGUgYXMgbG9hZGVkXG4gXHRcdG1vZHVsZS5sID0gdHJ1ZTtcblxuIFx0XHQvLyBSZXR1cm4gdGhlIGV4cG9ydHMgb2YgdGhlIG1vZHVsZVxuIFx0XHRyZXR1cm4gbW9kdWxlLmV4cG9ydHM7XG4gXHR9XG5cblxuIFx0Ly8gZXhwb3NlIHRoZSBtb2R1bGVzIG9iamVjdCAoX193ZWJwYWNrX21vZHVsZXNfXylcbiBcdF9fd2VicGFja19yZXF1aXJlX18ubSA9IG1vZHVsZXM7XG5cbiBcdC8vIGV4cG9zZSB0aGUgbW9kdWxlIGNhY2hlXG4gXHRfX3dlYnBhY2tfcmVxdWlyZV9fLmMgPSBpbnN0YWxsZWRNb2R1bGVzO1xuXG4gXHQvLyBkZWZpbmUgZ2V0dGVyIGZ1bmN0aW9uIGZvciBoYXJtb255IGV4cG9ydHNcbiBcdF9fd2VicGFja19yZXF1aXJlX18uZCA9IGZ1bmN0aW9uKGV4cG9ydHMsIG5hbWUsIGdldHRlcikge1xuIFx0XHRpZighX193ZWJwYWNrX3JlcXVpcmVfXy5vKGV4cG9ydHMsIG5hbWUpKSB7XG4gXHRcdFx0T2JqZWN0LmRlZmluZVByb3BlcnR5KGV4cG9ydHMsIG5hbWUsIHsgZW51bWVyYWJsZTogdHJ1ZSwgZ2V0OiBnZXR0ZXIgfSk7XG4gXHRcdH1cbiBcdH07XG5cbiBcdC8vIGRlZmluZSBfX2VzTW9kdWxlIG9uIGV4cG9ydHNcbiBcdF9fd2VicGFja19yZXF1aXJlX18uciA9IGZ1bmN0aW9uKGV4cG9ydHMpIHtcbiBcdFx0aWYodHlwZW9mIFN5bWJvbCAhPT0gJ3VuZGVmaW5lZCcgJiYgU3ltYm9sLnRvU3RyaW5nVGFnKSB7XG4gXHRcdFx0T2JqZWN0LmRlZmluZVByb3BlcnR5KGV4cG9ydHMsIFN5bWJvbC50b1N0cmluZ1RhZywgeyB2YWx1ZTogJ01vZHVsZScgfSk7XG4gXHRcdH1cbiBcdFx0T2JqZWN0LmRlZmluZVByb3BlcnR5KGV4cG9ydHMsICdfX2VzTW9kdWxlJywgeyB2YWx1ZTogdHJ1ZSB9KTtcbiBcdH07XG5cbiBcdC8vIGNyZWF0ZSBhIGZha2UgbmFtZXNwYWNlIG9iamVjdFxuIFx0Ly8gbW9kZSAmIDE6IHZhbHVlIGlzIGEgbW9kdWxlIGlkLCByZXF1aXJlIGl0XG4gXHQvLyBtb2RlICYgMjogbWVyZ2UgYWxsIHByb3BlcnRpZXMgb2YgdmFsdWUgaW50byB0aGUgbnNcbiBcdC8vIG1vZGUgJiA0OiByZXR1cm4gdmFsdWUgd2hlbiBhbHJlYWR5IG5zIG9iamVjdFxuIFx0Ly8gbW9kZSAmIDh8MTogYmVoYXZlIGxpa2UgcmVxdWlyZVxuIFx0X193ZWJwYWNrX3JlcXVpcmVfXy50ID0gZnVuY3Rpb24odmFsdWUsIG1vZGUpIHtcbiBcdFx0aWYobW9kZSAmIDEpIHZhbHVlID0gX193ZWJwYWNrX3JlcXVpcmVfXyh2YWx1ZSk7XG4gXHRcdGlmKG1vZGUgJiA4KSByZXR1cm4gdmFsdWU7XG4gXHRcdGlmKChtb2RlICYgNCkgJiYgdHlwZW9mIHZhbHVlID09PSAnb2JqZWN0JyAmJiB2YWx1ZSAmJiB2YWx1ZS5fX2VzTW9kdWxlKSByZXR1cm4gdmFsdWU7XG4gXHRcdHZhciBucyA9IE9iamVjdC5jcmVhdGUobnVsbCk7XG4gXHRcdF9fd2VicGFja19yZXF1aXJlX18ucihucyk7XG4gXHRcdE9iamVjdC5kZWZpbmVQcm9wZXJ0eShucywgJ2RlZmF1bHQnLCB7IGVudW1lcmFibGU6IHRydWUsIHZhbHVlOiB2YWx1ZSB9KTtcbiBcdFx0aWYobW9kZSAmIDIgJiYgdHlwZW9mIHZhbHVlICE9ICdzdHJpbmcnKSBmb3IodmFyIGtleSBpbiB2YWx1ZSkgX193ZWJwYWNrX3JlcXVpcmVfXy5kKG5zLCBrZXksIGZ1bmN0aW9uKGtleSkgeyByZXR1cm4gdmFsdWVba2V5XTsgfS5iaW5kKG51bGwsIGtleSkpO1xuIFx0XHRyZXR1cm4gbnM7XG4gXHR9O1xuXG4gXHQvLyBnZXREZWZhdWx0RXhwb3J0IGZ1bmN0aW9uIGZvciBjb21wYXRpYmlsaXR5IHdpdGggbm9uLWhhcm1vbnkgbW9kdWxlc1xuIFx0X193ZWJwYWNrX3JlcXVpcmVfXy5uID0gZnVuY3Rpb24obW9kdWxlKSB7XG4gXHRcdHZhciBnZXR0ZXIgPSBtb2R1bGUgJiYgbW9kdWxlLl9fZXNNb2R1bGUgP1xuIFx0XHRcdGZ1bmN0aW9uIGdldERlZmF1bHQoKSB7IHJldHVybiBtb2R1bGVbJ2RlZmF1bHQnXTsgfSA6XG4gXHRcdFx0ZnVuY3Rpb24gZ2V0TW9kdWxlRXhwb3J0cygpIHsgcmV0dXJuIG1vZHVsZTsgfTtcbiBcdFx0X193ZWJwYWNrX3JlcXVpcmVfXy5kKGdldHRlciwgJ2EnLCBnZXR0ZXIpO1xuIFx0XHRyZXR1cm4gZ2V0dGVyO1xuIFx0fTtcblxuIFx0Ly8gT2JqZWN0LnByb3RvdHlwZS5oYXNPd25Qcm9wZXJ0eS5jYWxsXG4gXHRfX3dlYnBhY2tfcmVxdWlyZV9fLm8gPSBmdW5jdGlvbihvYmplY3QsIHByb3BlcnR5KSB7IHJldHVybiBPYmplY3QucHJvdG90eXBlLmhhc093blByb3BlcnR5LmNhbGwob2JqZWN0LCBwcm9wZXJ0eSk7IH07XG5cbiBcdC8vIF9fd2VicGFja19wdWJsaWNfcGF0aF9fXG4gXHRfX3dlYnBhY2tfcmVxdWlyZV9fLnAgPSBcIi9cIjtcblxuXG4gXHQvLyBMb2FkIGVudHJ5IG1vZHVsZSBhbmQgcmV0dXJuIGV4cG9ydHNcbiBcdHJldHVybiBfX3dlYnBhY2tfcmVxdWlyZV9fKF9fd2VicGFja19yZXF1aXJlX18ucyA9IDEpO1xuIiwiaW1wb3J0ICcuL21haW4nXHJcbmltcG9ydCAnLi9wZXJmZWN0LXNjcm9sbGJhcidcclxuaW1wb3J0ICcuL3ByZWxvYWRlcidcclxuaW1wb3J0ICcuL3NpZGViYXInXHJcbi8vIGltcG9ydCAnLi9wb3BvdmVyJ1xyXG4vLyBpbXBvcnQgJy4vbWRrLWNhcm91c2VsLWNvbnRyb2wnXHJcbi8vIGltcG9ydCAnLi9yZWFkLW1vcmUnXHJcblxyXG4oZnVuY3Rpb24oKSB7XHJcbiAgJ3VzZSBzdHJpY3QnO1xyXG5cclxuICAkKCdbZGF0YS10b2dnbGU9XCJ0YWJcIl0nKS5vbignaGlkZS5icy50YWInLCBmdW5jdGlvbiAoZSkge1xyXG4gICAgJChlLnRhcmdldCkucmVtb3ZlQ2xhc3MoJ2FjdGl2ZScpXHJcbiAgfSlcclxuXHJcbiAgLy8vLy8vLy8vLy8vLy8vLy8vLy8vLy8vLy8vLy8vLy8vLy9cclxuICAvLyBDdXN0b20gSmF2YVNjcmlwdCBjYW4gZ28gaGVyZSAvL1xyXG4gIC8vLy8vLy8vLy8vLy8vLy8vLy8vLy8vLy8vLy8vLy8vLy8vXHJcblxyXG59KSgpXHJcbiIsIihmdW5jdGlvbigpIHtcclxuICAndXNlIHN0cmljdCc7XHJcbiAgXHJcbiAgLy8gU2VsZiBJbml0aWFsaXplIERPTSBGYWN0b3J5IENvbXBvbmVudHNcclxuICBkb21GYWN0b3J5LmhhbmRsZXIuYXV0b0luaXQoKVxyXG5cclxuICAvLyBFTkFCTEUgVE9PTFRJUFNcclxuICAkKCdbZGF0YS10b2dnbGU9XCJ0b29sdGlwXCJdJykudG9vbHRpcCgpXHJcblxyXG59KSgpIiwiKGZ1bmN0aW9uKCkge1xyXG4gICd1c2Ugc3RyaWN0JztcclxuICBcclxuICAkKCdbZGF0YS1wZXJmZWN0LXNjcm9sbGJhcl0nKS5lYWNoKGZ1bmN0aW9uKCkge1xyXG4gICAgY29uc3QgJGVsZW1lbnQgPSAkKHRoaXMpXHJcbiAgICBjb25zdCBlbGVtZW50ID0gdGhpc1xyXG4gICAgY29uc3QgcHMgPSBuZXcgUGVyZmVjdFNjcm9sbGJhcihlbGVtZW50LCB7XHJcbiAgICAgIHdoZWVsUHJvcGFnYXRpb246IHZvaWQgMCAhPT0gJGVsZW1lbnQuZGF0YSgncGVyZmVjdC1zY3JvbGxiYXItd2hlZWwtcHJvcGFnYXRpb24nKVxyXG4gICAgICAgID8gJGVsZW1lbnQuZGF0YSgncGVyZmVjdC1zY3JvbGxiYXItd2hlZWwtcHJvcGFnYXRpb24nKVxyXG4gICAgICAgIDogZmFsc2UsXHJcbiAgICAgIHN1cHByZXNzU2Nyb2xsWTogdm9pZCAwICE9PSAkZWxlbWVudC5kYXRhKCdwZXJmZWN0LXNjcm9sbGJhci1zdXBwcmVzcy1zY3JvbGwteScpXHJcbiAgICAgICAgPyAkZWxlbWVudC5kYXRhKCdwZXJmZWN0LXNjcm9sbGJhci1zdXBwcmVzcy1zY3JvbGwteScpXHJcbiAgICAgICAgOiBmYWxzZSxcclxuICAgICAgc3dpcGVFYXNpbmc6IGZhbHNlXHJcbiAgICB9KVxyXG4gICAgT2JqZWN0LmRlZmluZVByb3BlcnR5KGVsZW1lbnQsICdQZXJmZWN0U2Nyb2xsYmFyJywge1xyXG4gICAgICBjb25maWd1cmFibGU6IHRydWUsXHJcbiAgICAgIHdyaXRhYmxlOiBmYWxzZSxcclxuICAgICAgdmFsdWU6IHBzXHJcbiAgICB9KVxyXG4gIH0pXHJcblxyXG59KSgpXHJcbiIsIihmdW5jdGlvbigpIHtcclxuICAndXNlIHN0cmljdCc7XHJcblxyXG4gIC8vIFBSRUxPQURFUlxyXG4gIHdpbmRvdy5hZGRFdmVudExpc3RlbmVyKCdsb2FkJywgZnVuY3Rpb24oKSB7XHJcbiAgICAkKCcucHJlbG9hZGVyJykuZmFkZU91dCgpXHJcbiAgICBkb21GYWN0b3J5LmhhbmRsZXIudXBncmFkZUFsbCgpXHJcbiAgfSlcclxuXHJcbn0pKCkiLCIoZnVuY3Rpb24oKSB7XHJcbiAgJ3VzZSBzdHJpY3QnO1xyXG4gIFxyXG4gIC8vIENvbm5lY3QgYnV0dG9uKHMpIHRvIGRyYXdlcihzKVxyXG4gIHZhciBzaWRlYmFyVG9nZ2xlID0gZG9jdW1lbnQucXVlcnlTZWxlY3RvckFsbCgnW2RhdGEtdG9nZ2xlPVwic2lkZWJhclwiXScpXHJcbiAgc2lkZWJhclRvZ2dsZSA9IEFycmF5LnByb3RvdHlwZS5zbGljZS5jYWxsKHNpZGViYXJUb2dnbGUpXHJcblxyXG4gIHNpZGViYXJUb2dnbGUuZm9yRWFjaChmdW5jdGlvbiAodG9nZ2xlKSB7XHJcbiAgICB0b2dnbGUuYWRkRXZlbnRMaXN0ZW5lcignY2xpY2snLCBmdW5jdGlvbiAoZSkge1xyXG4gICAgICB2YXIgc2VsZWN0b3IgPSBlLmN1cnJlbnRUYXJnZXQuZ2V0QXR0cmlidXRlKCdkYXRhLXRhcmdldCcpIHx8ICcjZGVmYXVsdC1kcmF3ZXInXHJcbiAgICAgIHZhciBkcmF3ZXIgPSBkb2N1bWVudC5xdWVyeVNlbGVjdG9yKHNlbGVjdG9yKVxyXG4gICAgICBpZiAoZHJhd2VyKSB7XHJcbiAgICAgICAgZHJhd2VyLm1ka0RyYXdlci50b2dnbGUoKVxyXG4gICAgICB9XHJcbiAgICB9KVxyXG4gIH0pXHJcblxyXG4gIGxldCBkcmF3ZXJzID0gZG9jdW1lbnQucXVlcnlTZWxlY3RvckFsbCgnLm1kay1kcmF3ZXInKVxyXG4gIGRyYXdlcnMgPSBBcnJheS5wcm90b3R5cGUuc2xpY2UuY2FsbChkcmF3ZXJzKVxyXG4gIGRyYXdlcnMuZm9yRWFjaCgoZHJhd2VyKSA9PiB7XHJcbiAgICBkcmF3ZXIuYWRkRXZlbnRMaXN0ZW5lcignbWRrLWRyYXdlci1jaGFuZ2UnLCAoZSkgPT4ge1xyXG4gICAgICBpZiAoIWUudGFyZ2V0Lm1ka0RyYXdlcikge1xyXG4gICAgICAgIHJldHVyblxyXG4gICAgICB9XHJcbiAgICAgIGRvY3VtZW50LnF1ZXJ5U2VsZWN0b3IoJ2JvZHknKS5jbGFzc0xpc3RbZS50YXJnZXQubWRrRHJhd2VyLm9wZW5lZCA/ICdhZGQnIDogJ3JlbW92ZSddKCdoYXMtZHJhd2VyLW9wZW5lZCcpXHJcbiAgICAgIGxldCBidXR0b24gPSBkb2N1bWVudC5xdWVyeVNlbGVjdG9yKCdbZGF0YS10YXJnZXQ9XCIjJyArIGUudGFyZ2V0LmlkICsgJ1wiXScpXHJcbiAgICAgIGlmIChidXR0b24pIHtcclxuICAgICAgICBidXR0b24uY2xhc3NMaXN0W2UudGFyZ2V0Lm1ka0RyYXdlci5vcGVuZWQgPyAnYWRkJyA6ICdyZW1vdmUnXSgnYWN0aXZlJylcclxuICAgICAgfVxyXG4gICAgfSlcclxuICB9KVxyXG5cclxuICAvLyBTSURFQkFSIENPTExBUFNFIE1FTlVTXHJcbiAgXHJcbiAgJCgnLnNpZGViYXIgLmNvbGxhcHNlJykub24oJ3Nob3cuYnMuY29sbGFwc2UnLCBmdW5jdGlvbiAoZSkge1xyXG4gICAgZS5zdG9wUHJvcGFnYXRpb24oKVxyXG4gICAgbGV0IHBhcmVudCA9ICQodGhpcykucGFyZW50KCkuY2xvc2VzdCgndWwnKVxyXG4gICAgcGFyZW50LmZpbmQoJy5vcGVuJykuZmluZCgnLmNvbGxhcHNlJykubm90KHRoaXMpLmNvbGxhcHNlKCdoaWRlJylcclxuICAgICQodGhpcykuY2xvc2VzdCgnbGknKS5hZGRDbGFzcygnb3BlbicpXHJcbiAgfSk7XHJcblxyXG4gICQoJy5zaWRlYmFyIC5jb2xsYXBzZScpLm9uKCdoaWRkZW4uYnMuY29sbGFwc2UnLCBmdW5jdGlvbiAoZSkge1xyXG4gICAgZS5zdG9wUHJvcGFnYXRpb24oKVxyXG4gICAgJCh0aGlzKS5jbG9zZXN0KCdsaScpLnJlbW92ZUNsYXNzKCdvcGVuJyk7XHJcbiAgfSk7XHJcblxyXG59KSgpIiwiaW1wb3J0ICd1aS1odW1hL2pzL2FwcCciXSwic291cmNlUm9vdCI6IiJ9