var dataSeries = [[]];
var options_default = {
    series: [{
        name: "Rendimiento",
        data: [45, 52, 38, 24, 33, 26, 21, 20, 6, 8, 15, 10]
    }],
    chart: {
        type: 'line',
        height: 350,
        zoom: {
            enabled: false
        },
        toolbar: {
            show: false,
        },
    },
    dataLabels: {
        enabled: true
    },
    colors: [
        '#004fff',
    ],
    stroke: {
        curve: 'straight',
        colors: ['#004fff']
    },

    title: {
        //text: 'Fundamental Analysis of Stocks',
        //align: 'left'
    },
    subtitle: {
        //text: 'Price Movements',
        //align: 'left'
    },

    xaxis: {
        categories: ['01 Jan', '02 Jan', '03 Jan', '04 Jan', '05 Jan', '06 Jan', '07 Jan', '08 Jan', '09 Jan',
            '10 Jan', '11 Jan', '12 Jan'
        ],
        labels: {
            show: true,
        }
    },
    yaxis: {
        opposite: false,
        show: true
    },
    legend: {
        horizontalAlign: 'right',
        show: false
    }
};
function formatDate(date) {
    var d = new Date(date),
        month = '' + (d.getMonth() + 1),
        day = '' + d.getDate(),
        year = d.getFullYear();

    if (month.length < 2)
        month = '0' + month;
    if (day.length < 2)
        day = '0' + day;

    return [year, month, day].join('-');
}

function updateGraphics(data) {

    let array = [];
    let value_old = 0;
    let dataArray = [];
    var dates = [];
    var ts2 = 1484418600000;
    data.grafico.forEach(element => {
        ts2 += 86400000;
        let amount = parseInt(element.amount) + parseInt(value_old);
        let date = formatDate(element.updated_at);
        array.push(amount);
        dataArray.push({ "date": date, "value": amount });
        var innerArr = [ts2, amount];
        dates.push(innerArr);
        value_old = amount;
    });
    dataSeries = [dataArray];

    var options = {
        series: [{
            name: 'Rendimiento',
            data: dates
        }],
        chart: {
            type: 'area',
            stacked: false,
            height: 350,
            zoom: {
                type: 'x',
                enabled: true,
                autoScaleYaxis: true
            },
            toolbar: {
                autoSelected: 'zoom'
            }
        },
        dataLabels: {
            enabled: false
        },
        markers: {
            size: 0,
        },
        title: {

        },
        fill: {
            type: 'gradient',
            gradient: {
                shadeIntensity: 1,
                inverseColors: false,
                opacityFrom: 0.5,
                opacityTo: 0,
                stops: [0, 90, 100]
            },
        },
        yaxis: {
            labels: {
                formatter: function (val) {
                    return (val / 10).toFixed(0);
                },
            },
            title: {
                text: 'Rendimiento'
            },
        },
        xaxis: {
            type: 'datetime',
        },
        tooltip: {
            shared: false,
            y: {
                formatter: function (val) {
                    return (val / 1).toFixed(0)
                }
            }
        }
    };


    if (array.length == 0) {
        $('#nohaydatos').show();
        $('#Barchart').hide();
    }
    else {
        $('#Barchart').html('');
        $('#Barchart').show();
        $('#nohaydatos').hide();
        new ApexCharts(document.querySelector("#Barchart"), options).render();
    }
}
function getStadistics(idAccount, type) {
    var url = baseUrl + '/' + type + '/stadistics/' + idAccount;
    $.ajax({
        type: "GET",
        url: url,
        success: function (data) {
            console.log(data);

            // Create our number formatter.
            var formatter = new Intl.NumberFormat('en-US', {
                style: 'currency',
                currency: 'USD',
            });

            var inversionAcumulada = formatter.format(data.initial_investment);
            var gananciaAcumulada = formatter.format(data.gananciaAcumulada);
            var rendimientoAcomulado = parseFloat(data.porcentaje).toFixed(2);
            var saldoAcumulada = formatter.format(data.saldo);
            var retiros = formatter.format(data.retiros);
            var comisiones = formatter.format(data.comisiones);
            var performance_fee = parseFloat(data.performance_fee).toFixed(2);
            var management_fee = parseFloat(data.management_fee).toFixed(2);


            $('#inversionAcumulada').html(inversionAcumulada + " USD");
            $('#gananciaAcumulada').html(gananciaAcumulada + " USD");
            $('#rendimientoAcumulado').html(rendimientoAcomulado + " %");
            $('#saldoAcumulada').html(saldoAcumulada + " USD");
            $('#retiros').html(retiros + " USD");
            $('#actualizacion').html(data.actualizacion);
            $('#inicioContrato').html(data.inicioContrato);
            $('#finContrato').html(data.finContrato);
            $('#comisiones').html(comisiones + " USD");
            $('#performance_fee').html(performance_fee + " %");
            $('#management_fee').html(management_fee + " %");
            updateGraphics(data);
        },
        error: function (e) {
            console.log(e);
        }
    });
}

//new ApexCharts(document.querySelector("#Barchart"), options).render();
//Grafica de dona seccion portafolio  --}}

function Donut() {
    setTimeout(function () {

        var options = {
            series: [44, 55],
            labels: ['Indices del dolar', 'Euros'],
            chart: {
                type: 'donut',
                height: 350,
                zoom: {
                    enabled: false
                },
                toolbar: {
                    show: false,
                },
            },
            dataLabels: {
                enabled: false
            },
            colors: [
                '#004fff',
                '#73a3ea',
            ],
            stroke: {
                width: [0, 4],
                curve: 'straight',
                colors: ['#004fff', '#73a3ea']
            },
            legend: {
                position: 'bottom'
            },

        };


        //var chart = new ApexCharts(document.querySelector("#portafolioDonut"), options).render();

    }, 100);
};

// Grafica de area seccion portafolio  

function Area() {
    setTimeout(function () {
        var options = {
            series: [{
                name: "Total de beneficio",
                data: [7, 4, 6, 4, 5, 4, 7],
            }],
            chart: {
                type: 'area',
                height: 350,
                zoom: {
                    enabled: false
                },
                toolbar: {
                    show: false,
                },
            },
            dataLabels: {
                enabled: false
            },
            colors: [
                '#004fff',
            ],
            stroke: {
                curve: 'straight',
                colors: ['#004fff']
            },

            title: {
                //   text: 'Fundamental Analysis of Stocks',
                //   align: 'left'
            },
            subtitle: {
                //   text: 'Price Movements',
                //   align: 'left'
            },

            xaxis: {
                categories: [
                    'Enero',
                    'Febrero',
                    'Marzo',
                    'Abril',
                    'Mayo',
                    'Junio',
                    'Julio',
                ],
                labels: {
                    show: true,
                }
            },
            yaxis: {
                opposite: false,
                show: true
            },
            legend: {
                horizontalAlign: 'right',
                show: false
            }
        };
        //var chart = new ApexCharts(document.querySelector("#portafolioArea"), options).render();
    }, 100);
};
