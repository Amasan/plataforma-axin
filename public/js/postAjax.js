var getUrl = window.location;
var baseUrl = getUrl.protocol + "//" + getUrl.host;

/* Mandar headers para enviar petición post con Ajax */
$(document).ready(function () {
    $.ajaxSetup({
        headers: { 'X-CSRF-Token': $('input[name=_token]').attr('value') }
    });
});

/* Mandar POST con Ajax cuando el campo is_politically_exposed tiene el valor de 1 */

$(document).ready(function () {
    $('#is_politically_exposed').on('change', function () {
        if (this.value == 1) {
            var modalConfirm = function (callback) {

                $("#mi-modal").modal('show');

                $("#modal-btn-si").on("click", function () {
                    callback(true);
                    $("#mi-modal").modal('hide');

                    var _token = $('#_token').val();
                    var state_employee = $('#state_employee').val();
                    var anual_activity_income_id = $('#anual_activity_income_id').val();
                    var total_funds_invest_id = $('#total_funds_invest_id').val();
                    var family_member_holds_public_office_id = $('#family_member_holds_public_office_id').val();
                    var is_politically_exposed = $('#is_politically_exposed').val();
                    var is_declared_bankrupt = 0;
                    var data = {
                        _token: _token,
                        state_employee: state_employee,
                        anual_activity_income_id: anual_activity_income_id,
                        total_funds_invest_id: total_funds_invest_id,
                        family_member_holds_public_office_id: family_member_holds_public_office_id,
                        is_politically_exposed: is_politically_exposed,
                        is_declared_bankrupt: is_declared_bankrupt,
                    };

                    $.ajax({
                        type: "POST",
                        url: baseUrl + "/personal/update_actividade",
                        data: {
                            _token: _token,
                            state_employee: state_employee,
                            anual_activity_income_id: anual_activity_income_id,
                            total_funds_invest_id: total_funds_invest_id,
                            family_member_holds_public_office_id: family_member_holds_public_office_id,
                            is_politically_exposed: is_politically_exposed,
                            is_declared_bankrupt: is_declared_bankrupt,
                        },

                        success: function (msg) {
                            window.location.href = baseUrl + "/soporte";
                        },
                        error: function (e) {
                            console.log(e);
                        }
                    });

                });

                $("#modal-btn-no").on("click", function () {
                    callback(false);
                    $("#mi-modal").modal('hide');
                });
            };

            modalConfirm(function (confirm) {
                if (confirm) {
                    //Acciones si el usuario confirma

                } else {
                    //Acciones si el usuario no confirma

                }
            });


        } else {

        }
    });
});


/* Efecto select dropdown  */

$(".cuentaI").css("display", "none");
$(".cuentaM").css("display", "none");
$(".dropdown-content").css("display", "none");

$(function () {
    $("html").click(function () {
        $('.dropdown-content').hide();
    });
});


$(function () {

    $('.cuentaI').click(function (e) {
        e.stopPropagation();
        if ($('.dropdown-content').is(':visible')) {
            $('.dropdown-content').hide();
        } else {
            $('.dropdown-content').show();
        }
        $('.accountI').hide();
        $('.accountA').show();
        $('.accountM').show();

    });


});

$(function () {
    $('.cuentaA').click(function (e) {
        e.stopPropagation();
        if ($('.dropdown-content').is(':visible')) {
            $('.dropdown-content').hide();
        } else {
            $('.dropdown-content').show();
        }
        $('.accountA').hide();
        $('.accountI').show();
        $('.accountM').show();

    });

});
$(function () {

    $('.cuentaM').click(function (e) {
        e.stopPropagation();
        if ($('.dropdown-content').is(':visible')) {
            $('.dropdown-content').hide();
        } else {
            $('.dropdown-content').show();
        }
        $('.accountM').hide();
        $('.accountA').show();
        $('.accountI').show();

    });

});

$(function () {

    $('.accountA').click(function (e) {
        e.stopPropagation();
        $('.cuentaA').show();
        $('.cuentaI').hide();
        $('.cuentaM').hide();
        $('.dropdown-content').hide();
        getStadistics(1);
    });

});

$(function () {
    $('.accountI').click(function (e) {
        e.stopPropagation();
        $('.cuentaA').hide();
        $('.cuentaI').show();
        $('.cuentaM').hide();
        $('.dropdown-content').hide();
        getStadistics(2);
    });

});
$(function () {
    $('.accountM').click(function (e) {
        e.stopPropagation();
        $('.cuentaA').hide();
        $('.cuentaI').hide();
        $('.cuentaM').show();
        $('.dropdown-content').hide();
        getStadistics(3);
    });

});

/*Loading en botones de guardar*/
$(".spinner-grow").css("display", "none");

$(function () {
    $('.botonSiguienteP').click(function () {
        setTimeout(() => {
            $('.spinner-text').hide();
            $('.spinner-grow').show();
            $(".botonSiguienteP").attr('disabled', 'disabled');
        }, 100);
        setInterval(() => {
            $('.spinner-text').show();
            $('.spinner-grow').hide();
            $(".botonSiguienteP").removeAttr('disabled');
        }, 20000);
    });

});


/* Mandar POST con Ajax cuando el campo is_declared_bankrupt tiene el valor de 1 */

$(document).ready(function () {
    $('#is_declared_bankrupt').on('change', function () {
        if (this.value == 1) {
            var modalConfirm = function (callback) {

                $("#mi-modal").modal('show');

                $("#modal-btn-si").on("click", function () {
                    callback(true);
                    $("#mi-modal").modal('hide');
                    var _token = $('#_token').val();
                    var state_employee = $('#state_employee').val();
                    var anual_activity_income_id = $('#anual_activity_income_id').val();
                    var total_funds_invest_id = $('#total_funds_invest_id').val();
                    var family_member_holds_public_office_id = $('#family_member_holds_public_office_id').val();
                    var is_politically_exposed = 0;
                    var is_declared_bankrupt = $('#is_declared_bankrupt').val();
                    var data = {
                        _token: _token,
                        state_employee: state_employee,
                        anual_activity_income_id: anual_activity_income_id,
                        total_funds_invest_id: total_funds_invest_id,
                        family_member_holds_public_office_id: family_member_holds_public_office_id,
                        is_politically_exposed: is_politically_exposed,
                        is_declared_bankrupt: is_declared_bankrupt,
                    };

                    $.ajax({
                        type: "POST",
                        url: baseUrl + "/personal/update_actividade",
                        data: {
                            _token: _token,
                            state_employee: state_employee,
                            anual_activity_income_id: anual_activity_income_id,
                            total_funds_invest_id: total_funds_invest_id,
                            family_member_holds_public_office_id: family_member_holds_public_office_id,
                            is_politically_exposed: is_politically_exposed,
                            is_declared_bankrupt: is_declared_bankrupt,
                        },

                        success: function (msg) {

                            console.log(data);
                            window.location.href = baseUrl + "/soporte";
                        },
                        error: function (e) {
                            console.log(e);
                            console.log(data);
                        }
                    });

                    console.log('deberia guaradarse');
                });

                $("#modal-btn-no").on("click", function () {
                    callback(false);
                    $("#mi-modal").modal('hide');
                });
            };

            modalConfirm(function (confirm) {
                if (confirm) {
                    //Acciones si el usuario confirma

                } else {
                    //Acciones si el usuario no confirma

                }
            });





        } else {

        }
    });
});

/* Validacion para que los input tipo file solo acepten archivos con las extenciones especificadas y que pesen menos de 3MB */

$(document).on('change', 'input[type="file"]', function () {

    var inputs = document.querySelectorAll('.file-input')

    function customInput(el) {
        const fileInput = el.querySelector('[type="file"]')
        const label = el.querySelector('[data-js-label]')

        fileInput.onchange =
            fileInput.onmouseout = function () {
                if (!fileInput.value) return

                var value = fileInput.value.replace(/^.*[\\\/]/, '')
                el.className += ' -chosen'
                label.innerText = value
            }
    }

    // this.files[0].size recupera el tamaño del archivo
    // alert(this.files[0].size);

    var fileName = this.files[0].name;
    var fileSize = this.files[0].size;

    if (fileSize > 3000000) {
        $(".invalid").html("Elije otro archivo de menos de 3MB");
        this.value = '';
        this.files[0].name = '';
    } else {
        $(".invalid").html("");
        // recuperamos la extensión del archivo
        var ext = fileName.split('.').pop();

        // Convertimos en minúscula porque 
        // la extensión del archivo puede estar en mayúscula
        ext = ext.toLowerCase();

        // si el archivo cumple con la extencion coloca le nombre

        if (ext === 'jpg' || ext === 'jpeg' || ext === 'png' || ext === 'pdf') {
            for (var i = 0, len = inputs.length; i < len; i++) {
                customInput(inputs[i])
            }
        }

        // console.log(ext);
        switch (ext) {
            case 'jpg': $(".invalid").html(""); break;
            case 'jpeg': $(".invalid").html(""); break;
            case 'png': $(".invalid").html(""); break;
            case 'pdf': break;
            default:
                $(".invalid").html("Elije otro archivo con la extencion PNG, JPG o PDF");
                this.value = ''; // reset del valor
                this.files[0].name = '';
        }

    }
});

/* Función para mostrar popover cuando un texto es copiado */

$(document).ready(function () {
    $('[data-toggle="popover"]').popover({
        placement: 'right',
        delay: {
            "show": 500,
            "hide": 100
        }
    });

    $('[data-toggle="popover"]').click(function () {

        setTimeout(function () {
            $('.popover').fadeOut('slow');
        }, 5000);

    });

    $('[data-toggle="tooltip"]').tooltip();
});

//Peticion ajax para mostrar los archivos del cliente (Identificacion oficial, comprobante de domicilio, contrato) almacenadas en aws s3

function getfile(id, name, key, value) {

    var url = baseUrl + '/legal/detail/' + id + "/" + name + "/" + key + '/' + value;
    $.ajax({
        type: "GET",
        url: url,
        success: function (data) {
            console.log(data);
            window.open(data, '_blank');
        },
        error: function (e) {
            console.log(e);
        }
    });
}

//Peticion ajax para mostrar los archivos del socio (Identificacion oficial, comprobante de domicilio, contrato) almacenadas en aws s3

function getfileSocio(id, name, key, value) {

    var url = baseUrl + '/legal/detailSocio/' + id + "/" + name + "/" + key + '/' + value;
    $.ajax({
        type: "GET",
        url: url,
        success: function (data) {
            console.log(data);
            window.open(data, '_blank');
        },
        error: function (e) {
            console.log(e);
        }
    });
}
//Peticion ajax para mostrar los archivos de clientes (comprobantes de depositos) almacenadas en aws s3

function getcomprobante(id, name, key) {
    console.log(id, name, key);
    var url = baseUrl + '/rendimiento/clientes/comprobante_cliente/' + id + "/" + name + "/" + key;
    console.log(url);
    $.ajax({
        type: "GET",
        url: url,
        success: function (data) {
            window.open(data, '_blank');
        },
        error: function (e) {
            console.log(e);
        }
    });
}
//Peticion ajax para mostrar los archivos de socios (comprobantes de depositos) almacenadas en aws s3

function getcomprobanteSocio(id, name, key) {
    console.log(id, name, key);
    var url = baseUrl + '/rendimiento/socios/comprobante_socio/' + id + "/" + name + "/" + key;
    console.log(url);
    $.ajax({
        type: "GET",
        url: url,
        success: function (data) {
            window.open(data, '_blank');
        },
        error: function (e) {
            console.log(e);
        }
    });
}
//Peticion ajax para mostrar los archivos de clientes (comprobantes de depositos) almacenadas en aws s3

function admingetcomprobante(id, name, key) {
    console.log(id, name, key);
    var url = baseUrl + '/admin/clientes/comprobante_cliente/' + id + "/" + name + "/" + key;
    console.log(url);
    $.ajax({
        type: "GET",
        url: url,
        success: function (data) {
            window.open(data, '_blank');
        },
        error: function (e) {
            console.log(e);
        }
    });
}
//Peticion ajax para mostrar los archivos de socios (comprobantes de depositos) almacenadas en aws s3

function admingetcomprobanteSocio(id, name, key) {
    console.log(id, name, key);
    var url = baseUrl + '/admin/socios/comprobante_socio/' + id + "/" + name + "/" + key;
    console.log(url);
    $.ajax({
        type: "GET",
        url: url,
        success: function (data) {
            window.open(data, '_blank');
        },
        error: function (e) {
            console.log(e);
        }
    });
}

//transferir fondos
$(document).ready(function () {
    $("#Transacction").click(function (e) {
        $('#transfiriendo').show();
        $('#Transacction').hide();
        e.preventDefault();
        var cuenta1 = $('#type_account_id1').val();
        var cuenta2 = $('#type_account_id2').val();
        var cantidad = $('#amount_money').val();
        var _token = $('#_token').val();
        var s = {
            c: cuenta1,
            c2: cuenta2,
            ca: cantidad,
            token: _token
        }
        console.log(baseUrl + "/updateTranslate");
        $.ajax({
            type: "POST",
            url: baseUrl + "/updateTranslate",
            data: {
                _token: _token,
                type_account_id1: cuenta1,
                type_account_id2: cuenta2,
                amount_money: cantidad,
            }, success: function (msg) {
                var b = document.querySelector("#Transacction");
                b.disabled = true;
                console.log(msg);
                $("#confirmacion").modal({
                    backdrop: 'static',
                    keyboard: false
                });

                if (msg.status == 200) {
                    $(".exitoso").html("Transsacción Exitosa");
                    $(".exitoso").css("color", "blue");
                    $("#imgexitosa").css("display", "block");
                    $("#imgfallida").css("display", "none");
                }
                else {
                    $(".exitoso").html("Transsacción Erronea");
                    $(".exitoso").css("color", "red");
                    $("#imgexitosa").css("display", "none");
                    $("#imgfallida").css("display", "block");
                }
                $(".cantidad").html(msg.message);
            },
            error: function (er) {
                var b = document.querySelector("#Transacction");
                b.disabled = true;
                $("#confirmacion").modal();
                $(".exitoso").html("Transsacción Erronea");
                $(".exitoso").css("color", "red");
                $(".cantidad").html("Servidor desconectado, intente mas tarde");
                $("#imgexitosa").css("display", "none");
                $("#imgfallida").css("display", "block");
            }
        });
    });
});

function revisado(id) {
    var reviewed = $('#reviewed' + id).val();
    if (reviewed == '1') {
        $('#amount' + id).prop("required", true);
        $('#amount' + id).removeAttr('disabled');
        $('#receiptbutton' + id).removeAttr('disabled');
    } else if (reviewed == '0') {
        $('#amount' + id).removeAttr("required");
        $('#amount' + id).attr('disabled', 'disabled');
        $('#receiptbutton' + id).removeAttr('disabled');
    } else if (reviewed == 'NULL') {
        $('#amount' + id).removeAttr("required");
        $('#amount' + id).attr('disabled', 'disabled');
        $('#receiptbutton' + id).attr('disabled', 'disabled');
    }
}
function retiro(id) {
    var status = $('#status' + id).val();
    if (status == 'Realizado' || status == 'Rechazado') {
        $('#retirarfondosbutton' + id).removeAttr('disabled');
    } else if (status == 'Pendiente') {
        $('#retirarfondosbutton' + id).attr('disabled', 'disabled');
    }
}

function getComisionStatus(client_id) {
    let account = $('#type_account_id').val();
    let date = $('#year').val() + '-' + $('#month').val() + '-01';
    let comission = $('#commision').val();
    if (account == null || comission == "") {
        $('#resultadoComision').html('');
        return;
    }
    $('#resultadoComision').html('Obteniendo informacion');
    console.log(account, comission, client_id);
    var url = baseUrl + '/rendimiento/operaciones/getComisionStatus/' + account + '/' + comission + '/' + client_id + '/' + date;
    console.log(url);
    $.ajax({
        type: "GET",
        url: url,
        success: function (data) {
            console.log(data);
            $('#commision_usd').val(data.comision);
            $('#realizar').removeAttr('disabled');
            $('#resultadoComision').html('ganancia acumulada: ' + data.acumulado + ' USD, comision: ' + data.comision + ' USD');
        },
        error: function (e) {
            console.log(e);
        }
    });
}

function getComisionStatusPartner(partner_id) {
    let account = $('#type_account_id').val();
    let date = $('#year').val() + '-' + $('#month').val() + '-01';
    let comission = $('#commision').val();
    if (account == null || comission == "") {
        $('#resultadoComision').html('');
        return;
    }
    $('#resultadoComision').html('Obteniendo informacion');
    console.log(account, comission, partner_id);
    var url = baseUrl + '/rendimiento/socios/operaciones/getComisionStatus/' + account + '/' + comission + '/' + partner_id + '/' + date;
    console.log(url);
    $.ajax({
        type: "GET",
        url: url,
        success: function (data) {
            console.log(data);
            $('#commision_usd').val(data.comision);
            $('#realizar').removeAttr('disabled');
            $('#resultadoComision').html('ganancia acumulada: ' + data.acumulado + ' USD, comision: ' + data.comision + ' USD');
        },
        error: function (e) {
            console.log(e);
        }
    });
}
$('#admin_name_business').hide();
$('#label_name_business').hide();
$('#navEmpresa').hide();
$('#navRepresentante').hide();
$('#navPersonal').show();
$('.adminempresa').hide();
$('.adminempresa').removeAttr("required");

$(document).ready(function () {
    $('#tipo').on('change', function () {

        if (this.value == 0) {
                $('#admin_name_business').prop("required", true);
                $('#admin_name_business').show();
                $('#label_name_business').show();
                $('.adminempresa').show();
                $('.adminempresa').prop("required", true);
                $('.adminactividad').hide();
                $('.adminactividad').removeAttr("required");
                $('#navEmpresa').show();
                $('#navRepresentante').show();
                $('#navPersonal').hide();
        } else if (this.value == 1) {
            $('#admin_name_business').removeAttr("required");
            $('#admin_name_business').hide();
            $('#label_name_business').hide();
            $('.adminactividad').show();
            $('.adminactividad').prop("required", true);
            $('.adminempresa').hide();
            $('.adminempresa').removeAttr("required");
            $('#navEmpresa').hide();
            $('#navRepresentante').hide();
            $('#navPersonal').show();
        }

    });
});



