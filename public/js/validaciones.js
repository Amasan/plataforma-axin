/*Función para habilitar el boton submit cuando todo los campos esten llenados*/
function validar() {
    var validado = true;
    elementos = document.getElementsByClassName("inputFormu");
    for (i = 0; i < elementos.length; i++) {
        if (elementos[i].value == "" || elementos[i].value == null) {
            validado = false
        }
    }
    if (validado) {
        document.getElementById("boton").disabled = false;

    } else {
        document.getElementById("boton").disabled = true;
    }
}


function validarT() {
    var validado = true;
    elementos = document.getElementsByClassName("inputFormuT");
    for (i = 0; i < elementos.length; i++) {
        if (elementos[i].value == "" || elementos[i].value == null) {
            validado = false
        }
    }
    if (validado) {
        document.getElementById("Transacction").disabled = false;

    } else {
        document.getElementById("Transacction").disabled = true;
    }
}

/* Función para validar fechas, obtiene la fecha actual y le resta 18 años */

let date = new Date()

let day = date.getDate()
let month = date.getMonth() + 1
let year = date.getFullYear()
let yearmax = year - 18;

let ceroday = day < 10 ? "0" : "";
let ceromonth = month < 10 ? "0" : "";
let fulldate = `${yearmax}-${ceromonth}${month}-${ceroday}${day}`;
document.getElementById('date').setAttribute('max', fulldate);

/* Función para validar que solo acepte Caracteres con todos los tipos de acentos */

function validarCaracteres(e) { // 1
    tecla = (document.all) ? e.keyCode : e.which; // 2
    if (tecla == 8) return true; // 3
    if (tecla == 9) return true; // 3
    if (tecla == 11) return true; // 3
    patron = /[A-Za-zäÄëËïÏöÖüÜáéíóúáéíóúÁÉÍÓÚÂÊÎÔÛâêîôûàèìòùÀÈÌÒÙñÑ\s\t]/; // 4

    te = String.fromCharCode(tecla); // 5
    return patron.test(te); // 6
}

/* Función para validar que solo acepte numeros */

function validarNumeros(e) { // 1
    tecla = (document.all) ? e.keyCode : e.which; // 2
    if (tecla == 8) return true; // 3
    if (tecla == 9) return true; // 3
    if (tecla == 11) return true; // 3
    patron = /[0-9\s\t]/; // 4

    te = String.fromCharCode(tecla); // 5
    return patron.test(te); // 6
}

/* Función para validar que solo acepte caracteres y numeros */

function validarNumerosLetras(e) { // 1
    tecla = (document.all) ? e.keyCode : e.which; // 2
    if (tecla == 8) return true; // 3
    if (tecla == 9) return true; // 3
    if (tecla == 11) return true; // 3
    patron = /[A-Za-z0-9\s\t]/; // 4

    te = String.fromCharCode(tecla); // 5
    return patron.test(te); // 6
}

/* Función para copiar al portapapeles */

function copyToClipboard(element) {
    var $temp = $("<input>");
    $("body").append($temp);
    $temp.val($(element).text()).select();
    document.execCommand("copy");
    $temp.remove();
}

function changeIdentificacion(value) {
    document.getElementById("type_identity_oficial").value = value;
    if (value == 0) {
        document.getElementById("changeIdentificacion0").style.backgroundColor = '#185bc3';
        document.getElementById("changeIdentificacion0").style.color = '#EDF0F2';
        document.getElementById("changeIdentificacion1").style.backgroundColor = '#EDF0F2';
        document.getElementById("changeIdentificacion1").style.color = '#185bc3';
        return;
    }
    document.getElementById("changeIdentificacion1").style.backgroundColor = '#185bc3';
    document.getElementById("changeIdentificacion1").style.color = '#EDF0F2';
    document.getElementById("changeIdentificacion0").style.backgroundColor = '#EDF0F2';
    document.getElementById("changeIdentificacion0").style.color = '#185bc3';
}