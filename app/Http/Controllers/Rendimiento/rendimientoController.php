<?php

namespace App\Http\Controllers\Rendimiento;
use App\Http\Controllers\Controller;

use App\Models\AccountClient;
use App\Models\AccountPartner;
use Illuminate\Http\Request;
use Carbon\Carbon;
use App\Models\ClientReceipt;
use App\User;
use App\Models\TypeAccount;
use App\Models\Accumulated;
use App\Models\ClientWithDrawalRequest;
use App\Models\Client;
use App\Models\Commision;
use App\Models\Partner;
use Illuminate\Support\Facades\Auth; 
use Illuminate\Support\Facades\Storage;

class rendimientoController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
        
    }

    /************************************************************ */
    /** Funciones para Back office de Cliente                     */
    /************************************************************ */


    /**
     * Vista de depositos de los clientes
     * Se encuenta en resources/views/rendimiento/clientes/depositos.blade.php
     * method GET
     * url rendimiento/clientes/depositos
     */
    public function DepositosCliente(Request $request) {
        $name = $request->get('name');
        $email = $request->get('email');
        $clientReceipts = ClientReceipt::join('clients','clients.id','=','receipts.client_id')
        ->join('users','users.id','=','clients.user_id')
        ->join('account_clients','account_clients.id','=','receipts.account_client_id')
        ->join('type_accounts', 'type_accounts.id', '=', 'account_clients.type_account_id')
        ->where('users.name', 'LIKE', "%$name%")
        ->select(
            'clients.id as clientId',
            'type_accounts.name as accountName',
            'users.name',
            'users.first_last_name',
            'users.second_last_name',
            'users.email',
            'receipts.file_name',
            'receipts.reviewed',
            'receipts.created_at',
            'receipts.id',
            'account_clients.id as account_client_id'
        )
        ->OrderBy('receipts.id', 'DESC')
        ->email($email)
        ->paginate(20);
    
        return view('rendimiento.clientes.depositos', compact('clientReceipts'));

    }

    /**
     * Funcion para mostrar los comprobantes de depositos de clientes almecenados en aws
     * method GET
     * url /rendimiento/clientes/comprobante_cliente/{idClient}/{nameImage}/{image}
    */

    public function ComprobanteCliente($idClient,$nameImage,$image) {
        //$value = $image == 'file_document_identity_oficial_V1'?
        $hasname = ClientReceipt::where('client_id',$idClient)
        ->where($image."_name",$nameImage)
        ->select($image."_url as image")
        ->first();
        $url = Storage::disk('s3')->temporaryUrl(
            $hasname->image, time() + (session_cache_expire() * 6)
        );
        return $url;
    }
    

    /**
     * Funcion para validar los depositos de los clientes
     * url /rendimiento/clientes/depositos_cliente_reviewed
     * method POST
     */
    public function RevisarComprobranteCliente(Request $request) {
        
        $clientReceipt = ClientReceipt::find($request->id);
        $clientReceipt->reviewed = $request->reviewed??NULL;
        
        $amount = $request->amount;
        $accountClient = AccountClient::where('id', request('account_client_id'))
        ->first();
        $clientWithDrawalRequests = ClientWithDrawalRequest::where('account_client_id',$accountClient->id)
        ->where('status', 'Pendiente')
        ->get()
        ->toArray();
        if($clientReceipt->reviewed == NULL){
            return redirect()->back();//error valor nulo
        }
        if ($amount == null) {
            if($clientReceipt->reviewed == 1){
                return redirect()->back();//error no ingreso valor
            }
            $clientReceipt->save();
            $draweRequest = null;
            foreach($clientWithDrawalRequests as $clientWithDrawalRequest) {
                $now = Carbon::parse($clientWithDrawalRequest['created_at']);
                $nowi = Carbon::parse($clientReceipt['created_at']);
                if($now->diffInSeconds($nowi) < 60) {
                    $draweRequest = $clientWithDrawalRequest;

                }
                
            }
            $clientWithDrawalRequest = ClientWithDrawalRequest::where('id', $draweRequest['id'])
            ->first();
            $clientWithDrawalRequest->amount = '0';
            $clientWithDrawalRequest->status = 'Rechazado';
            $clientWithDrawalRequest->save();
            return redirect()->back();
        }
        
        $draweRequest = null;
        foreach($clientWithDrawalRequests as $clientWithDrawalRequest) {
            $now = Carbon::parse($clientWithDrawalRequest['created_at']);
            $nowi = Carbon::parse($clientReceipt['created_at']);
            if($now->diffInSeconds($nowi) < 60) {
                $draweRequest = $clientWithDrawalRequest;
            }
            
        }
        $clientWithDrawalRequest = ClientWithDrawalRequest::where('id', $draweRequest['id'])
        ->first();
        
        $clientWithDrawalRequest->amount = $amount;
        $clientWithDrawalRequest->status = 'Realizado';
        if($accountClient->date_contract_init == null){
            $date_contract = date("d-m-Y");
            $fecha_actual = date("d-m-Y");
            
            $date_contract_inicio = new Carbon($date_contract);
            
            if($accountClient->type_account_id == 1){
                if(($amount + $accountClient->amount_money) >=100){
                    $accountClient->date_contract_init = $date_contract_inicio;
                }
            }
            if($accountClient->type_account_id == 2){
                if(($amount + $accountClient->amount_money) >=1000){
                    $date_contract_fin = date("d-m-Y",strtotime($fecha_actual."+ 1 years"));
                    $date_contract_fin = new Carbon($date_contract_fin);
                    $accountClient->date_contract_init = $date_contract_inicio;
                    $accountClient->date_contract_fin = $date_contract_fin;
                }
            }
            if($accountClient->type_account_id == 3){
                if(($amount + $accountClient->amount_money) >=1000){
                    $date_contract_fin = date("d-m-Y",strtotime($fecha_actual."+ 3 years"));
                    $date_contract_fin = new Carbon($date_contract_fin);
                    $accountClient->date_contract_init = $date_contract_inicio;
                    $accountClient->date_contract_fin = $date_contract_fin;
                }
            }
        }
        
        $accountClient->amount_money = $amount + $accountClient->amount_money;
        $clientWithDrawalRequest->save();
        $clientReceipt->save();
        $accountClient->save();

        return redirect()->back();

    }

    /**
     * Vista de retiros de los clientes
     * Se encuenta en resources/views/rendimiento/clientes/retirarFondos.blade.php
     * method GET
     * url rendimiento/clientes/retirarFondos
    */
    public function retirarFondosCliente(Request $request) {
        $name = $request->get('name');
        $email = $request->get('email');
        $clientWithDrawalRequests = ClientWithDrawalRequest::where('concept','Retiro')
        ->where('account_client_id','!=',null)
        ->OrderBy('id', 'DESC')
        ->name($name)
        ->email($email)
        ->paginate(20);
        return view('rendimiento.clientes.retirarFondos', compact('clientWithDrawalRequests'));

    }

    /**
     * Funcion para validar los retiros de los clientes
     * url /rendimiento/clientes/retiros_cliente_reviewed
     * method POST
     */
    public function RevisarRetiroCliente(Request $request) {
        
        if($request->status == 'Pendiente'){
            return redirect()->back();
        }
        $clientWithDrawalRequest = ClientWithDrawalRequest::find($request->id);
        $client = $clientWithDrawalRequest->accountClient->client_id;
        
        $type_account = $clientWithDrawalRequest->accountClient->type_account_id;
        
        $accountClient = AccountClient::where('client_id',$client)
        ->where('type_account_id', $type_account)
        ->first();
        
        $gananciaAcumulada = Accumulated::where('client_id',$client)
            ->where('account_client_id',$accountClient->id)
            ->sum('amount');
        
        $amount = $gananciaAcumulada - $accountClient->withdrawals;
        if($request->status == 'Rechazado'){
            $clientWithDrawalRequest->status = 'Rechazado';
            $clientWithDrawalRequest->save();
            return redirect()->back();
        }
        if($amount > $clientWithDrawalRequest->amount){
            
            $accountClient->withdrawals = $accountClient->withdrawals + $clientWithDrawalRequest->amount;
            $accountClient->save();
            $clientWithDrawalRequest->status = 'Realizado';
            $clientWithDrawalRequest->save();
        }
        
        return redirect()->back();

    }

    /**
     * Vista de clientes para ver sus movimientos (retiros y depositos)
     * Se encuenta en resources/views/rendimiento/clientes/movimientos.blade.php
     * method GET
     * url rendimiento/clientes/movimientos
    */
    public function clientes(Request $request) {
        $name = $request->get('name');
        $email = $request->get('email');
        $clients = Client::join('users','users.id','=','clients.user_id')
        ->where('clients.complete_profile','5')
        ->select(
            'clients.id',
            'clients.birth_date_country',
            'clients.number_phone',
            'users.name',
            'users.first_last_name',
            'users.second_last_name',
            'users.email',
            
        )
        ->orderBy('clients.id', 'DESC')
        ->name($name)
        ->email($email)
        ->paginate(20);
        return view('rendimiento.clientes.movimientos', compact('clients'));
    }
    /**
     * Vista de las transacciones de un cliente (retiros y depositos)
     * Se encuenta en resources/views/rendimiento/clientes/movimientos_cliente.blade.php
     * method GET
     * url rendimiento/clientes/movimientos_cliente
    */
    public function movimientoscliente($id) {
        $items = ClientWithDrawalRequest::where('client_id',$id)
        ->join('account_clients', 'withdrawalrequests.account_client_id', '=', 'account_clients.id')
        ->join('type_accounts', 'account_clients.type_account_id', '=', 'type_accounts.id')
        ->orderBy('withdrawalrequests.id','DESC')
        ->select(

            'withdrawalrequests.id',
            'withdrawalrequests.concept',
            'withdrawalrequests.amount',
            'withdrawalrequests.status',
            'withdrawalrequests.date_deposite',
            'account_clients.account_number',
            'type_accounts.name')
        
        ->paginate(20);
        return view('rendimiento.clientes.movimientos_cliente', compact('items'));
    }

    /**
     * Vista de clientes para realizar operaciones (comisión, actualización e historial)
     * Se encuenta en resources/views/rendimiento/clientes/operaciones.blade.php
     * method GET
     * url rendimiento/clientes/operaciones
    */
    public function Operaciones(Request $request) {
        $name = $request->get('name');
        $email = $request->get('email');

        $clients = Client::join('users','users.id','=','clients.user_id')
        ->where('clients.complete_profile','5')
        ->orderBy('clients.id', 'DESC')
        ->name($name)
        ->email($email)
        ->paginate(20);
        $years = [];
        $year_max = date("Y");
        for ($i=$year_max-1; $i > 1980; $i--) { 
            array_push($years,$i);
        }
        $days = [];
        for ($i=2; $i < 31; $i++) { 
            array_push($days,$i);
        }

        $accounts = TypeAccount::where('is_socio','0')->get();
        return view('rendimiento.clientes.operaciones', compact('clients','years','days','accounts'));

    }

    /**
     * Vista de formulario para generar estadisticas de ganancia de un cliente
     * Se encuenta en resources/views/rendimiento/clientes/operaciones_form.blade.php
     * method GET
     * url rendimiento/clientes/operaciones_form
    */
    public function OperacionesForm(Client $client) {
        $accounts = TypeAccount::where('is_socio','0')->get();
        $years = [];
        $year_max = date("Y");
        for ($i=$year_max-1; $i > 1980; $i--) { 
            array_push($years,$i);
        }
        $days = [];
        for ($i=2; $i < 31; $i++) { 
            array_push($days,$i);
        }
        return view('rendimiento.clientes.operaciones_form', compact('accounts', 'client','years','days'));
    }

    public function updateOperation(Request $request){
        $date = new Carbon($request->year.'-'.$request->month.'-'.$request->day);
        $client = Client::where('id',$request->client_id)->first();
        $account_client = AccountClient::where('client_id',$client->id)
        ->where('type_account_id', $request->type_account_id)
        ->first();
        
        
        
        $accumulated = Accumulated::create(
            ['date'=>$date,
            'client_id'=>$client->id,
            'account_client_id'=>$account_client->id,
            'amount' => $request->amount,
            'commissions'=>0
            ]
        );
        $accumulated->save();
        return redirect('/rendimiento/clientes/historyOperations/'.$client->id);
    }

    public function updateComission(Request $request){
        $date = new Carbon($request->year.'-'.$request->month.'-'.$request->day);
        //$id_account_client = $input['id_account_client'];
        //'id','account_client_id','commissions','date'

        $account_client = AccountClient::where('client_id',$request->client_id)
        ->where('type_account_id', $request->type_account_id)
        ->first();

        $commision = Commision::create(
            ['date'=>$date,
            'account_client_id'=>$account_client->id,
            'commissions' => $request->commision_usd
            ]
        );
        $account_client->update();
        $commision->save();
        return redirect()->back();
    }

    public function historyOperations($client) {

        $accumulateds = Accumulated::join('account_clients','account_clients.id','=','accumulateds.account_client_id')
        ->join('type_accounts','type_accounts.id','=','account_clients.type_account_id')
        ->where('is_commision','0')
        ->where('accumulateds.client_id',$client)
        ->select('accumulateds.id as id',
        'accumulateds.client_id',
        'account_clients.account_number',
        'type_accounts.name',
        'accumulateds.amount',
        'accumulateds.date',
        'accumulateds.created_at',
        'accumulateds.updated_at'
        )
        ->orderBy('updated_at', 'DESC')->paginate(20);
        $client_account = Client::where('id',$client)
        ->first();
        
        return view('rendimiento.clientes.historial_operacion',
         compact('accumulateds','client','client_account'));
    }

    public function getComisionStatus($account, $comision, $client_id, $date){
        
        $datePicker = new Carbon($date);
        $start = Carbon::parse($datePicker->year.'-'.$datePicker->month.'-01');
        $end = Carbon::parse($datePicker->year.'-'.(($datePicker->month)).'-31');
        //dd($end);
        
        $acumulates = Accumulated::join('account_clients','account_clients.id', '=', 'accumulateds.account_client_id')
        ->join('type_accounts','type_accounts.id','=','account_clients.type_account_id')
        ->where('account_clients.client_id', $client_id)
        ->where('accumulateds.account_client_id', $client_id)
        ->where('type_accounts.id',$account)
        ->whereDate('accumulateds.date','>=',$start->format('y-m-d'))
        ->whereDate('accumulateds.date','<=',$end->format('y-m-d'))
        ->select('accumulateds.amount')
        ->sum('accumulateds.amount');
        
        $accountClient = AccountClient::where('client_id',$client_id)
        ->where('type_account_id',$account)->first();
        $amount = $acumulates - $accountClient->withdrawals;
        $valor_comision = ($amount*$comision)/100;
        $data['acumulado'] = $amount;
        $data['comision'] = $valor_comision;
        return $data;
    }

   


     /************************************************************ */
    /** Funciones para Back office de Socios                     */
    /************************************************************ */


    /**
     * Vista de depositos de los socios
     * Se encuenta en resources/views/rendimiento/socios/depositos.blade.php
     * method GET
     * url rendimiento/socios/depositos
     */
    public function DepositosSocio(Request $request) {
        $name = $request->get('name');
        $email = $request->get('email');
        $partnerReceipts = ClientReceipt::join('partners','partners.id','=','receipts.partner_id')
        ->join('users','users.id','=','partners.user_id')
        ->join('accountpathers','accountpathers.id','=','receipts.account_partner_id')
        ->join('type_accounts', 'type_accounts.id', '=', 'accountpathers.type_account_id')
        ->where('users.name', 'LIKE', "%$name%")
        ->select(
            'partners.id as partnerId',
            'type_accounts.name as accountName',
            'users.name',
            'users.first_last_name',
            'users.second_last_name',
            'users.email',
            'receipts.file_name',
            'receipts.reviewed',
            'receipts.created_at',
            'receipts.id',
            'accountpathers.id as account_partner_id'
        )
        ->OrderBy('receipts.id', 'DESC')
        ->email($email)
        ->paginate(20);
        return view('rendimiento.socios.depositos', compact('partnerReceipts'));

    }

    /**
     * Funcion para mostrar los comprobantes de depositos de socios almecenados en aws
     * method GET
     * url /rendimiento/socios/comprobante_socio/{idClient}/{nameImage}/{image}
    */

    public function ComprobanteSocio($idPartner,$nameImage,$image) {
        //$value = $image == 'file_document_identity_oficial_V1'?
        $hasname = ClientReceipt::where('partner_id',$idPartner)
        ->where($image."_name",$nameImage)
        ->select($image."_url as image")
        ->first();
        $url = Storage::disk('s3')->temporaryUrl(
            $hasname->image, time() + (session_cache_expire() * 6)
        );
        return $url;
    }

    /**
     * Funcion para validar los depositos de los socios
     * url /rendimiento/socios/depositos_socio_reviewed
     * method POST
     */

    public function RevisarComprobranteSocio(Request $request) {
        
        $partnerReceipt = ClientReceipt::find($request->id);
        $partnerReceipt->reviewed = $request->reviewed??NULL;
        
        $amount = $request->amount;
        $accountPartner = AccountPartner::where('id', request('account_partner_id'))
        ->first();
        $partnerWithDrawalRequests = ClientWithDrawalRequest::where('account_partner_id',$accountPartner->id)
        ->where('status', 'Pendiente')
        ->get()
        ->toArray();
        if($partnerReceipt->reviewed == NULL){
            return redirect()->back();//error valor nulo
        }
        if ($amount == null) {
            if($partnerReceipt->reviewed == 1){
                return redirect()->back();//error no ingreso valor
            }
            $partnerReceipt->save();
            $draweRequest = null;
            foreach($partnerWithDrawalRequests as $partnerWithDrawalRequest) {
                $now = Carbon::parse($partnerWithDrawalRequest['created_at']);
                $nowi = Carbon::parse($partnerReceipt['created_at']);
                if($now->diffInSeconds($nowi) < 60) {
                    $draweRequest = $partnerWithDrawalRequest;

                }
                
            }
            $partnerWithDrawalRequest = ClientWithDrawalRequest::where('id', $draweRequest['id'])
            ->first();
            $partnerWithDrawalRequest->amount = '0';
            $partnerWithDrawalRequest->status = 'Rechazado';
            $partnerWithDrawalRequest->save();
            return redirect()->back();
        }
        
        $draweRequest = null;
        foreach($partnerWithDrawalRequests as $partnerWithDrawalRequest) {
            $now = Carbon::parse($partnerWithDrawalRequest['created_at']);
            $nowi = Carbon::parse($partnerReceipt['created_at']);
            if($now->diffInSeconds($nowi) < 60) {
                $draweRequest = $partnerWithDrawalRequest;
            }
            
        }
        $partnerWithDrawalRequest = ClientWithDrawalRequest::where('id', $draweRequest['id'])
        ->first();
        
        $partnerWithDrawalRequest->amount = $amount;
        $partnerWithDrawalRequest->status = 'Realizado';
        
        $accountPartner->amount_money = $amount + $accountPartner->amount_money;
        $partnerWithDrawalRequest->save();
        $partnerReceipt->save();
        $accountPartner->save();

        return redirect()->back();

    }

    /**
     * Vista de retiros de los socios
     * Se encuenta en resources/views/rendimiento/socios/retirarFondos.blade.php
     * method GET
     * url rendimiento/socios/retirarFondos
    */
    public function retirarFondosSocio(Request $request) {
        $name = $request->get('name');
        $email = $request->get('email');
        $partnerWithDrawalRequests = ClientWithDrawalRequest::where('concept','Retiro')
        ->where('account_partner_id','!=',null)
        ->OrderBy('id', 'DESC')
        ->name($name)
        ->email($email)
        ->paginate(20);
        return view('rendimiento.socios.retirarFondos', compact('partnerWithDrawalRequests'));

    }

    /**
     * Funcion para validar los retiros de los socios
     * url /rendimiento/socios/retiros_socio_reviewed
     * method POST
     */
    public function RevisarRetiroSocio(Request $request) {
        
        if($request->status == 'Pendiente'){
            return redirect()->back();
        }
        $partnerWithDrawalRequest = ClientWithDrawalRequest::find($request->id);
        $partner = $partnerWithDrawalRequest->accountPartner->partner_id;
        
        $type_account = $partnerWithDrawalRequest->accountPartner->type_account_id;
        
        $accountPartner = AccountPartner::where('partner_id',$partner)
        ->where('type_account_id', $type_account)
        ->first();
        
        $gananciaAcumulada = Accumulated::where('partner_id',$partner)
            ->where('account_partner_id',$accountPartner->id)
            ->sum('amount');
        
        $amount = $gananciaAcumulada - $accountPartner->withdrawals;
        if($request->status == 'Rechazado'){
            $partnerWithDrawalRequest->status = 'Rechazado';
            $partnerWithDrawalRequest->save();
            return redirect()->back();
        }
        if($amount > $partnerWithDrawalRequest->amount){
            
            $accountPartner->withdrawals = $accountPartner->withdrawals + $partnerWithDrawalRequest->amount;
            $accountPartner->save();
            $partnerWithDrawalRequest->status = 'Realizado';
            $partnerWithDrawalRequest->save();
        }
        
        return redirect()->back();

    }

    /**
     * Vista de socios para ver sus movimientos (retiros y depositos)
     * Se encuenta en resources/views/rendimiento/socios/movimientos.blade.php
     * method GET
     * url rendimiento/socios/movimientos
    */
    public function socios(Request $request) {
        $name = $request->get('name');
        $email = $request->get('email');
        $partners = Partner::join('users','users.id','=','partners.user_id')
        ->where('partners.complete_profile','5')
        ->select(
            'partners.id',
            'partners.birth_date_country',
            'partners.number_phone',
            'users.name',
            'users.first_last_name',
            'users.second_last_name',
            'users.email',
            
        )
        ->orderBy('partners.id', 'DESC')
        ->name($name)
        ->email($email)
        ->paginate(20);
        return view('rendimiento.socios.movimientos', compact('partners'));
    }
    /**
     * Vista de las transacciones de un socio (retiros y depositos)
     * Se encuenta en resources/views/rendimiento/socios/movimientos_socio.blade.php
     * method GET
     * url rendimiento/socios/movimientos_socio
    */
    public function movimientossocio($id) {
        
        $items = ClientWithDrawalRequest::where('partner_id',$id)
        ->join('accountpathers', 'withdrawalrequests.account_partner_id', '=', 'accountpathers.id')
        ->join('type_accounts', 'accountpathers.type_account_id', '=', 'type_accounts.id')
        ->orderBy('withdrawalrequests.id','DESC')
        ->select(

            'withdrawalrequests.id',
            'withdrawalrequests.concept',
            'withdrawalrequests.amount',
            'withdrawalrequests.status',
            'withdrawalrequests.date_deposite',
            'accountpathers.account_number',
            'type_accounts.name')
        
        ->paginate(20);
        return view('rendimiento.socios.movimientos_socio', compact('items'));
    }

    /**
     * Vista de socios para realizar operaciones (comisión, actualización e historial)
     * Se encuenta en resources/views/rendimiento/socios/operaciones.blade.php
     * method GET
     * url rendimiento/socios/operaciones
    */
    public function OperacionesSocios(Request $request) {
        $name = $request->get('name');
        $email = $request->get('email');

        $partners = Partner::join('users','users.id','=','partners.user_id')
        ->where('partners.complete_profile','5')
        ->select(
            'users.name',
            'users.first_last_name',
            'users.second_last_name',
            'users.email',
            'partners.id',
            'partners.birth_date_country',
            'partners.number_phone',
        )
        ->orderBy('partners.id', 'DESC')
        ->name($name)
        ->email($email)
        ->paginate(20);
        $years = [];
        $year_max = date("Y");
        for ($i=$year_max-1; $i > 1980; $i--) { 
            array_push($years,$i);
        }
        $days = [];
        for ($i=1; $i < 31; $i++) { 
            array_push($days,$i);
        }

        $accounts = TypeAccount::where('is_socio','1')->get();
        return view('rendimiento.socios.operaciones', compact('partners','years','days','accounts'));

    }

    /**
     * Vista de formulario para generar estadisticas de ganancia de un socios
     * Se encuenta en resources/views/rendimiento/socios/operaciones_form.blade.php
     * method GET
     * url rendimiento/socios/operaciones_form
    */
    public function OperacionesSociosForm(Partner $partner) {
        $accounts = TypeAccount::where('is_socio','1')->get();
        $years = [];
        $year_max = date("Y");
        for ($i=$year_max-1; $i > 1980; $i--) { 
            array_push($years,$i);
        }
        $days = [];
        for ($i=2; $i < 31; $i++) { 
            array_push($days,$i);
        }
        return view('rendimiento.socios.operaciones_form', compact('accounts', 'partner','years','days'));
    }

    public function updateOperationPartner(Request $request){
        
        $date = new Carbon($request->year.'-'.$request->month.'-'.$request->day);
        $partner = Partner::where('id',$request->partner_id)->first();
        $account_partner = AccountPartner::where('partner_id',$partner->id)
        ->where('type_account_id', $request->type_account_id)
        ->first();
        
        $is_comision = $request->is_commision=='1'?true:false;
        $amount = $is_comision?((-1)*$request->amount):$request->amount;
        $accumulated = Accumulated::create(
            ['date'=>$date,
            'partner_id'=>$partner->id,
            'account_partner_id'=>$account_partner->id,
            'amount' => $amount,
            'commissions'=>0,
            ]
        );
        if($is_comision){
            $date_end = new Carbon($request->year_end.'-'.$request->month_end.'-'.$request->day_end);
            $accumulated->date_end = $date_end;
        }
        $accumulated->is_commision = $is_comision;
        $accumulated->account_partner_id = $account_partner->id;
        $accumulated->partner_id = $partner->id;
        $accumulated->save();
        return redirect('/rendimiento/socios/historyOperationsPartner/'.$partner->id);
    }
    public function updateOperationPartnerEdit(Request $request){
        
        $accumulated = Accumulated::find($request->id);
        
        $date = new Carbon($request->year.'-'.$request->month.'-'.$request->day);
        $account_partner = AccountPartner::where('partner_id',$accumulated->partner_id)
        ->where('type_account_id', $request->type_account_id)
        ->first();
        
        $is_comision = $request->is_commision=='1'?true:false;
        $accumulated->account_partner_id = $account_partner->id;
        $amount = $is_comision?((-1)*$request->amount):$request->amount;
        $accumulated->amount = $amount;
        $accumulated->date = $date;
        if($is_comision){
            $date_end = new Carbon($request->year_end.'-'.$request->month_end.'-'.$request->day_end);
            $accumulated->date_end = $date_end;
        }
        $accumulated->is_commision = $is_comision;
        $accumulated->save();
        return redirect('/rendimiento/socios/historyOperationsPartner/'.$accumulated->partner_id);
    }
    public function updateOperationClientEdit(Request $request){
        
        $accumulated = Accumulated::find($request->id);
        
        $date = new Carbon($request->year.'-'.$request->month.'-'.$request->day);
        $account_client = AccountClient::where('client_id',$accumulated->client_id)
        ->where('type_account_id', $request->type_account_id)
        ->first();
        
        $accumulated->account_client_id = $account_client->id;
        $amount = $request->amount;
        $accumulated->amount = $amount;
        $accumulated->date = $date;
        $accumulated->save();
        return redirect('/rendimiento/clientes/historyOperations/'.$accumulated->client_id);
    }
    public function updateComissionPartner(Request $request){
        $date = new Carbon($request->year.'-'.$request->month.'-'.$request->day);
        //$id_account_client = $input['id_account_client'];
        //'id','account_client_id','commissions','date'

        $account_partner = AccountPartner::where('partner_id',$request->partner_id)
        ->where('type_account_id', $request->type_account_id)
        ->first();

        $commision = Commision::create(
            ['date'=>$date,
            'account_partner_id'=>$account_partner->id,
            'commissions' => $request->commision_usd
            ]
        );
        $account_partner->update();
        $commision->save();
        return redirect()->back();
    }
    public function historyOperationsPartnerEdit($idAcumulated){
        $accumulated = Accumulated::where('id',$idAcumulated)->first();
        $accumulated->amount = (-1)*$accumulated->amount;
        $partner = Partner::where('id',$accumulated->partner_id)->first();
        $accounts = TypeAccount::join('accountpathers','accountpathers.type_account_id','=','type_accounts.id')
        ->where('type_accounts.is_socio','1')
        ->where('accountpathers.id',$accumulated->account_partner_id)
        ->select('type_accounts.id',
        'accountpathers.account_number',
        'type_accounts.name')
        ->first();
        $accounts2 = TypeAccount::join('accountpathers','accountpathers.type_account_id','=','type_accounts.id')
        ->where('accountpathers.partner_id',$accumulated->partner_id)
        ->where('type_accounts.is_socio','1')
        ->select('type_accounts.id',
        'accountpathers.account_number',
        'type_accounts.name')
        ->get();
        $years = [];
        $year_max = date("Y");
        for ($i=$year_max-1; $i > 1980; $i--) { 
            array_push($years,$i);
        }
        $days = [];
        for ($i=1; $i <= 31; $i++) { 
            array_push($days,$i);
        }
        $date_end = new Carbon($accumulated->date_end);
        $date = new Carbon($accumulated->date);
        return view('rendimiento.socios.operaciones_form_edit', compact('accounts','accounts2', 'partner','years','days','accumulated','date_end','date'));
    }
    public function historyOperationsClientEdit($idAcumulated){
        $accumulated = Accumulated::where('id',$idAcumulated)->first();

        $client = Client::where('id',$accumulated->client_id)->first();
        $accounts = TypeAccount::join('account_clients','account_clients.type_account_id','=','type_accounts.id')
        ->where('type_accounts.is_socio','0')
        ->where('account_clients.id',$accumulated->account_client_id)
        ->select('type_accounts.id',
        'account_clients.account_number',
        'type_accounts.name')
        ->first();
        $accounts2 = TypeAccount::join('account_clients','account_clients.type_account_id','=','type_accounts.id')
        ->where('account_clients.client_id',$accumulated->client_id)
        ->where('type_accounts.is_socio','0')
        ->select('type_accounts.id',
        'account_clients.account_number',
        'type_accounts.name')
        ->get();
        $years = [];
        $year_max = date("Y");
        for ($i=$year_max-1; $i > 1980; $i--) { 
            array_push($years,$i);
        }
        $days = [];
        for ($i=1; $i <= 31; $i++) { 
            array_push($days,$i);
        }
        $date = new Carbon($accumulated->date);
        return view('rendimiento.clientes.operaciones_form_edit', compact('accounts','accounts2', 'client','years','days','accumulated','date'));
    }
    public function historyOperationsPartner($partner) {

        $accumulateds = Accumulated::join('accountpathers','accountpathers.id','=','accumulateds.account_partner_id')
        ->join('type_accounts','type_accounts.id','=','accountpathers.type_account_id')
        ->where('accumulateds.partner_id',$partner)
        ->select('accumulateds.id as id',
        'accumulateds.partner_id',
        'accountpathers.account_number',
        'type_accounts.name',
        'accumulateds.amount',
        'accumulateds.date',
        'accumulateds.created_at',
        'accumulateds.updated_at'
        )
        ->orderBy('updated_at', 'DESC')->paginate(20);
        $client_account = Partner::where('id',$partner)
        ->first();
        return view('rendimiento.socios.historial_operacion',
         compact('accumulateds','partner','client_account'));
    }

    public function getComisionStatusPartner($account, $comision, $partner_id, $date){
        
        $datePicker = new Carbon($date);
        $start = Carbon::parse($datePicker->year.'-'.$datePicker->month.'-01');
        $end = Carbon::parse($datePicker->year.'-'.(($datePicker->month)).'-31');
        
        $acumulates = Accumulated::join('accountpathers','accountpathers.id', '=', 'accumulateds.account_partner_id')
        ->join('type_accounts','type_accounts.id','=','accountpathers.type_account_id')
        ->where('accountpathers.partner_id', $partner_id)
        ->where('accumulateds.account_partner_id', $partner_id)
        ->where('type_accounts.id',$account)
        ->whereDate('accumulateds.date','>=',$start->format('y-m-d'))
        ->whereDate('accumulateds.date','<=',$end->format('y-m-d'))
        ->select('accumulateds.amount')
        ->sum('accumulateds.amount');
        
        $accountPartner = AccountPartner::where('partner_id',$partner_id)
        ->where('type_account_id',$account)->first();
        $amount = $acumulates - $accountPartner->withdrawals;
        $valor_comision = ($amount*$comision)/100;
        $data['acumulado'] = $amount;
        $data['comision'] = $valor_comision;
        return $data;
    }
}
