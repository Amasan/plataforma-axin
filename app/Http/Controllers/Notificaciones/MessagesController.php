<?php

namespace App\Http\Controllers\Notificaciones;
use App\Http\Controllers\Controller;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth; 
use App\Models\Client;
use App\Models\ClientWithDrawalRequest;
use App\User;
use App\Message;
use App\Models\ClientReceipt;
use App\Notifications\MessageSent;
use App\Notifications\MassNotifications;
use App\Post;
use App\Events\PostCreated;
use App\Models\Partner;
use Illuminate\Support\Facades\Notification;

use LaravelFCM\Message\OptionsBuilder;
use LaravelFCM\Message\PayloadDataBuilder;
use LaravelFCM\Message\PayloadNotificationBuilder;
use FCM;


class MessagesController extends Controller
{

    public function __construct() {
        $this->middleware('auth');
    }

    /** 
     * Vista del formulario para enviar notificacion sobre la aprobacion o rechazo de documentos del cliente
     * solo para el perfil de legal
     * se encuentra en la carpeta resources/views/notificaiones/transacciones/mensajeDocs.blade.php
     * method GET
     * url /notificaciones/transacciones/mensajeDocs
    */
    public function mensajeDocs(Client $client) {
        return view('notificaciones.documentos.mensajeDocs', compact('client'));
    }
    
    public function mensajeDocsSocio(Partner $partner) {
        return view('notificaciones.documentos.mensajeDocsSocio', compact('partner'));
    }
    
    /** 
     * Funcion para guardar el mensaje que se envia del formulario de la funcion mensajeDocs  
     * solo para el perfil de legal
     * url /mensajeDocsStore
     * method POST
    */
    public function mensajeDocsStore(Request $request) {
        /**
         * Validacion de datos
         */
        $this->validate($request, [
            'body' => 'required',
            'recipient_id' => 'required|exists:users,id'
        ]);

        $input = $request->all();
        $title = $input['recipient_id'];
        $body = $input['body'];
      

        /**
         * Guardar el mensaje
         */
        $message = new Message([
            'sender_id' => auth()->id(), //El que envia el msj (Usuario logueado)
            'recipient_id' => $request->recipient_id, //El que recibe el msj
            'body' => $request->body, //Mensaje
        ]);

        $message->save();

        $recipient = User::find($request->recipient_id); //Buscar el id del que recibe el msj para despues enviar la notificacion
        /**
         * Al encontrar el usuario se hace una instancia de la notificacion y se le pasa el msj creado
         * la notificacion se encuentra en app/Notifications/MessageSent
         */

        $url = route('messages.show', $message->id);
        $this->broadcastMessageNotify($title,$body,$url);
        $recipient->notify(new MessageSent($message));

       
       

        return redirect()->to('/legal/clientes/perfiles');
        
    }

    private function broadcastMessageNotify($title=null,$body=null,$click_action) {
        $optionBuilder = new OptionsBuilder();
        $optionBuilder->setTimeToLive(60*20);

        $notificationBuilder = new PayloadNotificationBuilder('Mensaje de axin capital');
        $notificationBuilder->setBody($body)
                            ->setSound('default')
                            ->setClickAction($click_action);

        
        $dataBuilder = new PayloadDataBuilder();
        $dataBuilder->addData([
            'title' => $title,
            'body' => $body
        ]);

        $option = $optionBuilder->build();
        $notification = $notificationBuilder->build();
     
        $data = $dataBuilder->build();
        // You must change it to get your tokens
        $tokens = User::where('id',$title)->pluck('fcm_token')->toArray();
       

      
        
        $downstreamResponse = FCM::sendTo($tokens, $option, $notification, $data);
      
        return $downstreamResponse->numberSuccess();

        $downstreamResponse->numberSuccess();
        $downstreamResponse->numberFailure();
        $downstreamResponse->numberModification();
        
        // return Array - you must remove all this tokens in your database
        $downstreamResponse->tokensToDelete();
        
        // return Array (key : oldToken, value : new token - you must change the token in your database)
        $downstreamResponse->tokensToModify();
        
        // return Array - you should try to resend the message to the tokens in the array
        $downstreamResponse->tokensToRetry();
        
        // return Array (key:token, value:error) - in production you should remove from your database the tokens present in this array
        $downstreamResponse->tokensWithError();
    }

    /** 
     * Vista del formulario para enviar notificacion sobre la aprobacion o rechazo de un deposito del cliente 
     * solo para el perfil de rendimiento
     * se encuentra en la carpeta resources/views/notificaiones/transacciones/mensajeDeposito.blade.php
     * method GET
     * url /notificaciones/transacciones/mensajeDeposito/{clientReceipt}
    */
    public function mensajeTransaccionDeposito(ClientReceipt $clientReceipt) {
        return view('notificaciones.transacciones.mensajeDeposito', compact('clientReceipt'));
    }
    public function mensajeTransaccionDepositoSocio(ClientReceipt $partnerReceipt) {
        return view('notificaciones.transacciones.mensajeDepositoSocio', compact('partnerReceipt'));
    }
    /** 
     * Vista del formulario para enviar notificacion sobre la aprobacion o rechazo de un retiro del cliente 
     * solo para el perfil de rendimiento
     * se encuentra en la carpeta resources/views/notificaiones/transacciones/mensajeRetiro.blade.php
     * method GET
     * url /notificaciones/transacciones/mensajeRetiro/{clientWithDrawalRequest}
    */
    public function mensajeTransaccionRetiro(ClientWithDrawalRequest $clientWithDrawalRequest) {
        return view('notificaciones.transacciones.mensajeRetiro', compact('clientWithDrawalRequest'));
    }
    public function mensajeTransaccionRetiroSocio(ClientWithDrawalRequest $partnerWithDrawalRequest) {
        return view('notificaciones.transacciones.mensajeRetiroSocio', compact('partnerWithDrawalRequest'));
    }

    /** 
     * Funcion para guardar el mensaje que se envia del formulario de la funcion mensajeDeposito y mensajeRetiro  
     * solo para el perfil de legal
     * url /notificaciones/transacciones/mensaje_transaccion_store
     * method POST
    */
    public function mensajeTransaccionStore(Request $request) {
        /**
         * Validacion de datos
        */
        $this->validate($request, [
            'body' => 'required',
            'recipient_id' => 'required|exists:users,id'
        ]);

        $input = $request->all();
        $title = $input['recipient_id'];
        $body = $input['body'];
      
        /**
         * Guardar el mensaje
         */
        $message = new Message([
            'sender_id' => auth()->id(), //El que envia el msj (Usuario logueado)
            'recipient_id' => $request->recipient_id, //El que recibe el msj
            'body' => $request->body, //Mensaje
        ]);
        
        $message->save();
        $recipient = User::find($request->recipient_id); //Buscar el id del que recibe el msj para despues enviar la notificacion
        /**
         * Al encontrar el usuario se hace una instancia de la notificacion y se le pasa el msj creado
         * la notificacion se encuentra en app/Notifications/MessageSent
         */
        $url = route('messages.show', $message->id);
        $this->broadcastMessageNotify($title,$body,$url);
        $recipient->notify(new MessageSent($message));

        return redirect()->to('/');
        
    }

    /** 
     * Funcion para ver el mensaje que se recibe del formulario de la funcion mensajeDeposito y mensajeRetiro  
     * solo para el perfil cliente
     * se encuentra en la carpeta resources/views/notificaiones/transacciones/verMensaje.blade.php
     * method GET
     * url /notificaciones/transacciones/verMensaje
    */
    public function show($id) {
        $this->shareCompact();
        $message = Message::findorFail($id);
        return view('notificaciones.transacciones.verMensaje', compact('message'));
    }


    /*********************************************************************** */
    /** Rutas para notificaciones masivas (Novedades)                        */
    /*********************************************************************** */

    /** 
     * Vista del formulario para enviar notificacion masivas a todos los clientes y socios
     * solo para el perfil de rendimiento y legal
     * se encuentra en la carpeta resources/views/notificaciones/novedades/novedad.blade.php
     * method GET
     * url /novedad_axin
    */
    public function NovedadAxinForm() {
        return view('notificaciones.novedades.novedad');
    }

    /** 
     * Funcion para guardar los mensajes masivos (novedades)  
     * url /novedad_axin_store
     * method POST
    */

    public function NovedadAxinStore(Request $request) {
        /**
         * Validacion de datos
        */
        // $this->validate($request, [
        //     'title' => 'required',
        //     'body' => 'required'
        // ]);

        $input = $request->all();
        $title = $input['title'];
        $body = $input['body'];
        /**
         * Validacion de datos
        */
        $post = new Post([
            'title' => $request->title, //Titulo del post masivo
            'body' => $request->body, //Cuerpo del post masivo
        ]);

     

       

        $post->save();

        $url = route('notificaciones.novedades.verNovedad', $post->id);
         $this->broadcastMessage($title, $body,$url);
        /**
         * Se genera una instancia del evento PostCreated que recibe el post creado y se genera al crear un post
         * Se encuentra en la carpeta app/Events/PostCreated
         */
        event(new PostCreated($post));

        return redirect('/');

    }

    private function broadcastMessage($title=null, $body=null,$click_action) {
        $optionBuilder = new OptionsBuilder();
        $optionBuilder->setTimeToLive(60*20);

        $notificationBuilder = new PayloadNotificationBuilder('Novedad axin');
        $notificationBuilder->setBody($body)
                            ->setSound('default')
                            ->setClickAction($click_action);

        
        $dataBuilder = new PayloadDataBuilder();
        $dataBuilder->addData([
            'title' => $title,
            'body' => $body
        ]);

        $option = $optionBuilder->build();
        $notification = $notificationBuilder->build();
     
        $data = $dataBuilder->build();
    
        // You must change it to get your tokens
        $tokens = User::whereHas('roles',function($query) {
            $query->where('roles_id','>=',4);
        })->pluck('fcm_token')->toArray();

      
        
        $downstreamResponse = FCM::sendTo($tokens, $option, $notification, $data);
      
        return $downstreamResponse->numberSuccess();

        $downstreamResponse->numberSuccess();
        $downstreamResponse->numberFailure();
        $downstreamResponse->numberModification();
        
        // return Array - you must remove all this tokens in your database
        $downstreamResponse->tokensToDelete();
        
        // return Array (key : oldToken, value : new token - you must change the token in your database)
        $downstreamResponse->tokensToModify();
        
        // return Array - you should try to resend the message to the tokens in the array
        $downstreamResponse->tokensToRetry();
        
        // return Array (key:token, value:error) - in production you should remove from your database the tokens present in this array
        $downstreamResponse->tokensWithError();
    }

    /** 
     * Funcion para ver el mensaje de la novedad axin  
     * solo para el perfil cliente Y socio
     * se encuentra en la carpeta resources/views/notificaciones/novedades/verNovedad.blade.php 
     * url /ver_novedad_axin/{id}
     * metodo GET
    */
    public function verNovedadAxin($id) {
        $this->shareCompact();
        $post = Post::findorFail($id);
        return view('notificaciones.novedades.verNovedad', compact('post'));
    }

    private function shareCompact() {
        $user = Auth::user();
        if($user->roles()->first()->id == 4){
        $client = Client::where('user_id',$user->id)->first();
        $data = $client->complete_profile;
        view()->share('complete_profile', ($data*2)==0?1:($data*2));
        } else {
            $partner = Partner::where('user_id',$user->id)->first();
            $data = $partner->complete_profile;
            view()->share('complete_profile', ($data*2)==0?1:($data*2));
        }
    }

}