<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth; 
use App\Models\Client; 
use App\Models\AccountClient; 
class portafolioController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }
    Public function index() {
        $this->shareCompact();
        $user = Auth::user();
        $client = Client::where('user_id',$user->id)->first();
        $accountA = AccountClient::where('type_account_id',1)
        ->where('client_id',$client->id)
        ->first();
        $accountI = AccountClient::where('type_account_id',2)
        ->where('client_id',$client->id)
        ->first();
        $accountM = AccountClient::where('type_account_id',3)
        ->where('client_id',$client->id)
        ->first();
        return view('cliente.portafolio',compact('accountA', 'accountI', 'accountM'));
    }
    private function shareCompact(){
        $user = Auth::user();
        $client = Client::where('user_id',$user->id)->first();
        $data = $client->complete_profile;
        view()->share('complete_profile', ($data*2)==0?1:($data*2));
    }
}
