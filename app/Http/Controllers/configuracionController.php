<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\User;
use Barryvdh\DomPDF\Facade as PDF;
use Carbon\Carbon;
use Illuminate\Support\Facades\Auth; 
use App\Models\Client;
use App\Models\Partner;
use App\Models\AnualIncomes; 
use App\Models\PublicOfficeEmployee;
use App\Models\TotalFundInvest;
use Mail;
use App\Http\Requests\ClientPersonalRequest;
use App\Http\Requests\ClientHouseRequest;
use App\Http\Requests\ClientFilesRequest;
use App\Http\Requests\ClientFirmarRequest;
use App\Http\Requests\ClientChangePaswwordRequest;
use App\Http\Requests\ClientNotificationRequest;
use Illuminate\Support\Facades\Storage;
use \stdClass;
class configuracionController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    private function shareCompact(){
        $user = Auth::user();
        $data = 0;
        $account = null;
        if($user->roles()->first()->id == 4){
            $account = Client::where('user_id',$user->id)->first();
        }
        else{
            $account = Partner::where('user_id',$user->id)->first();
            
        }
        $data = $account->complete_profile;
        view()->share('complete_profile', ($data*2)==0?1:($data*2));
    }
    /** 
     * identificado el estado de llenado 
     * del formulario para reubicar en la vista correcta
     * /personal 
     * method GET
     */
    public function researchPersonal(){
        $user = Auth::user();
        $client = Client::where('user_id',$user->id)->first();
        $direction="";
        switch ($client->complete_profile) {
            case 0:
                $direction = "/datosp";
                break;
            case 1:
                $direction = "/actividade";
                break;
            case 2:
                $direction = "/cargard";
                break;
            case 3:
                $direction = "/firmac";
                break;    
            default:
                return redirect()->to('/inicio');
        }
        return redirect()->to('/personal'.$direction);
    }
    /*
    * llenado de formulario de registro
    * url get /personal/datosp/
    */
    Public function datosp() {
         
        $user = Auth::user();
        $client = Client::where('user_id',$user->id)->first();
        
        if(!$client->is_active){
            return redirect('/inicio');
        }
       
        if($client->complete_profile != '0'){
            return redirect('/personal');
        }
        
        $this->shareCompact();
        return view('cliente.datos_personales');
    }
    /** 
     * registro de datos personales  
     * 
     * method POST personal/update_datosp/
     */
    public function updateDatosp(ClientPersonalRequest $request){
        $input = $request->all(); 
        $user = Auth::user();
        $user->name = strtoupper($input['name']);
        $user->first_last_name = strtoupper($input['first_last_name']);
        $user->second_last_name = strtoupper($input['second_last_name']);
        $user->direction = strtoupper($input['direction']);
        $client = Client::where('user_id',$user->id)->first();
        $client->birth_date = strtoupper($input['birth_date']);
        $client->city = strtoupper($input['city']);
        $client->code_postal = strtoupper($input['code_postal']);
        $client->birth_date_country = strtoupper($input['birth_date_country']);
        $client->number_phone = strtoupper($input['number_phone']);
        $client->country_code = strtoupper($input['country_code']);
        if(!$client->is_active){
            return redirect('/inicio');
        }
        if($client->complete_profile != 0){
            return redirect('/personal');
        }
        $client->complete_profile = 1;
        $user->save();
        $client->save();
        return redirect('/personal/actividade');
    }
    
    /*
    * formulario de registro - actividad economica
    * get /personal/actividade/
    */
    Public function actividade() {
        $user = Auth::user();
        $client = Client::where('user_id',$user->id)->first();
        if(!$client->is_active){
            return redirect('/inicio');
        }
        if($client->complete_profile != 1){
            return redirect('/personal');
        }
        $anualIncomes = AnualIncomes::all()->toArray();
        $publicOfficeEmployees=PublicOfficeEmployee::all()->toArray();
        $totalFundInvest = TotalFundInvest::all()->toArray();
        $this->shareCompact();
        return view('cliente.actividad_economica')
        ->with('anualIncomes',$anualIncomes)
        ->with('publicOfficeEmployees',$publicOfficeEmployees)
        ->with('totalFundInvest',$totalFundInvest);
    }
    /** 
     * seccion registro actividad economica de datos personales 
     * 
     * method POST personal/update_actividade/
     * 
     */
    public function updateDatosActividade(ClientHouseRequest $request){
        $input = $request->all();
        $user = Auth::user();
        $client = Client::where('user_id',$user->id)->first();
        if(!$client->is_active){
            return redirect('/inicio');
        }
        $client->state_employee = strtoupper($input['state_employee']);
        $client->anual_activity_income_id = strtoupper($input['anual_activity_income_id']);
        $client->family_member_holds_public_office_id = strtoupper($input['family_member_holds_public_office_id']);
        $client->total_funds_invest_id = strtoupper($input['total_funds_invest_id']);
        $client->is_politically_exposed = strtoupper($input['is_politically_exposed']);
        $client->is_declared_bankrupt = strtoupper($input['is_declared_bankrupt']);
        if(!$client->is_active){

            return redirect('/inicio');
        }

        if($client->complete_profile == 4 || $client->complete_profile == 5) {
            if ($client->is_politically_exposed ||
                $client->is_declared_bankrupt){
                $client->is_active = false;
            }

            $client->save();
            return redirect('/personal/editar_actividade');
        }
        

        if($client->complete_profile != 1){
 
            return redirect('/personal');
        }
        
        if($client->is_politically_exposed ||
            $client->is_declared_bankrupt){
                $client->is_active = false;
        }
        else{
            $client->complete_profile = 2;
        }
        $client->save();
        
        return redirect('/personal/cargard');
    }

    /**
     * vista de edicion - datos de actividad economica
     * get personal/editar_actividade
     */
    public function editActividade() {
        $user = Auth::user();
        $client = Client::where('user_id',$user->id)->first();
        if(!$client->is_active){
            return redirect('/inicio');
        }
        if($client->complete_profile < 4){
            return redirect('/inicio');
        }
        $anualIncomes = AnualIncomes::all()->toArray();
        $publicOfficeEmployees=PublicOfficeEmployee::all()->toArray();
        $totalFundInvest = TotalFundInvest::all()->toArray();
        $this->shareCompact();
        return view('cliente.editar_actividad_economica', compact('client', 'user', 'anualIncomes', 'publicOfficeEmployees', 'totalFundInvest'));
    
    }

    /**
     * Editar datos de actividad economica
     */
    public function editCargard() {
        $user = Auth::user();
        $client = Client::where('user_id',$user->id)->first();
        if(!$client->is_active){
            return redirect('/inicio');
        }
        if($client->complete_profile < 4){
            return redirect('/inicio');
        }
        $type_identity_oficial = ['IFE/INE', 'Pasaporte'];
        $this->shareCompact();
        return view('cliente.editar_cargar_documentos', compact('client', 'user','type_identity_oficial'));
    }



    /** 
     * seccion registro - cargar documentos de datos personales 
     * 
     * method POST personal/update_cargard
     * 
     */
    Public function updateDatosCargard(ClientFilesRequest $request) {
        $input = $request->all();
        $user = Auth::user();
        $client = Client::where('user_id',$user->id)->first();
        if(!$client->is_active){
            return redirect('/inicio');
        }
        if($client->complete_profile != 2){

            return redirect('/personal');
        }
        $type_identity_oficial = $input['type_identity_oficial'];
        $client->type_identity_oficial = $type_identity_oficial=='0'?'IFE/INE':'Pasaporte';
        $fileV1 = $input['file_document_identity_oficial_V1'];
        $resultV1 = $this->validateFile($fileV1,true);
        $resultV2 = new stdClass;
        $resultV2->status = 200;
        $resultV2->message = "";
        $fileV2 = $input['file_document_identity_oficial_V2']??'';
        if($type_identity_oficial=="0"){
            $fileV2 = $input['file_document_identity_oficial_V2'];
            $resultV2 = $this->validateFile($fileV2,true);
        }
        

        $file_doc = $input['file_document_home'];
        $resultdoc = $this->validateFile($file_doc,false);

        if($resultV1->status==300 ||
                $resultV2->status==300 ||
                    $resultdoc->status==300){
            return redirect()->back()
            ->withInput()
            ->withErrors(['file_document_identity_oficial_V1' => $resultV1->message,
                'file_document_identity_oficial_V2' => $resultV2->message,
                    'file_document_home' => $resultdoc->message]);
        }
        //$fileV1->store('axin','s3');

        Storage::disk('s3')->put('', $fileV1);
        $client->file_document_identity_oficial_V1 = $fileV1->hashName();
        $client->file_document_identity_oficial_V1_name = $fileV1->getClientOriginalName();

        if($type_identity_oficial=="0"){
            Storage::disk('s3')->put('', $fileV2);
            $client->file_document_identity_oficial_V2 = $fileV2->hashName();
            $client->file_document_identity_oficial_V2_name = $fileV2->getClientOriginalName();
        }
        
        Storage::disk('s3')->put('', $file_doc);
        $client->file_document_home = $file_doc->hashName();
        $client->file_document_home_name = $file_doc->getClientOriginalName();

        $client->complete_profile = 3;
        $client->save();
        return redirect('/personal');

    }

    private function data(){
        return response()->json('status', 200);
    }
    /**
     * edicion de registro de cargado de documentos
     * post personal/update_cargard2/
    */
    public function updateFilesClient(Request $request){
        $input = $request->all();
        $user = Auth::user();
        $client = Client::where('user_id',$user->id)->first();
        $resultV1 = $this->data();
        $type_identity_oficial = $input['type_identity_oficial'];
        $client->type_identity_oficial = $type_identity_oficial=='0'?'IFE/INE':'Pasaporte';
        
        if(!$client->is_active){
            return redirect('/inicio');
        }

        $fileV1 = $input['file_document_identity_oficial_V1']??'undefined';
        $resultV1->status = 200;
        
        if($fileV1 != 'undefined'){
            if($type_identity_oficial=="0"){
                $resultV1 = $this->validateFile($fileV1,true);
            }
            
        }
        
        $resultV2 = $this->data();
        $resultV2->status = 200;
        $fileV2 = $input['file_document_identity_oficial_V2']??'undefined';
        if($fileV2 != 'undefined'){
            $resultV2 = $this->validateFile($fileV2,true);
        }
        
        $resultdoc= $this->data();
        $resultdoc->status = 200;
        $file_doc = $input['file_document_home']??'undefined';
        if($file_doc != 'undefined'){
            $resultdoc = $this->validateFile($file_doc,false);
        }
        
        if(($resultV1->status==300) ||
                $resultV2->status==300 ||
                    $resultdoc->status==300){
            return redirect()->back()
            ->withInput()
            ->withErrors(['file_document_identity_oficial_V1' => $resultV1->message,
                'file_document_identity_oficial_V2' => $resultV2->message,
                    'file_document_home' => $resultdoc->message]);
        }
        if($fileV1 != 'undefined'){
            Storage::disk('s3')->put('', $fileV1);
            $client->file_document_identity_oficial_V1 = $fileV1->hashName();
            $client->file_document_identity_oficial_V1_status = '';
            $client->file_document_identity_oficial_V1_name = $fileV1->getClientOriginalName();

        }
        if($fileV2 != 'undefined'){
            if($type_identity_oficial=="0"){
                Storage::disk('s3')->put('', $fileV2);
                $client->file_document_identity_oficial_V2 = $fileV2->hashName();
                $client->file_document_identity_oficial_V2_status = '';
                $client->file_document_identity_oficial_V2_name = $fileV2->getClientOriginalName();
            }
        
        }
        if($file_doc != 'undefined'){
            Storage::disk('s3')->put('', $file_doc);
            $client->file_document_home = $file_doc->hashName();
            $client->file_document_home_status = 'NULL';
            $client->file_document_home_name = $file_doc->getClientOriginalName();
        }

        $client->save();
        return redirect('/personal/editar_cargard');
    }
    //validacion de archivo
    public function validateFile($file,$is_format){
        $result = new stdClass;
        $result->status = 300;
        $result->message = "";
        //validar posible error : ejemplo.txt.png
        $filename = $file->getClientOriginalName();
        $names = explode(".",$filename);
        if(count($names)>2){
            $result->message = "El archivo incluye varios formatos";
            return $result;
        }
        //validar posible error: archivo.obj
        $fileExtension = $file->getClientOriginalExtension();
        $array = $is_format?['png','jpeg','jpg']:['png','jpeg','jpg','pdf'];
        $is_extension = array_search($fileExtension,$array);
        if($is_extension<0){
            $result->message = "El archivo incluye formato no permitido";
            return $result;
        }
        // validar posible error: 50 MB
        $tamañoArchivoByte = filesize($file);
        $tamañoArchivoKbyte = $tamañoArchivoByte/1024;
        if($tamañoArchivoKbyte>3000){//30 MB
            $result->message = "El archivo supera el valor de 30 MB";
            return $result;
        }

        //validar posible error: tamanio texto excedido
        $datefilename = date("Ymd-hisa-").$filename;
        $path = public_path() . '//images/';
        
        $length_path = strlen($path . $datefilename);
        if($length_path>200){
            $result->message = "El nombre del archivo es muy largo";
            return $result;
        }
        
        $result->status = 200;
        $result->name = $datefilename;
        $result->url = $path;
        return $result;
    }
    /**
     * vista de cargado de documentos
     * get /personal/cargard/
    */
    Public function cargard() {
        $user = Auth::user();
        $client = Client::where('user_id',$user->id)->first();
        if(!$client->is_active){
            return redirect('/inicio');
        }
        if($client->complete_profile != 2){
            return redirect('/personal');
        }
        return $this->showView('cliente.cargar_documentos');
    }
    /**
     * vista de firma de contrato
     * get /personal/firmac
    */
    Public function firmac() {
        $user = Auth::user();
        $client = Client::where('user_id',$user->id)->first();
        if(!$client->is_active){
            return redirect('/inicio');
        }
        if($client->complete_profile != 3){
            return redirect('/personal');
        }
        $fullname = $user->name ." ". $user->first_last_name ." ". $user->second_last_name;

        return $this->showView('cliente.firmar_contrato')
            ->with('fullname',$fullname);
    }
    /**
     * registro de firma de contrato
     * post /personal/update_firmac
    */
    public function updateFirmar(ClientFirmarRequest $request){//deprecado
        $user = Auth::user();
        $client = Client::where('user_id',$user->id)->first();
        if(!$client->is_active){
            return redirect('/inicio');
        }
        if($client->complete_profile != 3){
            return redirect('/personal');
        }
        $client->complete_profile = 4;
        $client->save();
        return redirect('/inicio');
    }
    /**
     * vista de cambio de contrasenia
     * get /configuracion/password
    */
    Public function password() {
        return $this->showView('cliente.cambiar_password');
    }
    /**
     * registro de actualizacion de cambio de contrasenia
     * post /configuracion/update_password
    */
    public function updatePassword(ClientChangePaswwordRequest $request){
        $input = $request->all();
        $user = Auth::user();
        $input['password'] = bcrypt($input['password']);
        $user->password = $input['password'];
        $user->save();
        return redirect('/');
    }
    /**
     * vista de configuracion de notificaciones
     * get /configuracion/notificaciones
    */
    Public function notificaciones() {
        $user = Auth::user();
        $account = null;
        if($user->roles()->first()->id == 4){
            $account = Client::where('user_id',$user->id)->first();
        }
        else{
            $account = Partner::where('user_id',$user->id)->first();
            
        }
        $is_active_newsletter = $account->is_active_newsletter;
        $is_active_movements = $account->is_active_movements;
        return $this->showView('cliente.recibir_notificaciones')
        ->with('is_active_movements',$is_active_movements)
        ->with('is_active_newsletter',$is_active_newsletter);
    }
    /**
     * registro de configuracion de notificaciones
     * post /configuracion/update_notificaciones
    */
    public function updateNotifications(ClientNotificationRequest $request){
        $input = $request->all();
        $user = Auth::user();
        $account = null;
        if($user->roles()->first()->id == 4){
            $account = Client::where('user_id',$user->id)->first();
        }
        else{
            $account = Partner::where('user_id',$user->id)->first();
        }
        $account->is_active_newsletter = $input['is_active_newsletter']??'0';
        $account->is_active_movements = $input['is_active_movements']??'0';
        $account->save();
        return redirect('/configuracion/notificaciones');
    }
    /**
     * registro de firma ajax
     * post personal/update_firm
    */
    public function uploadFirm(Request $request){
        $input = $request->all();
        $user = Auth::user();
        
        $pdf = null;
        $date = Carbon::now()->toDateTimeString();
        $client = Client::where('user_id',$user->id)->first();
        
        if(!$client->is_active){
            return redirect('/inicio');
        }
        if($client->complete_profile != 3){
            return redirect('/personal');
        }
        $client->complete_profile = 4;

        $file = $input['signature']; 
        Storage::disk('s3')->put('', $file);
        $client->signature = $file->hashName();
        //$client->save();

        $image = $client->signature??'';
        $nombre = $user->name.' '.$user->first_last_name.' '.$user->second_last_name;
        $url_firm = Storage::disk('s3')->temporaryUrl(
            $image, time() + (session_cache_expire() * 6)
            );
        $pdf = PDF::loadView('cliente.contrato',
        compact('date','nombre','url_firm'))->output();
        
        Storage::disk('s3')->put($nombre.'.pdf',$pdf);

        $url = Storage::disk('s3')->temporaryUrl(
            $nombre.'.pdf', time() + (session_cache_expire() * 6)
        );
        $client->file_document_contract = $nombre.'.pdf';
        $client->save();
        $subject = "Firma de Contrato";
        $for = $user->email;
        $file_to_attach = $url;
        Mail::send('emails.contrato',['url' => 'https://amaliath3code.com/ver_ContratoPdf'], function($msj) use($subject,$for,$file_to_attach){
            $msj->from(ENV('MAIL_USERNAME'),"Firma de contrato AXIN");
            $msj->subject($subject);
            $msj->to($for);
            $msj->attach($file_to_attach, [
                    'as' => 'contrato.pdf',
                    'mime' => 'application/pdf',
                ]);
        });
        return $url;
    }
    
    public  function showView($viewName){
        $this->shareCompact();
        return view($viewName);
    }
    
}
