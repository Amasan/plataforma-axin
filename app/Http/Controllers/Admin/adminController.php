<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;


use App\Models\AccountClient;
use App\Models\AccountPartner;
use Carbon\Carbon;
use App\Models\ClientReceipt;
use App\User;
use App\Models\TypeAccount;
use App\Models\Accumulated;
use App\Models\ClientWithDrawalRequest;
use App\Models\Client;
use App\Models\Commision;
use App\Models\Partner;
use Illuminate\Support\Facades\Auth; 
use Illuminate\Support\Facades\Storage;
use App\Events\ReceiptCreated;
use App\Models\AnualIncomes; 
use App\Models\PublicOfficeEmployee;
use App\Models\TotalFundInvest;
use App\Http\Controllers\configuracionController;
use \stdClass;

use App\Notifications\SignupActivate; 
use App\Models\Setting;
use Illuminate\Support\Str;


class adminController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
        
    }

    /************************************************************ */
    /** Funciones para Back office de Cliente                     */
    /************************************************************ */

    public function clientes(Request $request) {
        $name = $request->get('name');
        $email = $request->get('email');
        $clients = Client::join('users','users.id','=','clients.user_id')
        ->where('clients.complete_profile', '5')
        ->select(
            'users.name',
            'users.first_last_name',
            'users.second_last_name',
            'clients.birth_date_country',
            'users.email',
            'clients.number_phone',
            'clients.id',
        )
        ->orderBy('clients.id', 'DESC')
        ->name($name)
        ->email($email)
        ->paginate(20);
        return view('admin.clientes.clientes', compact('clients'));
    }

    /**
     * Vista de formulario comprobar deposito
     * Se encuenta en resources/views/admin/clientes/comprobar_deposito.blade.php
     * method GET
     * url /admin/clientes/comprobante
     */
    public function comprobante($id) {

        $client = Client::find($id);  

        $accounts = AccountClient::where('client_id', $id)->get();
        
        return view('admin.clientes.comprobar_deposito', compact('client', 'accounts'));

    }



    public function movimientoscliente($id) {
        $clientWithDrawalRequests = ClientWithDrawalRequest::where('client_id',$id)
        ->join('account_clients', 'withdrawalrequests.account_client_id', '=', 'account_clients.id')
        ->join('type_accounts', 'account_clients.type_account_id', '=', 'type_accounts.id')
        ->orderBy('withdrawalrequests.id','DESC')
        ->select(

            'withdrawalrequests.id',
            'withdrawalrequests.concept',
            'withdrawalrequests.amount',
            'withdrawalrequests.status',
            'withdrawalrequests.date_deposite',
            'account_clients.account_number',
            'type_accounts.name')
        
        ->paginate(20);
        return view('admin.clientes.movimientos_cliente', compact('clientWithDrawalRequests'));
    }

      /**
     * Vista de formulario para editar movimientos
     * Se encuenta en resources/views/admin/clientes/editarMovimiento.blade.php
     * method GET
     * url /admin/clientes/editar_movimiento_cliente/{id}
     */
    public function editarmovimientocliente($id) {
    
        $clientWithDrawalRequest = ClientWithDrawalRequest::where('id', $id)->first();

        return view('admin.clientes.editarMovimiento', compact('clientWithDrawalRequest'));

    }

    public function updateMovimientoCliente(Request $request, $id) {

        $clientWithDrawalRequest = ClientWithDrawalRequest::find($id);

        $accountClient = AccountClient::where('id', $clientWithDrawalRequest->account_client_id)
        ->first();

        $accountClient->amount_money = $accountClient->amount_money - $clientWithDrawalRequest->amount;
        $accountClient->amount_money = $request->amount + $accountClient->amount_money;
        $accountClient->save();

        $clientWithDrawalRequest->date_deposite = $request->date_deposite;
        $clientWithDrawalRequest->amount = $request->amount;

        $clientWithDrawalRequest->save();

        return redirect()->back();


    }


    /**
     * Guarda el registro de un deposito sin comprobante
     * method POST
     * url /admin/clientes/subir_comprobante
     */
    
    public function updateComprobante(Request $request) {
        $clientReceipt =  new ClientReceipt();
        $clientReceipt->client_id = $request->client_id;
        $clientReceipt->file_name = null;
        $clientReceipt->file_url = null;
        $clientReceipt->account_client_id = $request->account_client_id;
        $clientReceipt->reviewed = 1;
        $clientReceipt->created_at = $request->created_at;
        $clientReceipt->partner_id = null;
        $clientReceipt->account_partner_id = null;
        

        $clientWithDrawalRequest =  new ClientWithDrawalRequest();

        $clientWithDrawalRequest->account_client_id = $request->account_client_id;
        $clientWithDrawalRequest->name = $request->name;
        $clientWithDrawalRequest->email = $request->email;
        $clientWithDrawalRequest->bitcoin_address = '';
        $clientWithDrawalRequest->concept = 'Deposito';
        $clientWithDrawalRequest->date_deposite = $request->created_at;
        $clientWithDrawalRequest->amount = $request->amount;
        $clientWithDrawalRequest->method = 'Transferencia';
        $clientWithDrawalRequest->status = 'Realizado';

        $accountClient = AccountClient::where('id', $request->account_client_id)
        ->first();

        $accountClient->amount_money = $request->amount + $accountClient->amount_money;
        
        $accountClient->save();
        
        $clientWithDrawalRequest->save();

        
        $clientReceipt->save();

        event(new ReceiptCreated($clientReceipt));

        return redirect('admin/clientes/clientes');
    }

    

    public function socios(Request $request) {
        $name = $request->get('name');
        $email = $request->get('email');
        $partners = Partner::join('users','users.id','=','partners.user_id')
        ->where('partners.complete_profile', '5')
        ->select(
            'users.name',
            'users.first_last_name',
            'users.second_last_name',
            'partners.birth_date_country',
            'users.email',
            'partners.number_phone',
            'partners.id',
        )
        ->orderBy('partners.id', 'DESC')
        ->name($name)
        ->email($email)
        ->paginate(20);
        return view('admin.socios.socios', compact('partners'));
    }

    /**
     * Vista de formulario comprobar deposito
     * Se encuenta en resources/views/admin/socios/comprobar_deposito.blade.php
     * method GET
     * url /admin/socios/comprobante
     */
    public function socioComprobante($partnerid) {
        $partner = Partner::find($partnerid);  
     
        $accounts = AccountPartner::where('partner_id', $partnerid)->get();
        
        return view('admin.socios.comprobar_deposito', compact('partner', 'accounts'));

    }

    
    public function movimientossocio($id) {
        $clientWithDrawalRequests = ClientWithDrawalRequest::where('partner_id',$id)
        ->join('accountpathers', 'withdrawalrequests.account_partner_id', '=', 'accountpathers.id')
        ->join('type_accounts', 'accountpathers.type_account_id', '=', 'type_accounts.id')
        ->orderBy('withdrawalrequests.id','DESC')
        ->select(

            'withdrawalrequests.id',
            'withdrawalrequests.concept',
            'withdrawalrequests.amount',
            'withdrawalrequests.status',
            'withdrawalrequests.date_deposite',
            'accountpathers.account_number',
            'type_accounts.name')
        
        ->paginate(20);
        return view('admin.socios.movimientos_socio', compact('clientWithDrawalRequests'));
    }

      /**
     * Vista de formulario para editar movimientos
     * Se encuenta en resources/views/admin/socios/editarMovimiento.blade.php
     * method GET
     * url /admin/socios/editar_movimiento_socio/{id}
     */
    public function editarmovimientosocio($id) {
    
        $clientWithDrawalRequest = ClientWithDrawalRequest::where('id', $id)->first();

        return view('admin.socios.editarMovimiento', compact('clientWithDrawalRequest'));

    }

    public function updateMovimientoSocio(Request $request, $id) {

        $clientWithDrawalRequest = ClientWithDrawalRequest::find($id);

        $accountPartner = AccountPartner::where('id', $clientWithDrawalRequest->account_partner_id)
        ->first();

        $accountPartner->amount_money = $accountPartner->amount_money - $clientWithDrawalRequest->amount;
        $accountPartner->amount_money = $request->amount + $accountPartner->amount_money;
        $accountPartner->save();

        $clientWithDrawalRequest->date_deposite = $request->date_deposite;
        $clientWithDrawalRequest->amount = $request->amount;

        $clientWithDrawalRequest->save();

        return redirect()->back();


    }



    /**
     * Guarda el registro de un deposito sin comprobante
     * method POST
     * url /admin/socios/subir_comprobante
     */
    
    public function updateComprobantesocio(Request $request) {
        $clientReceipt =  new ClientReceipt();
        $clientReceipt->client_id = null;
        $clientReceipt->file_name = null;
        $clientReceipt->file_url = null;
        $clientReceipt->account_client_id = null;
        $clientReceipt->reviewed = 1;
        $clientReceipt->created_at = $request->created_at;
        $clientReceipt->partner_id = $request->partner_id;
        $clientReceipt->account_partner_id = $request->account_partner_id;
        

        $clientWithDrawalRequest =  new ClientWithDrawalRequest();

        $clientWithDrawalRequest->account_partner_id = $request->account_partner_id;
        $clientWithDrawalRequest->name = $request->name;
        $clientWithDrawalRequest->email = $request->email;
        $clientWithDrawalRequest->bitcoin_address = '';
        $clientWithDrawalRequest->concept = 'Deposito';
        $clientWithDrawalRequest->date_deposite = $request->created_at;
        $clientWithDrawalRequest->amount = $request->amount;
        
        $clientWithDrawalRequest->method = 'Transferencia';
        $clientWithDrawalRequest->status = 'Realizado';

        $accountPartner = AccountPartner::where('id', $request->account_partner_id)
        ->first();

        $accountPartner->amount_money = $request->amount + $accountPartner->amount_money;
        
        $accountPartner->save();
        
        $clientWithDrawalRequest->save();

        
        $clientReceipt->save();

        event(new ReceiptCreated($clientReceipt));

        return redirect('admin/socios/socios');
    }



    /** Registro */

    public function usuario() {
        $anualIncomes = AnualIncomes::all()->toArray();
        $publicOfficeEmployees = PublicOfficeEmployee::all()->toArray();
        $totalFundInvest = TotalFundInvest::all()->toArray();

        return view('admin.registro.usuario', compact('anualIncomes', 'publicOfficeEmployees', 'totalFundInvest'));
    }


    public function userStore(Request $request) {
        $input = $request->all(); 
        $input['password'] = bcrypt($input['password']);//encrypt($input['password']);
        $user = User::create($input);

        if( $input['tipo'] != '0') {
            $user->roles()->sync([4]);
            $user->is_active = false;
            $user->activation_token = Str::random(60);
            
            $user->name = strtoupper($input['name']);
            $user->first_last_name = strtoupper($input['first_last_name']);
            $user->second_last_name = strtoupper($input['second_last_name']);
            $user->direction = strtoupper($input['direction']);
            $user->save();

            $client = Client::create(['user_id' => $user->id,'complete_profile'=>5]);
            $client->birth_date = strtoupper($input['birth_date']);
            $client->city = strtoupper($input['city']);
            $client->code_postal = strtoupper($input['code_postal']);
            $client->birth_date_country = strtoupper($input['birth_date_country']);
            $client->number_phone = strtoupper($input['number_phone']);
            $client->country_code = strtoupper($input['country_code']);

            $client->state_employee = strtoupper($input['state_employee']);
            $client->anual_activity_income_id = strtoupper($input['anual_activity_income_id']);
            $client->family_member_holds_public_office_id = strtoupper($input['family_member_holds_public_office_id']);
            $client->total_funds_invest_id = strtoupper($input['total_funds_invest_id']);
            $client->is_politically_exposed = strtoupper($input['is_politically_exposed']);
            $client->is_declared_bankrupt = strtoupper($input['is_declared_bankrupt']);

            $config = new configuracionController;

            $type_identity_oficial = $input['type_identity_oficial'];
            $client->type_identity_oficial = $type_identity_oficial=='0'?'IFE/INE':'Pasaporte';
            $client->file_document_identity_oficial_V1_status = '1';
            $client->file_document_identity_oficial_V2_status = '1';
            $client->file_document_home_status = '1';
            $client->file_document_contract_status = '1';
            $fileV1 = $input['file_document_identity_oficial_V1'];
            $resultV1 = $config->validateFile($fileV1,true);
            $resultV2 = new stdClass;
            $resultV2->status = 200;
            $resultV2->message = "";
            $fileV2 = $input['file_document_identity_oficial_V2']??'';
            if($type_identity_oficial=="0"){
                $fileV2 = $input['file_document_identity_oficial_V2'];
                $resultV2 = $config->validateFile($fileV2,true);
            }
            

            $file_doc = $input['file_document_home'];
            $resultdoc = $config->validateFile($file_doc,false);

            if($resultV1->status==300 ||
                    $resultV2->status==300 ||
                        $resultdoc->status==300){
                return redirect()->back()
                ->withInput()
                ->withErrors(['file_document_identity_oficial_V1' => $resultV1->message,
                    'file_document_identity_oficial_V2' => $resultV2->message,
                        'file_document_home' => $resultdoc->message]);
            }
            //$fileV1->store('axin','s3');

            Storage::disk('s3')->put('', $fileV1);
            $client->file_document_identity_oficial_V1 = $fileV1->hashName();
            $client->file_document_identity_oficial_V1_name = $fileV1->getClientOriginalName();

            if($type_identity_oficial=="0"){
                Storage::disk('s3')->put('', $fileV2);
                $client->file_document_identity_oficial_V2 = $fileV2->hashName();
                $client->file_document_identity_oficial_V2_name = $fileV2->getClientOriginalName();
            }
            
            Storage::disk('s3')->put('', $file_doc);
            $client->file_document_home = $file_doc->hashName();
            $client->file_document_home_name = $file_doc->getClientOriginalName();

    
            $client->save();
            $settings = Setting::where('is_payment','0')->get()->toArray();
            $generate_account = 0;
            $id_setting = 0;
            //$generate_account = (int) $this->getvalue('generate_account',$settings);
            for($x = 0; $x <count($settings); $x++) {
                if($settings[$x]['key'] === 'generate_account_client'){
                    $generate_account = (int) $settings[$x]['value'];
                    $id_setting = $settings[$x]['id'];
                }
            }
            $setting = Setting::where('id', $id_setting)
                ->update(['value' => ($generate_account+3)]);
            $accountClient1 = AccountClient::create([
                'client_id'=>$client->id,
                'type_account_id'=>1,
                'account_number'=>$generate_account+1,
                'amount_money'=>0,
                'is_active'=>true
            ]);
            
            $accountClient2 = AccountClient::create([
                'client_id'=>$client->id,
                'type_account_id'=>2,
                'account_number'=>$generate_account+2,
                'amount_money'=>0,
                'is_active'=>false
            ]);
            $accountClient3 = AccountClient::create([
                'client_id'=>$client->id,
                'type_account_id'=>3,
                'account_number'=>$generate_account+3,
                'amount_money'=>0,
                'is_active'=>false
            ]);
            $accountClient1->save();
            $accountClient2->save();
            $accountClient3->save();
            $success['access_token'] = $user->createToken('token')->accessToken;
            $user->notify(new SignupActivate($user));
            return redirect('/admin/registro/datos_personales'); 
        } else {
            
            $user->roles()->sync([5]);
            $user->is_active = false;
            $user->activation_token = Str::random(60);
            
            $user->name = strtoupper($input['name']);
            $user->first_last_name = strtoupper($input['first_last_name']);
            $user->second_last_name = strtoupper($input['second_last_name']);
            $user->direction = strtoupper($input['direction']);
            $user->save();

            $partner = Partner::create(['user_id' => $user->id,'complete_profile'=>5]);

            $partner->rfc_business = strtoupper($input['rfc_business']);
            $partner->name_business = strtoupper($input['name_business']);
            $partner->code_postal_business = $input['code_postal_business'];
            $partner->country_business = strtoupper($input['country_business']);
            $partner->city_business = strtoupper($input['city_business']);
            $partner->code_postal_business = $input['code_postal_business'];
            $partner->direction_company = strtoupper($input['direction_company']);

            $partner->birth_date = $input['birth_date'];
            $partner->city = strtoupper($input['city']);
            $partner->code_postal = $input['code_postal'];
            $partner->birth_date_country = strtoupper($input['birth_date_country']);
            $partner->number_phone = $input['number_phone'];
            $partner->country_code = $input['country_code'];

            $config = new configuracionController;
            $type_identity_oficial = $input['type_identity_oficial'];
            $partner->type_identity_oficial = $type_identity_oficial=='0'?'IFE/INE':'Pasaporte';
            $partner->file_document_identity_oficial_V1_status = '1';
            $partner->file_document_identity_oficial_V2_status = '1';
            $partner->file_document_home_status = '1';
            $partner->file_document_contract_status = '1';
            $fileV1 = $input['file_document_identity_oficial_V1'];
            $resultV1 = $config->validateFile($fileV1,true);
            $resultV2 = new stdClass;
            $resultV2->status = 200;
            $resultV2->message = "";
    
            $fileV2 = $input['file_document_identity_oficial_V2']??'';
            if($type_identity_oficial=="0"){
                $fileV2 = $input['file_document_identity_oficial_V2'];
                $resultV2 = $config->validateFile($fileV2,true);
            }
    
            $file_doc = $input['file_document_home'];
            $resultdoc = $config->validateFile($file_doc,false);
    
            if($resultV1->status==300 ||
                    $resultV2->status==300 ||
                        $resultdoc->status==300){
                return redirect()->back()
                ->withInput()
                ->withErrors(['file_document_identity_oficial_V1' => $resultV1->message,
                    'file_document_identity_oficial_V2' => $resultV2->message,
                        'file_document_home' => $resultdoc->message]);
            }
            Storage::disk('s3')->put('', $fileV1);
            $partner->file_document_identity_oficial_V1 = $fileV1->hashName();
            $partner->file_document_identity_oficial_V1_name = $fileV1->getClientOriginalName();
    
            if($type_identity_oficial=="0"){
                Storage::disk('s3')->put('', $fileV2);
                $partner->file_document_identity_oficial_V2 = $fileV2->hashName();
                $partner->file_document_identity_oficial_V2_name = $fileV2->getClientOriginalName();
            }
            
            Storage::disk('s3')->put('', $file_doc);
            $partner->file_document_home = $file_doc->hashName();
            $partner->file_document_home_name = $file_doc->getClientOriginalName();
    
         
            $partner->save();
            $settings = Setting::where('is_payment','0')->get()->toArray();
            $generate_account = 0;
            $id_setting = 0;
            for($x = 0; $x <count($settings); $x++) {
                if($settings[$x]['key'] === 'generate_account_socio'){
                    $generate_account = (int) $settings[$x]['value'];
                    $id_setting = $settings[$x]['id'];
                }
            }
            $setting = Setting::where('id', $id_setting)
                ->update(['value' => ($generate_account+50)]);
            $accountPartner1 = AccountPartner::create([
                'partner_id'=>$partner->id,
                'type_account_id'=>'4',
                'account_number'=>$generate_account+1,
                'amount_money'=>0,
                'is_active'=>true
            ]);
            $accountPartner1->type_account_id = '4';
            $accountPartner1->save();
            $success['access_token'] = $user->createToken('token')->accessToken;
            $user->notify(new SignupActivate($user));
            return  redirect('/'); 
        }
     
    }




}
