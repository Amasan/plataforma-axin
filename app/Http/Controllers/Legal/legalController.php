<?php
namespace App\Http\Controllers\Legal;

use App\Events\VerifiedPartnerCreated;
use App\Events\VerifiedClientCreated;
use App\Http\Controllers\Controller;
use App\Models\Client; 
use App\Models\Partner; 
use Illuminate\Http\Request;
use App\Http\Requests\ContactRequest;
use App\Listeners\NotifyPartnerOfFullVerification;
use Illuminate\Support\Facades\Storage;
use App\Models\TypeAccount;
use App\Models\AccountClient;
use App\Models\AccountPartner;
use App\User;
use App\Models\AnualIncomes; 
use App\Models\PublicOfficeEmployee;
use App\Models\TotalFundInvest;
use App\Models\Contacts;
use App\Notifications\VerifiedPartner;
use Illuminate\Support\Facades\Auth; 

use LaravelFCM\Message\OptionsBuilder;
use LaravelFCM\Message\PayloadDataBuilder;
use LaravelFCM\Message\PayloadNotificationBuilder;
use FCM;

class legalController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    

    /************************************************************ */
    /** Funciones para Back office de Cliente                     */
    /************************************************************ */


    /**
     * Vista de perfiles de cliente (todos los registros)
     * Se encuenta en resources/views/legal/clientes/perfiles.blade.php
     * method GET
     * url legal/clientes/perfiles
     */
    public function perfilesCliente(Request $request) {
        $name = $request->get('name');
        $email = $request->get('email');
        
        $clients = Client::join('users','users.id','=','clients.user_id')
        ->select(
            'clients.id',
            'clients.created_at',
            'clients.updated_at',
            'clients.birth_date_country',
            'clients.file_document_identity_oficial_V1_status',
            'clients.file_document_identity_oficial_V1_name',
            'clients.file_document_identity_oficial_V2_status',
            'clients.file_document_identity_oficial_V2_name',
            'clients.type_identity_oficial',
            'clients.file_document_home_status',
            'clients.file_document_home_name',
            'clients.file_document_contract_status',
            'clients.file_document_contract',
            'clients.complete_profile',
            'clients.city',
            'clients.is_active',
            'users.name',
            'users.first_last_name',
            'users.second_last_name',
            'users.email',
        )
        ->name($name)
        ->email($email)
        ->orderBy('clients.id', 'DESC')->paginate(20);
        
        return view('legal.clientes.perfiles', compact('clients'));
    }

    /**
     * Funcion para validar los documentos de perfiles de socio, asi como actualizar su estado de registro
     * url legal/socios/perfiles_store
     * method POST
     */
    public function perfilesClienteStore(Request $request){
    
        $client = Client::find($request->id);
        $existV2 = false;
        $client->file_document_home_status = $request->file_document_home_status;
        $client->file_document_identity_oficial_V2_status = $request->file_document_identity_oficial_V2_status;
        
        $client->file_document_identity_oficial_V1_status = $request->file_document_identity_oficial_V1_status;
        $client->file_document_contract_status = $request->file_document_contract_status;
        

        if($client->file_document_identity_oficial_V2_status == null){
            $existV2 = true;
        }

        if($client->file_document_home_status && $client->file_document_identity_oficial_V1_status && $client->file_document_contract_status){
            if($existV2){
                $client->complete_profile = '5';
                $title = User::where('id', $client->user_id);
                $body = 'Cliente verificado';
                $url = route('inicio');
                $this->broadcastMessageVerify($title,$body,$url);
                event(new VerifiedClientCreated($client));
            }
            else if(!$existV2 && $client->file_document_identity_oficial_V2_status){
                $client->complete_profile = '5';
                $title = User::where('id', $client->user_id);
                $body = 'Cliente verificado';
                $url = route('inicio');
                $this->broadcastMessageVerify($title,$body,$url);
                event(new VerifiedClientCreated($client));
            }
        }
        $client->save();

       

        return redirect()->back();
    }

    /**
     * Vista de nuevos perfiles de cliente (ultimos 7 dias)
     * Se encuenta en resources/views/legal/clientes/nuevos.blade.php
     * method GET
     * url legal/clientes/nuevos
     */
    public function clientesNuevos(Request $request) {
        $name = $request->get('name');
        $email = $request->get('email');

        $clients = Client::join('users','users.id','=','clients.user_id')
        ->where('users.created_at', '>=', now()->subDays(7))
        ->select(
            'clients.id',
            'clients.created_at',
            'clients.updated_at',
            'clients.birth_date_country',
            'clients.file_document_identity_oficial_V1_status',
            'clients.file_document_identity_oficial_V1_name',
            'clients.file_document_identity_oficial_V2_status',
            'clients.file_document_identity_oficial_V2_name',
            'clients.type_identity_oficial',
            'clients.file_document_home_status',
            'clients.file_document_home_name',
            'clients.file_document_contract_status',
            'clients.file_document_contract',
            'clients.complete_profile',
            'clients.city',
            'clients.is_active',
            'users.name',
            'users.first_last_name',
            'users.second_last_name',
            'users.email',
        )
        ->name($name)
        ->email($email)
        ->orderBy('clients.id', 'DESC')->paginate(20);
        
        return view('legal.clientes.nuevos', compact('clients'));
    }

    /**
     * Vista de perfiles de cliente no activados
     * Se encuenta en resources/views/legal/clientes/noActivados.blade.php
     * method GET
     * url legal/clientes/noActivados
     */
    public function clientesNoActivados(Request $request) {
        $name = $request->get('name');
        $email = $request->get('email');

        $clients = Client::join('users','users.id','=','clients.user_id')
        ->where('users.is_active', '0')
        ->select(
            'clients.id',
            'clients.created_at',
            'clients.updated_at',
            'clients.birth_date_country',
            'clients.file_document_identity_oficial_V1_status',
            'clients.file_document_identity_oficial_V1_name',
            'clients.file_document_identity_oficial_V2_status',
            'clients.file_document_identity_oficial_V2_name',
            'clients.type_identity_oficial',
            'clients.file_document_home_status',
            'clients.file_document_home_name',
            'clients.file_document_contract_status',
            'clients.file_document_contract',
            'clients.complete_profile',
            'clients.city',
            'clients.is_active',
            'users.name',
            'users.first_last_name',
            'users.second_last_name',
            'users.email',
        )
        ->name($name)
        ->email($email)
        ->orderBy('clients.id', 'DESC')->paginate(20);
        
        return view('legal.clientes.noActivados', compact('clients'));
    }

    /**
     * Vista de perfiles de cliente incompletos (con 1, 2 o 3 dsocumentos)
     * Se encuenta en resources/views/legal/clientes/incompletos.blade.php
     * method GET
     * url legal/clientes/incompletos
     */
    public function clientesIncompletos(Request $request) {
        $name = $request->get('name');
        $email = $request->get('email');

        $clients = Client::join('users','users.id','=','clients.user_id')
        ->where('clients.complete_profile', '<=', '4')
        ->select(
            'clients.id',
            'clients.created_at',
            'clients.updated_at',
            'clients.birth_date_country',
            'clients.file_document_identity_oficial_V1_status',
            'clients.file_document_identity_oficial_V1_name',
            'clients.file_document_identity_oficial_V2_status',
            'clients.file_document_identity_oficial_V2_name',
            'clients.type_identity_oficial',
            'clients.file_document_home_status',
            'clients.file_document_home_name',
            'clients.file_document_contract_status',
            'clients.file_document_contract',
            'clients.complete_profile',
            'clients.city',
            'clients.is_active',
            'users.name',
            'users.first_last_name',
            'users.second_last_name',
            'users.email',
        )
        ->name($name)
        ->email($email)
        ->orderBy('clients.id', 'DESC')->paginate(20);
        
        return view('legal.clientes.incompletos', compact('clients'));
    }

    /**
     * Vista de perfiles de cliente verificados
     * Se encuenta en resources/views/legal/clientes/verificados.blade.php
     * method GET
     * url legal/clientes/verificados
     */
    public function clientesVerificados(Request $request) {
        $name = $request->get('name');
        $email = $request->get('email');

        $clients = Client::join('users','users.id','=','clients.user_id')
        ->where('clients.complete_profile', '=', '5')
        ->select(
            'clients.id',
            'clients.created_at',
            'clients.updated_at',
            'clients.birth_date_country',
            'clients.file_document_identity_oficial_V1_status',
            'clients.file_document_identity_oficial_V1_name',
            'clients.file_document_identity_oficial_V2_status',
            'clients.file_document_identity_oficial_V2_name',
            'clients.type_identity_oficial',
            'clients.file_document_home_status',
            'clients.file_document_home_name',
            'clients.file_document_contract_status',
            'clients.file_document_contract',
            'clients.complete_profile',
            'clients.city',
            'clients.is_active',
            'users.name',
            'users.first_last_name',
            'users.second_last_name',
            'users.email',
        )
        ->name($name)
        ->email($email)
        ->orderBy('clients.id', 'DESC')->paginate(20);
        
        return view('legal.clientes.verificados', compact('clients'));
    }

    /**
     * Vista de datos de un cliente en especifico
     * Se encuenta en resources/views/legal/clientes/infocliente.blade.php
     * method GET
     * url legal/clientes/infoCliente
     */
    public function infoPerfilCliente($id) {

        $client = Client::find($id);  

        $contract = AccountClient::where('client_id',$id)
        ->select(
            'account_clients.date_contract_init',
            'account_clients.date_contract_fin'
            )
        ->first();

        $anualIncomes = AnualIncomes::all()->toArray();
        $publicOfficeEmployees=PublicOfficeEmployee::all()->toArray();
        $totalFundInvest = TotalFundInvest::all()->toArray();
        return view('legal.clientes.infoCliente', compact('client', 'contract', 'anualIncomes', 'publicOfficeEmployees', 'totalFundInvest'));
    }

    public function gainsCliente(Request $request) {
        $client = Client::where('id', $request->id)->first();
    
        $client->performance_fee = $request->performance_fee;
        $client->management_fee = $request->management_fee;
        $client->save();

        return redirect()->back();
    }

    /**
     * Vista de documentos almacenados en aws de un cliente en especifico
     * method GET
     * url /legal/detail/{idClient}/{nameImage}/{image}/{value}
     */
    public function detail($idClient,$nameImage,$image,$value) {
        //return $value;
        $cadena = $value == '1'?'':'_name';
        $hasname = Client::where('id',$idClient)
        ->where($image.$cadena,$nameImage)
        ->select($image." as image")
        ->first();
        $url = Storage::disk('s3')->temporaryUrl(
            $hasname->image, time() + (session_cache_expire() * 6)
        );
        return $url;
    }


    /************************************************************ */
    /** Funciones para Back office de Socios                       */
    /************************************************************ */

    /**
     * Vista de perfiles de socio (todos los registros)
     * Se encuenta en resources/views/legal/socios/perfiles.blade.php
     * method GET
     * url legal/socios/perfiles
     */
    public function perfilesSocio(Request $request) {
        $name = $request->get('name');
        $email = $request->get('email');

        $partners = Partner::join('users','users.id','=','partners.user_id')
        ->select(
            'partners.id',
            'partners.name_business',
            'partners.created_at',
            'partners.updated_at',
            'partners.birth_date_country',
            'partners.file_document_identity_oficial_V1_status',
            'partners.file_document_identity_oficial_V1_name',
            'partners.file_document_identity_oficial_V2_status',
            'partners.file_document_identity_oficial_V2_name',
            'partners.type_identity_oficial',
            'partners.file_document_home_status',
            'partners.file_document_home_name',
            'partners.file_document_contract_status',
            'partners.file_document_contract',
            'partners.complete_profile',
            'partners.city',
            'partners.is_active',
            'users.name',
            'users.first_last_name',
            'users.second_last_name',
            'users.email',
        )
        ->name($name)
        ->email($email)
        ->orderBy('partners.id', 'DESC')->paginate(20);
        return view('legal.socios.perfiles', compact('partners'));
    }
    /**
     * Funcion para validar los documentos de perfiles de socio, asi como actualizar su estado de registro
     * url legal/socios/perfiles_store
     * method POST
     */
    public function perfilesSocioStore(Request $request){
    
        $partner = Partner::find($request->id);
      
        //dd($request);
        $existV2 = false;
        $partner->file_document_home_status = $request->file_document_home_status;
        $partner->file_document_identity_oficial_V2_status = $request->file_document_identity_oficial_V2_status;
        
        $partner->file_document_identity_oficial_V1_status = $request->file_document_identity_oficial_V1_status;
        $partner->file_document_contract_status = $request->file_document_contract_status;
        
        if($partner->file_document_identity_oficial_V2_status == null){
            $existV2 = true;
        }

        if($partner->file_document_home_status && $partner->file_document_identity_oficial_V1_status && $partner->file_document_contract_status){
            if($existV2){
                $partner->complete_profile = '5';
                event(new VerifiedPartnerCreated($partner));
            }
            else if(!$existV2 && $partner->file_document_identity_oficial_V2_status){
                $partner->complete_profile = '5';
                event(new VerifiedPartnerCreated($partner));
            }
        }
        if($partner->save()) {
            $title = $partner->user_id;
            $body = $partner->user_id;
            $url = route('socio.inicio');
            $this->broadcastMessageVerify($title,$body,$url);
        }
        

    

        return redirect()->back();
    }

    private function broadcastMessageVerify($title=null, $body=null,$click_action) {
        $optionBuilder = new OptionsBuilder();
        $optionBuilder->setTimeToLive(60*20);

        $notificationBuilder = new PayloadNotificationBuilder('Has sido verificado');
        $notificationBuilder->setBody($body)
                            ->setSound('default')
                            ->setClickAction($click_action);

        
        $dataBuilder = new PayloadDataBuilder();
        $dataBuilder->addData([
            'title' => $title,
            'body' => $body
        ]);

        $option = $optionBuilder->build();
        $notification = $notificationBuilder->build();
     
        $data = $dataBuilder->build();
    
        // You must change it to get your tokens
        $tokens = User::where('id', $title)->pluck('fcm_token')->toArray();

      
        
        $downstreamResponse = FCM::sendTo($tokens, $option, $notification, $data);
      
        return $downstreamResponse->numberSuccess();

        $downstreamResponse->numberSuccess();
        $downstreamResponse->numberFailure();
        $downstreamResponse->numberModification();
        
        // return Array - you must remove all this tokens in your database
        $downstreamResponse->tokensToDelete();
        
        // return Array (key : oldToken, value : new token - you must change the token in your database)
        $downstreamResponse->tokensToModify();
        
        // return Array - you should try to resend the message to the tokens in the array
        $downstreamResponse->tokensToRetry();
        
        // return Array (key:token, value:error) - in production you should remove from your database the tokens present in this array
        $downstreamResponse->tokensWithError();
    }

    /**
     * Vista de nuevos perfiles de socio (ultimos 7 dias)
     * Se encuenta en resources/views/legal/socios/nuevos.blade.php
     * method GET
     * url legal/socios/nuevos
     */
    public function sociosNuevos(Request $request) {
        $name = $request->get('name');
        $email = $request->get('email');

        $partners = Partner::join('users','users.id','=','partners.user_id')
        ->where('users.created_at', '>=', now()->subDays(7))
        ->select(
            'partners.id',
            'partners.created_at',
            'partners.updated_at',
            'partners.name_business',
            'partners.city',
            'partners.is_active',
            'partners.birth_date_country',
            'partners.file_document_identity_oficial_V1_status',
            'partners.file_document_identity_oficial_V1_name',
            'partners.file_document_identity_oficial_V2_status',
            'partners.file_document_identity_oficial_V2_name',
            'partners.type_identity_oficial',
            'partners.file_document_home_status',
            'partners.file_document_home_name',
            'partners.file_document_contract_status',
            'partners.file_document_contract',
            'partners.complete_profile',
            'users.name',
            'users.first_last_name',
            'users.second_last_name',
            'users.email',
          
        )
        ->name($name)
        ->email($email)
        ->orderBy('partners.id', 'DESC')->paginate(20);
        
        return view('legal.socios.nuevos', compact('partners'));
    }

    /**
     * Vista de perfiles de socio no activados
     * Se encuenta en resources/views/legal/socios/noActivados.blade.php
     * method GET
     * url legal/socios/noActivados
     */
    public function sociosNoActivados(Request $request) {
        $name = $request->get('name');
        $email = $request->get('email');

        $partners = Partner::join('users','users.id','=','partners.user_id')
        ->where('users.is_active', '0')
        ->select(
            'partners.id',
            'partners.created_at',
            'partners.updated_at',
            'partners.birth_date_country',
            'partners.file_document_identity_oficial_V1_status',
            'partners.file_document_identity_oficial_V1_name',
            'partners.file_document_identity_oficial_V2_status',
            'partners.file_document_identity_oficial_V2_name',
            'partners.type_identity_oficial',
            'partners.file_document_home_status',
            'partners.file_document_home_name',
            'partners.file_document_contract_status',
            'partners.file_document_contract',
            'partners.complete_profile',
            'users.name',
            'users.first_last_name',
            'users.second_last_name',
            'users.email',
          
        )
        ->name($name)
        ->email($email)
        ->orderBy('partners.id', 'DESC')->paginate(20);
        
        return view('legal.socios.noActivados', compact('partners'));
    }

    /**
     * Vista de perfiles de socio incompletos (con 1, 2 o 3 documentos)
     * Se encuenta en resources/views/legal/socios/incompletos.blade.php
     * method GET
     * url legal/socios/incompletos
     */
    public function sociosIncompletos(Request $request) {
        $name = $request->get('name');
        $email = $request->get('email');

        $partners = Partner::join('users','users.id','=','partners.user_id')
        ->where('partners.complete_profile', '<=', '4')
        ->select(
            'partners.id',
            'partners.created_at',
            'partners.updated_at',
            'partners.birth_date_country',
            'partners.file_document_identity_oficial_V1_status',
            'partners.file_document_identity_oficial_V1_name',
            'partners.file_document_identity_oficial_V2_status',
            'partners.file_document_identity_oficial_V2_name',
            'partners.type_identity_oficial',
            'partners.file_document_home_status',
            'partners.file_document_home_name',
            'partners.file_document_contract_status',
            'partners.file_document_contract',
            'partners.complete_profile',
            'users.name',
            'users.first_last_name',
            'users.second_last_name',
            'users.email',
        
        )
        ->name($name)
        ->email($email)
        ->orderBy('partners.id', 'DESC')->paginate(20);
        
        return view('legal.socios.incompletos', compact('partners'));
    }

    /**
     * Vista de perfiles de socio verificados
     * Se encuenta en resources/views/legal/socios/verificados.blade.php
     * method GET
     * url legal/socios/verificados
     */
    public function sociosVerificados(Request $request) {
        $name = $request->get('name');
        $email = $request->get('email');

        $partners = Partner::join('users','users.id','=','partners.user_id')
        ->where('partners.complete_profile', '=', '5')
        ->select(
            'partners.id',
            'partners.created_at',
            'partners.updated_at',
            'partners.birth_date_country',
            'partners.file_document_identity_oficial_V1_status',
            'partners.file_document_identity_oficial_V1_name',
            'partners.file_document_identity_oficial_V2_status',
            'partners.file_document_identity_oficial_V2_name',
            'partners.type_identity_oficial',
            'partners.file_document_home_status',
            'partners.file_document_home_name',
            'partners.file_document_contract_status',
            'partners.file_document_contract',
            'partners.complete_profile',
            'users.name',
            'users.first_last_name',
            'users.second_last_name',
            'users.email',
        )
        ->name($name)
        ->email($email)
        ->orderBy('partners.id', 'DESC')->paginate(20);
        
        return view('legal.socios.verificados', compact('partners'));
    }

    /**
     * Vista de datos de un socio en especifico
     * Se encuenta en resources/views/legal/socios/infoSocio.blade.php
     * method GET
     * url legal/socios/infoSocio
     */
    public function infoPerfilSocio($id) {
        $user = Auth::user();
        $roles = $user->roles()->get();
        $partner = Partner::find($id);  
        $contract = AccountPartner::where('partner_id',$id)
        ->select(
            'accountpathers.date_contract_init',
            'accountpathers.date_contract_fin'
            )
        ->first();
 
        return view('legal.socios.infoSocio', compact('partner', 'contract','roles'));
    }


    public function gainsSocio(Request $request) {
        $partner = Partner::where('id', $request->id)->first();
    
        $partner->performance_fee = $request->performance_fee;
        $partner->management_fee = $request->management_fee;
        $partner->save();
        $accountPartner = AccountPartner::where('partner_id', $request->id)->first();
        $accountPartner->date_contract_init = $request->date_contract_init;
        $accountPartner->date_contract_fin = $request->date_contract_fin;

        $accountPartner->save();

        return redirect()->back();
    }

    /**
     * Vista de documentos almacenados en aws de un socio en especifico
     * method GET
     * url /legal/detailSocio/{idSocio}/{nameImage}/{image}/{value}
     */
    public function detailSocio($idSocio,$nameImage,$image,$value) {
        //return $value;
        $cadena = $value == '1'?'':'_name';
        $hasname = Partner::where('id',$idSocio)
        ->where($image.$cadena,$nameImage)
        ->select($image." as image")
        ->first();
        $url = Storage::disk('s3')->temporaryUrl(
            $hasname->image, time() + (session_cache_expire() * 6)
        );
        return $url;
    }

}
