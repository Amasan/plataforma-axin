<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth; 
use App\Models\Client;
use Carbon\Carbon;
use App\Models\AccountClient;
use App\Models\Accumulated;
use App\Models\Commision;
use App\Models\ClientWithDrawalRequest;
class inicioController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
        
    }
    /**
     * vista principal
     * get /home/
    */
    Public function index() {
        $this->shareCompact();
        $user = Auth::user();
        $client = Client::where('user_id',$user->id)->first();
        $accounts = AccountClient::where('client_id',$client->id)
        ->get();
        $accountA = AccountClient::where('type_account_id',1)
        ->where('client_id',$client->id)
        ->first();
        $accountI = AccountClient::where('type_account_id',2)
        ->where('client_id',$client->id)
        ->first();
        $accountM = AccountClient::where('type_account_id',3)
        ->where('client_id',$client->id)
        ->first();
        return view('cliente.inicio',compact('accounts', 'accountA', 'accountI', 'accountM'));
    }
    /**
     * transferencia de cuentas
     * post /updateTranslate/
    */
    public function updateTranslate(Request $request){
        $user = Auth::user();
        $input = $request->all();
        $client = Client::where('user_id',$user->id)->first();
        $amount = $input['amount_money'];
        $accountClient1 = AccountClient::where('client_id',$client->id)
        ->where('type_account_id',$input['type_account_id1'])->first();
        
        $accountClient2 = AccountClient::where('client_id',$client->id)
        ->where('is_commision','0')
        ->where('type_account_id',$input['type_account_id2'])->first();
        if($accountClient1->type_account_id != 1){
            return response()->json(['status'=>300,'message'=>'No es posible realizar una transferencia de '.$accountClient1->typeaccount->name.' a '. $accountClient2->typeaccount->name]); 
        }
        if($accountClient1->id == $accountClient2->id){
            return response()->json(['status'=>300,'message'=>'Las cuentas son iguales, debe seleccionar cuentas distintas']); 
        }
        $gananciaAcumulada = Accumulated::where('client_id',$client->id)
        ->where('is_commision','0')
        ->where('account_client_id',$accountClient1->id)
        ->sum('amount');
        $saldo = $gananciaAcumulada - $accountClient1->withdrawals;
        if($saldo <= 0 || $saldo < $amount){
            return response()->json(['status'=>300,'message'=>'Tu saldo en la cuenta '.$accountClient1->typeaccount->name . ' es insuciente']); 
        }
        $accountClient1->withdrawals = $accountClient1->withdrawals + $amount;
        $accountClient2->amount_money = $accountClient2->amount_money + $amount;

        $accountClient1->save();
        $accountClient2->save();
        return response()->json(['status'=>200,'message'=>'Has transferido $'.$amount.' USD de '.$accountClient1->typeaccount->name.' a '.$accountClient2->typeaccount->name]); 
    }
    //vista de soporte tecnico
    public function soporte() {
        $this->shareCompact();
        return view('cliente.soporte');
    }
    /**
     * estadisticas en base a una cuenta X
     * get /stadistics/{id}/
    */
    public function estadisticsHomeAccount($account_id){
        $user = Auth::user();
        $client = Client::where('user_id',$user->id)->first();
        $accountClient = AccountClient::where('client_id',$client->id)
        ->where('type_account_id',$account_id)->first();
        $gananciaAcumulada = Accumulated::where('client_id',$client->id)
        ->where('is_commision','0')
        ->where('account_client_id',$accountClient->id)
        ->sum('amount');
        $gananciasIndividuales = Accumulated::where('client_id',$client->id)
        ->where('account_client_id',$accountClient->id)->OrderBy('updated_at', 'DESC')->get()->first();
        
        //calcular ultima actualizacion
        $difference = '';
        $now = Carbon::now();
        $created = new Carbon($accountClient->updated_at);
        if($gananciasIndividuales != null){
            $created = new Carbon($gananciasIndividuales->updated_at);
            //$created->addHour(); 4 veces por zona
        }
        $difference = $created->diffForHumans($now);
        $difference = str_replace("antes","",$difference);
        
        $acumulates = Accumulated::where('client_id',$client->id)
        ->where('is_commision','0')
        ->where('account_client_id',$accountClient->id)
        ->OrderBy('date', 'ASC')
        ->select('amount','date as updated_at')
        ->get()
        ->toArray();
        $comisions = Commision::where('account_client_id',$accountClient->id)
        ->sum('commissions');

        $ganancia_indivual = $gananciaAcumulada - $comisions;
        $porcentaje_acumulado = $gananciaAcumulada - $comisions;
        $porcentaje_acumulado = $porcentaje_acumulado == 0?0: (($porcentaje_acumulado/$accountClient->amount_money)*100);
        $saldo_acumulado = ($accountClient->amount_money + ($ganancia_indivual-$accountClient->withdrawals));
        $data['initial_investment'] = round($accountClient->amount_money, 2);
        $data['gananciaAcumulada'] = round($ganancia_indivual, 2);
        $data['porcentaje'] = round($porcentaje_acumulado, 2);
        $data['saldo'] = round($saldo_acumulado, 2);
        $data['retiros'] =  $accountClient->withdrawals;
        $data['actualizacion'] = $difference==''?'':"Hace ".$difference;
        $data['grafico'] = $acumulates;
        $data['comisiones'] = round($comisions, 2);
        $data['inicioContrato'] = $accountClient->date_contract_init??'N/A';
        $data['finContrato'] = $accountClient->date_contract_fin??'N/A';
        $data['performance_fee'] = round($client->performance_fee, 2);
        $data['management_fee'] = round($client->management_fee, 2);
        return $data;
    }
    
    private function shareCompact(){
        $user = Auth::user();
        $client = Client::where('user_id',$user->id)->first();
        $data = $client->complete_profile;
        view()->share('complete_profile', ($data*2)==0?1:($data*2));
    }
}
