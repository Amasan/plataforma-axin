<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\User; 
use Illuminate\Support\Facades\Auth; 
class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }
    /**
     * verfificacion de tipo de cuentas
    */
    public function authtentication(){
        $user = Auth::user();
        $role = $user->roles()->first();
        $direction = "";
        switch ($role->id) {
            case 1:
                $direction = "legal/clientes/perfiles";//admin
                break;
            case 2:
                $direction = "legal/clientes/perfiles";
                break;
            case 3:
                $direction = "legal/clientes/perfiles";
                break;
            case 4:
                $direction = "personal";
                break;
            default:
                $direction = "socio";
        }
        return redirect()->to('/'.$direction);
    }
}
