<?php

namespace App\Http\Controllers\Socio;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth; 
use App\User;
use App\Models\Partner;
use Barryvdh\DomPDF\Facade as PDF;
use Carbon\Carbon;
use Mail;
use Illuminate\Support\Facades\Storage;
use App\Http\Requests\SocioPersonalDatosEmpresa;
use App\Http\Controllers\Controller;
use App\Http\Controllers\configuracionController;
use App\Http\Requests\ClientFilesRequest;
use \stdClass;
use Exception;
class perfilController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }
    private function shareCompact(){
        $user = Auth::user();
        $partner = Partner::where('user_id',$user->id)->first();
        $data = $partner->complete_profile;
        view()->share('complete_profile', ($data*2)==0?1:($data*2));
    }
    /** 
     * identificado el estado de llenado 
     * del formulario para reubicar en la vista correcta
     * /socio 
     * method GET
     */
    public function researchPersonal(){
        $user = Auth::user();
        $partner = Partner::where('user_id',$user->id)->first();
        $direction="";
        switch ($partner->complete_profile) {
            case 0:
                $direction = "/registro_datos_empresa";
                break;
            case 1:
                $direction = "/registro_representante_legal";
                break;
            case 2:
                $direction = "/registro_documentos_representante";
                break;
            case 3:
                $direction = "/registro_firma_contrato";
                break;    
            default:
                $direction = "/inicio";
        }
        return redirect()->to('/socio'.$direction);
    }

    /**
     * llenado de formulario de registro datos de la empresa
     * Se encuenta en resources/views/socio/datos_empresa.blade.php
     * method GET
     * url /socio/registro_datos_empresa
     */
    Public function datosEmpresaForm() {
         
        $user = Auth::user();
        $partner = Partner::where('user_id',$user->id)->first();
        $name_business = $partner->name_business;
        if(!$partner->is_active){
            return redirect('socio/inicio');
        }
        
        if($partner->complete_profile != '0'){
            return redirect('/socio');
        }
        
        $this->shareCompact();
        
        return view('socio.datos_empresa')
                ->with('name_business',$name_business);
    }
    /**
     * Funcion para guardar los datos de la empresa
     * url /socio/registro_datos_empresa/store
     * method POST
     */
    public function DatosEmpresaStore(SocioPersonalDatosEmpresa $request){
        $input = $request->all(); 
        $user = Auth::user();
        
        $partner = Partner::where('user_id',$user->id)->first();
        $partner->rfc_business = strtoupper($input['rfc_business']);
        $partner->name_business = strtoupper($input['name_business']);
        $partner->code_postal_business = $input['code_postal_business'];
        $partner->country_business = strtoupper($input['country_business']);
        $partner->city_business = strtoupper($input['city_business']);
        $partner->code_postal_business = $input['code_postal_business'];
        $partner->direction_company = strtoupper($input['direction_company']);

        if(!$partner->is_active){
            return redirect('/socio/inicio');
        }
        if($partner->complete_profile != 0){
            return redirect('/socio');
        }
        $partner->complete_profile = 1;
        $partner->save();
        return redirect('socio/registro_representante_legal');
    }
    /**
     * llenado de formulario de registro representante legal
     * Se encuenta en resources/views/socio/representante_legal.blade.php
     * method GET
     * url /socio/registro_representante_legal
     */
    Public function DatosRepresentanteLegalForm() {
         
        $user = Auth::user();
        $partner = partner::where('user_id',$user->id)->first();
        
        if(!$partner->is_active){
            return redirect('/socio/inicio');
        }
       
        if($partner->complete_profile != 1){
            return redirect('/socio');
        }
        
        $this->shareCompact();
        return view('socio.representante_legal');
    }
     /**
     * Funcion para guardar los datos de representante legal
     * url /socio/registro_representante_legal/store
     * method POST
     */
    public function DatosRepresentanteLegalStore(Request $request){
        $input = $request->all();
        $user = Auth::user();
        $user->name = strtoupper($input['name']);
        $user->first_last_name = strtoupper($input['first_last_name']);
        $user->second_last_name = strtoupper($input['second_last_name']);
        $user->direction = strtoupper($input['direction']);
        $partner = Partner::where('user_id',$user->id)->first();
        $partner->birth_date = $input['birth_date'];
        $partner->city = strtoupper($input['city']);
        $partner->code_postal = $input['code_postal'];
        $partner->birth_date_country = strtoupper($input['birth_date_country']);
        $partner->number_phone = $input['number_phone'];
        $partner->country_code = $input['country_code'];
        if(!$partner->is_active){
            return redirect('/socio/inicio');
        }
        if($partner->complete_profile != 1){
            return redirect('/socio');
        }
        $partner->complete_profile = 2;
        $user->save();
        $partner->save();
        return redirect('/socio/registro_documentos_representante');
    }

    /**
     * vista de cargado de documentos representante legal
     * Se encuenta en resources/views/socio/documentos_representante.blade.php
     * method GET
     * url /socio/registro_documentos_representante
    */
    Public function DocumentosRepresentanteForm() {
        $user = Auth::user();
        $partner = Partner::where('user_id',$user->id)->first();
        if(!$partner->is_active){
            return redirect('/socio/inicio');
        }
        if($partner->complete_profile != 2){
            return redirect('/socio');
        }
        $this->shareCompact();
        return view('socio.documentos_representante');
    }

    /**
     * Funcion para guardar los documentos del representante legal
     * url /socio/registro_documentos_representante/store
     * method POST
    */
    Public function DocumentosRepresentanteLegalStore(ClientFilesRequest $request) {
        $input = $request->all();
        $user = Auth::user();

        $parther = Partner::where('user_id',$user->id)->first();
        if(!$parther->is_active){
            return redirect('/socio/inicio');
        }

        if($parther->complete_profile != 2){
            return redirect('/socio');
        }

        $config = new configuracionController;
        $type_identity_oficial = $input['type_identity_oficial'];
        $parther->type_identity_oficial = $type_identity_oficial=='0'?'IFE/INE':'Pasaporte';
        $fileV1 = $input['file_document_identity_oficial_V1'];
        $resultV1 = $config->validateFile($fileV1,true);
        $resultV2 = new stdClass;
        $resultV2->status = 200;
        $resultV2->message = "";

        $fileV2 = $input['file_document_identity_oficial_V2']??'';
        if($type_identity_oficial=="0"){
            $fileV2 = $input['file_document_identity_oficial_V2'];
            $resultV2 = $config->validateFile($fileV2,true);
        }

        $file_doc = $input['file_document_home'];
        $resultdoc = $config->validateFile($file_doc,false);

        if($resultV1->status==300 ||
                $resultV2->status==300 ||
                    $resultdoc->status==300){
            return redirect()->back()
            ->withInput()
            ->withErrors(['file_document_identity_oficial_V1' => $resultV1->message,
                'file_document_identity_oficial_V2' => $resultV2->message,
                    'file_document_home' => $resultdoc->message]);
        }
        Storage::disk('s3')->put('', $fileV1);
        $parther->file_document_identity_oficial_V1 = $fileV1->hashName();
        $parther->file_document_identity_oficial_V1_name = $fileV1->getClientOriginalName();

        if($type_identity_oficial=="0"){
            Storage::disk('s3')->put('', $fileV2);
            $parther->file_document_identity_oficial_V2 = $fileV2->hashName();
            $parther->file_document_identity_oficial_V2_name = $fileV2->getClientOriginalName();
        }
        
        Storage::disk('s3')->put('', $file_doc);
        $parther->file_document_home = $file_doc->hashName();
        $parther->file_document_home_name = $file_doc->getClientOriginalName();

        $parther->complete_profile = 3;
        $parther->save();
        return redirect('/socio');
    }
    /**
     * vista de firma de contrato
     * Se encuenta en resources/views/socio/firma_contrato.blade.php
     * method GET
     * url /socio/registro_firma_contrato
    */

    Public function FirmarContratoForm() {
        $user = Auth::user();
        $partner = Partner::where('user_id',$user->id)->first();
        if(!$partner->is_active){
            return redirect('/socio/inicio');
        }
        if($partner->complete_profile != 3){
            return redirect('/socio');
        }
        $fullname = $user->name ." ". $user->first_last_name ." ". $user->second_last_name;
        $this->shareCompact();
        return view('socio.firma_contrato')
            ->with('fullname',$fullname);
    }
    private function data(){
        return response()->json('status', 200);
    }
    /**
     * Funcion para guardar las firma del socio y generar el contrato 
     * url /socio/registro_firma_contrato/store
     * method POST
    */

    public function FirmaContratoStore(Request $request){
        $input = $request->all();
        $user = Auth::user();
        
        $pdf = null;
        $date = Carbon::now()->toDateTimeString();
        $partner = Partner::where('user_id',$user->id)->first();
        
        if(!$partner->is_active){
            return redirect('/socio/inicio');
        }
        if($partner->complete_profile != 3){
            return redirect('/socio');
        }
        $partner->complete_profile = 4;

        $file = $input['signature']; 
        Storage::disk('s3')->put('', $file);
        $partner->signature = $file->hashName();
        //$partner->save();

        $image = $partner->signature??'';
        $nombre = $user->name.' '.$user->first_last_name.' '.$user->second_last_name;
        $url_firm = Storage::disk('s3')->temporaryUrl(
            $image, time() + (session_cache_expire() * 6)
            );
        $pdf = PDF::loadView('socio.contrato',
        compact('date','nombre','url_firm'))->output();
        
        Storage::disk('s3')->put($nombre.'.pdf',$pdf);

        $url = Storage::disk('s3')->temporaryUrl(
            $nombre.'.pdf', time() + (session_cache_expire() * 6)
        );
        $partner->file_document_contract = $nombre.'.pdf';
        $partner->save();
        $subject = "Firma de Contrato";
        $for = $user->email;
        $file_to_attach = $url;
        try { 
            Mail::send('emails.contrato',['url' => 'https://amaliath3code.com/ver_ContratoPdf'], function($msj) use($subject,$for,$file_to_attach){
            $msj->from(ENV('MAIL_USERNAME'),"Firma de contrato AXIN");
            $msj->subject($subject);
            $msj->to($for);
            $msj->attach($file_to_attach, [
                    'as' => 'contrato.pdf',
                    'mime' => 'application/pdf',
                ]);
            });
        }
        catch (Exception $e) {
            return $url;
        }
        
        return $url;
    }
    
    /**
     * Editar datos de actividad economica
     * get /socio/editar_documentos
     */
    public function editCargard() {
        $user = Auth::user();
        $partner = Partner::where('user_id',$user->id)->first();
        if(!$partner->is_active){
            return redirect('/socio');
        }
        if($partner->complete_profile < 4){
            return redirect('/socio');
        }
        $type_identity_oficial = ['IFE/INE', 'Pasaporte'];
        $this->shareCompact();
        return view('socio.editar_cargar_documentos', compact('partner', 'user','type_identity_oficial'));
    }
    /**
     * Editar datos de actividad empresarial
     * get /socio/editar_empresa
     */
    public function editDatosEmpresa() {
        $user = Auth::user();
        $partner = Partner::where('user_id',$user->id)->first();
        if(!$partner->is_active){
            return redirect('/socio');
        }
        if($partner->complete_profile < 4){
            return redirect('/socio');
        }
        $this->shareCompact();
        return view('socio.editar_datos_empresa', compact('partner', 'user'));
    }
    
    /** 
     * seccion registro - cargar documentos de datos personales 
     * 
     * method POST personal/update_cargard
     * 
     */
    Public function updateFilesClient(Request $request) {
        $input = $request->all();
        $user = Auth::user();
        $partner = Partner::where('user_id',$user->id)->first();
        $resultV1 = $this->data();
        $type_identity_oficial = $input['type_identity_oficial'];
        $partner->type_identity_oficial = $type_identity_oficial=='0'?'IFE/INE':'Pasaporte';
        $config = new configuracionController;
        if(!$partner->is_active){
            return redirect('/inicio');
        }

        $fileV1 = $input['file_document_identity_oficial_V1']??'undefined';
        $resultV1->status = 200;
        
        if($fileV1 != 'undefined'){
            if($type_identity_oficial=="0"){
                $resultV1 = $config->validateFile($fileV1,true);
            }
            
        }
        
        $resultV2 = $this->data();
        $resultV2->status = 200;
        $fileV2 = $input['file_document_identity_oficial_V2']??'undefined';
        if($fileV2 != 'undefined'){
            $resultV2 = $config->validateFile($fileV2,true);
        }
        
        $resultdoc= $this->data();
        $resultdoc->status = 200;
        $file_doc = $input['file_document_home']??'undefined';
        if($file_doc != 'undefined'){
            $resultdoc = $config->validateFile($file_doc,false);
        }
        
        if(($resultV1->status==300) ||
                $resultV2->status==300 ||
                    $resultdoc->status==300){
            return redirect()->back()
            ->withInput()
            ->withErrors(['file_document_identity_oficial_V1' => $resultV1->message,
                'file_document_identity_oficial_V2' => $resultV2->message,
                    'file_document_home' => $resultdoc->message]);
        }
        if($fileV1 != 'undefined'){
            Storage::disk('s3')->put('', $fileV1);
            $partner->file_document_identity_oficial_V1 = $fileV1->hashName();
            $partner->file_document_identity_oficial_V1_status = '';
            $partner->file_document_identity_oficial_V1_name = $fileV1->getClientOriginalName();

        }
        if($fileV2 != 'undefined'){
            if($type_identity_oficial=="0"){
                Storage::disk('s3')->put('', $fileV2);
                $partner->file_document_identity_oficial_V2 = $fileV2->hashName();
                $partner->file_document_identity_oficial_V2_status = '';
                $partner->file_document_identity_oficial_V2_name = $fileV2->getClientOriginalName();
            }
        
        }
        if($file_doc != 'undefined'){
            Storage::disk('s3')->put('', $file_doc);
            $partner->file_document_home = $file_doc->hashName();
            $partner->file_document_home_status = 'NULL';
            $partner->file_document_home_name = $file_doc->getClientOriginalName();
        }

        $partner->save();
        return redirect('/socio/editar_documentos');

    }

}
