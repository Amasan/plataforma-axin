<?php

namespace App\Http\Controllers\Socio;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

use Illuminate\Support\Facades\Auth; 
use App\Models\Partner;
use Carbon\Carbon;
use App\Models\AccountPartner;
use App\Models\Accumulated;
use App\Models\Commision;
use App\Models\partnerWithDrawalRequest;
class InicioController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
        
    }
    /**
     * vista de inicio de socio
     * Se encuenta en resources/views/socio/inicio.blade.php
     * method GET
     * url /socio/inicio
    */
    public function index(){
        return redirect()->to('/socio/inicio');
    }
    Public function inicioSocio() {
        $this->shareCompact();
        $user = Auth::user();
        $partner = Partner::where('user_id',$user->id)->first();
        $accounts = AccountPartner::join('type_accounts', 'accountpathers.type_account_id', '=', 'type_accounts.id')
        ->where('partner_id',$partner->id)
        ->select(
            'accountpathers.id',
            'type_accounts.id as idaccount',
            'accountpathers.partner_id',
            'accountpathers.is_active',
            'accountpathers.account_number',
            'accountpathers.amount_money',
            'type_accounts.name as nameaccount')
        ->get();
        return view('socio.inicio',compact('accounts'));
    }

    /**
     * estadisticas en base a una cuenta X
     * get /stadistics/{id}/
    */
    public function estadisticsHomeAccount($account_id){
        $user = Auth::user();
        $partner = Partner::where('user_id',$user->id)->first();
        $accountPartner = AccountPartner::where('partner_id',$partner->id)
        ->where('type_account_id',$account_id)->first();
        $gananciaAcumulada = Accumulated::where('partner_id',$partner->id)
        ->where('is_commision','0')
        ->where('account_partner_id',$accountPartner->id)
        ->sum('amount');
        $gananciasIndividuales = Accumulated::where('partner_id',$partner->id)
        ->where('account_partner_id',$accountPartner->id)->OrderBy('updated_at', 'DESC')->get()->first();
        
        //calcular ultima actualizacion
        $difference = '';
        $now = Carbon::now();
        $created = new Carbon($accountPartner->updated_at);
        if($gananciasIndividuales != null){
            $created = new Carbon($gananciasIndividuales->updated_at);
            //$created->addHour(); 4 veces por zona
        }
        $difference = $created->diffForHumans($now);
        $difference = str_replace("antes","",$difference);
        
        $acumulates = Accumulated::where('partner_id',$partner->id)
        ->where('account_partner_id',$accountPartner->id)
        ->where('is_commision','0')
        ->OrderBy('date', 'ASC')
        ->select('amount','date as updated_at')
        ->get()
        ->toArray();

        $commisions = Accumulated::where('partner_id',$partner->id)
        ->where('account_partner_id',$accountPartner->id)
        ->where('is_commision','1')
        ->sum('amount');
        $commisions = (-1)*$commisions;
        $ganancia_indivual = $gananciaAcumulada - $commisions;
        $porcentaje_acumulado = $gananciaAcumulada - $commisions;
        $porcentaje_acumulado = $porcentaje_acumulado == 0?0: (($porcentaje_acumulado/$accountPartner->amount_money)*100);
        $saldo_acumulado =  ($accountPartner->amount_money + $ganancia_indivual) - $accountPartner->withdrawals;
        $data['initial_investment'] = round($accountPartner->amount_money, 2);
        $data['gananciaAcumulada'] = round($ganancia_indivual, 2);
        $data['porcentaje'] = round($porcentaje_acumulado, 2);
        $data['saldo'] = round($saldo_acumulado, 2);
        $data['retiros'] =  $accountPartner->withdrawals;
        $data['actualizacion'] = $difference==''?'':"Hace ".$difference;
        $data['grafico'] = $acumulates;
        $data['comisiones'] = round($commisions, 2);//change
        $data['inicioContrato'] = $accountPartner->date_contract_init??'N/A';
        $data['finContrato'] = $accountPartner->date_contract_fin??'N/A';
        $data['performance_fee'] = round($partner->performance_fee, 2);
        $data['management_fee'] = round($partner->management_fee, 2);
        return $data;
    }

    private function shareCompact(){
        $user = Auth::user();
        $partner = Partner::where('user_id',$user->id)->first();
        $data = $partner->complete_profile;
        view()->share('complete_profile', ($data*2)==0?1:($data*2));
    }
}
