<?php

namespace App\Http\Controllers\API;

use App\Http\Controllers\Controller;
use Mail;
use Illuminate\Http\Request;


class ContactController extends Controller
{
    public function contact(Request $request){
        $subject = "Informacion Empresa Axin";
        
        $data = array(
            'name' => $request->name,
            'lastname' => $request->lastname,
            'business' => $request->business,
            'email' => $request->email,
            'country' => $request->country,
            'phone' => $request->phone,
            'profile' => $request->profile,
        );

        Mail::send('emails.contact',$data, function($msj) use($subject,$data){
            $msj->from(ENV('MAIL_USERNAME'), 'Informacion Socio');
            $msj->subject($subject);
            $msj->to(ENV('MAIL_USERNAME'));
        });
    }
}
