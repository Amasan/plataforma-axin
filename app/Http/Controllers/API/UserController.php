<?php

namespace App\Http\Controllers\API;
use Illuminate\Http\Request;
use App\Http\Requests\RegisterRequest;
use App\Http\Requests\RegisterSocioRequest;
use App\Http\Controllers\Auth\LoginController;

use Illuminate\Support\Facades\Auth; 
use Illuminate\Support\Str;
use Illuminate\Support\Facades\DB;

use App\Http\Controllers\Controller;
use App\Notifications\SignupActivate; 

use Validator;
use Session;

use App\User;
use App\Models\Client;
use App\Models\Partner;
use App\Models\AccountClient;
use App\Models\AccountPartner;
use App\Models\Setting;
class UserController extends Controller
{
    public $successStatus = 200;
     /** 
     * login de cuenta
     * api/login 
     * method POST
     * retorna token de acceso
     */ 
    public function login(Request $request){
            
        $credentials = $request->only('email', 'password');

        if (!Auth::attempt($credentials)) {
            return response()->json(['error'=>'Email o Contraseña incorrecta'], 401); 
        }
        $user = Auth::user();
        if($user==null){
            return response()->json(['error'=>'La cuenta no existe'], 401); 
        }
            
        if(!$user->is_active){
            return response()->json(['error'=>'La cuenta esta inactiva'], 401); 
        }
        $success['access_token'] = $user->createToken('token')-> accessToken;
        $user->activation_token = base64_encode(Str::random(250) . $user->email);//md5(Str::random(150) . $user->email);//Str::random(150);
        $user->save();
        return response()
            ->json(['mensaje'=>"OK",
                    'estado'=>200,
                    'token_access'=>$success,
                    'token_login'=>$user->activation_token,
                    'email'=>$user->email
                    ], 200); 
    }
    public function loginConfirm($token){
        Auth::logout();
        if (Auth::check()) {
            $user = Auth::user();
            Auth::logout($user);
        }
        $user = User::where('activation_token', $token)->first();
        if (!$user) {
            return redirect('/page/not/found');
        }
        $user->activation_token = '';
        $user->save();
        Auth::login($user, true);
        return redirect('/');
    }
    private function getvalue($key,$array){
        for($x = 0; $x <count($array); $x++) {
            if($array[$x]['key'] === $key){
                return $array[$x]['value'];
            }
        }
        return null;
    }
    /** 
     * registro de cuenta cliente
     * api/register 
     * method POST
     * envia correo de confirmacion
     */
    public function register(RegisterRequest $request) 
    { 
        $input = $request->all(); 
        $input['password'] = bcrypt($input['password']);//encrypt($input['password']);
        $user = User::create($input);
        $user = User::find($user->id);
        $user->roles()->sync([4]);
        $user->fcm_token = null;
        $user->is_active = false;
        $user->activation_token = Str::random(60);
        $user->save();
        $client = Client::create(['user_id' => $user->id,'complete_profile'=>0]);
        $settings = Setting::where('is_payment','0')->get()->toArray();
        $generate_account = 0;
        $id_setting = 0;
        //$generate_account = (int) $this->getvalue('generate_account',$settings);
        for($x = 0; $x <count($settings); $x++) {
            if($settings[$x]['key'] === 'generate_account_client'){
                $generate_account = (int) $settings[$x]['value'];
                $id_setting = $settings[$x]['id'];
            }
        }
        $setting = Setting::where('id', $id_setting)
            ->update(['value' => ($generate_account+3)]);
        $accountClient1 = AccountClient::create([
            'client_id'=>$client->id,
            'type_account_id'=>1,
            'account_number'=>$generate_account+1,
            'amount_money'=>0,
            'is_active'=>true
        ]);
        
        $accountClient2 = AccountClient::create([
            'client_id'=>$client->id,
            'type_account_id'=>2,
            'account_number'=>$generate_account+2,
            'amount_money'=>0,
            'is_active'=>false
        ]);
        $accountClient3 = AccountClient::create([
            'client_id'=>$client->id,
            'type_account_id'=>3,
            'account_number'=>$generate_account+3,
            'amount_money'=>0,
            'is_active'=>false
        ]);
        $accountClient1->save();
        $accountClient2->save();
        $accountClient3->save();
        $success['access_token'] = $user->createToken('token')->accessToken;
        $user->notify(new SignupActivate($user));
        return response()->json(['success'=>$success]); 
    }
    /** 
     * registro de cuenta Socio
     * api/register 
     * method POST
     * envia correo de confirmacion
     */
    public function registerSocio(RegisterSocioRequest $request) 
    { 
        $input = $request->all(); 
        $input['password'] = bcrypt($input['password']);//encrypt($input['password']);
        $user = User::create($input);
        $user = User::find($user->id);
        $user->roles()->sync([5]);
        $user->is_active = false;
        $user->activation_token = Str::random(60);
        $user->save();
        $partner = Partner::create(['user_id' => $user->id,'complete_profile'=>0]);
        $partner->name_business = strtoupper($input['name_business']);
        $partner->save();
        $settings = Setting::where('is_payment','0')->get()->toArray();
        $generate_account = 0;
        $id_setting = 0;
        for($x = 0; $x <count($settings); $x++) {
            if($settings[$x]['key'] === 'generate_account_socio'){
                $generate_account = (int) $settings[$x]['value'];
                $id_setting = $settings[$x]['id'];
            }
        }
        $setting = Setting::where('id', $id_setting)
            ->update(['value' => ($generate_account+50)]);
        $accountPartner1 = AccountPartner::create([
            'partner_id'=>$partner->id,
            'type_account_id'=>'4',
            'account_number'=>$generate_account+1,
            'amount_money'=>0,
            'is_active'=>true
        ]);
        $accountPartner1->type_account_id = '4';
        $accountPartner1->save();
        $success['access_token'] = $user->createToken('token')->accessToken;
        $user->notify(new SignupActivate($user));
        return response()->json(['success'=>$success]); 
    }
    /** 
     * activacion de cuenta
     * api/auth/signup/activate/{token}
     * method GET
     * confirmacion de cuenta
     */ 
    public function signupActivate($token)
    {
        $user = User::where('activation_token', $token)->first();
        if (!$user) {
            return response()->json(['error'=>'Activacion invalida'], 404);
        }
        $user->is_active = true;
        $user->activation_token = '';
        $user->save();
        $email = $this->binary($user->email); 
        $controller = new LoginController;
        return redirect()->action('Auth\LoginController@showLoginForm',['email' => $email]);
        
    }
    /** 
     * api/details 
     * method GET
     * header token de acceso unico
     * demo
     */ 
    public function details() 
    { 
        $user = Auth::user();
        return response()->json(['success' => $user], $this-> successStatus); 
    } 
    /** 
     * api/detail 
     * method GET
     * acceso general
     * demo
     */ 
    public function detail() 
    { 
        $edito = "edito";
        return response()->json(['success' => $edito], $this-> successStatus); 
    } 
    /** 
     * api/logout 
     * method GET
     * header token de acceso unico
     * cerrar sesion
     */ 
    public function logout(Request $request)
    {
        //$user = Auth::user();
        //setcookie('token', '', time() + (86400), "/");
        $request->user()->token()->revoke();
        return response()->json(['message' => 
            'Successfully logged out']);
    }
    private function binary($str){
	
        if(!empty($str)){
        
            $binario = '';
            $str = trim($str);
            $str_arr = str_split($str, 4);
            for($i = 0; $i<count($str_arr); $i++)
                $binario = $binario.str_pad(decbin(hexdec(bin2hex($str_arr[$i]))), strlen($str_arr[$i])*8, "0", STR_PAD_LEFT);  
            return $binario;
            
        }
		
    }
}
