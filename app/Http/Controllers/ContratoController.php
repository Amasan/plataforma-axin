<?php

namespace App\Http\Controllers;

use Barryvdh\DomPDF\Facade as PDF;
use Carbon\Carbon;
use App\Models\Client;
use Illuminate\Support\Facades\Auth; 
use Illuminate\Http\Request;
use App\models\Partner;
use Illuminate\Support\Facades\Storage;
class ContratoController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }
    /**
     * vista de ver contrato por default
     * get /ver_contrato/
    */
    public function verContrato() {
        $date = Carbon::now();
        $user = Auth::user();
        $nombre = $user->name.' '.$user->first_last_name.' '.$user->second_last_name;
        $url_firm = null;
        $pdf = PDF::loadView('cliente.contrato', compact('date','nombre', 'url_firm'));
        return $pdf->stream();
      
    }

    public function ContratoPdf() {

       
        $user = Auth::user();
        $profile = 0;
        if($user->roles()->first()->id == '5') {
            
            $partner = Partner::where('user_id', $user->id)->first();
            $profile = $partner->complete_profile;
           
        } else {
            $client = Client::where('user_id', $user->id)->first();
            $profile = $client->complete_profile;
        }
       
        if ($profile <= 3) {
            return redirect('/');
    
        } 

        $nombre = $user->name.' '.$user->first_last_name.' '.$user->second_last_name;
        $url = Storage::disk('s3')->temporaryUrl(
            $nombre.'.pdf', time() + (session_cache_expire() * 6)
        );

        return view('contrato', compact('url'));


    }

}
