<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use App\Providers\RouteServiceProvider;
use Illuminate\Foundation\Auth\AuthenticatesUsers;
use Illuminate\Http\Request;
use App\User; 
use Illuminate\Support\Facades\Auth;
class LoginController extends Controller
{
    public function __construct()
    {
        view()->share('complete_profile', '1');
    }
    
    public function showLoginForm(Request $request){
        return view('auth.login')->with('email',$request->email);
    }

    public function login_back(Request $request){
        $email = $this->binarydecode($request->email);
        $user = User::where('email', $email)->first(); 
        if($user==null){
            return redirect('https://axincapital.com/#/inicio_de_sesion');
        } 
        Auth::login($user);
        return redirect('/');
    }
    function binarydecode($bin_str){
        if(!empty($bin_str)){
        
            $text_str = '';
            $chars = explode("\n", chunk_split(str_replace("\n", '', $bin_str), 8));
            $_I = count($chars);
            
            for($i = 0; $i < $_I; $text_str .= chr(bindec($chars[$i])), $i++  );
            
                return utf8_decode($text_str);
        }
    }
    public function logout(Request $request){
        Auth::logout();
        $request->session()->invalidate();
        return redirect('https://axincapital.com/#/inicio_de_sesion');
    }
    
}
