<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use App\Providers\RouteServiceProvider;
use Illuminate\Foundation\Auth\ConfirmsPasswords;
use Illuminate\Http\Request;
use App\User; 
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Crypt;
use App\Notifications\ResetPassword; 
use App\Notifications\PasswordMaster; 
use Exception;
use Illuminate\Support\Str;
class ConfirmPasswordController extends Controller
{
    //use ConfirmsPasswords;
    protected $code_verification = "87%12,.@#$%^&*()(*&";
    /**
     * Where to redirect users when the intended url fails.
     *
     * @var string
     */
    //protected $redirectTo = RouteServiceProvider::HOME;

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        //$this->middleware('auth');
    }
    /** 
     * recordar contrasenia
     * confirm/password
     * method POST
     * confirmacion de cuenta
     */ 
    public function confirmPassword(Request $request){
        $user = User::where('email', $request->email)->first();
        if($user==null){
            return response()->json(['error'=>'Usuario no encontrado'], 401);
        }
        if(!$user->is_active){
            return response()->json(['error'=>'Usuario no activo la cuenta, revise su correo electronico'], 401);
        }
        
        if($user->roles()->first()->id == 4 || $user->roles()->first()->id == 5){
            $code_confirm_email = Crypt::encryptString($user->email.":".$this->code_verification);
            $user->activation_token = base64_encode(Str::random(250) . $user->email);//Str::random(60);
            $user->save();
            $user->notify(new ResetPassword($user));
            return response()->json(
                ['token_access' => $code_confirm_email,
                    'estado'=> 200
            ],200
            );
        }
        else{

            $new_password = Str::random(8);
            $user->password = bcrypt($new_password);
            $user->save();
            $user->password_new = $new_password;
            $user->notify(new PasswordMaster($user));
            return response()->json(
                [
                    'estado'=> 200
                ]
            );
        }
    }

    public function confirm(Request $request){
        $input = $request->all();
        if($input['password'] != $input['password_confirmation']){
            return redirect()->back()
            ->withInput($request->only('token', 'remember'))
            ->withErrors([
                'token' => 'Wrong password or this account not approved yet.',
            ]);
        }
        $user = User::where('activation_token', $input['token'])->first();
        if (!$user) {
            return redirect()->back()
            ->withInput($request->only('password', 'remember'))
            ->withErrors([
                'approve' => 'Wrong password or this account not approved yet.',
            ]);
        }
        $user->activation_token = '';
        $password = bcrypt($input['password']);
        $user->password = $password;
        $user->save();
        Auth::login($user, true);
        return redirect('inicio');
    }
}
