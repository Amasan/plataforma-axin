<?php

namespace App\Http\Controllers;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth; 
use App\Models\Client;
use App\Models\Partner; 
use App\Models\Setting;
use App\Models\AccountClient;
use App\Models\AccountPartner;
use App\Models\ClientReceipt;
use App\Models\Accumulated;
use App\Models\ClientWithDrawalRequest;
use App\Http\Requests\ClientWithdrawFundRequest;
use Carbon\Carbon;
use Illuminate\Support\Facades\Storage;
use App\Events\ReceiptCreated;
use App\Events\WithDrawalRequestCreated;

class bancaController extends Controller
{
    
    public function __construct()
    {
        $this->middleware('auth');
    }
    /*
    * obtiene valores de estado de cliente 1-5
    */
    private function shareCompact(){
        $user = Auth::user();
        $data = 0;
        if($user->roles()->first()->id == 4){
            $client = Client::where('user_id',$user->id)->first();
            $data = $client->complete_profile;
        }
        else{
            $partner = Partner::where('user_id',$user->id)->first();
            $data = $partner->complete_profile;
        }
        view()->share('complete_profile', ($data*2)==0?1:($data*2));
    }

    /*
    * vista de depositar fondos
    * url get banca/depositar_fondos/
    */
    Public function depositarFondos() {
        $settings = Setting::where('is_payment','1')
        ->get();
        return $this->showView('banca.depositar_fondos')
                ->with('settings',$settings);
    }
    /*
    * vista de retiro de fondos
    * url get banca/retirar_fondos/
    */
    public function retirarFondos() {
        $user = Auth::user();
        $accounts = null;
        if($user->roles()->first()->id == 4){
            $client = Client::where('user_id',$user->id)->first();
            $accounts = AccountClient::where('client_id',$client->id)
            ->join('type_accounts', 'account_clients.type_account_id', '=', 'type_accounts.id')
            ->select('account_clients.account_number',
                    'account_clients.id',
                    'account_clients.is_active',
                    'type_accounts.name')
            ->get()
            ->toArray();
        }
        else{
            $partner = Partner::where('user_id',$user->id)->first();
            $accounts = AccountPartner::where('partner_id',$partner->id)
            ->join('type_accounts', 'accountpathers.type_account_id', '=', 'type_accounts.id')
            ->select('accountpathers.account_number',
                    'accountpathers.id',
                    'accountpathers.is_active',
                    'type_accounts.name')
            ->get()
            ->toArray();
        }
        $full_name = $user->name ." ". $user->first_last_name ." ". $user->second_last_name??"";
        $email = $user->email;
        

        return $this->showView('banca.retirar_fondos')
        ->with('name',$full_name)
        ->with('email',$email)
        ->with('accounts',$accounts);
    }

    /*
    * vista de comprobante de deposito 
    * url get banca/comprobante/
    */
    public function comprobante() {
        $user = Auth::user();
        $accounts = null;
        $full_name = $user->name ." ". $user->first_last_name ." ". $user->second_last_name??"";
        $email = $user->email;
        if($user->roles()->first()->id == 4){
            $client = Client::where('user_id',$user->id)->first();
            $accounts = AccountClient::where('client_id',$client->id)
            ->get();
        }
        else{
            $partner = Partner::where('user_id',$user->id)->first();
            $accounts = AccountPartner::where('partner_id',$partner->id)
            ->get();
        }
        
        return $this->showView('banca.comprobar_deposito')
        ->with('name',$full_name)
        ->with('email',$email)
        ->with('accounts',$accounts);
    }

    /*
    * crea registro de comprante pago
    * url post banca/update_comprobante/
    */
    public function updateComprobante(Request $request) {
        $user = Auth::user();
        $full_name = $user->name ." ". $user->first_last_name ." ". $user->second_last_name??"";
        $input = $request->all();
        
        $is_client = false;
        $id = 0;
        $idAccount = 0;
        if($user->roles()->first()->id == 4){//modo client
            $is_client = true;
            $client = Client::where('user_id',$user->id)->first();
            $id = $client->id;
            $accountClient = AccountClient::where('id', $input['account_client_id'])
                ->first();
            $idAccount = $accountClient->id;
        }
        else{//modo socio
            $partner = Partner::where('user_id',$user->id)->first();
            $id = $partner->id;
            $accountPartner = AccountPartner::where('id',$input['account_client_id'])
            ->first();
            $idAccount = $accountPartner->id;
        }
        $file = $input['file_url'];
        Storage::disk('s3')->put('', $file);
        //guarda datos para revision
        $clientReceipt = ClientReceipt::create([
            'file_name'=>$file->getClientOriginalName(),
            'file_url'=>$file->hashName(),
            'reviewed' => 'NULL'
        ]);
        if($is_client){
            $clientReceipt->account_client_id = $idAccount;
            $clientReceipt->client_id = $id;
        }
        else{
            $clientReceipt->account_partner_id = $idAccount;
            $clientReceipt->partner_id = $id;
        }
        $clientWithDrawalRequest = ClientWithDrawalRequest::create([
            'name' => $full_name,
            'email' => $user->email,
            'bitcoin_address' => '',
            'concept' => 'Deposito',
            'date_deposite' =>  Carbon::now()->toDateTimeString(),
            'amount'=> 0,
            'method'=> 'Transferencia',
            'status' => 'Pendiente',
            
        ]);
        if($is_client){
            $clientWithDrawalRequest->account_client_id = $idAccount;
        }
        else{
            $clientWithDrawalRequest->account_partner_id = $idAccount;
        }
        $clientWithDrawalRequest->concept = 'Deposito';
        
        $clientWithDrawalRequest->save();
        
        $clientReceipt->save();

        event(new ReceiptCreated($clientReceipt));

        return redirect('/banca/movimientos');
    }

    /*
    * retiro de fondos
    * url post banca/update_retirar_fondos/
    */
    public function ClientWithdrawFunds(ClientWithdrawFundRequest $request){
        
        $valid_numbers = "/[A-Za-z0-9\s\t]/";
        $valid_address_bitcoin = "/^1[A-Za-z0-9]{27,34}$|^3[A-Za-z-0-9]{27,34}$/";
        $valid_names = "/[A-Za-zäÄëËïÏöÖüÜáéíóúáéíóúÁÉÍÓÚÂÊÎÔÛâêîôûàèìòùÀÈÌÒÙñÑ\s\t]/";
        
        $input = $request->all();
        $is_valid = false;
        $bitcoin_address = "";
        $bank_name = "";
        $bank_number = "";
        $ivan_code = "";
        $swift_code ="";

        if($input['method'] == 'Bitcoin'){
            if($this->isValid($valid_numbers,$input['bitcoin_address']) ||
                $this->isValid($valid_address_bitcoin,$input['bitcoin_address'])){
                $bitcoin_address = "Debe comenzar con 1 o 3  y debe contener entre 27 y 34 caracteres";
                $is_valid = true;
            }
        }
        if($input['method'] == 'Transferencia'){
            if($this->isValid($valid_names,$input['bank_name'])){
                $bank_name = "El campo NOMBRE DEL BANCO es incorrecto";
                $is_valid = true;
            }
            if($this->isValid($valid_numbers,$input['bank_number'])){
                $bank_number = "El campo NUMERO EN LA CUENTA BANCARIA es incorrecto";
                $is_valid = true;
            }
            if($input['ivan_code']!=null &&
                $this->isValid($valid_numbers,$input['ivan_code'])){
                $ivan_code = "El campo CODIGO IBAN es incorrecto";
                $is_valid = true;
            }
            if($input['swift_code']!=null && $this->isValid($valid_numbers,$input['swift_code'])){
                $swift_code = "El campo CODIGO SWIFT/ABA es incorrecto";
                $is_valid = true;
            }
        }
        if($is_valid){
            return redirect()->back()
            ->withInput()
            ->withErrors(['bitcoin_address' => $bitcoin_address,
                'bank_number' => $bank_number,
                'bank_name' => $bank_name,
                    'swift_code' => $swift_code,
                    'ivan_code' => $ivan_code]);
        }
        
        return $this->saveWithDrawalRequest($input);
        
    }
    //valida patron de texto
    private function isValid($pathern,$data){
        $valid = preg_match($pathern, $data??"");
        return $valid == 0;
    }
    //guarda retiro de fondos
    private function saveWithDrawalRequest($input){
        
        $user = Auth::user();
        $isClient = false;
        $idmodo = 0;
        $idAccount = 0;
        $withdrawals = 0;
        if($user->roles()->first()->id == 4){//modo cliente
            $isClient = true;
            $client = Client::where('user_id',$user->id)->first();
            $idmodo = $client->id;
            $accountClient = AccountClient::where('id',$input['account_client_id'])
            ->first();
            $withdrawals = $accountClient->withdrawals;
            $idAccount = $accountClient->id;
        }
        else if($user->roles()->first()->id == 5){//modo socio
            $partner = Partner::where('user_id',$user->id)->first();
            $idmodo = $partner->id;
            $accountPartner = AccountPartner::where('id',$input['account_client_id'])
            ->first();
            $withdrawals = $accountPartner->withdrawals;
            $idAccount = $accountPartner->id;
        }
        $textAccount = $isClient?'account_client_id':'account_partner_id';
        $textU = $isClient?'client_id':'partner_id';
        $amount = floatval($input['amount']);
        
        $gananciaAcumulada = Accumulated::where($textU,$idmodo)
            ->where($textAccount,$idAccount)
            ->sum('amount');
        
        $gananciaAcumulada = $gananciaAcumulada - $withdrawals;
        
        if($gananciaAcumulada < $amount){
            return redirect()->back()
            ->withInput()
            ->withErrors(['amount' => "Saldo insuficiente para realizar esta transacción"]);
        }
        if($amount<=0){
            return redirect()->back()
            ->withInput()
            ->withErrors(['amount' => "El monto no puede ser procesado"]);
        }
        
        $input['ivan_code'] = $input['ivan_code']??"";
        $input['swift_code'] = $input['swift_code']??"";
        $input['bitcoin_address'] = $input['bitcoin_address']??"";
        $input['bank_name'] = $input['bank_name']??"";
        $input['bank_number'] = $input['bank_number']??"";
        $input['date_deposite'] = Carbon::now()->toDateTimeString();
        $clientWithDrawalRequest = ClientWithDrawalRequest::create($input);
        if(!$isClient){//socio
            $clientWithDrawalRequest->account_partner_id = $idAccount;
            $clientWithDrawalRequest->account_client_id = NULL;
        }else{//cliente
            $clientWithDrawalRequest->account_partner_id = NULL;
            $clientWithDrawalRequest->account_client_id = $idAccount;
        }
        $clientWithDrawalRequest->save();

        event(new WithDrawalRequestCreated($clientWithDrawalRequest));
        return redirect('banca/movimientos');
    }
    /*
    * lista de transaciones retiros y depositos
    * url get banca/movimientos/
    */
    public function movimientos() {
        $user = Auth::user();
        $items = null;
        if($user->roles()->first()->id == 4){
            $client = Client::where('user_id',$user->id)->first();
            $items = ClientWithDrawalRequest::where('client_id',$client->id)
            ->join('account_clients', 'withdrawalrequests.account_client_id', '=', 'account_clients.id')
            ->join('type_accounts', 'account_clients.type_account_id', '=', 'type_accounts.id')
            ->orderBy('withdrawalrequests.id','DESC')
            ->select(

                'withdrawalrequests.id',
                'withdrawalrequests.concept',
                'withdrawalrequests.amount',
                'withdrawalrequests.status',
                'withdrawalrequests.date_deposite',
                'account_clients.account_number',
                'type_accounts.name')
            ->paginate(20);
        }
        else{
            $partner = Partner::where('user_id',$user->id)->first();
            $items = ClientWithDrawalRequest::where('partner_id',$partner->id)
            ->join('accountpathers', 'withdrawalrequests.account_partner_id', '=', 'accountpathers.id')
            ->join('type_accounts', 'accountpathers.type_account_id', '=', 'type_accounts.id')
            ->orderBy('withdrawalrequests.id','DESC')
            ->select(
                'withdrawalrequests.id',
                'withdrawalrequests.concept',
                'withdrawalrequests.amount',
                'withdrawalrequests.status',
                'withdrawalrequests.date_deposite',
                'accountpathers.account_number',
                'type_accounts.name')
            ->paginate(20);
        }

        return $this->showView('banca.movimientos')
        ->with('items',$items);
    }
    //devuelve una visya en base a datos agregados
    public function showView($viewName){
        $this->shareCompact();
        return view($viewName);
    }

    
}
