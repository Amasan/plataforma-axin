<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class ClientPersonalRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array 'curp'=>'required|unique:clients,curp,' . $this->id,
     */
    public function rules()
    {
        return [
            'name'=>['required', 'min:3', 'max:80','regex:/[A-Za-zäÄëËïÏöÖüÜáéíóúáéíóúÁÉÍÓÚÂÊÎÔÛâêîôûàèìòùÀÈÌÒÙñÑ\s\t]/'],
            'first_last_name'=>['required','min:3','max:80','regex:/[A-Za-zäÄëËïÏöÖüÜáéíóúáéíóúÁÉÍÓÚÂÊÎÔÛâêîôûàèìòùÀÈÌÒÙñÑ\s\t]/'],
            'second_last_name'=>['max:80','regex:/[A-Za-zäÄëËïÏöÖüÜáéíóúáéíóúÁÉÍÓÚÂÊÎÔÛâêîôûàèìòùÀÈÌÒÙñÑ\s\t]*/'],
            'birth_date'=>['required','date','before:-18 years'],
            'country_code'=>['required'],
            'number_phone'=>['required','min:6','max:15','regex:/^[0-9]*$/'],
            'birth_date_country'=>['required'],
            'city'=>['required','min:4','max:40'],
            'code_postal'=>['required','min:2','max:10','regex:/^[0-9]*$/'],
            'direction'=>['required','min:10','max:100']
        ];
    }
}
