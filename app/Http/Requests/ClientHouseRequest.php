<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class ClientHouseRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'anual_activity_income_id'=>['required','regex:/^[0-9]*$/'],
            'family_member_holds_public_office_id'=>['required','regex:/^[0-9]*$/'],
            'total_funds_invest_id'=>['required','regex:/^[0-9]*$/'],
            'state_employee' =>['required'],
            'is_politically_exposed'=>['required','regex:/^[0-9]*$/'],
            'is_declared_bankrupt'=>['required','regex:/^[0-9]*$/'],
        ];
    }
}
