<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class ClientFilesRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'file_document_identity_oficial_V1'=>['required'],//,'mimes:JPEG,PNG','regex:/^data:image/'
            'file_document_identity_oficial_V2'=>[''],//,'mimes:JPEG,PNG','regex:/^data:image/'
            'file_document_home'=>['required'],//'mimes:JPEG,PBG,PDF','regex:/^data:image/'
            'type_identity_oficial'=>['required']
        ];
    }
}
