<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class SocioPersonalDatosEmpresa extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'name_business'=>['required', 'min:3', 'max:80','regex:/[A-Za-zäÄëËïÏöÖüÜáéíóúáéíóúÁÉÍÓÚÂÊÎÔÛâêîôûàèìòùÀÈÌÒÙñÑ\s\t]/'],
            'second_last_name'=>['max:80','regex:/[A-Za-zäÄëËïÏöÖüÜáéíóúáéíóúÁÉÍÓÚÂÊÎÔÛâêîôûàèìòùÀÈÌÒÙñÑ\s\t]*/'],
            'country_business'=>['required'],
            'city_business'=>['required','min:4','max:40'],
            'code_postal_business'=>['required','min:2','max:10','regex:/^[0-9]*$/'],
            'rfc_business'=>['required','min:10','max:100'],
            'direction_company'=>['required','min:10','max:100']
        ];
    }
}
