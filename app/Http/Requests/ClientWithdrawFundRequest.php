<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class ClientWithdrawFundRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'email'=>['required'],
            'name'=>['required','regex:/[A-Za-zäÄëËïÏöÖüÜáéíóúáéíóúÁÉÍÓÚÂÊÎÔÛâêîôûàèìòùÀÈÌÒÙñÑ\s\t]/'],
            'account_client_id'=>['required','regex:/[0-9]/'],
            'amount'=>['required','regex:/[0-9\s\t]/'],
            'method'=>['required'],
            'bitcoin_address'=>'',//3 o 1 empieza
            'bank_name'=>'',
            'bank_number'=>'',
            'swift_code'=>'',
            'ivan_code'=>''
        ];
    }
}
