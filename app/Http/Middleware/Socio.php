<?php

namespace App\Http\Middleware;

use Closure;

class Socio
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        $role = $request->user()->roles()->first();
        if ($role->id == 5) {
            return $next($request);
        }
        return redirect('/page/not/found');
    }
}
