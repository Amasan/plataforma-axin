<?php

namespace App\Http\Middleware;

use Closure;

class Administrador
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        $roles = $request->user()->roles()->get();
        foreach ($roles as $role) { 
            if ($role->id == 1) {
                return $next($request);
            }
        }
        return redirect('/page/not/found');
    }
}
