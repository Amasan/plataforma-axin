<?php

namespace App\Listeners;

use App\Events\VerifiedClientCreated;
use App\Notifications\VerifiedClient;
use App\User;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Support\Facades\Notification;

class NotifyClientOfFullVerification
{
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Handle the event.
     *
     * @param  VerifiedClientCreated  $event
     * @return void
     */
    public function handle(VerifiedClientCreated $event)
    {
        $user = User::where('id',$event->client->user_id)->first();
        Notification::send($user, new VerifiedClient($event->client));
    }
}
