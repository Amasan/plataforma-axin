<?php

namespace App\Listeners;

use App\Events\VerifiedPartnerCreated;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Support\Facades\Notification;
use App\Notifications\VerifiedPartner;
use App\User;
class NotifyPartnerOfFullVerification
{
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Handle the event.
     *
     * @param  VerifiedPartnerCreated  $event
     * @return void
     */
    public function handle(VerifiedPartnerCreated $event)
    {   

        $user = User::where('id',$event->partner->user_id)->first();
        Notification::send($user, new VerifiedPartner($event->partner));
    }
}
