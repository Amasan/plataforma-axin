<?php

namespace App\Listeners;
use Artisan;
use App\Events\PostCreated;
use App\Notifications\MassNotifications;
use App\User;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Support\Facades\Notification;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Bus\Queueable;


class NotifyUsersAboutNewPost implements ShouldQueue
{
    // use InteractsWithQueue, Queueable;
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Handle the event.
     *
     * @param  PostCreated  $event
     * @return void
     */

    /**
     * Funcion que crea la instancia del evento Postcreated y obtiene el mensaje mediante la variable event
     */
    public function handle(PostCreated $event)
    {
        /**
         * Busca a todos los usuarios de tipo cliente y les notifica el mensaje recibido
         */
            $users = User::whereHas('roles',function($query) {
                $query->where('roles_id','>=',4);
            })->get();
            /**
             * Al encontrar los usuarios se hace una instancia de la notificacion y se le pasa el mensaje creado a todos los usuarios de tipo cliente
             * la notificacion se encuentra en app/Notifications/MassNotifications.php
            */
            Notification::send($users, new MassNotifications($event->post)); 
            
    
    }
}
