<?php

namespace App\Listeners;

use App\Events\ReceiptCreated;
use App\Notifications\NewReceipt;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Support\Facades\Notification;
use App\User;

class NotifyAdminAboutNewReceipt
{
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Handle the event.
     *
     * @param  ReceiptCreated  $event
     * @return void
     */

    /**
     * Funcion que crea la instancia del evento ReceiptCreated al generar un deposito por parte del cliente
     */
    public function handle(ReceiptCreated $event)
    {
         /**
         * Busca a los usuarios de tipo rendimiento y les notifica que se ha realizado un deposito
         */
        $user = User::join('roles_user','roles_user.user_id', '=', 'users.id')
        ->where('roles_user.roles_id', 3)
        ->get();
        /**
         * Al encontrar los usuarios se hace una instancia de la notificacion y se le envia una notificacion de un deposito de un cliente
         * la notificacion se encuentra en app/Notifications/NewReceipt.php
        */
        Notification::send($user, new NewReceipt($event->clientReceipt));
    }
}
