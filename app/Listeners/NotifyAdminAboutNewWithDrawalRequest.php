<?php

namespace App\Listeners;

use App\Events\WithDrawalRequestCreated;
use App\Notifications\NewWithDrawalRequest;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Support\Facades\Notification;
use App\User;

class NotifyAdminAboutNewWithDrawalRequest
{
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Handle the event.
     *
     * @param  WithDrawalRequestCreated  $event
     * @return void
     */

    /**
     * Funcion que crea la instancia del evento WithDrawalRequestCreated al generar un retiro por parte del cliente
     */
    
    public function handle(WithDrawalRequestCreated $event)
    {
         /**
         * Busca a los usuarios de tipo rendimiento y les notifica que se ha solicitado un retiro
         */
        $user = User::join('roles_user','roles_user.user_id', '=', 'users.id')
        ->where('roles_user.roles_id', 3)
        ->get();
        /**
         * Al encontrar los usuarios se hace una instancia de la notificacion y se le envia una notificacion de un retiro de un cliente
         * la notificacion se encuentra en app/Notifications/NewWithDrawalRequest.php
        */
        Notification::send($user, new NewWithDrawalRequest($event->clientWithDrawalRequest));
    }
}
