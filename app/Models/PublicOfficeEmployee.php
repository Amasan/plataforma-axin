<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class PublicOfficeEmployee extends Model
{
    protected $table = 'family_member_holds_public_offices';

    protected $fillable = [
        'id','description'
    ];
}
