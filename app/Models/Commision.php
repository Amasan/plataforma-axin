<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Commision extends Model
{
    protected $table = 'commision';

    protected $fillable = [
        'id','account_client_id','commissions','date'
    ];
}
