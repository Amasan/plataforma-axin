<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class TotalFundInvest extends Model
{
    protected $table = 'total_funds_invests';

    protected $fillable = [
        'id','description'
    ];
}
