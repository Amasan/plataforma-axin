<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Accumulated extends Model
{
    protected $table = 'accumulateds';

    protected $fillable = [
        'id','amount','client_id','account_client_id','date'
    ];
    public function accountClient() {
        return $this->belongsTo('App\Models\AccountClient', 'account_client_id');
    }
    public function accountPartner() {
        return $this->belongsTo('App\Models\AccountPartner', 'account_partner_id');
    }
}
