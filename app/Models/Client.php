<?php

namespace App\Models;

use Illuminate\Notifications\Notifiable;
use Illuminate\Database\Eloquent\Model;

class Client extends Model
{
    use Notifiable;
    protected $table = 'clients';

    protected $fillable = [
        'complete_profile', 'country_code', 'number_phone',

        'user_id','type_account_id','email', 'anual_activity_income_id',
        'family_member_holds_public_office_id',
        'email', 'signature',
    ];
    public function user()
    {
        return $this->belongsTo('App\User', 'user_id');
    }
    public function anual()
    {
        return $this->belongsTo('App\Models\Anualincomes', 'anual_activity_income_id');
    }
    public function total()
    {
        return $this->belongsTo('App\Models\TotalFundInvest', 'total_funds_invest_id');
    }
    public function public()
    {
        return $this->belongsTo('App\Models\PublicOfficeEmployee', 'family_member_holds_public_office_id');
    }


    //Scope

    public function scopeName($query, $name)
    {
        if($name)
            return $query->where('name', 'LIKE', "%$name%");
    }

    public function scopeFirstLastName($query, $first_last_name)
    {
        if($first_last_name)
            return $query->where('first_last_name', 'LIKE', "%$first_last_name%");
    }

    public function scopeSecondLastName($query, $second_last_name)
    {
        if($second_last_name)
            return $query->where('second_last_name', 'LIKE', "%$second_last_name%");
    }

    public function scopeEmail($query, $email)
    {
        if($email)
            return $query->where('email', 'LIKE', "%$email%");
    }

}
