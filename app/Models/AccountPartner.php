<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class AccountPartner extends Model
{
    protected $table = 'accountpathers';

    protected $fillable = [
        'id','partner_id','account','account_number','amount_money','is_active','withdrawals','date_contract_init','date_contract_fin'
    ];
    public function typeaccount(){
        return $this->belongsTo('App\Models\TypeAccount', 'type_account_id');
    }
    public function partner(){
        return $this->belongsTo('App\Models\Partner', 'partner_id');
    }
}
