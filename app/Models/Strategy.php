<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Strategy extends Model
{
    protected $table = 'strategies';

    protected $fillable = [
        'id','number','strategy','date_opened','date_closed','instrument','action',
        'lotes','sl_price','pt_price','open_price','close_price','pips','net_profit','gain'
    ];
}
