<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Roles extends Model
{
    protected $table = 'roles';

    protected $fillable = [
        'id','name','description','is_active'
    ];

    public function users(){
        return $this->belongsToMany('App\User')->withTimestamps();
    }
}
