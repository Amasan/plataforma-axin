<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class AnualIncomes extends Model
{
    //anual_activity_incomes
    protected $table = 'anual_activity_incomes';

    protected $fillable = [
        'id','description'
    ];
}
