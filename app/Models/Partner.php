<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use App\Events\VerifiedPartnerCreated;

class Partner extends Model
{
    protected $table = 'partners';

    protected $fillable = [
        'complete_profile', 'country_code','name_business', 'rfc_business','code_postal_business', 'country_business', 'city_business', 'direction_company', 'number_phone',
        'user_id','type_account_id','email', 'is_active_movements','is_active_newsletter',
        'file_document_contract_status','file_document_contract', 'signature',
    ];

    public function user()
    {
        return $this->belongsTo('App\User', 'user_id');
    }

    // protected $events = [
    //     'updated' => VerifiedPartnerCreated::class
    // ];


    //Scope

    public function scopeName($query, $name)
    {
        if($name)
            return $query->where('name', 'LIKE', "%$name%");
    }

    public function scopeFirstLastName($query, $first_last_name)
    {
        if($first_last_name)
            return $query->where('first_last_name', 'LIKE', "%$first_last_name%");
    }

    public function scopeSecondLastName($query, $second_last_name)
    {
        if($second_last_name)
            return $query->where('second_last_name', 'LIKE', "%$second_last_name%");
    }

    public function scopeEmail($query, $email)
    {
        if($email)
            return $query->where('email', 'LIKE', "%$email%");
    }
}
