<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class AccountClient extends Model
{
    protected $table = 'account_clients';

    protected $fillable = [
        'id','client_id','type_account_id','account_number','amount_money','is_active','withdrawals','date_contract_init','date_contract_fin'
    ];
    public function typeaccount(){
        return $this->belongsTo('App\Models\TypeAccount', 'type_account_id');
    }
    public function client(){
        return $this->belongsTo('App\Models\Client', 'client_id');
    }
}
