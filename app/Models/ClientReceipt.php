<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Carbon\Carbon;
use App\Events\ReceiptCreated;

class ClientReceipt extends Model
{
    protected $table = 'receipts';

    protected $events = [
        'created' => ReceiptCreated::class
    ];
    
    protected $fillable = [
        'client_id','file_name','file_url','account_client_id', 'reviewed'
    ]; 

    public function client(){
        return $this->belongsTo('App\Models\Client', 'client_id');
    }
    public function partner(){
        return $this->belongsTo('App\Models\Partner', 'partner_id');
    }

    public function accountClient() {
        return $this->belongsTo('App\Models\AccountClient', 'account_client_id');
    }

    //Scope

    public function scopeName($query, $name)
    {
        if($name)
            return $query->where('name', 'LIKE', "%$name%");
    }

    public function scopeFirstLastName($query, $first_last_name)
    {
        if($first_last_name)
            return $query->where('first_last_name', 'LIKE', "%$first_last_name%");
    }

    public function scopeSecondLastName($query, $second_last_name)
    {
        if($second_last_name)
            return $query->where('second_last_name', 'LIKE', "%$second_last_name%");
    }

    public function scopeEmail($query, $email)
    {
        if($email)
            return $query->where('email', 'LIKE', "%$email%");
    }


}
