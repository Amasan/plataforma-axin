<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use App\Events\WithDrawalRequestCreated;

class ClientWithDrawalRequest extends Model
{
    protected $table = 'withdrawalrequests';

    protected $events = [
        'created' => WithDrawalRequestCreated::class
    ];
    
    protected $fillable = [
        'email','name','account_client_id','amount','method','bitcoin_address',
        'bank_name','bank_number','bank_titular_name','swift_code','ivan_code',
        'date_deposite','id', 'status','account_partner_id'
    ];

    public function accountClient(){
        return $this->belongsTo('App\Models\AccountClient', 'account_client_id');
    }
    
    public function accountPartner(){
        return $this->belongsTo('App\Models\AccountPartner', 'account_partner_id');
    }

     //Scope

    public function scopeName($query, $name)
    {
        if($name)
            return $query->where('name', 'LIKE', "%$name%");
    }


    public function scopeEmail($query, $email)
    {
        if($email)
            return $query->where('email', 'LIKE', "%$email%");
    }
 
}
