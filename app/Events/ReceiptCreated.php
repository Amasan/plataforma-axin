<?php

namespace App\Events;

use Illuminate\Broadcasting\Channel;
use Illuminate\Broadcasting\InteractsWithSockets;
use Illuminate\Broadcasting\PresenceChannel;
use Illuminate\Broadcasting\PrivateChannel;
use Illuminate\Contracts\Broadcasting\ShouldBroadcast;
use Illuminate\Foundation\Events\Dispatchable;
use Illuminate\Queue\SerializesModels;
use Pusher\Laravel\Facades\Pusher;

/**
 * Este evento es escuchado por el Listener NotifyAdminAboutNewReceipt
 * se encuentra en la carpeta app/Listeners/NotifyAdminAboutNewReceipt.php
*/

class ReceiptCreated
{
    use Dispatchable, InteractsWithSockets, SerializesModels;
    
    /**
     * Se crea una variable para almacenar el contenido del clientReceipt
    */
    public $clientReceipt;


    /**
     * Create a new event instance.
     *
     * @return void
     */

    /**
     * Recibimos en el constructor el clientReceipt que recibimos de la funcion updateComprobante
     * se encuentra en la carpeta app/http/controllers/bancaController.php
    */
    public function __construct($clientReceipt)
    {
        /**
         * Igualamos el clientReceipt recibido a la variable creada 
        */
        $this->clientReceipt = $clientReceipt;

    }

    /**
     * Get the channels the event should broadcast on.
     *
     * @return \Illuminate\Broadcasting\Channel|array
     */

    public function broadcastOn()
    {

    }

}
