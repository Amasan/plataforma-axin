<?php

namespace App\Events;

use Illuminate\Broadcasting\Channel;
use Illuminate\Broadcasting\InteractsWithSockets;
use Illuminate\Broadcasting\PresenceChannel;
use Illuminate\Broadcasting\PrivateChannel;
use Illuminate\Contracts\Broadcasting\ShouldBroadcast;
use Illuminate\Foundation\Events\Dispatchable;
use Illuminate\Queue\SerializesModels;

/**
 * Este evento es escuchado por el Listener NotifyAdminAboutNewWithDrawalRequest
 * se encuentra en la carpeta app/Listeners/NotifyAdminAboutNewWithDrawalRequest.php
*/
class WithDrawalRequestCreated
{
    use Dispatchable, InteractsWithSockets, SerializesModels;

    /**
     * Se crea una variable para almacenar el contenido del clientWithDrawalRequest
    */
    public $clientWithDrawalRequest;

    /**
     * Create a new event instance.
     *
     * @return void
     */

    /**
     * Recibimos en el constructor el clientWithDrawalRequest que recibimos de la funcion saveWithDrawalRequest
     * se encuentra en la carpeta app/http/controllers/bancaController.php
    */
    public function __construct($clientWithDrawalRequest)
    {
        /**
         * Igualamos el clientWithDrawalRequest recibido a la variable creada 
        */
        $this->clientWithDrawalRequest = $clientWithDrawalRequest;
    }

    /**
     * Get the channels the event should broadcast on.
     *
     * @return \Illuminate\Broadcasting\Channel|array
     */
    public function broadcastOn()
    {
        return new PrivateChannel('channel-name');
    }
}
