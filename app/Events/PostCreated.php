<?php

namespace App\Events;

use Illuminate\Broadcasting\Channel;
use Illuminate\Broadcasting\InteractsWithSockets;
use Illuminate\Broadcasting\PresenceChannel;
use Illuminate\Broadcasting\PrivateChannel;
use Illuminate\Contracts\Broadcasting\ShouldBroadcast;
use Illuminate\Foundation\Events\Dispatchable;
use Illuminate\Queue\SerializesModels;

/**
 * Este evento es escuchado por el Listener NotifyUsersAboutNewPost
 * se encuentra en la carpeta app/Listeners/NotifyUsersAboutNewPost.php
*/

class PostCreated
{
    use Dispatchable, InteractsWithSockets, SerializesModels;
    
    /**
     * Se crea una variable para almacenar el contenido del post
    */
    public $post;

    /**
     * Create a new event instance.
     *
     * @return void
     */

    /**
     * Recibimos en el constructor el post que recibimos de la funcion massMessagesStorelegal o massMessagesStore
     * se encuentra en la carpeta app/http/controllers/messagesController.php
    */
    public function __construct($post)
    {
        /**
         * Igualamos el post recibido a la variable creada 
        */
        $this->post=$post;
    }

    /**
     * Get the channels the event should broadcast on.
     *
     * @return \Illuminate\Broadcasting\Channel|array
     */
    public function broadcastOn()
    {
        return new PrivateChannel('channel-name');
    }
}
