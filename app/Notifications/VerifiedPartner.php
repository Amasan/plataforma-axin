<?php

namespace App\Notifications;

use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Notifications\Messages\MailMessage;
use Illuminate\Notifications\Notification;
use App\Models\Partner;
use App\Models\AccountClient;
use App\Models\AccountPartner;

class VerifiedPartner extends Notification
{
    use Queueable;

    public $partner;
    /** 
     * Create a new notification instance.
     *
     * @return void
     */
    public function __construct(Partner $partner)
    {
        $this->partner = $partner;
    }

    /**
     * Get the notification's delivery channels.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function via($notifiable)
    {
        return ['database', 'mail'];
    }

    /**
     * Get the mail representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return \Illuminate\Notifications\Messages\MailMessage
     */
    public function toMail($notifiable)
    {
        $accountP = AccountPartner::join('type_accounts', 'accountpathers.type_account_id', '=', 'type_accounts.id')
        ->where('partner_id', $notifiable->id)
        ->select(
            'accountpathers.id',
            'type_accounts.id as idaccount',
            'accountpathers.partner_id',
            'accountpathers.is_active',
            'accountpathers.account_number',
            'accountpathers.amount_money',
            'type_accounts.name as nameaccount')
        ->get();

        return (new MailMessage)->view(
            'emails.verifiedPartner', [
                'data' => $this->partner,
                'user' => $notifiable,
                'portafolio' => $accountP,
                ]
        )->subject('Verificación Axin');
    }
 
    /**
     * Get the array representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function toArray($notifiable)
    {
        return [
            'link' => route('socio.inicio'),
            'text' => "Has sido verificado",
        ];
    }
}
