<?php

namespace App\Providers;

use Illuminate\Auth\Events\Registered;
use Illuminate\Auth\Listeners\SendEmailVerificationNotification;
use Illuminate\Foundation\Support\Providers\EventServiceProvider as ServiceProvider;
use Illuminate\Support\Facades\Event;

class EventServiceProvider extends ServiceProvider
{
    /**
     * The event listener mappings for the application.
     *
     * @var array
     */
    protected $listen = [
        Registered::class => [
            SendEmailVerificationNotification::class,
        ],
        'App\Events\PostCreated' => [
            'App\Listeners\NotifyUsersAboutNewPost',
        ],
        'App\Events\ReceiptCreated' => [
            'App\Listeners\NotifyAdminAboutNewReceipt',
        ],
        'App\Events\WithDrawalRequestCreated' => [
            'App\Listeners\NotifyAdminAboutNewWithDrawalRequest',
        ],
        'App\Events\VerifiedPartnerCreated' => [
            'App\Listeners\NotifyPartnerOfFullVerification',
        ],
        'App\Events\VerifiedClientCreated' => [
            'App\Listeners\NotifyClientOfFullVerification',
        ],
    ];

    /**
     * Register any events for your application.
     *
     * @return void
     */
    public function boot()
    {
        parent::boot();

    }
}
