@extends('layoutAdmins')

@section('content')
    <style>
        #phone { width: 100%; }
        .iti{ width: 100%;}
        .mayus{
            text-transform: uppercase;
        }
        input:invalid {
            border-color: #DD2C00!important;
        }
        select:invalid {
            border-color: #DD2C00!important;
        }

    </style>
<!-- MultiStep Form -->
<div class="container-fluid" id="grad1">
    <div class="row justify-content-center mt-0">
        <div class="col-11 col-sm-9 col-md-7 col-lg-10 text-center p-0 mt-3 mb-2">
            <div class="card px-0 pt-4 pb-0 mt-3 mb-3">
                <h1 class="fs-title text-center">Registro de usuario</h1>
                <div class="row">
                    <div class="col-md-12 mx-0">
                        <form id="msform" enctype="multipart/form-data" method="POST" action="{{ route('admin.registro.usuario_store') }}">
                            <input type="hidden" name="_token" value="{{ csrf_token() }}"></input>
                            <!-- progressbar -->
                            <ul id="progressbar">
                                <li class="active" id="usuario"><strong>1</strong></li>
                                <li id="personal"><strong>2</strong></li>
                                <li id="actividades"><strong>3</strong></li>
                                <li id="documentos"><strong>4</strong></li>
                            </ul> <!-- fieldsets -->
                            <fieldset>
                                <div class="form-card">
                                    <h2 class="fs-title">Usuario</h2>

                                   
                                            <div class="form-group row align-items-center mb-0">
                                                <div class="col-sm-12">
                                                    <div class="form-group mb-0">
                                                        <div class="form-row">
                                                            <div class="col-12 col-md-12 col-lg-12">
                                                                <label class="col-form-label form-label col-12 col-md-12 col-lg-12" style="padding:0;">TIPO DE USUARIO</label>
                                                                <div class="col-12 col-md-12 col-lg-12" style="padding:0;">
                                                                    <select onchange="validar()" onchange="type" name="tipo" value="{{ old('tipo') }}" id="tipo" class="form-control custom-select @error('tipo') is-invalid @enderror inputFormu" required>
                                                                        <option value="1" selected>Personal</option>
                                                                        <option value="0">Empresa</option>
                                                                    </select>
                                                                    
                                                                    @error('tipo')
                                                                        <span class="invalid-feedback" role="alert">
                                                                            <strong style="color:red!important">{{ $message }}</strong>
                                                                        </span>
                                                                    @enderror
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>  
                                                    @error('tipo')
                                                        <span class="invalid-feedback" role="alert">
                                                            <strong style="color:red!important">{{ $message }}</strong>
                                                        </span>
                                                    @enderror
                                                </div>
                                            </div>
                                            <div class="form-group row align-items-center mb-0">
                                                <label class="col-form-label form-label col-sm-12" id="label_name_business">NOMBRE DE LA EMPRESA</label>
                                                <div class="col-sm-12">
                                                    
                                                    <input  type="text" value="{{ old('name_business') }}" name="name_business" id="admin_name_business" class="form-control @error('name_business') is-invalid @enderror mayus" placeholder=""/>
                                                    @error('name_business')
                                                        <span class="invalid-feedback" role="alert">
                                                            <strong style="color:red!important">{{ $message }}</strong>
                                                        </span>
                                                    @enderror
                                                </div>
                                            </div>
                                            <div class="form-group row align-items-center mb-0">
                                                <label class="col-form-label form-label col-sm-12">CORREO ELECTRÓNICO</label>
                                                <div class="col-sm-12">
                                                    
                                                    <input onkeyup="validar()" type="email" value="{{ old('email') }}" name="email" id="email" class="form-control @error('email') is-invalid @enderror inputFormu" placeholder="" required/>
                                                    @error('email')
                                                        <span class="invalid-feedback" role="alert">
                                                            <strong style="color:red!important">{{ $message }}</strong>
                                                        </span>
                                                    @enderror
                                                </div>
                                            </div>
                                            
                                            <div class="form-group mb-0">
                                                <div class="form-row">
                                                    <div class="col-xs-12 col-md-6 col-lg-6">
                                                        <label class="col-form-label form-label col-xs-12 col-md-12 col-lg-12" style="padding:0;">CONTRASEÑA</label>
                                                        <div class="col-xs-12 col-md-12 col-lg-12" style="padding:0;">
                                                            <input onkeyup="validar()"  minlength="8" maxlength="16" type="password" name="password" value="{{ old('password') }}" class="form-control col-xs-12 col-md-12 col-lg-12  @error('password') is-invalid @enderror inputFormu mayus" placeholder="" required />
                                                            @error('password')
                                                                <span class="invalid-feedback" role="alert">
                                                                    <strong style="color:red!important">{{ $message }}</strong>
                                                                </span>
                                                            @enderror
                                                        </div>
                                                    </div>
                                                    <div class="col-xs-12 col-md-6 col-lg-6">
                                                        <label class="col-form-label form-label col-xs-12 col-md-12 col-lg-12" style="padding:0;">REPETIR CONTRASEÑA</label>
                                                        <div class="col-xs-12 col-md-12 col-lg-12" style="padding:0;">
                                                            <input onkeyup="validar()" minlength="8" maxlength="16" type="password" name="confirm_password" value="{{ old('confirm_password') }}" class="form-control col-xs-12 col-md-12 col-lg-12 @error('confirm_password') is-invalid @enderror inputFormu mayus" placeholder="" required />
                                                            @error('confirm_password')
                                                                <span class="invalid-feedback" role="alert">
                                                                    <strong style="color:red!important">{{ $message }}</strong>
                                                                </span>
                                                            @enderror
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>   
                                            
                                  

                                </div> <input type="button" name="next" class="next btn btn-primary" value="Siguiente" />
                            </fieldset>
                            <fieldset>
                                <div class="form-card">
                                    <h2 class="fs-title adminactividad">Datos personales</h2>
                              

                                    
                                            <div class="form-group row align-items-center mb-0">
                                                <label class="col-form-label form-label col-sm-12">NOMBRE(S)</label>
                                                <div class="col-sm-12">
                                                    
                                                    <input onkeyup="validar()" onkeypress="return validarCaracteres(event)" minlength="3" maxlength="80" type="text" value="{{ old('name') }}" name="name" id="name" class="form-control @error('name') is-invalid @enderror inputFormu mayus" placeholder="" required/>
                                                    @error('name')
                                                        <span class="invalid-feedback" role="alert">
                                                            <strong style="color:red!important">{{ $message }}</strong>
                                                        </span>
                                                    @enderror
                                                </div>
                                            </div>
                                            
                                            <div class="form-group mb-0">
                                                <div class="form-row">
                                                    <div class="col-xs-12 col-md-6 col-lg-6">
                                                        <label class="col-form-label form-label col-xs-12 col-md-12 col-lg-12" style="padding:0;">APELLIDO PATERNO</label>
                                                        <div class="col-xs-12 col-md-12 col-lg-12" style="padding:0;">
                                                            <input onkeyup="validar()" onkeypress="return validarCaracteres(event)" minlength="4" maxlength="80" type="text" name="first_last_name" value="{{ old('first_last_name') }}" class="form-control col-xs-12 col-md-12 col-lg-12  @error('first_last_name') is-invalid @enderror inputFormu mayus" placeholder="" required />
                                                            @error('first_last_name')
                                                                <span class="invalid-feedback" role="alert">
                                                                    <strong style="color:red!important">{{ $message }}</strong>
                                                                </span>
                                                            @enderror
                                                        </div>
                                                    </div>
                                                    <div class="col-xs-12 col-md-6 col-lg-6">
                                                        <label class="col-form-label form-label col-xs-12 col-md-12 col-lg-12" style="padding:0;">APELLIDO MATERNO</label>
                                                        <div class="col-xs-12 col-md-12 col-lg-12" style="padding:0;">
                                                            <input onkeyup="validar()" onkeypress="return validarCaracteres(event)" minlength="4" maxlength="80" type="text" name="second_last_name" value="{{ old('second_last_name') }}" class="form-control col-xs-12 col-md-12 col-lg-12 @error('second_last_name') is-invalid @enderror inputFormu mayus" placeholder="" required />
                                                            @error('second_last_name')
                                                                <span class="invalid-feedback" role="alert">
                                                                    <strong style="color:red!important">{{ $message }}</strong>
                                                                </span>
                                                            @enderror
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>   
                                            <div class="form-group mb-0">
                                                <div class="form-row">
                                                    <div class="col-xs-12 col-md-6 col-lg-6">
                                                        <label class="col-form-label form-label col-xs-12 col-md-12 col-lg-12" style="padding:0;">FECHA DE NACIMIENTO</label>
                                                        <div class="col-xs-12 col-md-12 col-lg-12" style="padding:0;">
                                                            <input onchange="validar()" id="date" type="date" name="birth_date" value="{{ old('birth_date') }}" class="form-control col-xs-12 col-md-12 col-lg-12 @error('birth_date') is-invalid @enderror inputFormu" required/>
                                                            @error('birth_date')
                                                                <span class="invalid-feedback" role="alert">
                                                                    <strong style="color:red!important">{{ $message }}</strong>
                                                                </span>
                                                            @enderror
                                                        </div>
                                                    </div>
                                                    <div class="col-xs-12 col-md-6 col-lg-6">
                                                        <label class="col-form-label form-label col-xs-12 col-md-12 col-lg-12" style="padding:0;">TÉLEFONO CELULAR</label>
                                                        <div class="col-xs-12 col-md-12 col-lg-12" style="padding:0;">
                                                            <div class="col-xs-12 col-md-12 col-lg-12" style="padding:0;">
                                                                <select hidden id="country_code" class="form-control @error('country_code') is-invalid @enderror" name="country_code" value="{{ old('country_code') }}" required></select>
                                                                <input onkeyup="validar()" onkeypress="return validarNumeros(event)" class="form-control iti @error('number_phone') is-invalid @enderror inputFormu" id="phone" name="number_phone" value="{{ old('number_phone') }}" pattern="^[0-9]\d{5,10}$" minlength="5" maxlength="10" type="tel" required>
                                                                @error('number_phone')
                                                                    <span class="invalid-feedback" role="alert">
                                                                        <strong style="color:red!important">{{ $message }}</strong>
                                                                    </span>
                                                                @enderror                    
                
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>   
                                            <div class="form-group mb-0">
                                                <div class="form-row">
                                                    <div class="col-xs-12 col-md-6 col-lg-6">
                                                        <label class="col-form-label form-label col-xs-12 col-md-12 col-lg-12" style="padding:0;">PAIS DE NACIMIENTO</label>
                                                        <select onchange="validar()" id="birth_date_country" name="birth_date_country" value="{{ old('birth_date_country') }}" class="form-control custom-select @error('birth_date_country') is-invalid @enderror inputFormu" required>
                                                        </select>
                                                        @error('birth_date_country')
                                                            <span class="invalid-feedback" role="alert">
                                                                <strong style="color:red!important">{{ $message }}</strong>
                                                            </span>
                                                        @enderror
                                                    </div>
                                                    <div class="col-xs-12 col-md-6 col-lg-6">
                                                        <div class="form-row">
                                                            <div class="col-xs-12 col-md-6 col-lg-6">
                                                                <label class="col-form-label form-label col-xs-12 col-md-12 col-lg-12" style="padding:0; font-size:0.8rem!important;">CIUDAD</label>
                                                                <div class="col-xs-12 col-md-12 col-lg-12" style="padding:0;">
                                                                    <input onkeyup="validar()" onkeypress="return validarCaracteres(event)" type="text" name="city" value="{{ old('city') }}" class="form-control col-xs-12 col-md-12 col-lg-12 @error('city') is-invalid @enderror inputFormu mayus" placeholder="" required/>
                                                                    @error('city')
                                                                        <span class="invalid-feedback" role="alert">
                                                                            <strong style="color:red!important">{{ $message }}</strong>
                                                                        </span>
                                                                    @enderror
                                                                </div>
                                                            </div>
                                                            <div class="col-xs-12 col-md-6 col-lg-6">
                                                                <label class="col-form-label form-label col-xs-12 col-md-12 col-lg-12" style="padding:0; font-size:0.8rem!important;">CÓDIGO POSTAL</label>
                                                                <div class="col-xs-12 col-md-12 col-lg-12" style="padding:0;">
                                                                    <input onkeyup="validar()" onkeypress="return validarNumeros(event)" type="text" pattern="^[0-9]\d{2,10}$" name="code_postal" value="{{ old('code_postal') }}" class="form-control col-xs-12 col-md-12 col-lg-12 @error('code_postal') is-invalid @enderror inputFormu" placeholder="" required/>
                                                                    @error('code_postal')
                                                                        <span class="invalid-feedback" role="alert">
                                                                            <strong style="color:red!important">{{ $message }}</strong>
                                                                        </span>
                                                                    @enderror
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>   
                                            <div class="form-group mb-0">
                                                <div class="form-row">
                                                    <div class="col-xs-12 col-md-12 col-lg-12">
                                                        <label class="col-form-label form-label col-xs-12 col-md-12 col-lg-12" style="padding:0;">DIRECCIÓN</label>
                                                        <div class="col-xs-12 col-md-12 col-lg-12" style="padding:0;">
                                                            <input onkeyup="validar()" type="text" name="direction" value="{{ old('direction') }}" class="form-control col-xs-12 col-md-12 col-lg-12 @error('direction') is-invalid @enderror inputFormu mayus" pattern="^[A-Za-z0-9äÄëËïÏöÖüÜáéíóúáéíóúÁÉÍÓÚÂÊÎÔÛâêîôûàèìòùÀÈÌÒÙñÑ, ]{10,100}$" placeholder="" required/> 
                                                            @error('direction')
                                                                <span class="invalid-feedback" role="alert">
                                                                    <strong style="color:red!important">{{ $message }}</strong>
                                                                </span>
                                                            @enderror
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>   
                                            
             
                                    
                                </div> <input type="button" name="previous" class="previous btn btn btn-default" value="Anterior" /> <input type="button" name="next" class="next btn btn-primary" value="Siguiente" />
                            </fieldset>
                            <fieldset>
                                <div class="form-card">
                                    <h2 class="fs-title adminactividad">Actividad economica</h2> 

                                  
                                            <div class="form-group mb-0">
                                                <div class="form-row">
                                                    <div class="col-12 col-md-12 col-lg-12">
                                                        <label class="col-form-label form-label col-12 col-md-12 col-lg-12 adminactividad" style="padding:0;">¿ESTADO DE EMPLEO?</label>
                                                        <div class="col-12 col-md-12 col-lg-12" style="padding:0;">
                                                            <select onchange="validar()" name="state_employee" value="{{ old('state_employee') }}" id="state_employee" class="form-control custom-select @error('state_employee') is-invalid @enderror inputFormu adminactividad">
                                                                <option value="" selected>Selecciona una opción</option>
                                                                <option value="Empleado">Empleado</option>
                                                                <option value="Independiente">Independiente</option>
                                                                <option value="Desempleado">Desempleado</option>
                                                                <option value="Jubilado">Jubilado</option>
                                                                <option value="Estudiante">Estudiante</option>
                                                            </select>
                                                            
                                                            @error('state_employee')
                                                                <span class="invalid-feedback" role="alert">
                                                                    <strong style="color:red!important">{{ $message }}</strong>
                                                                </span>
                                                            @enderror
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>   
                                            <div class="form-group mb-0">
                                                <div class="form-row">
                                                    <div class="col-12 col-md-12 col-lg-12">
                                                        <label class="col-form-label form-label col-12 col-md-12 col-lg-12 adminactividad" style="padding:0; font-size:0.85rem!important;">INGRESO ANUAL (USD)</label>
                                                        <div class="col-12 col-md-12 col-lg-12" style="padding:0;">
                                                            <select onchange="validar()" name="anual_activity_income_id" value="{{ old('anual_activity_income_id') }}" id="anual_activity_income_id" class="form-control custom-select @error('anual_activity_income_id') is-invalid @enderror inputFormu adminactividad" >
                                                                <option value="" selected>Selecciona una opción</option>
                                                                @foreach($anualIncomes as $item)
                                                                <option value="{{$item['id']}}">{{$item['description']}}</option>
                                                                @endforeach
                                                            </select>
                                                            @error('anual_activity_income_id')
                                                                <span class="invalid-feedback" role="alert">
                                                                    <strong style="color:red!important">{{ $message }}</strong>
                                                                </span>
                                                            @enderror
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>   
                                            <div class="form-group mb-0">
                                                <div class="form-row">
                                                    <div class="col-12 col-md-12 col-lg-12">
                                                        <label class="col-form-label form-label col-12 col-md-12 col-lg-12 adminactividad" style="padding:0;">TOTAL DE FONDOS A INVERTIR (USD)</label>
                                                        <div class="col-12 col-md-12 col-lg-12" style="padding:0;">
                                                            <select onchange="validar()" name="total_funds_invest_id" value="{{ old('total_funds_invest_id') }}" id="total_funds_invest_id" class="form-control custom-select @error('total_funds_invest_id') is-invalid @enderror inputFormu adminactividad" >
                                                                <option value="" selected>Selecciona una opción</option>
                                                                @foreach($totalFundInvest as $item)
                                                                <option value="{{$item['id']}}">{{$item['description']}}</option>
                                                                @endforeach
                                                            </select>
                                                            @error('total_funds_invest_id')
                                                                <span class="invalid-feedback" role="alert">
                                                                    <strong style="color:red!important">{{ $message }}</strong>
                                                                </span>
                                                            @enderror
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>   
                                            <div class="form-group mb-0">
                                                <div class="form-row">
                                                    <div class="col-12 col-md-12 col-lg-12">
                                                        <label class="col-form-label form-label col-12 col-md-12 col-lg-12 adminactividad" style="padding:0;">ORIGEN DE LOS FONDOS</label>
                                                        <div class="col-12 col-md-12 col-lg-12" style="padding:0;">
                                                            <select onchange="validar()" name="family_member_holds_public_office_id" value="{{ old('family_member_holds_public_office_id') }}" id="family_member_holds_public_office_id" class="form-control custom-select @error('family_member_holds_public_office_id') is-invalid @enderror inputFormu adminactividad" >
                                                                <option value="" selected>Selecciona una opción</option>
                                                                @foreach($publicOfficeEmployees as $item)
                                                                <option value="{{$item['id']}}">{{$item['description']}}</option>
                                                                @endforeach
                                                            </select>
                                                            @error('family_member_holds_public_office_id')
                                                                <span class="invalid-feedback" role="alert">
                                                                    <strong style="color:red!important">{{ $message }}</strong>
                                                                </span>
                                                            @enderror
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>   
                                            <div class="form-group mb-0">
                                                <div class="form-row">
                                                    <div class="col-xs-12 col-md-6 col-lg-6 1 box">
                                                        <label class="col-form-label form-label col-xs-12 col-md-12 col-lg-12 adminactividad" style="padding:0; font-size: 11px!important;">¿ERES UNA PERSONA EXPUESTA POLÌTICAMENTE?</label>
                                                        <select onchange="validar()" name="is_politically_exposed" value="{{ old('is_politically_exposed') }}" id="is_politically_exposed" class="form-control custom-select @error('is_politically_exposed') is-invalid @enderror inputFormu adminactividad" >
                                                            <option value="" selected>Selecciona una opción</option>
                                                            <option value="1">Si</option>
                                                            <option value="0">No</option>
                                                        </select>
                                                        @error('is_politically_exposed')
                                                                <span class="invalid-feedback" role="alert">
                                                                    <strong style="color:red!important">{{ $message }}</strong>
                                                                </span>
                                                        @enderror
                                                    </div>
                                                    <div class="col-xs-12 col-md-6 col-lg-6 1 box">
                                                        <label class="col-form-label form-label col-xs-12 col-md-12 col-lg-12 adminactividad" style="padding:0; font-size: 11px!important;">¿ESTÀS DECLARADO EN BANCA ROTA?</label>
                                                        <select onchange="validar()" name="is_declared_bankrupt" value="{{ old('is_declared_bankrupt') }}" id="is_declared_bankrupt" class="form-control custom-select @error('is_declared_bankrupt') is-invalid @enderror inputFormu adminactividad" >
                                                            <option value="" selected>Selecciona una opción</option>
                                                            <option value="1">Si</option>
                                                            <option value="0">No</option>
                                                        </select>
                                                        @error('is_declared_bankrupt')
                                                                <span class="invalid-feedback" role="alert">
                                                                    <strong style="color:red!important">{{ $message }}</strong>
                                                                </span>
                                                        @enderror
                                                    </div>
                                            
                                                </div>
                                            </div>   
                                            
                                    

                                    <h2 class="fs-title adminempresa">Datos de la empresa</h2>

                                   
                                    
                                            <div class="form-group row align-items-center mb-0">
                                                <label class="col-form-label form-label col-sm-12 adminempresa">RFC</label>
                                                <div class="col-sm-12">
                                                    
                                                    <input onkeyup="validar()" onkeypress="return validarNumerosLetras(event)" minlength="10" maxlength="80" type="text" value="{{ old('rfc_business') }}" name="rfc_business" id="rfc_business" class="form-control @error('rfc_business') is-invalid @enderror inputFormu mayus adminempresa" placeholder="" />
                                                    @error('rfc_business')
                                                        <span class="invalid-feedback" role="alert">
                                                            <strong style="color:red!important">{{ $message }}</strong>
                                                        </span>
                                                    @enderror
                                                </div>
                                            </div>
                                          
                                            <div class="form-group mb-0">
                                                <div class="form-row">
                                                    <div class="col-xs-12 col-md-6 col-lg-6">
                                                        <label class="col-form-label form-label col-xs-12 col-md-12 col-lg-12 adminempresa" style="padding:0;">PAIS</label>
                                                        <select onchange="validar()" id="country_company" name="country_business" value="{{ old('country_business') }}" class="form-control custom-select @error('country_business') is-invalid @enderror inputFormu adminempresa" required>
                                                        </select>
                                                        @error('country_business')
                                                            <span class="invalid-feedback" role="alert">
                                                                <strong style="color:red!important">{{ $message }}</strong>
                                                            </span>
                                                        @enderror
                                                    </div>
                                                    <div class="col-xs-12 col-md-6 col-lg-6">
                                                        <div class="form-row">
                                                            <div class="col-xs-12 col-md-6 col-lg-6">
                                                                <label class="col-form-label form-label col-xs-12 col-md-12 col-lg-12 adminempresa" style="padding:0; font-size:0.8rem!important;">CIUDAD</label>
                                                                <div class="col-xs-12 col-md-12 col-lg-12" style="padding:0;">
                                                                    <input onkeyup="validar()" onkeypress="return validarCaracteres(event)" type="text" name="city_business" value="{{ old('city_business') }}" class="form-control col-xs-12 col-md-12 col-lg-12 @error('city_business') is-invalid @enderror inputFormu mayus adminempresa" placeholder=""/>
                                                                    @error('city_business')
                                                                        <span class="invalid-feedback" role="alert">
                                                                            <strong style="color:red!important">{{ $message }}</strong>
                                                                        </span>
                                                                    @enderror
                                                                </div>
                                                            </div>
                                                            <div class="col-xs-12 col-md-6 col-lg-6">
                                                                <label class="col-form-label form-label col-xs-12 col-md-12 col-lg-12 adminempresa" style="padding:0; font-size:0.8rem!important;">CÓDIGO POSTAL</label>
                                                                <div class="col-xs-12 col-md-12 col-lg-12 adminempresa" style="padding:0;">
                                                                    <input onkeyup="validar()" onkeypress="return validarNumeros(event)" type="text" pattern="^[0-9]\d{2,10}$" name="code_postal_business" value="{{ old('code_postal_business') }}" class="form-control col-xs-12 col-md-12 col-lg-12 @error('code_postal_business') is-invalid @enderror inputFormu" placeholder=""/>
                                                                    @error('code_postal_business')
                                                                        <span class="invalid-feedback" role="alert">
                                                                            <strong style="color:red!important">{{ $message }}</strong>
                                                                        </span>
                                                                    @enderror
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>   
                                            <div class="form-group mb-0">
                                                <div class="form-row">
                                                    <div class="col-xs-12 col-md-12 col-lg-12">
                                                        <label class="col-form-label form-label col-xs-12 col-md-12 col-lg-12 adminempresa" style="padding:0;">DOMICILIO FISCAL</label>
                                                        <div class="col-xs-12 col-md-12 col-lg-12" style="padding:0;">
                                                            <input onkeyup="validar()" type="text" name="direction_company" value="{{ old('direction_company') }}" class="form-control col-xs-12 col-md-12 col-lg-12 @error('direction_company') is-invalid @enderror inputFormu mayus adminempresa" pattern="^[A-Za-z0-9äÄëËïÏöÖüÜáéíóúáéíóúÁÉÍÓÚÂÊÎÔÛâêîôûàèìòùÀÈÌÒÙñÑ, ]{10,100}$" placeholder=""/> 
                                                            @error('direction_company')
                                                                <span class="invalid-feedback" role="alert">
                                                                    <strong style="color:red!important">{{ $message }}</strong>
                                                                </span>
                                                            @enderror
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>   
                                            
                                  

                                </div> <input type="button" name="previous" class="previous btn btn btn-default" value="Anterior" /> <input type="button" name="next" class="next btn btn-primary" value="Siguiente" />
                            </fieldset>
                            <fieldset>
                                <div class="form-card">
                                    <h2 class="fs-title">Documentos</h2>
                                    
    
                                            <div class="form-row col-12 col-md-12 col-lg-12">
                                                <div class="col-12 col-md-12 col-lg-12" style="background-color: #eaf6f9; display:flex; height:60px!important">
                                                    <label class="col-12 col-md-12 col-lg-12 text-center" style="margin: auto!important;">Te recomendamos que cada archivo sea menor a <span style="color: black; font-weight:bold;">3 MB</span></label>
                                                </div>
                                            </div><br><br>
                                            <div class="form-group mb-0">
                                                <div class="form-row @error('type_identity_oficial') is-invalid @enderror">
                                                    <div class="col-12 col-md-6 col-lg-6" style="display: inline!important   ">
                                                        <div class="form-row">
                                                            <img src="{{ asset('images/axin/Identificacion_oficial.png') }}" class="col-form-label form-label col-2 col-md-2 col-lg-2" style="bottom: 13px;" alt="">
                                                            <label class="col-form-label form-label col-10 col-md-10 col-lg-10" style="padding:0; color: black!important">IDENTIFICACIÓN OFICIAL</label>
                                                        </div>
                                                        <label class="col-form-label form-label col-12 col-md-12 col-lg-12" style="padding:0; font-size:0.6rem!important; bottom:20px;">Asegurate que esté vigente. En formato JPG o PNG.</label>
                
                                                    </div>
                                                    <div class="col-12 col-md-6 col-lg-6">
                                                        <div class="form-row">
                                                            <div class="col-6 col-md-6 col-lg-6">
                                                                <div class="col-12 col-md-12 col-lg-12" style="padding:0;">
                                                                    <button type="button" id="changeIdentificacion0" onclick="changeIdentificacion(0)" class="form-control col-12 col-md-12 col-lg-12" value="IFE/INE" style="color: #185bc3!important">IFE/INE</button>
                                                                </div>
                                                            </div>
                                                            <div class="col-6 col-md-6 col-lg-6">
                                                                <div class="col-12 col-md-12 col-lg-12" style="padding:0;">
                                                                    <button type="button" id="changeIdentificacion1" onclick="changeIdentificacion(1)" class="form-control col-12 col-md-12 col-lg-12" value="Pasaporte" style="color: #185bc3!important">Pasaporte</button>
                                                                </div> 
                                                            </div>
                                                        </div>
                                                    </div>
                                                    
                                                </div>
                                                @error('type_identity_oficial')
                                                        <span class="invalid-feedback" role="alert">
                                                            <strong style="color:red!important">{{ $message }}</strong>
                                                        </span>
                                                @enderror
                                            </div>
                                            <input hidden type="text" value="{{ old('type_identity_oficial') }}" id="type_identity_oficial"  name="type_identity_oficial">
                                            <div class="form-group mb-0">
                                                <div class="text-center col-12"><h8 class="invalid" style="color:red;"></h8></div>
                                                <div class="form-row">
                                                    <div class="col-12 col-md-12 col-lg-12">
                                                        <div class="file-input @error('file_document_identity_oficial_V1') is-invalid @enderror" id="file_document_identity_oficial_V1">
                                                            <input value="{{ old('file_document_identity_oficial_V1') }}" id="file_document_identity_oficial_V1" name="file_document_identity_oficial_V1" type="file" accept="image/jpeg,image/jpg,image/png" required>
                                                            <span style="word-wrap: break-word!important;" class='button' data-js-label>+ Selecciónar frente</span>
                                                        </div>
                                                          
                                                        @error('file_document_identity_oficial_V1')
                                                        <span class="invalid-feedback" role="alert">
                                                            <strong style="color:red!important">{{ $message }}</strong>
                                                        </span>
                                                        @enderror
                                                    </div>
                                                </div>
                                            </div>   
                                            <div class="form-group mb-0">
                                                <div class="form-row">
                                                    <div class="col-12 col-md-12 col-lg-12">
                                                        <div class="file-input @error('file_document_identity_oficial_V2') is-invalid @enderror" id="file_document_identity_oficial_V2">
                                                            <input value="{{ old('file_document_identity_oficial_V2') }}" id="file_document_identity_oficial_V2" name="file_document_identity_oficial_V2" type='file' accept="image/jpeg,image/jpg,image/png" required>
                                                            <span style="word-wrap: break-word!important;" class='button' data-js-label>+ Selecciónar el reverso</span>
                                                        </div>
                                                        
                                                        @error('file_document_identity_oficial_V2')
                                                        <span class="invalid-feedback" role="alert">
                                                            <strong style="color:red!important">{{ $message }}</strong>
                                                        </span>
                                                        @enderror
                                                    </div>
                                                    
                                                </div>
                                            </div>
                
                                            <div class="form-group mb-0">
                                                <div class="form-row">
                                                    <div class="col-12 col-md-6 col-lg-6" style="display: inline!important   ">
                                                        <div class="form-row">
                                                            <img src="{{ asset('images/axin/Domicilio.png') }}" class="col-form-label form-label col-2 col-md-2 col-lg-2" style="bottom: 13px;" alt="">
                                                            <label class="col-form-label form-label col-10 col-md-10 col-lg-10" style="padding:0; color: black!important">DOMICILIO</label>
                                                        </div>
                                                    </div><BR></BR>
                                                    <label class="col-form-label form-label col-12 col-md-12 col-lg-12" style="padding:0; font-size:0.6rem!important; bottom:20px;">Puedes subir recibo de gas, internet, luz, estado de cuenta, no mayor a 3 meses.</label>
                                                    <label class="col-form-label form-label col-12 col-md-12 col-lg-12" style="padding:0; font-size:0.6rem!important; bottom:20px;">En formato JPG, PNG o PDF.</label>
                                                </div>
                                            </div>
                
                                            <div class="form-group mb-0">
                                                <div class="form-row">
                                                    <div class="col-12 col-md-12 col-lg-12">
                                                        <div class="file-input @error('file_document_home') is-invalid @enderror">
                                                            <input value="{{ old('file_document_home') }}" name="file_document_home" type='file' accept="image/jpeg,image/jpg,image/png,application/pdf" required>
                                                            <span style="word-wrap: break-word!important;" class='button' data-js-label>+ Selecciónar archivo</span>
                                                        </div>
                                                        
                                                        @error('file_document_home')
                                                        <span class="invalid-feedback" role="alert">
                                                            <strong style="color:red!important">{{ $message }}</strong>
                                                        </span>
                                                        @enderror
                                                    </div>
                                                    
                                                </div>
                                            </div>  
                                            
                                     
                                </div> <input type="button" name="previous" class="previous btn btn btn-default" value="Anterior" /> <button type="submit" class="btn btn-primary">Guardar</button>
                            </fieldset>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>


@endsection