@extends('layoutAdmins')

@section('content')

<div class="container page__container">
    <style>
        ::placeholder { 
            color: #185bc3!important;
            opacity: 1; 
        }
        input:invalid {
			border-color: #DD2C00!important;
        }

        select:invalid {
			border-color: #DD2C00!important;
        }

        .check-ok {
            color:#185bc3!important;
            position: inherit; right: -20px; top: 10px!important;
        }
        
        .check-No {
            color: red!important;
            position: inherit; right: -20px; top: 10px!important;
        }
    
        input:invalid ~ .check-ok {
            display: none!important;
        }

        input:valid ~ .check-ok {
            display: inline!important;
        }
        input:invalid ~ .check-No {
            display: inline!important;
        }

        input:valid ~ .check-No {
            display: none!important;
        }

        select:invalid ~ .check-ok {
            display: none!important;
        }

        select:valid ~ .check-ok {
            display: inline!important;
        }
        
    </style>
    <form method="POST"  action="{{route('admin.socios.subir_comprobante')}}">
        <input type="hidden" name="_token" value="{{ csrf_token() }}"></input>
        <div class="row">
            <div class="col-lg-12 pr-lg-0">

                <div class="page-section">
                    <h4>Banca - Comprobar depositos</h4>
                    <br><br><br>
                    <div class="list-group list-group-form">
                        <div class="list-group-item space">
                            <br>
                            <h6 class="text-center">Subir depositos sin comprobante</h6>
                            <br><br>  
                            <div class="form-group row align-items-center mb-0">
                                <label class="col-form-label form-label col-sm-12">NOMBRE</label>
                                <div class="col-sm-12">
                                    <input name="partner_id" value="{{ $partner->id }}" type="hidden" class="form-control inputFormu @error('partner_id') is-invalid @enderror" placeholder="" required readonly />
                                    <input name="name" value="{{ $partner->user->name }} {{ $partner->user->first_last_name }} {{ $partner->user->second_last_name }}" onkeyup="validar()" type="text" class="form-control inputFormu @error('name') is-invalid @enderror" placeholder="" required readonly /><i class="fa fa-check check-ok" style="color: #185bc3!important;position: absolute;right: 43px;top: 10px!important;"></i>
                                    @error('name')
                                        <span class="invalid-feedback" role="alert">
                                            <strong style="color:red!important">{{ $message }}</strong>
                                        </span>
                                    @enderror
                                </div>
                            </div>
                            <div class="form-group row align-items-center mb-0">
                                <label class="col-form-label form-label col-sm-12">CORREO ELÉCTRONICO</label>
                                <div class="col-sm-12">
                                    <input name="email" value="{{ $partner->user->email }}" onkeyup="validar()" type="email" class="form-control inputFormu @error('email') is-invalid @enderror" placeholder=""  required readonly /><i class="fa fa-check check-ok" style="color: #185bc3!important;position: absolute;right: 43px;top: 10px!important;"></i>
                                    @error('email')
                                        <span class="invalid-feedback" role="alert">
                                            <strong style="color:red!important">{{ $message }}</strong>
                                        </span>
                                    @enderror
                                </div>
                            </div>
                            <div class="form-group row align-items-center mb-0">
                                <label class="col-form-label form-label col-sm-12">CUENTA DE INVERSIÓN</label>
                                <div class="col-sm-12">
                                    <select name="account_partner_id" value="{{ old('account_partner_id') }}" onclick="validar()" id="account_partner_id" class="form-control custom-select inputFormu @error('account_partner_id') is-invalid @enderror" required>
                                        <option value="" selected>Selecciona la cuenta del deposito</option>
                                        @foreach($accounts as $account)
                                        <option value="{{$account->id}}">{{$account->typeaccount->name}} - {{$account->account_number}}</option>  
                                        @endforeach 
                                    </select><i class="fa fa-check check-ok" style="color: #185bc3!important;position: absolute;right: 43px;top: 10px!important;"></i>
                                    @error('account_partner_id')
                                        <span class="invalid-feedback" role="alert">
                                            <strong style="color:red!important">{{ $message }}</strong>
                                        </span>
                                    @enderror
                                </div>
                            </div>
                            <div class="form-group row align-items-center mb-0">
                                <label class="col-form-label form-label col-sm-12">CANTIDAD</label>
                                <div class="col-sm-12">
                                    <input name="amount" value="" onkeyup="validar()" type="text" class="form-control inputFormu @error('amount') is-invalid @enderror" placeholder="" required /><i class="fa fa-check check-ok" style="color: #185bc3!important;position: absolute;right: 43px;top: 10px!important;"></i>
                                    @error('amount')
                                        <span class="invalid-feedback" role="alert">
                                            <strong style="color:red!important">{{ $message }}</strong>
                                        </span>
                                    @enderror
                                </div>
                            </div>
                            <div class="form-group row align-items-center mb-0">
                                <label class="col-form-label form-label col-sm-12">Fecha de deposito</label>
                                <div class="col-sm-12">
                                    <input name="created_at" value="" onkeyup="validar()" type="date" class="form-control inputFormu @error('created_at') is-invalid @enderror" placeholder="" required /><i class="fa fa-check check-ok" style="color: #185bc3!important;position: absolute;right: 43px;top: 10px!important;"></i>
                                    @error('created_at')created_at
                                        <span class="invalid-feedback" role="alert">
                                            <strong style="color:red!important">{{ $message }}</strong>
                                        </span>
                                    @enderror
                                </div>
                            </div>
                            <br>
                            <div class="form-group row align-items-center mb-0">
                                <div class="col-sm-12" style="display: flex; justify-content:center;">
                                    <button class="btn btn-accent botonSiguienteP" style="background-color: #185bc3; width:15rem;color:white"  type="submit" >
                                        <span class="spinner-text">Enviar</span>
                                        <span class="spinner-grow spinner-grow-sm" role="status" aria-hidden="true"></span>
                                        <span class="spinner-grow spinner-grow-sm" role="status" aria-hidden="true"></span>
                                        <span class="spinner-grow spinner-grow-sm" role="status" aria-hidden="true"></span>
                                    </button>
                                </div>
                            </div>
                            
                        </div>
                
                    </div>
                </div>

            </div>

        </div>
    </form>

</div>

@endsection