@extends('layoutAdmins')

@section('content')
<div class="m-4"> 
    <br>
    <h1 class="text-center">Movimientos</h1>
    <br>
    <div class="table-responsive">
        <table class="table">
            <thead>
                <tr>
                    <th>CUENTA</th>
                    <th>CONCEPTO</th>
                    <th>FECHA</th>
                    <th>MONTO</th>
                    <th>ESTADO</th>     
                    <th></th>                
                </tr>
            </thead>
            <tbody>
                @foreach ( $clientWithDrawalRequests as $clientWithDrawalRequest )
                    <tr>
                        <td>{{$clientWithDrawalRequest['name']}} - {{$clientWithDrawalRequest['account_number']}}</td>
                        <td>{{$clientWithDrawalRequest['concept']}}</td>
                        <td>{{$clientWithDrawalRequest['date_deposite']}}</td>
                        <td>
                            @if ($clientWithDrawalRequest['amount'] == 0)
                            ---------
                            @else
                            ${{$clientWithDrawalRequest['amount']}} USD
                            @endif
                        </td>
                        <td>{{$clientWithDrawalRequest['status']}}</td>
                        <td>
                            <a href="{{route('admin.socios.editar_movimientos', $clientWithDrawalRequest['id'])}}" class="btn btn-primary btn-block" style="color:white" type="submit">
                                <span class="spinner-text">Editar</span>
                            </a>
                        </td>
                    </tr>
                @endforeach
                    

            </tbody>
        </table>
        {{ $clientWithDrawalRequests->links() }}
    </div>
</div>
@endsection