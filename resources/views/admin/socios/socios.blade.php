@extends('layoutAdmins')

@section('content')
    <style>
        .activa {
            background-color: #185bc3!important;
            color: white!important;
        }
    </style>
    <div class="m-4">
        <ul class="nav nav-tabs nav-tabs-card">
            <li class="nav-item">
                <a class="nav-link" href="{{ route('rendimiento.clientes.depositos') }}">Depositos</a>
            </li>
            <li class="nav-item">
                <a class="nav-link" href="{{ route('rendimiento.clientes.retirarFondos') }}">Retiros</a>
            </li>
            <li class="nav-item">
                <a class="nav-link" href="{{ route('rendimiento.clientes.movimientos') }}">Movimientos</a>
            </li>
            <li class="nav-item">
                <a class="nav-link" href="{{ route('rendimiento.clientes.operaciones') }}">Operaciones</a>
            </li>
            <li class="nav-item">
                <a class="nav-link activa" href="{{ route('admin.socios.socios') }}">+ Depositos</a>
            </li>
        </ul>
        <br>
        <br>
        <form action="{{ route('admin.socios.socios') }}" method="GET" class="form-inline float-left">
            <div class="form-group">
                <input type="text" name="name" class="form-control" placeholder="Nombre">
            </div>
            <div class="form-group">
                <input type="text" name="email" class="form-control" placeholder="Correo electronico">
            </div>
            <div class="form-group">
                <button type="submit" class="btn btn-default"><span class="fas fa-search"></span></button>
            </div>
        </form>
        <div class="table-responsive">
        <table class="table">
            <thead>
                <tr>
                    <th>partnere</th>
                    <th>Pais</th>
                    <th>Correo</th>
                    <th>Telefono</th>
                    <th></th>
                    <th></th>
                </tr>
            </thead>
            <tbody>
            @foreach ( $partners as $partner )
                 <tr>
                    <td>{{$partner->name}} {{$partner->first_last_name}} {{$partner->second_last_name}}</td>
                    <td>{{$partner->birth_date_country}}</td>
                    <td>{{$partner->email}}</td>
                    <td>{{$partner->number_phone}}</td>
                    <td>
                        <a href="{{route('admin.socios.comprobante', $partner->id)}}" class="btn btn-primary btn-block" style="color:white" type="submit">
                            <span class="spinner-text">Crear depositos</span>
                        </a>
                    </td>                     
                    <td>
                        <a href="{{route('admin.socios.movimientossocio', $partner->id)}}" class="btn btn-primary btn-block" style="color:white" type="submit">
                            <span class="spinner-text">Historial</span>
                        </a>
                    </td>              
                </tr>

            @endforeach
            </tbody>
        </table>
        {{ $partners->links() }}
        
    </div>
</div>
@endsection

