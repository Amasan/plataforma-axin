@extends('layoutAdmins')

@section('content')
    <style>
        .activa {
            background-color: #185bc3!important;
            color: white!important;
        }
    </style>
    <div class="m-4">
        <ul class="nav nav-tabs nav-tabs-card">
            <li class="nav-item">
                <a class="nav-link" href="{{ route('rendimiento.clientes.depositos') }}">Depositos</a>
            </li>
            <li class="nav-item">
                <a class="nav-link" href="{{ route('rendimiento.clientes.retirarFondos') }}">Retiros</a>
            </li>
            <li class="nav-item">
                <a class="nav-link" href="{{ route('rendimiento.clientes.movimientos') }}">Movimientos</a>
            </li>
            <li class="nav-item">
                <a class="nav-link" href="{{ route('rendimiento.clientes.operaciones') }}">Operaciones</a>
            </li>
            <li class="nav-item">
                <a class="nav-link activa" href="{{ route('admin.clientes.clientes') }}">+ Depositos</a>
            </li>
        </ul>
        <br>
        <br>
        <form action="{{ route('admin.clientes.clientes') }}" method="GET" class="form-inline float-left">
            <div class="form-group">
                <input type="text" name="name" class="form-control" placeholder="Nombre">
            </div>
            <div class="form-group">
                <input type="text" name="email" class="form-control" placeholder="Correo electronico">
            </div>
            <div class="form-group">
                <button type="submit" class="btn btn-default"><span class="fas fa-search"></span></button>
            </div>
        </form>
        <div class="table-responsive">
        <table class="table">
            <thead>
                <tr>
                    <th>Cliente</th>
                    <th>Pais</th>
                    <th>Correo</th>
                    <th>Telefono</th>
                    <th></th>
                    <th></th>
                </tr>
            </thead>
            <tbody>
            @foreach ( $clients as $client )
                 <tr>
                    <td>{{$client->name}} {{$client->first_last_name}} {{$client->second_last_name}}</td>
                    <td>{{$client->birth_date_country}}</td>
                    <td>{{$client->email}}</td>
                    <td>{{$client->number_phone}}</td>
                    <td>
                        <a href="{{route('admin.clientes.comprobante', $client->id)}}" class="btn btn-primary btn-block" style="color:white" type="submit">
                            <span class="spinner-text">Crear depositos</span>
                        </a>
                    </td>                     
                    <td>
                        <a href="{{route('admin.clientes.movimientoscliente', $client->id)}}" class="btn btn-primary btn-block" style="color:white" type="submit">
                            <span class="spinner-text">Historial</span>
                        </a>
                    </td>            
                </tr>

            @endforeach
            </tbody>
        </table>
        {{ $clients->links() }}
        
    </div>
</div>
@endsection

