<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title></title>
    
    <style>
      
        #logo {
            background-color: #1d69f3!important;
        }
        img {
            width: 150px!important;
            display: block;
            margin: auto;
        }
        #subject {
            padding-bottom: 10px!important;
            padding-left: 50px!important;
            padding-right: 50px!important;
        }
        h1, p {
            text-align: center!important;
            font-family: Arial, Helvetica, sans-serif!important;
            color: black!important;
            margin-left: 40px!important;
            margin-right: 40px!important;
        }

        .text-justify {
            text-align: justify!important;
        }
        .link {
            color:#406edd!important;
            text-align: center!important;
            margin: auto!important;
            padding-top: 4px!important; 
            padding-bottom: 4px!important;
            padding-left: 15px!important; 
            padding-right: 15px!important;
            word-wrap: break-word!important;
            -webkit-box-shadow: 0 5px 22px 1px #406edd;
	        -moz-box-shadow: 0 5px 22px 1px #406edd;
	        box-shadow: 0 5px 22px 1px #406edd;
        }
        .box {
            background: #ebf6fc!important; 
            border: #406edd 2px dashed!important;
            margin-left: 20px!important;
            margin-right: 20px!important;
        }
        .boton {
            font-family: Arial, Helvetica, sans-serif!important;
            background-color: #1d69f3!important;
            border: none!important;
            border-radius: 10px!important;
            padding: 8px!important;
            display: flex!important;
            align-items: center!important;
            height: 30px!important;
            width: 250px!important;
            color: white!important;
            font-weight: bold!important;
            text-align: center!important;
            margin: auto!important;
            text-decoration: none!important;
            -webkit-box-shadow: 0 5px 22px 6px gray;
	        -moz-box-shadow: 0 5px 22px 6px gray;
	        box-shadow: 0 5px 22px 6px gray;
        }
        .head {
            color: white!important;
            font-family: Arial, Helvetica, sans-serif!important;
            font-size: 50px!important;
            font-weight: bold!important;
            text-align: center!important;
        }
        .borde {
            margin-left: 20vw!important;
            margin-right: 20vw!important;
            -webkit-box-shadow: -1px 7px 26px -5px rgba(0,0,0,0.75)!important;
            -moz-box-shadow: -1px 7px 26px -5px rgba(0,0,0,0.75)!important;
            box-shadow: -1px 7px 26px -5px rgba(0,0,0,0.75)!important;
            border: .5px solid gray;
        }
        .shadow1 {
            color: white;
            -webkit-text-stroke: 0px #F8F8F8;
            text-shadow: 0px 1px 4px #23430C;
        }

        .shadow2 {
            color: white;
            -webkit-text-stroke: 0px #F8F8F8;
            text-shadow: 0px 2px 4px rgb(0, 0, 255);
        }

        .shadow3 {
            color: #333;
            -webkit-text-stroke: 0px #282828;
            text-shadow: 0px 4px 4px #282828;
        }
        
        @media (max-width: 600px) {
            .borde {
                margin-left: 0px!important;
                margin-right: 0px!important;
                border: .5px solid gray;
            }
        }
    </style>
</head>
<body>
    <div class="borde">
        <section id="logo">   

            <img src="https://amaliath3code.com/images/axin/Logo-Axin.png" />

        </section>
        <br>
        <section id="subject">
            <h1 class="shadow3">HOLA, {{$user->name}} {{$user->first_last_name}} {{$user->second_last_name}}:</h1><br><br>
            <p class="text-justify">
                ¡Felicidades!, has sido verificado ahora tienes disponible 3 cuentas AXIN, te compartimos los detalles: <br>
                Ahorro <br>
                Inversionista <br>
                Mercado <br><br>

                Puedes activarlas en cualquier momento haciendo un deposito inicial de: <br>
                Ahorro $100 USD.    <br>
                Inversionista $1000 USD.    <br>
                Mercado $1000 USD.  <br><br>
                Saludos.    <br><br>
                Equipo AXIN.    <br>
            </p>

            <br><br>
            <hr>
            <p>Este es un mensaje automático desde una dirección válida sólo para notificaciones que no puede aceptar correos electrónicos entrantes.</p>
            <p><strong>Por favor no respondas este mensaje.</strong></p>
            <br><br>
            <p>&copy; 2020 Axin Capital LLC</p>
        </section>
    </div>
</body>
</html>