<!DOCTYPE html>
<html lang="es" dir="ltr">

<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <link rel="shortcut icon" href="{{{ asset('images/axin/favicon.ico') }}}">
    <title>Portal Axin</title>

    {{-- Prevent the demo from appearing in search engines --}}
    <meta name="robots" content="noindex">

    <link href="https://fonts.googleapis.com/css?family=Lato:400,700%7COswald:300,400,500,700%7CRoboto:400,500%7CExo+2:600&display=swap" rel="stylesheet">

    {{-- Custom Styles --}}
    <link type="text/css" href="{{ asset('css/personalizados.css') }}" rel="stylesheet">
    <link type="text/css" href="{{ asset('css/wizar.css') }}" rel="stylesheet">

 

    {{-- Perfect Scrollbar --}}
    <link type="text/css" href="{{ asset('vendor/perfect-scrollbar.css') }}" rel="stylesheet">

    {{-- Material Design Icons --}}
    <link type="text/css" href="{{ asset('css/material-icons.css') }}" rel="stylesheet">
    <link type="text/css" href="{{ asset('css/material-icons.rtl.css') }}" rel="stylesheet">

    {{-- Font Awesome Icons --}}
    <link type="text/css" href="{{ asset('css/fontawesome.css') }}" rel="stylesheet">
    <link type="text/css" href="{{ asset('css/fontawesome.rtl.css') }}" rel="stylesheet">

    {{-- Preloader --}}
    <link type="text/css" href="{{ asset('css/preloader.css') }}" rel="stylesheet">
    <link type="text/css" href="{{ asset('css/preloader.rtl.css') }}" rel="stylesheet">

    {{-- App CSS --}}
    <link type="text/css" href="{{ asset('css/app.css') }}" rel="stylesheet">
    <link type="text/css" href="{{ asset('css/app.rtl.css') }}" rel="stylesheet">

    <link rel="stylesheet" href="{{ asset('css/intlTelInput.css') }}">
    <style>
        .iti__flag {background-image: url("{{ asset('images/flags.png') }}");}
  
        @media (-webkit-min-device-pixel-ratio: 2), (min-resolution: 192dpi) {
            .iti__flag {background-image: url("{{ asset('images/flags@2x.png') }}");}
        }

        #lista-notifications-new, #lista-notifications {
            padding: 0;
        }
    </style>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.5.1/jquery.js" integrity="sha512-WNLxfP/8cVYL9sj8Jnp6et0BkubLP31jhTG9vhL/F5uEZmg5wEzKoXp1kJslzPQWwPT1eyMiSxlKCgzHLOTOTQ==" crossorigin="anonymous"></script>
    <script src="https://cdn.jsdelivr.net/npm/axios/dist/axios.min.js"></script>
    <!-- The core Firebase JS SDK is always required and must be listed first -->
    <script src="https://www.gstatic.com/firebasejs/7.17.2/firebase-app.js"></script>
    <script src="https://www.gstatic.com/firebasejs/7.17.2/firebase-messaging.js"></script>
    <script>
        // Your web app's Firebase configuration
        var firebaseConfig = {
            apiKey: "AIzaSyBCFlUMyv9cOB1v4qz98VphHB7Ubxvar6E",
            authDomain: "axin-ebb8f.firebaseapp.com",
            databaseURL: "https://axin-ebb8f.firebaseio.com",
            projectId: "axin-ebb8f",
            storageBucket: "axin-ebb8f.appspot.com",
            messagingSenderId: "496139219905",
            appId: "1:496139219905:web:5c6d96e585bcc150b63540",
            measurementId: "G-QLYQX9DJ5D"
        };
        // Initialize Firebase
        firebase.initializeApp(firebaseConfig);

    </script>

    <script>
        var acount = 0;
        const messaging = firebase.messaging();

        messaging.usePublicVapidKey("BHct0vxpmMfffPvVtb2yJPqpLI5Uan4kv9OET5U-9tk6iv3YlfO0iYXi4VAML_ag74b-iqsFviJCHaMKD2wV5_8");
        
        function sendTokenToServer(fcm_token) {

            const user_id = '{{Auth::user()->id}}';
            console.log(user_id);
            axios.post('/api/save-token', {
                'fcm_token': fcm_token, 
                'user_id': user_id
            }).then(res => {
                console.log(res);
            }); 
        }

        function retrieveToken() {
            // Get Instance ID token. Initially this makes a network call, once retrieved
            // subsequent calls to getToken will return from cache.
            messaging.getToken().then((currentToken) => {
                if (currentToken) {
                    console.log('Token received :' + currentToken)
                    sendTokenToServer(currentToken);
                    // updateUIForPushEnabled(currentToken);
                } else {
                    alert('Debes permitir las notificaciones');
                }
            }).catch((err) => {
                console.log('An error occurred while retrieving token. ', err);
                // showToken('Error retrieving Instance ID token. ', err);
                // setTokenSentToServer(false);
            });

        }

        retrieveToken();

        messaging.onTokenRefresh(() => {
            retrieveToken();
        });

        function list() {
            $.get("/notificaciones", function( data ) {
                $("#lista-notifications").empty();
                console.log(data.length);
                acount = data.length;
                $("#acount").html(acount);
                data.forEach(function(item){
                    var li = document.createElement("li");
                    var a = document.createElement("a");
                    li.setAttribute('style','list-style:none;');
                    a.setAttribute('href',item.data.link);
                    a.setAttribute('class','dropdown-item');
                    a.appendChild(document.createTextNode(item.data.text));
                    document.querySelector("#lista-notifications").appendChild(li).appendChild(a);
                }); 
            });
        }

        list();  
        
        messaging.onMessage((payload) => {
            console.log('Message received');
            console.log('payload',payload);
            acount = acount+1;
            $("#acount").html(acount);
            var li = document.createElement("li");
            var a = document.createElement("a");
            li.setAttribute('style','list-style:none;');
            a.setAttribute('href',payload.notification.click_action);
            a.setAttribute('class','dropdown-item');
            a.appendChild(document.createTextNode(payload.notification.title));
            document.querySelector("#lista-notifications-new").appendChild(li).appendChild(a);

        });


    </script>

</head>

<body class="layout-compact layout-sticky-subnav layout-compact">























    <div class="preloader">
        <div class="sk-double-bounce">
            <div class="sk-child sk-double-bounce1"></div>
            <div class="sk-child sk-double-bounce2"></div>
        </div>
    </div>

    <div class="mdk-drawer-layout js-mdk-drawer-layout" data-push data-responsive-width="992px">
        <div class="mdk-drawer-layout__content page-content">

            {{-- Header --}}





            <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                 @csrf
            </form>
            <div style="background-color: white!important;" class="navbar navbar-expand navbar-shadow px-0  pl-lg-16pt navbar-light bg-body" id="default-navbar" data-primary>
                {{-- Navbar toggler --}}
                <button class="navbar-toggler d-block d-lg-none rounded-0" type="button" data-toggle="sidebar">
                    <span class="material-icons">menu</span>
                </button>
                <div class="flex"></div>

                <div class="nav navbar-nav flex-nowrap d-flex ml-0 mr-16pt">
                    <div class="nav-item dropdown d-none d-sm-flex">
                        <a class="nav-link d-flex align-items-center dropdown-toggle" data-toggle="dropdown">
                            <span class="flex d-flex flex-column mr-8pt">
                                    <span style="color: #1c6af4;" class="fas fa-bell float-left"></span>
                                    @guest
                                        
                                    @else
                                        {{--  @if ($count = Auth::user()->unreadNotifications->count())  --}}
                                        <span class="badge badge-dark float-right" id="acount"></span>
                                        {{--  @endif  --}}
                                    @endguest
                            </span>
                        </a>
                        
                        <div class="dropdown-menu dropdown-menu-right">
                            @guest
                                        
                            @else
                            <ul id="lista-notifications-new">

                            </ul>
                            <ul id="lista-notifications">

                            </ul>
                            <a class="dropdown-item delete" style="color:#1c6af4;" href="{{ route('maskAsRead')}}">Borrar notificaciones</a>
                    
                        @endguest
                        </div>
                        
                    </div>
                </div>
                <div class="nav navbar-nav flex-nowrap d-flex ml-0 mr-16pt">
                    <div class="nav-item dropdown d-none d-sm-flex">
                        <a href="#" class="nav-link d-flex align-items-center dropdown-toggle" data-toggle="dropdown">
                            {{--  <img width="32" height="32" class="rounded-circle mr-8pt" src="{{ asset('images/axin/') }}" alt="account" />  --}}
                            <span class="flex d-flex flex-column mr-8pt">
                                @if(Auth::user())
                                    <span class="navbar-text-100">{{ Auth::user()->email}}</span>
                                @endif
                            </span>
                        </a>
                        <div class="dropdown-menu dropdown-menu-right">
                            <a class="dropdown-item" href="{{ route('logout') }}"
                                        onclick="event.preventDefault();
                                                    document.getElementById('logout-form').submit();">
                                        Salir
                            </a>
                            
                        </div>
                    </div>

                </div>

            </div>



              {{-- // END Header --}}

        @yield('content')







        </div>
          {{-- // END drawer-layout__content --}}



        {{-- drawer --}}
        <div class="mdk-drawer js-mdk-drawer layout-compact__drawer" id="default-drawer">
            <div class="mdk-drawer__content js-sidebar-mini" data-responsive-width="992px" data-layout="compact">

                <div class="sidebar sidebar-mini sidebar-dark sidebar-left" data-perfect-scrollbar>

                    {{-- Navbar toggler --}}
                
                    <a href="{{ url('/') }}" class="navbar-toggler navbar-toggler-custom w-auto d-flex align-items-center justify-content-center" >
                        <img width="100" height="auto" class="rounded-circle mr-4pt" src="{{ asset('images/axin/Logo-Axin.png') }}" alt="account" />
                    </a>

                    <ul class="nav flex-column sidebar-menu sm-item-bordered" id="sidebar-mini-tabs" role="tablist">
                        @if (Auth::user())
                        <li class="sidebar-menu-item hoverdocumentos {{ Request::is('legal/clientes*') ? 'active' : '' }}">
                            <a class="sidebar-menu-button" href="{{ route('legal.clientes.perfiles') }}">
                                <span class="sidebar-menu-text">Back Office Cliente</span>
                            </a>
                        </li>
                        <li class="sidebar-menu-item hoverdocumentos {{ Request::is('legal/socios*') ? 'active' : '' }}">
                            <a class="sidebar-menu-button" href="{{ route('legal.socios.perfiles') }}">
                                <span class="sidebar-menu-text">Back Office Socio</span>
                            </a>
                        </li>
                        @endif
                        @foreach (Auth::user()->roles()->get() as $rol)
                        @if($rol->id == 1 || $rol->id == 3)
                        <li class="sidebar-menu-item hoverdocumentos {{ Request::is('rendimiento/clientes*') ? 'active' : '' }}">
                            <a class="sidebar-menu-button" href="{{ route('rendimiento.clientes.depositos') }}">
                                <span class="sidebar-menu-text">Banca Cliente</span>
                            </a>
                        </li>
                        <li class="sidebar-menu-item hoverdocumentos {{ Request::is('rendimiento/socios*') ? 'active' : '' }}">
                            <a class="sidebar-menu-button" href="{{ route('rendimiento.socios.depositos') }}">
                                <span class="sidebar-menu-text">Banca Socio</span>
                            </a>
                        </li>
                        @php break; @endphp
                        @endif
                        @endforeach
                        @if(Auth::user()->roles()->first()->id == 1)
                        <li class="sidebar-menu-item hovernotificaciones {{ Request::is('admin*') ? 'active' : '' }}" >
                            <a class="sidebar-menu-button" href="{{ route('admin.registro.usuario') }}">
                                <span class="sidebar-menu-text">Registro</span>
                            </a>
                        </li>
                        @endif
                        <li class="sidebar-menu-item hovernotificaciones {{ Request::is('novedad_axin*') ? 'active' : '' }}" >
                            <a class="sidebar-menu-button" href="{{ route('notificaciones.novedades.novedad') }}">
                                <span class="sidebar-menu-text">Notificaciones</span>
                            </a>
                        </li>
                        <div class="positionButtom" style="left:38px;">
                            
                            <li class="sidebar-menu-item active">
                                <a class="sidebar-menu-button" href="{{ route('logout') }}"
                                            onclick="event.preventDefault();
                                                        document.getElementById('logout-form').submit();">
                                <img width="40" height="40" class="mr-6pt" src="{{ asset('images/axin/Salir.png')}}" alt="">
                                <span class="sidebar-menu-text">Salir</span>
                                </a>
                            </li>
                        </div>
                    </ul>
                </div>

                

            </div>
        </div>
        
    </div>

    


    {{-- Validaciones --}}
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
    <script src="{{ asset('js/validaciones.js') }}"></script>

    {{-- Peticion Post con Ajax--}}
    <script src="https://code.jquery.com/jquery-1.12.4.min.js"></script>
    <script src="{{ asset('js/postAjax.js') }}"></script>
    <script src="{{ asset('js/wizard.js') }}"></script>

        {{-- Libreria para generar codigo de telefono  --}}
        <script src="{{ asset('js/intlTelInput.js') }}"></script>
        <script>
           // get the country data from the plugin
            var countryData = window.intlTelInputGlobals.getCountryData(),
            input = document.querySelector("#phone"),
            addressDropdown = document.querySelector("#country_code");
            var countryDataB = window.intlTelInputGlobals.getCountryData(),
            addressDropdownB = document.querySelector("#birth_date_country");
            var countryDataC = window.intlTelInputGlobals.getCountryData(),
            addressDropdownC = document.querySelector("#country_company");
    
            // init plugin
            var iti = window.intlTelInput(input, {
                initialCountry: 'mx',
                separateDialCode: true,
                enablePlaceholder: false,
                utilsScript: "{{ asset('js/utils.js') }}" // just for formatting/placeholders etc
            });
    
            // populate the dialcode dropdown
            for (var i = 0; i < countryData.length; i++) {
            var country = countryData[i];
            var optionNode = document.createElement("option");
            optionNode.value = country.dialCode;
            var textNode = document.createTextNode(country.dialCode);
            optionNode.appendChild(textNode);
            addressDropdown.appendChild(optionNode);
            }
            // set it's initial value
            addressDropdown.value = iti.getSelectedCountryData().dialCode;
    
            // listen to the telephone input for changes
            input.addEventListener('countrychange', function(e) {
            addressDropdown.value = iti.getSelectedCountryData().dialCode;
            });
    
            // listen to the address dropdown for changes
            addressDropdown.addEventListener('change', function() {
            iti.setCountry(this.value);
            });
    
            // populate the country dropdown
            for (var c = 0; c < countryDataB.length; c++) {
            var countryB = countryDataB[c];
            var optionNodeB = document.createElement("option");
            optionNodeB.value = countryB.name;
            var textNodeB = document.createTextNode(countryB.name);
            optionNodeB.appendChild(textNodeB);
            addressDropdownB.appendChild(optionNodeB);
            }
            // set it's initial value
            addressDropdownB.value = iti.getSelectedCountryData().name;
            
            // listen to the telephone input for changes
            input.addEventListener('countrychange', function(e) {
            addressDropdownB.value = iti.getSelectedCountryData().name;
            });

             // populate the country company dropdown
             for (var h = 0; h < countryDataC.length; h++) {
                var countryC = countryDataC[h];
                var optionNodeC = document.createElement("option");
                optionNodeC.value = countryC.name;
                var textNodeC = document.createTextNode(countryC.name);
                optionNodeC.appendChild(textNodeC);
                addressDropdownC.appendChild(optionNodeC);
                }
                // set it's initial value
                addressDropdownC.value = iti.getSelectedCountryData().name;
                
                // listen to the telephone input for changes
                input.addEventListener('countrychange', function(e) {
                addressDropdownB.value = iti.getSelectedCountryData().name;
                });
      
        
    
        </script> 

    {{-- jQuery --}}
    <script src="{{ asset('vendor/jquery.min.js') }}"></script>

    {{-- Bootstrap --}}
    <script src="{{ asset('vendor/popper.min.js') }}"></script>
    <script src="{{ asset('vendor/bootstrap.min.js') }}"></script>

    {{-- Perfect Scrollbar --}}
    <script src="{{ asset('vendor/perfect-scrollbar.min.js') }}"></script>

    {{-- DOM Factory --}}
    <script src="{{ asset('vendor/dom-factory.js') }}"></script>

    {{-- MDK --}}
    <script src="{{ asset('vendor/material-design-kit.js') }}"></script>


    {{-- App JS --}}
    <script src="{{ asset('js/app.js') }}"></script>

    {{-- Sidebar Mini JS --}}
    <script src="{{ asset('js/sidebar-mini.js') }}"></script>
    <script>
        (function() {
            'use strict';

            // ENABLE sidebar menu tabs
            $('#sidebar-mini-tabs [data-toggle="tab"]').on('click', function(e) {
                e.preventDefault()
                $(this).tab('show')
            })
        })()
    </script>


     @yield('script')
 

</body>

</html>