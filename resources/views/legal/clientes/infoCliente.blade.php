@extends('layoutAdmins')

@section('content')
    <style>
        .activa {
            background-color: #185bc3!important;
            color: white!important;
        }
    </style>
<div class="m-4">

    <br>
    <h3 class="text-center">INFORMACIÓN DE {{$client->user->name}} {{$client->user->first_last_name}} {{$client->user->second_last_name}}</h3>
    <br>

    <div class="container">
        <form method="POST" action="{{route('legal.clientes.infoCliente.gains')}}">
            <h3 class="text-center">Ganancia</h3>
            <hr>
            <div class="row">
                <input type="hidden" name="_token" value="{{ csrf_token() }}"></input>
                <input type="hidden" name="id" class="form-control" value="{{$client->id}}">
                <div class="form-group col-6">
                    <label>Performance Fee</label>
                    <input name="performance_fee" type="number" step="0.01" pattern="^\d+(?:\.\d{1,2})?$" class="form-control" value="{{$client->performance_fee}}">
                </div>
                <div class="form-group col-6">
                    <label>Maganament Fee</label>
                    <input name="management_fee" type="number" step="0.01" pattern="^\d+(?:\.\d{1,2})?$" class="form-control" value="{{$client->management_fee}}">
                </div>
                <div class="form-group col-6">
                    <label>Inicio de contrato</label>
                    <input type="text" class="form-control" @if($contract->date_contract_init == NULL) placeholder="NA" @endif value="{{ $contract->date_contract_init }}" readonly>
                </div>
                <div class="form-group col-6">
                    <label>Fin de contrato</label>
                    <input type="text" class="form-control" @if($contract->date_contract_fin == NULL) placeholder="NA" @endif value="{{ $contract->date_contract_fin }}" readonly>
                </div>
                <div class="form-group col-12 d-flex justify-content-center">
                    <button type="submit" class="btn btn-primary">Guardar</button>
                </div>
            </div>
            <br>
        </form>
        <form>
            <h3 class="text-center">Datos personales</h3>
            <hr>
            <div class="row">
                <div class="form-group col-4">
                    <label>Nombre(s)</label>
                    <input type="text" class="form-control" value="{{$client->user->name}}">
                </div>
                <div class="form-group col-4">
                    <label>Apellido paterno</label>
                    <input type="text" class="form-control" value="{{$client->user->first_last_name}}">
                </div>
                <div class="form-group col-4">
                    <label>Apellido materno</label>
                    <input type="text" class="form-control" value="{{$client->user->second_last_name}}">
                </div>
                <div class="form-group col-4">
                    <label>Fecha de nacimiento</label>
                    <input type="date" class="form-control" value="{{$client->birth_date}}">
                </div>
                <div class="form-group col-4">
                    <label>Télefono celular</label>
                    <input type="number" class="form-control" value="{{$client->number_phone}}">
                </div>
                <div class="form-group col-4">
                    <label>Pais de nacimiento</label>
                    <select class="form-control" >
                        <option value="{{$client->birth_date_country}}">{{$client->birth_date_country}}</option>
                    </select>
                </div>
                <div class="form-group col-4">
                    <label>Ciudad</label>
                    <input type="text" class="form-control" value="{{$client->city}}">
                </div>
                <div class="form-group col-4">
                    <label>Codigo postal</label>
                    <input type="number" class="form-control" value="{{$client->code_postal}}">
                </div>
                <div class="form-group col-4">
                    <label>Dirección</label>
                    <input type="text" class="form-control" value="{{$client->user->direction}}">
                </div>
            </div>
            <br>
            <h3 class="text-center">Actividad economica</h3>
            <hr>
            <div class="row">
                <div class="form-group col-4">
                    <label>Estado de empleo</label>
                    <select  name="state_employee" value="{{ $client->state_employee }}" id="state_employee" class="form-control @error('state_employee') is-invalid @enderror " required>
                        <option value="{{ $client->state_employee }}"> {{$client->state_employee}}</option>
                        @if($client->state_employee != 'Empleado')<option value="Empleado">Empleado</option>@endif
                        @if($client->state_employee != 'Independiente')<option value="Independiente">Independiente</option>@endif
                        @if($client->state_employee != 'Desempleado')<option value="Desempleado">Desempleado</option>@endif
                        @if($client->state_employee != 'Jubilado')<option value="Jubilado">Jubilado</option>@endif
                        @if($client->state_employee != 'Estudiante')<option value="Estudiante">Estudiante</option>@endif
                    </select>
                </div>
                <div class="form-group col-4">
                    <label>Ingreso anual (USD)</label>
                    <select  name="anual_activity_income_id" value="{{$client->anual_activity_income_id}}" id="anual_activity_income_id" class="form-control custom-select @error('anual_activity_income_id') is-invalid @enderror " required>
                        <option value="{{ $client->anual_activity_income_id }}"> {{$client->anual->description}}</option>
                        @foreach($anualIncomes as $item)
                        @if($item['id'] != $client->anual->id)<option value="{{$item['id']}}">{{$item['description']}}</option>@endif
                        
                        @endforeach
                    </select>
                </div>
                <div class="form-group col-4">
                    <label>Total de fondos a invertir (USD)</label>
                        <select  name="total_funds_invest_id" value="{{$client->total_funds_invest_id}}" id="total_funds_invest_id" class="form-control custom-select @error('total_funds_invest_id') is-invalid @enderror " required>
                        <option value="{{$client->total_funds_invest_id}}" selected>{{$client->total->description}}</option>
                        @foreach($totalFundInvest as $item)
                        @if($item['id'] != $client->total->id)<option value="{{$item['id']}}">{{$item['description']}}</option>@endif
                        
                        
                        @endforeach
                    </select>
                </div>
                <div class="form-group col-3">
                    <label>Origen de los fondos</label>
                    <select  name="family_member_holds_public_office_id" value="{{$client->total_funds_invest_id}}" id="family_member_holds_public_office_id" class="form-control custom-select @error('family_member_holds_public_office_id') is-invalid @enderror " required>
                        <option value="{{$client->total_funds_invest_id}}" selected>{{$client->public->description}}</option>
                        @foreach($publicOfficeEmployees as $item)
                        @if($item['id'] != $client->public->id)<option value="{{$item['id']}}">{{$item['description']}}</option>@endif
                        
                        @endforeach
                    </select>
                </div>
                <div class="form-group col-5">
                    <label>¿Eres una persona politicamente expuesta?</label>
                    <select  name="is_politically_exposed" value="{{$client->is_politically_exposed}}" id="is_politically_exposed" class="form-control custom-select @error('is_politically_exposed') is-invalid @enderror " required>
                        <option value="{{$client->is_politically_exposed}}" selected>
                            @if($client->is_politically_exposed) Si
                            @else No
                            @endif
                        </option>
                        @if($client->is_politically_exposed != '1')<option value="1">Si</option>@endif
                        @if($client->is_politically_exposed != '0')<option value="0">No</option>@endif
                        

                    </select>
                </div>
                <div class="form-group col-4">
                    <label>¿Estas declarado en banca rota?</label>
                        <select  name="is_declared_bankrupt" value="{{$client->is_declared_bankrupt}}" id="is_declared_bankrupt" class="form-control custom-select @error('is_declared_bankrupt') is-invalid @enderror " required>
                        <option value="{{$client->is_declared_bankrupt}}" selected>
                            @if($client->is_declared_bankrupt) Si
                            @else No
                            @endif
                        </option>
                        @if($client->is_declared_bankrupt != '1')<option value="1">Si</option>@endif
                        @if($client->is_declared_bankrupt != '0')<option value="0">No</option>@endif
                    </select>
                </div>
            </div>
            <br>
            <h3 class="text-center">Carga de documentos</h3>
            <br>
            <h3 class="text-center">Indentificación</h3>
            <hr>
            <div class="row">
                <div class="form-group col-12 text-center">
                   
                    @if ($client->type_identity_oficial == 'IFE/INE')
                    <button type="button" class="form-control" value="IFE/INE">IFE/INE</button>
                    @endif
                    @if ($client->type_identity_oficial == 'Pasaporte')
                        <button type="button" class="form-control" value="Pasaporte">Pasaporte</button>
                    @endif
                </div>
                @if ($client->type_identity_oficial == 'IFE/INE')
                    <div class="form-group col-6">
                        <label>Frente</label>
                        <a class="form-control" type="button" onclick="getfile('{{$client->id}}','{{$client->file_document_identity_oficial_V1_name}}','file_document_identity_oficial_V1','0')">
                            <i class="fas fa fa-file"></i>
                        </a>
                    </div>
                    <div class="form-group col-6">
                        <label>Reverso</label>
                        <a class="form-control" type="button" onclick="getfile('{{$client->id}}','{{$client->file_document_identity_oficial_V2_name}}','file_document_identity_oficial_V1','0')">
                            <i class="fas fa fa-file"></i>
                        </a>
                    </div>
                @endif
                @if ($client->type_identity_oficial == 'Pasaporte')
                    <div class="form-group col-12">
                        <label>Frente</label>
                        <a class="form-control" type="button" onclick="getfile('{{$client->id}}','{{$client->file_document_identity_oficial_V1_name}}','file_document_identity_oficial_V1','0')">
                            <i class="fas fa fa-file"></i>
                        </a>
                    </div>
                @endif
            </div>
            <br>
            <h3 class="text-center">Comprobante de domicilio</h3>
            <hr>
            <div class="row">
                <div class="form-group col-12">
                    <a class="form-control" type="button" onclick="getfile('{{$client->id}}','{{$client->file_document_home_name}}','file_document_home','0')">
                        <i class="fas fa fa-file"></i>
                    </a>
                </div>
                
            </div>
            <br>
            <h3 class="text-center">Contrato</h3>
            <hr>
            <div class="row">
                <div class="form-group col-12">
                    <a class="form-control" type="button" onclick="getfile('{{$client->id}}','{{$client->file_document_contract}}','file_document_contract','1')">
                        <i class="fas fa fa-file"></i>
                    </a>
                </div>
            </div>
                
        </form>    
    </div>
</div>
@endsection