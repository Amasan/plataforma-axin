@extends('layoutAdmins')

@section('content')
    <style>
        .activa {
            background-color: #185bc3!important;
            color: white!important;
        }
        table {
            font-size: 12px;
        }
    </style>
<div class="m-4">
    <ul class="nav nav-tabs nav-tabs-card">
        <li class="nav-item">
            <a class="nav-link" href="{{ route('legal.clientes.perfiles') }}">Perfiles</a>
        </li>
        <li class="nav-item">
            <a class="nav-link" href="{{ route('legal.clientes.nuevos') }}">Nuevos</a>
        </li>
        <li class="nav-item">
            <a class="nav-link" href="{{ route('legal.clientes.noActivados') }}">No activados</a>
        </li>
        <li class="nav-item">
            <a class="nav-link" href="{{ route('legal.clientes.incompletos') }}">Incompletos</a>
        </li>
        <li class="nav-item">
            <a class="nav-link activa" href="{{ route('legal.clientes.verificados') }}">Verificados</a>
        </li>
    </ul>
    <br>
    <br>
    <form action="{{  route('legal.clientes.perfiles') }}" method="GET" class="form-inline float-left">
        <div class="form-group">
            <input type="text" name="name" class="form-control" placeholder="Nombre">
        </div>
        <div class="form-group">
            <input type="text" name="email" class="form-control" placeholder="Correo electronico">
        </div>
        <div class="form-group">
            <button type="submit" class="btn btn-default"><span class="fas fa-search"></span></button>
        </div>
    </form>
    <div class="table-responsive mt-4">
        <table class="table table-striped">
            <thead>
                <tr>
                    <th>NOMBRE&nbsp;COMPLETO</th>
                    <th>CORREO</th>
                    <th>PAIS</th>
                    <th>NACIMIENTO</th>
                    <th>ID&nbsp;FRENTE</th>
                    <th>ID&nbsp;REVERSO</th>
                    <th>PASAPORTE</th>
                    <th>DOMICILIO</th>
                    <th>CONTRATO</th>
                    <th>DATOS</th>
                    <th>ESTATUS</th>
                    <th>CUENTA</th>
                    <th>FECHA&nbsp;DE&nbsp;ENVIO</th>
                    <th>FECHA&nbsp;ACTUALIZADO</th>
                    <th>NOTIFICACIÓN</th>
                </tr>
            </thead>
            <tbody>
                @foreach ($clients as $client)
                    <tr>
                        <td>{{$client->name}} {{$client->first_last_name}} {{$client->second_last_name}}</td>
                        <td>{{$client->email}}</td>
                        <td>{{$client->city}}</td>
                        <td>{{$client->birth_date_country}}</td>
                        <form method="POST" action="{{ route('legal.clientes.perfiles_store') }}">
                            <input type="hidden" name="_token" value="{{ csrf_token() }}"></input>
                            <input type="hidden" name="id" value="{{ $client->id }}"></input>

                            <td>
                                @if($client->type_identity_oficial == 'IFE/INE' && $client->file_document_identity_oficial_V1_name)
                                <a type="button" onclick="getfile('{{$client->id}}','{{$client->file_document_identity_oficial_V1_name}}','file_document_identity_oficial_V1','0')">
                                    @if($client->file_document_identity_oficial_V1_name && $client->file_document_identity_oficial_V1_status == '1')
                                    <img src="{{asset('images/axin/doc2.png')}}" width="40" height="auto">
                                    @elseif ($client->file_document_identity_oficial_V1_name && $client->file_document_identity_oficial_V1_status == '0')
                                    <img src="{{asset('images/axin/doc1.png')}}" width="40" height="auto">
                                    @elseif($client->file_document_identity_oficial_V1_name && $client->file_document_identity_oficial_V1_status == '')
                                    <img src="{{asset('images/axin/doc3.png')}}" width="40" height="auto">
                                    @endif
                                </a>
                            
                                <select @if($client->file_document_identity_oficial_V1_status == '1') hidden @endif id="file_document_identity_oficial_V1_status" name="file_document_identity_oficial_V1_status" class="form-control">
                                    <option value="{{$client->file_document_identity_oficial_V1_status}}" selected>
                                        @if($client->file_document_identity_oficial_V1_status == '') Pendiente
                                        @elseif($client->file_document_identity_oficial_V1_status == '1') Aceptado
                                        @elseif($client->file_document_identity_oficial_V1_status == '0') Rechazado
                                        
                                        @endif
                                    </option>
                                    @if($client->file_document_identity_oficial_V1_status != '1')<option value="1">Aceptado</option>@endif
                                    @if($client->file_document_identity_oficial_V1_status != '0')<option value="0">Rechazado</option>@endif
                                </select>
                                @endif   
                            </td>
                            
                            <td>
                                @if($client->file_document_identity_oficial_V2_name)
                                <a type="button" onclick="getfile('{{$client->id}}','{{$client->file_document_identity_oficial_V2_name}}','file_document_identity_oficial_V2','0')">
                                    @if($client->file_document_identity_oficial_V2_name && $client->file_document_identity_oficial_V2_status == '1')
                                    <img src="{{asset('images/axin/doc2.png')}}" width="40" height="auto">
                                    @elseif ($client->file_document_identity_oficial_V2_name && $client->file_document_identity_oficial_V2_status == '0')
                                    <img src="{{asset('images/axin/doc1.png')}}" width="40" height="auto">
                                    @elseif($client->file_document_identity_oficial_V2_name && $client->file_document_identity_oficial_V2_status == '')
                                    <img src="{{asset('images/axin/doc3.png')}}" width="40" height="auto">
                                    @endif
                                </a>
                            
                                <select @if($client->file_document_identity_oficial_V2_status == '1') hidden @endif id="file_document_identity_oficial_V2_status" name="file_document_identity_oficial_V2_status" class="form-control">
                                    <option value="{{$client->file_document_identity_oficial_V2_status}}" selected>
                                        @if($client->file_document_identity_oficial_V2_status == '') Pendiente
                                        @elseif($client->file_document_identity_oficial_V2_status == '1') Aceptado
                                        @elseif($client->file_document_identity_oficial_V2_status == '0') Rechazado
                                        @endif
                                    </option>
                                    @if($client->file_document_identity_oficial_V2_status != '1')<option value="1">Aceptado</option>@endif
                                    @if($client->file_document_identity_oficial_V2_status != '0')<option value="0">Rechazado</option>@endif
                                </select>
                            @endif
                            </td>

                            <td>
                                @if($client->type_identity_oficial == 'Pasaporte' && $client->file_document_identity_oficial_V1_name)
                                <a type="button" onclick="getfile('{{$client->id}}','{{$client->file_document_identity_oficial_V1_name}}','file_document_identity_oficial_V1','0')">
                                    @if($client->file_document_identity_oficial_V1_name && $client->file_document_identity_oficial_V1_status == '1')
                                    <img src="{{asset('images/axin/doc2.png')}}" width="40" height="auto">
                                    @elseif ($client->file_document_identity_oficial_V1_name && $client->file_document_identity_oficial_V1_status == '0')
                                    <img src="{{asset('images/axin/doc1.png')}}" width="40" height="auto">
                                    @elseif($client->file_document_identity_oficial_V1_name && $client->file_document_identity_oficial_V1_status == '')
                                    <img src="{{asset('images/axin/doc3.png')}}" width="40" height="auto">
                                    @endif
                                </a>
                            
                                <select @if($client->file_document_identity_oficial_V1_status == '1') hidden @endif id="file_document_identity_oficial_V1_status" name="file_document_identity_oficial_V1_status" class="form-control">
                                    <option value="{{$client->file_document_identity_oficial_V1_status}}" selected>
                                        @if($client->file_document_identity_oficial_V1_status == '') Pendiente
                                        @elseif($client->file_document_identity_oficial_V1_status == '1') Aceptado
                                        @elseif($client->file_document_identity_oficial_V1_status == '0') Rechazado
                                        
                                        @endif
                                    </option>
                                    @if($client->file_document_identity_oficial_V1_status != '1')<option value="1">Aceptado</option>@endif
                                    @if($client->file_document_identity_oficial_V1_status != '0')<option value="0">Rechazado</option>@endif
                                </select>
                                @endif  
                            </td>

                            <td>
                                @if($client->file_document_home_name)
                                <a type="button" onclick="getfile('{{$client->id}}','{{$client->file_document_home_name}}','file_document_home','0')">
                                    @if($client->file_document_home_name && $client->file_document_home_status == '1')
                                    <img src="{{asset('images/axin/doc2.png')}}" width="40" height="auto">
                                    @elseif ($client->file_document_home_name && $client->file_document_home_status == '0')
                                    <img src="{{asset('images/axin/doc1.png')}}" width="40" height="auto">
                                    @elseif($client->file_document_home_name && $client->file_document_home_status == 'NULL')
                                    <img src="{{asset('images/axin/doc3.png')}}" width="40" height="auto">
                                    @endif
                                </a>
                                <select @if($client->file_document_home_status == '1') hidden @endif id="file_document_home_status" name="file_document_home_status" class="form-control">
                                    <option value="{{$client->file_document_home_status}}" selected>
                                        @if($client->file_document_home_status == 'NULL') Pendiente
                                        @elseif($client->file_document_home_status == '1') Aceptado
                                        @elseif($client->file_document_home_status == '0') Rechazado
                                        
                                        @endif
                                    </option>
                                    @if($client->file_document_home_status != '1')<option value="1">Aceptado</option>@endif
                                    @if($client->file_document_home_status != '0')<option value="0">Rechazado</option>@endif
                                </select>
                                @endif
                            </td>

                            <td>
                                @if($client->file_document_contract)
                                <a type="button" onclick="getfile('{{$client->id}}','{{$client->file_document_contract}}','file_document_contract','1')">
                                    @if($client->file_document_contract && $client->file_document_contract_status == '1')
                                    <img src="{{asset('images/axin/doc2.png')}}" width="40" height="auto">
                                    @elseif ($client->file_document_contract && $client->file_document_contract_status == '0')
                                    <img src="{{asset('images/axin/doc1.png')}}" width="40" height="auto">
                                    @elseif($client->file_document_contract && $client->file_document_contract_status == '')
                                    <img src="{{asset('images/axin/doc3.png')}}" width="40" height="auto">
                                    @endif
                                </a>
                                <select @if($client->file_document_contract_status == '1') hidden @endif id="file_document_contract_status" name="file_document_contract_status" class="form-control">
                                    <option value="{{$client->file_document_contract_status}}" selected>
                                        @if($client->file_document_contract_status == '') Pendiente
                                        @elseif($client->file_document_contract_status == '1') Aceptado
                                        @elseif($client->file_document_contract_status == '0') Rechazado
                                        @endif
                                    </option>
                                    @if($client->file_document_contract_status != '1')<option value="1">Aceptado</option>@endif
                                    @if($client->file_document_contract_status != '0')<option value="0">Rechazado</option>@endif
                                </select>
                                @endif
                            </td>

                            <td>
                                @if ($client->complete_profile >= 4)
                                <a class="btn btn-primary btn-block" href="{{route('legal.clientes.infoCliente', $client->id)}}" target="_blank">Ver</a>
                                @endif
                            </td>
                            <td>
                            @if($client->is_active == '1')
                                Activa
                            @else
                                Inactiva
                            @endif
                            </td>
                            <td>{{$client->created_at->format('Y-m-d')}}</td>
                            <td>{{$client->updated_at->format('Y-m-d')}}</td>
                            <td>
                                @if($client->complete_profile == 4)
                                <button class="btn btn-primary btn-block botonSiguienteP"  id="boton" type="submit">
                                    <span class="spinner-text">Guardar</span>
                                    <span class="spinner-grow spinner-grow-sm" role="status" aria-hidden="true"></span>
                                    <span class="spinner-grow spinner-grow-sm" role="status" aria-hidden="true"></span>
                                    <span class="spinner-grow spinner-grow-sm" role="status" aria-hidden="true"></span>
                                </button>
                                @elseif($client->complete_profile == 5) Cliente&nbsp;verificado
                                @else Registro&nbsp;en&nbsp;proceso
                                @endif
                            </td>
                        </form>
                        <td><a href="{{ route('notificaciones.documentos.mensajeDocs',$client->id) }}" class="btn btn-primary btn-block">Notificar</a></td>
                    </tr>
                @endforeach
            </tbody>
        </table>
        {{ $clients->links() }}
    </div>
</div>
@endsection

