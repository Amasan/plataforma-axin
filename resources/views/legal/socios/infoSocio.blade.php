@extends('layoutAdmins')

@section('content')
    <style>
        .activa {
            background-color: #185bc3!important;
            color: white!important;
        }
    </style>
<div class="m-4">

    <br>
    <h3 class="text-center">INFORMACIÓN DE {{$partner->user->name}} {{$partner->user->first_last_name}} {{$partner->user->second_last_name}}</h3>
    <br>

    <div class="container">
        <form method="POST" action="{{route('legal.socios.infoSocio.gains')}}">
            <h3 class="text-center">Ganancia</h3>
            <hr>
            <div class="row">
                <input type="hidden" name="_token" value="{{ csrf_token() }}"></input>
                <input type="hidden" name="id" class="form-control" value="{{$partner->id}}">
                <div class="form-group col-6">
                    <label>Performance Fee</label>
                    <input name="performance_fee" type="number" step="0.01" pattern="^\d+(?:\.\d{1,2})?$" class="form-control" value="{{$partner->performance_fee}}">
                </div>
                <div class="form-group col-6">
                    <label>Maganament Fee</label>
                    <input name="management_fee" type="number" step="0.01" pattern="^\d+(?:\.\d{1,2})?$" class="form-control" value="{{$partner->management_fee}}">
                </div>
                <div class="form-group col-6">
                    <label>Inicio de contrato</label>
                    <input name="date_contract_init" type="date" class="form-control" value="{{ $contract->date_contract_init }}">
                </div>
                <div class="form-group col-6">
                    <label>Fin de contrato</label>
                    <input name="date_contract_fin" type="date" class="form-control" value="{{ $contract->date_contract_fin }}">
                </div>
                <div class="form-group col-12 d-flex justify-content-center">
                    <button type="submit" class="btn btn-primary">Guardar</button>
                </div>
            </div>
            <br>
        </form>
        <form>
            <h3 class="text-center">Datos de la empresa</h3>
            <hr>
            <div class="row">
                <div class="form-group col-4">
                    <label>Nombre de la empresa</label>
                    <input type="text" class="form-control" value="{{$partner->name_business}}">
                </div>
                <div class="form-group col-4">
                    <label>RFC</label>
                    <input type="text" class="form-control" value="{{$partner->rfc_business}}">
                </div>
                <div class="form-group col-4">
                    <label>Pais</label>
                    <select class="form-control" >
                        <option value="{{$partner->country_business}}">{{$partner->country_business}}</option>
                    </select>
                </div>
                <div class="form-group col-4">
                    <label>Ciudad</label>
                    <input type="text" class="form-control" value="{{$partner->city_business}}">
                </div>
                <div class="form-group col-4">
                    <label>Codigo postal</label>
                    <input type="number" class="form-control" value="{{$partner->code_postal_business}}">
                </div>
                <div class="form-group col-4">
                    <label>Domicilio fiscal</label>
                    <input type="text" class="form-control" value="{{$partner->direction_company}}">
                </div>
            </div>
            <br>
            <h3 class="text-center">Representante legal</h3>
            <hr>
            <div class="row">
                <div class="form-group col-4">
                    <label>Nombre(s)</label>
                    <input type="text" class="form-control" value="{{$partner->user->name}}">
                </div>
                <div class="form-group col-4">
                    <label>Apellido paterno</label>
                    <input type="text" class="form-control" value="{{$partner->user->first_last_name}}">
                </div>
                <div class="form-group col-4">
                    <label>Apellido materno</label>
                    <input type="text" class="form-control" value="{{$partner->user->second_last_name}}">
                </div>
                <div class="form-group col-4">
                    <label>Fecha de nacimiento</label>
                    <input type="date" class="form-control" value="{{$partner->birth_date}}">
                </div>
                <div class="form-group col-4">
                    <label>Télefono celular</label>
                    <input type="number" class="form-control" value="{{$partner->number_phone}}">
                </div>
                <div class="form-group col-4">
                    <label>Pais de nacimiento</label>
                    <select class="form-control" >
                        <option value="{{$partner->birth_date_country}}">{{$partner->birth_date_country}}</option>
                    </select>
                </div>
                <div class="form-group col-4">
                    <label>Ciudad</label>
                    <input type="text" class="form-control" value="{{$partner->city}}">
                </div>
                <div class="form-group col-4">
                    <label>Codigo postal</label>
                    <input type="number" class="form-control" value="{{$partner->code_postal}}">
                </div>
                <div class="form-group col-4">
                    <label>Dirección</label>
                    <input type="text" class="form-control" value="{{$partner->user->direction}}">
                </div>
            </div>
            <br>
            <h3 class="text-center">Carga de documentos</h3>
            <br>
            <h3 class="text-center">Indentificación</h3>
            <hr>
            <div class="row">
                <div class="form-group col-12 text-center">
                   
                    @if ($partner->type_identity_oficial == 'IFE/INE')
                    <button type="button" class="form-control" value="IFE/INE">IFE/INE</button>
                    @endif
                    @if ($partner->type_identity_oficial == 'Pasaporte')
                        <button type="button" class="form-control" value="Pasaporte">Pasaporte</button>
                    @endif
                </div>
                @if ($partner->type_identity_oficial == 'IFE/INE')
                    <div class="form-group col-6">
                        <label>Frente</label>
                        <a class="form-control" type="button" onclick="getfileSocio('{{$partner->id}}','{{$partner->file_document_identity_oficial_V1_name}}','file_document_identity_oficial_V1','0')">
                            <i class="fas fa fa-file"></i>
                        </a>
                    </div>
                    <div class="form-group col-6">
                        <label>Reverso</label>
                        <a class="form-control" type="button" onclick="getfileSocio('{{$partner->id}}','{{$partner->file_document_identity_oficial_V2_name}}','file_document_identity_oficial_V1','0')">
                            <i class="fas fa fa-file"></i>
                        </a>
                    </div>
                @endif
                @if ($partner->type_identity_oficial == 'Pasaporte')
                    <div class="form-group col-12">
                        <label>Frente</label>
                        <a class="form-control" type="button" onclick="getfileSocio('{{$partner->id}}','{{$partner->file_document_identity_oficial_V1_name}}','file_document_identity_oficial_V1','0')">
                            <i class="fas fa fa-file"></i>
                        </a>
                    </div>
                @endif
            </div>
            <br>
            <h3 class="text-center">Comprobante de domicilio</h3>
            <hr>
            <div class="row">
                <div class="form-group col-12">
                    <a class="form-control" type="button" onclick="getfileSocio('{{$partner->id}}','{{$partner->file_document_home_name}}','file_document_home','0')">
                        <i class="fas fa fa-file"></i>
                    </a>
                </div>
                
            </div>
            <br>
            <h3 class="text-center">Contrato</h3>
            <hr>
            <div class="row">
                <div class="form-group col-12">
                    <a class="form-control" type="button" onclick="getfileSocio('{{$partner->id}}','{{$partner->file_document_contract}}','file_document_contract','1')">
                        <i class="fas fa fa-file"></i>
                    </a>
                </div>
            </div>
                
        </form>    
    </div>
</div>
@endsection