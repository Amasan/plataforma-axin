@extends('layoutAdmins')

@section('content')
    <style>
        .activa {
            background-color: #185bc3!important;
            color: white!important;
        }
        table {
            font-size: 12px;
        }
    </style>
<div class="m-4">
    <ul class="nav nav-tabs nav-tabs-card">
        <li class="nav-item">
            <a class="nav-link" href="{{ route('legal.socios.perfiles') }}">Perfiles</a>
        </li>
        <li class="nav-item">
            <a class="nav-link" href="{{ route('legal.socios.nuevos') }}">Nuevos</a>
        </li>
        <li class="nav-item">
            <a class="nav-link activa" href="{{ route('legal.socios.noActivados') }}">No activados</a>
        </li>
        <li class="nav-item">
            <a class="nav-link" href="{{ route('legal.socios.incompletos') }}">Incompletos</a>
        </li>
        <li class="nav-item">
            <a class="nav-link" href="{{ route('legal.socios.verificados') }}">Verificados</a>
        </li>
    </ul>
    <br>
    <br>
    <form action="{{ route('legal.socios.noActivados') }}" method="GET" class="form-inline float-left">
        <div class="form-group">
            <input type="text" name="name" class="form-control" placeholder="Nombre">
        </div>
        <div class="form-group">
            <input type="text" name="email" class="form-control" placeholder="Correo electronico">
        </div>
        <div class="form-group">
            <button type="submit" class="btn btn-default"><span class="fas fa-search"></span></button>
        </div>
    </form>
    <div class="table-responsive mt-4">
        <table class="table table-striped">
            <thead>
                <tr>
                    
                    <th>NOMBRE&nbsp;COMPLETO</th>
                    <th>CORREO</th>
                    <th>PAIS</th>
                    <th>ID&nbsp;FRENTE</th>
                    <th>ID&nbsp;REVERSO</th>
                    <th>PASAPORTE</th>
                    <th>DOMICILIO</th>
                    <th>CONTRATO</th>
                    <th>DATOS</th>
                    <th>ESTATUS</th>
                    <th>FECHA&nbsp;DE&nbsp;ENVIO</th>
                    <th>FECHA&nbsp;ACTUALIZADO</th>
                    <th>NOTIFICACIÓN</th>
                </tr>
            </thead>
            <tbody>
                @foreach ($partners as $partner)
                    <tr>
                        
                        <td>{{$partner->name}} {{$partner->first_last_name}} {{$partner->second_last_name}}</td>
                        <td>{{$partner->email}}</td>
                        <td>{{$partner->birth_date_country}}</td>
                        <form method="POST" action="{{ route('legal.socios.perfiles_store') }}">
                            <input type="hidden" name="_token" value="{{ csrf_token() }}"></input>
                            <input type="hidden" name="id" value="{{ $partner->id }}"></input>

                            <td>
                                @if($partner->type_identity_oficial == 'IFE/INE' && $partner->file_document_identity_oficial_V1_name)
                                <a type="button" onclick="getfileSocio('{{$partner->id}}','{{$partner->file_document_identity_oficial_V1_name}}','file_document_identity_oficial_V1','0')">
                                    @if($partner->file_document_identity_oficial_V1_name && $partner->file_document_identity_oficial_V1_status == '1')
                                    <img src="{{asset('images/axin/doc2.png')}}" width="40" height="auto">
                                    @elseif ($partner->file_document_identity_oficial_V1_name && $partner->file_document_identity_oficial_V1_status == '0')
                                    <img src="{{asset('images/axin/doc1.png')}}" width="40" height="auto">
                                    @elseif($partner->file_document_identity_oficial_V1_name && $partner->file_document_identity_oficial_V1_status == '')
                                    <img src="{{asset('images/axin/doc3.png')}}" width="40" height="auto">
                                    @endif
                                </a>
                            
                                <select @if($partner->file_document_identity_oficial_V1_status == '1') hidden @endif id="file_document_identity_oficial_V1_status" name="file_document_identity_oficial_V1_status" class="form-control">
                                    <option value="{{$partner->file_document_identity_oficial_V1_status}}" selected>
                                        @if($partner->file_document_identity_oficial_V1_status == '') Pendiente
                                        @elseif($partner->file_document_identity_oficial_V1_status == '1') Aceptado
                                        @elseif($partner->file_document_identity_oficial_V1_status == '0') Rechazado
                                        
                                        @endif
                                    </option>
                                    @if($partner->file_document_identity_oficial_V1_status != '1')<option value="1">Aceptado</option>@endif
                                    @if($partner->file_document_identity_oficial_V1_status != '0')<option value="0">Rechazado</option>@endif
                                </select>
                                @endif   
                            </td>
                            
                            <td>
                                @if($partner->file_document_identity_oficial_V2_name)
                                <a type="button" onclick="getfileSocio('{{$partner->id}}','{{$partner->file_document_identity_oficial_V2_name}}','file_document_identity_oficial_V2','0')">
                                    @if($partner->file_document_identity_oficial_V2_name && $partner->file_document_identity_oficial_V2_status == '1')
                                    <img src="{{asset('images/axin/doc2.png')}}" width="40" height="auto">
                                    @elseif ($partner->file_document_identity_oficial_V2_name && $partner->file_document_identity_oficial_V2_status == '0')
                                    <img src="{{asset('images/axin/doc1.png')}}" width="40" height="auto">
                                    @elseif($partner->file_document_identity_oficial_V2_name && $partner->file_document_identity_oficial_V2_status == '')
                                    <img src="{{asset('images/axin/doc3.png')}}" width="40" height="auto">
                                    @endif
                                </a>
                            
                                <select @if($partner->file_document_identity_oficial_V2_status == '1') hidden @endif id="file_document_identity_oficial_V2_status" name="file_document_identity_oficial_V2_status" class="form-control">
                                    <option value="{{$partner->file_document_identity_oficial_V2_status}}" selected>
                                        @if($partner->file_document_identity_oficial_V2_status == '') Pendiente
                                        @elseif($partner->file_document_identity_oficial_V2_status == '1') Aceptado
                                        @elseif($partner->file_document_identity_oficial_V2_status == '0') Rechazado
                                        @endif
                                    </option>
                                    @if($partner->file_document_identity_oficial_V2_status != '1')<option value="1">Aceptado</option>@endif
                                    @if($partner->file_document_identity_oficial_V2_status != '0')<option value="0">Rechazado</option>@endif
                                </select>
                            @endif
                            </td>

                            <td>
                                @if($partner->type_identity_oficial == 'Pasaporte' && $partner->file_document_identity_oficial_V1_name)
                                <a type="button" onclick="getfileSocio('{{$partner->id}}','{{$partner->file_document_identity_oficial_V1_name}}','file_document_identity_oficial_V1','0')">
                                    @if($partner->file_document_identity_oficial_V1_name && $partner->file_document_identity_oficial_V1_status == '1')
                                    <img src="{{asset('images/axin/doc2.png')}}" width="40" height="auto">
                                    @elseif ($partner->file_document_identity_oficial_V1_name && $partner->file_document_identity_oficial_V1_status == '0')
                                    <img src="{{asset('images/axin/doc1.png')}}" width="40" height="auto">
                                    @elseif($partner->file_document_identity_oficial_V1_name && $partner->file_document_identity_oficial_V1_status == '')
                                    <img src="{{asset('images/axin/doc3.png')}}" width="40" height="auto">
                                    @endif
                                </a>
                            
                                <select @if($partner->file_document_identity_oficial_V1_status == '1') hidden @endif id="file_document_identity_oficial_V1_status" name="file_document_identity_oficial_V1_status" class="form-control">
                                    <option value="{{$partner->file_document_identity_oficial_V1_status}}" selected>
                                        @if($partner->file_document_identity_oficial_V1_status == '') Pendiente
                                        @elseif($partner->file_document_identity_oficial_V1_status == '1') Aceptado
                                        @elseif($partner->file_document_identity_oficial_V1_status == '0') Rechazado
                                        
                                        @endif
                                    </option>
                                    @if($partner->file_document_identity_oficial_V1_status != '1')<option value="1">Aceptado</option>@endif
                                    @if($partner->file_document_identity_oficial_V1_status != '0')<option value="0">Rechazado</option>@endif
                                </select>
                                @endif  
                            </td>

                            <td>
                                @if($partner->file_document_home_name)
                                <a type="button" onclick="getfileSocio('{{$partner->id}}','{{$partner->file_document_home_name}}','file_document_home','0')">
                                    @if($partner->file_document_home_name && $partner->file_document_home_status == '1')
                                    <img src="{{asset('images/axin/doc2.png')}}" width="40" height="auto">
                                    @elseif ($partner->file_document_home_name && $partner->file_document_home_status == '0')
                                    <img src="{{asset('images/axin/doc1.png')}}" width="40" height="auto">
                                    @elseif($partner->file_document_home_name && $partner->file_document_home_status == 'NULL')
                                    <img src="{{asset('images/axin/doc3.png')}}" width="40" height="auto">
                                    @endif
                                </a>
                                <select @if($partner->file_document_home_status == '1') hidden @endif id="file_document_home_status" name="file_document_home_status" class="form-control">
                                    <option value="{{$partner->file_document_home_status}}" selected>
                                        @if($partner->file_document_home_status == 'NULL') Pendiente
                                        @elseif($partner->file_document_home_status == '1') Aceptado
                                        @elseif($partner->file_document_home_status == '0') Rechazado
                                        
                                        @endif
                                    </option>
                                    @if($partner->file_document_home_status != '1')<option value="1">Aceptado</option>@endif
                                    @if($partner->file_document_home_status != '0')<option value="0">Rechazado</option>@endif
                                </select>
                                @endif
                            </td>

                            <td>
                                @if($partner->file_document_contract)
                                <a type="button" onclick="getfileSocio('{{$partner->id}}','{{$partner->file_document_contract}}','file_document_contract','1')">
                                    @if($partner->file_document_contract && $partner->file_document_contract_status == '1')
                                    <img src="{{asset('images/axin/doc2.png')}}" width="40" height="auto">
                                    @elseif ($partner->file_document_contract && $partner->file_document_contract_status == '0')
                                    <img src="{{asset('images/axin/doc1.png')}}" width="40" height="auto">
                                    @elseif($partner->file_document_contract && $partner->file_document_contract_status == '')
                                    <img src="{{asset('images/axin/doc3.png')}}" width="40" height="auto">
                                    @endif
                                </a>
                                <select @if($partner->file_document_contract_status == '1') hidden @endif id="file_document_contract_status" name="file_document_contract_status" class="form-control">
                                    <option value="{{$partner->file_document_contract_status}}" selected>
                                        @if($partner->file_document_contract_status == '') Pendiente
                                        @elseif($partner->file_document_contract_status == '1') Aceptado
                                        @elseif($partner->file_document_contract_status == '0') Rechazado
                                        @endif
                                    </option>
                                    @if($partner->file_document_contract_status != '1')<option value="1">Aceptado</option>@endif
                                    @if($partner->file_document_contract_status != '0')<option value="0">Rechazado</option>@endif
                                </select>
                                @endif
                            </td>

                            <td>
                                @if ($partner->complete_profile >= 4)
                                <a class="btn btn-primary btn-block" href="{{route('legal.socios.infoSocio', $partner->id)}}" target="_blank">Ver</a>
                                @endif
                            </td>

                            <td>
                                @if($partner->complete_profile == 4)
                                <button class="btn btn-primary btn-block botonSiguienteP"  id="boton" type="submit">
                                    <span class="spinner-text">Guardar</span>
                                    <span class="spinner-grow spinner-grow-sm" role="status" aria-hidden="true"></span>
                                    <span class="spinner-grow spinner-grow-sm" role="status" aria-hidden="true"></span>
                                    <span class="spinner-grow spinner-grow-sm" role="status" aria-hidden="true"></span>
                                </button>
                                @elseif($partner->complete_profile == 5) Socio&nbsp;verificado
                                @else Registro&nbsp;en&nbsp;proceso
                                @endif
                            </td>
                        </form>
                        <td>{{$partner->created_at->format('Y-m-d')}}</td>
                        <td>{{$partner->updated_at->format('Y-m-d')}}</td>
                        <td><a href="{{ route('notificaciones.documentos.mensajeDocsSocio',$partner->id) }}" class="btn btn-primary btn-block">Notificar</a></td>
                    </tr>
                @endforeach
            </tbody>
        </table>
        {{ $partners->links() }}
    </div>
</div>
@endsection

