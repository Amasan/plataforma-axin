@extends('layout') 
@section('content')
 <div class="container page__container">
    <div class="page-section">

        <div class="card card-form d-flex flex-column flex-sm-row mb-lg-32pt">
            <div class="card-form__body card-body-form-group flex text-center" style="background-color: white;">
                <h1>Novedad</h1>
                <p>{{ $post->title }}</p>
                <br>
                <p>{{ $post->body}}</p>
            </div>
        </div>
    </div>
</div>
@endsection