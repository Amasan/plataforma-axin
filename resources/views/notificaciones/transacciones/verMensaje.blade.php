@extends('layout') 
@section('content')
 <div class="container page__container">
    <div class="page-section">

        <div class="card card-form d-flex flex-column flex-sm-row mb-lg-32pt">
            <div class="card-form__body card-body-form-group flex text-center" style="background-color: white;">
                <h1>Mensaje</h1>
                <p>{{ $message->body }}</p>
                <small>Enviado por {{ $message->sender->email}}</small>
            </div>
        </div>
    </div>
</div>
@endsection