@extends('layoutAdmins')

@section('content')
 <div class="container page__container">
    <div class="page-section">

        <div class="card card-form d-flex flex-column flex-sm-row mb-lg-32pt">
            <div class="card-form__body card-body-form-group flex text-center" style="background-color: white;">
               <form action="{{ route('notifications.transacciones.mensajeTransaccionStore') }}" method="POST">
                    {{ csrf_field() }}
                    <div class="card-header">Enviar notificación a {{$clientReceipt->client->user->name}} </div>

                    <div class="card-body">
                        <div class="form-group {{$errors->has('recipient_id') ? 'has-error' : '' }}">
                            <input hidden type="text" name="recipient_id" class="form-control" value="{{$clientReceipt->client->user_id}}" name="" id="" readonly>
                            {!! $errors->first('recipient_id', "<span class=help-block>:message</span>") !!}
                        </div>
                        <div class="form-group {{$errors->has('body') ? 'has-error' : '' }}">
                            <textarea onkeyup="validar()" name="body" class="form-control inputFormu" name="" id="" placeholder="Mensaje"></textarea>
                            {!! $errors->first('body', "<span class=help-block>:message</span>") !!}
                        </div>
                        <div class="form-group">
                            <button class="btn btn-primary btn-block botonSiguienteP"  id="boton" type="submit" disabled>
                                <span class="spinner-text">Enviar</span>
                                <span class="spinner-grow spinner-grow-sm" role="status" aria-hidden="true"></span>
                                <span class="spinner-grow spinner-grow-sm" role="status" aria-hidden="true"></span>
                                <span class="spinner-grow spinner-grow-sm" role="status" aria-hidden="true"></span>
                            </button>
                        </div>
                    
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>

@endsection