@extends('layout')

@section('content')

<div class="container page__container">
    <style>
        input:invalid {
			border-color: #ff6600!important;
        }
        select:invalid {
			border-color: #ff6600!important;
        }

        .check-ok {
            color:#185bc3!important;
            position: absolute; right: 34px; top: 42px!important;
        }
    
        input:invalid ~ .check-ok {
            display: none!important;
        }

        input:valid ~ .check-ok {
            display: inline!important;
        }
        select:invalid ~ .check-ok {
            display: none!important;
        }

        select:valid ~ .check-ok {
            display: inline!important;
        }
        .modal .modal-dialog {
            width: 80%!important;
        }
        #transfiriendo {
            display: none;
        }
        .caretdown {
            position: absolute!important;
            right: -20px!important;
            top: 13px!important;
            font-size: 24px!important;
        }
        .text1 {
            font-size:13px;
            color: gray;
        }
        .text2 {
            font-size:13px;
            text-align: right;
            font-weight: bold;
        }

    </style>

    <div class="page-section">

        <div class="card card-form d-flex flex-column flex-sm-row mb-lg-32pt">
            <div class="card-form__body card-body-form-group flex" style="background-color: white;">

                <div class="dropdown">
                    <div class="cuentaA row dropbtn">
                        <div class="col-sm-auto">
                            <div class="form-group">
                                <div class="form-row">
                                    <div class="col-xs-6 col-md-6 col-lg-6">
                                        <label for="filter_category">Tipo de Cuenta</label><br>
                                        <label for="filter_category" style="font-size: 1rem; color:black; font-weight: bold;">{{$accountA->typeaccount->name}} - {{$accountA['account_number']}}</label>
                                    </div>
                                    <div class="col-xs-6 col-md-6 col-lg-6">
                                        <br>
                                        @if ($accountA->amount_money >= 100 )
                                            <p  class="statusActiva">Activa</p>
                                        @else
                                            <p  class="statusInactiva">Inactiva</p>
                                        @endif
                                        <span class="caretdown"><i class="fa fa-caret-down"></i></span>
                                    </div>
                                    
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="cuentaI row dropbtn">
                        <div class="col-sm-auto">
                            <div class="form-group">
                                <div class="form-row">
                                    <div class="col-xs-6 col-md-6 col-lg-6">
                                        <label for="filter_category">Tipo de Cuenta</label><br>
                                        <label for="filter_category" style="font-size: 1rem; color:black; font-weight: bold;">{{$accountI->typeaccount->name}} - {{$accountI['account_number']}}</label>
                                    </div>
                                    <div class="col-xs-6 col-md-6 col-lg-6">
                                        <br>
                                        @if ($accountI->amount_money >= 1000 )
                                            <p  class="statusActiva">Activa</p>
                                        @else
                                            <p  class="statusInactiva">Inactiva</p>
                                        @endif
                                        <span class="caretdown"><i class="fa fa-caret-down"></i></span>
                                    </div>
                                    
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="cuentaM row dropbtn">
                        <div class="col-sm-auto">
                            <div class="form-group">
                                <div class="form-row">
                                    <div class="col-xs-6 col-md-6 col-lg-6">
                                        <label for="filter_category">Tipo de Cuenta</label><br>
                                        <label for="filter_category" style="font-size: 1rem; color:black; font-weight: bold;">{{$accountM->typeaccount->name}} - {{$accountM['account_number']}}</label>
                                    </div>
                                    <div class="col-xs-6 col-md-6 col-lg-6">
                                        <br>
                                        @if ($accountM->amount_money >= 1000 )
                                            <p  class="statusActiva">Activa</p>
                                        @else
                                            <p  class="statusInactiva">Inactiva</p>
                                        @endif
                                        <span class="caretdown"><i class="fa fa-caret-down"></i></span>
                                    </div>
                                    
                                </div>
                            </div>
                        </div>
                    </div>
     
                    <div class="dropdown-content">
           
                        <a href="#" class="accountA">
                            <div class="row dropbtn">
                                <div class="col-sm-auto">
                                    <div class="form-group">
                                        <div class="form-row">
                                            <div class="col-xs-6 col-md-6 col-lg-6">
                                                <label for="filter_category">Tipo de Cuenta</label><br>
                                                <label for="filter_category" style="font-size: 1rem; color:black; font-weight: bold;">{{$accountA->typeaccount->name}} - {{$accountA['account_number']}}</label>
                                            </div>
                                            <div class="col-xs-6 col-md-6 col-lg-6">
                                                <br>
                                                @if ($accountA->amount_money >= 100 )
                                                    <p  class="statusActiva">Activa</p>
                                                @else
                                                    <p  class="statusInactiva">Inactiva</p>
                                                @endif
                                            </div>
                                            
                                        </div>
                                    </div>
                                </div>
        
                            </div>
                        </a>
                        <a href="#" class="accountI">
                            <div class="row dropbtn">
                                <div class="col-sm-auto">
                                    <div class="form-group">
                                        <div class="form-row">
                                            <div class="col-xs-6 col-md-6 col-lg-6">
                                                <label for="filter_category">Tipo de Cuenta</label><br>
                                                <label for="filter_category" style="font-size: 1rem; color:black; font-weight: bold;">{{$accountI->typeaccount->name}} - {{$accountI['account_number']}}</label>
                                            </div>
                                            <div class="col-xs-6 col-md-6 col-lg-6">
                                                <br>
                                                @if ($accountI->amount_money >= 1000 )
                                                <p  class="statusActiva">Activa</p>
                                                @else
                                                    <p  class="statusInactiva">Inactiva</p>
                                                @endif
                                            </div>
                                            
                                        </div>
                                    </div>
                                </div>
        
                            </div>
                        </a>
                        <a href="#" class="accountM">
                            <div class="row dropbtn">
                                <div class="col-sm-auto">
                                    <div class="form-group">
                                        <div class="form-row">
                                            <div class="col-xs-6 col-md-6 col-lg-6">
                                                <label for="filter_category">Tipo de Cuenta</label><br>
                                                <label for="filter_category" style="font-size: 1rem; color:black; font-weight: bold;">{{$accountM->typeaccount->name}} - {{$accountM['account_number']}}</label>
                                            </div>
                                            <div class="col-xs-6 col-md-6 col-lg-6">
                                                <br>
                                                @if ($accountM->amount_money >= 1000 )
                                                    <p  class="statusActiva">Activa</p>
                                                @else
                                                    <p  class="statusInactiva">Inactiva</p>
                                                @endif
                                            </div>
                                            
                                        </div>
                                    </div>
                                </div>
        
                            </div>
                        </a>
                    
  
                    </div>
                    
                </div>
            </div>

            <div class="card-form__body card-body-form-group flex" style="background-color: white;">
                <div class="row" style="float: right;">
                    <div class="col-sm-auto">
                        <div class="form-group">
                            <div class="form-row">
                                <div class="col-sm-auto">
                                    <div class="form-group">
                                        <div class="form-row">
                                            {{--  <p class="m-auto text-center p-4"><span style="font-weight: bold;">20%</span><br><span>P.FEE</span></p>  --}}
                                            <div>
                                                <div>
                                                    @if($complete_profile!=10)
                                                    <a href="{{ url('banca/depositar_fondos') }}" class="btn btn-accent disabled" style="border-radius:7px; background-color: #185bc3; border:none; width:12rem;"><img width="20" src="{{ asset('images/axin/Icono_2.png') }}" alt="">&nbsp;Depositar fondos</a><br><br>
                                                    <a href="" class="btn btn-accent disabled" data-toggle="modal" data-target="#transferir" data-backdrop="static" data-keyboard="false" style="border-radius:7px; background-color: #c1c127; border:none; width:12rem;"><img width="20" src="{{ asset('images/axin/Icono_1.png') }}" alt="">&nbsp;Transferir fondos</a>
                                                    @endif
                                                    @if($complete_profile==10)
                                                    <a href="{{ url('banca/depositar_fondos') }}" class="btn btn-accent" style="border-radius:7px; background-color: #185bc3; border:none; width:12rem;"><img width="20" src="{{ asset('images/axin/Icono_2.png') }}" alt="">&nbsp;Depositar fondos</a><br><br>
                                                    <a href="" class="btn btn-accent" data-toggle="modal" data-target="#transferir" data-backdrop="static" data-keyboard="false" style="border-radius:7px; background-color: #c1c127; border:none; width:12rem;"><img width="20" src="{{ asset('images/axin/Icono_1.png') }}" alt="">&nbsp;Transferir fondos</a>
                                                    @endif
                                            
                                                </div>
                                            </div>
                                            
                                        </div>
                                    </div>
                                </div>
                                
                            </div>
                        </div>
                    </div>

                </div>
            </div>

        </div>

        <div class="col-12 col-md-12 col-lg-12">
            {{--  Modal Tranferencia  --}}
            <div class="modal fade " id="transferir" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
            <div class="modal-dialog col-lg-12" role="document">
                <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalLabel">Transferir fondos</h5>
                </div>
                <div class="modal-body">
                    <form method="POST" action="updateTranslate">
                    <input type="hidden" name="_token" value="{{ csrf_token() }}" id="_token"></input>    
                        <div class="form-row">
                            <div class="form-group col-12">
                                <label for="">Selecciona la cuenta de donde haras la transacción</label>
                                <select id="type_account_id1" name="type_account_id1" onchange="validarT()" class="form-control inputFormuT" id="" required>
                                    <option value="">Selecciona una cuenta</option>    
                                    <option value="{{$accountA->typeaccount->id}}">{{$accountA->typeaccount->name}} - {{$accountA->account_number}}</option>  

                                </select><i class="fa fa-check check-ok"></i>
                            </div>
                            <div class="form-group col-12">
                                <label for="">Selecciona la cuenta a la que haras la transacción</label>
                                <select id="type_account_id2" name="type_account_id2" onchange="validarT()" class="form-control inputFormuT" required>
                                    <option value="">Selecciona una cuenta</option>      
                                    <option value="{{$accountI->typeaccount->id}}">{{$accountI->typeaccount->name}} - {{$accountI->account_number}}</option> 
                                    <option value="{{$accountM->typeaccount->id}}">{{$accountM->typeaccount->name}} - {{$accountM->account_number}}</option> 
                                </select><i class="fa fa-check check-ok"></i>
                            </div>
                            <div class="form-group col-12">
                                <label for="">Cantidad (USD)</label>
                                <input id="amount_money" name="amount_money" onkeyup="validarT()" onkeypress="return validarNumeros(event)" type="number" min="0" class="form-control inputFormuT" required><i class="fa fa-check check-ok"></i>
                            </div>
                        </div>
                    </form>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Cancelar</button>
                    <button id="Transacction" type="submit" class="btn btn-primary" data-toggle="modal" data-backdrop="static" data-keyboard="false" data-dismiss="modal">Transferir</button>
                    <button id="transfiriendo" class="btn btn-primary" type="button" disabled>
                        <span class="spinner-border spinner-border-sm" role="status" aria-hidden="true"></span>
                       &nbsp;Transfiriendo...
                    </button>
                </div>
                </div>
            </div>
            </div>

            {{--  Modal confirmacion de Tranferencia  --}}
            <div class="modal fade" id="confirmacion" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
            <div class="modal-dialog col-lg-12" role="document">
                <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title exitoso" id="exampleModalLabel" style="color: #185bc3;"></h5>
                </div>
                <div class="modal-body m-auto">
                    <img id="imgexitosa" width="50%" style="margin-left: auto; margin-right: auto; display: block;" src="{{ asset('images/axin/Iconomodal.png')}}" alt="">
                    <h1 class="text-center" style="font-size: 6rem; color: red; font-weight: bold;" id="imgfallida">X</h1>
                    <h6 class="cantidad"></h6>
                </div>
                <div class="modal-footer m-auto">
                    <a type="button" class="btn btn-primary" href="{{ route('inicio') }}" >Regresar a inicio</a>
                </div>
                </div>
            </div>
            </div>
        </div>
        
        <div class="row card-group-row mb-lg-8pt">
            <div class="col-lg-4 col-md-6 card-group-row__col">
                <div class="card card-group-row__card">
                    <div class="card-header p-0 nav">
                        <div class="row no-gutters flex" role="tablist">
                            <div class="col-auto">
                                <div class="p-card-header">
                                    <p class="mb-0"><strong>Estadísticas</strong></p>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="card-body">
                        <div class="row">
                            <div class="col-6 text1">
                                Rendimiento
                            </div>
                            <div class="col-6 text2" id="rendimientoAcumulado">
                                
                            </div>
                            <hr>
                            <div class="col-6 text1">
                                Depósitos
                            </div>
                            <div class="col-6 text2" id="inversionAcumulada">
                                
                            </div>
                            <div class="col-6 text1">
                                Ganancia
                            </div>
                            <div class="col-6 text2" id="gananciaAcumulada">
                            
                            </div>
                            <div class="col-6 text1">
                                Retiros
                            </div>
                            <div class="col-6 text2" id="retiros">
                            
                            </div>
                            
                            
                            <div class="col-6 text1">
                                Comisiones
                            </div>
                            <div class="col-6 text2" id="comisiones">
                            
                            </div>
                            <div class="col-6 text1">
                                Saldo Actual
                            </div>
                            <div class="col-6 text2"  id="saldoAcumulada">
                                
                            </div>
                        </div>
                        <hr>
                        <div class="row">
                            <div class="col-6 text1">
                                Performance fee
                            </div>
                            <div class="col-6 text2">
                            
                            </div>
                            <div class="col-6 text1">
                                Managment fee
                            </div>
                            <div class="col-6 text2">
                            
                            </div>
                        </div>
                        <hr>
                        <div class="row">
                            <div class="col-6 text1">
                                Inicio de contrato
                            </div>
                            <div class="col-6 text2" id="inicioContrato">
                            
                            </div>
                            <div class="col-6 text1">
                                Fin de contrato
                            </div>
                            <div class="col-6 text2" id="finContrato">
                            
                            </div>
                        </div>
                        <hr>
                        <div class="row">
                            <div class="col-6 text1">
                                Actualizado
                            </div>
                            <div class="col-6 text2" id="actualizacion">
                            
                            </div>
                        </div>

                    </div>
                </div>
            </div>
            <div class="col-lg-8 col-md-6 card-group-row__col">
                <div class="card card-group-row__card">
                    <div class="card-header p-0 nav">
                        <div class="row no-gutters flex" role="tablist">
                            <div class="col-auto">
                                <div class="p-card-header">
                                    <p class="mb-0"><strong>Gráfica</strong></p>
                                </div>
                            </div>
                            <div class="col-auto ml-sm-auto p-2">
                      
                                <div hidden class="flatpickr-wrapper">
                                    <div id="recent_orders_date" data-toggle="flatpickr" data-flatpickr-wrap="true" data-flatpickr-mode="range" data-flatpickr-alt-format="d/m/Y" data-flatpickr-date-format="d/m/Y">
                                        <a href="javascript:void(0)" class="link-date" data-toggle>13/03/2018 to 20/03/2018</a>
                                        <input class="flatpickr-hidden-input" type="hidden" value="13/03/2018 to 20/03/2018" data-input>
                                    </div>
                                </div>
                    
                            </div>
                        </div>
                    </div>
                    
                    <div class="card-body">
                    <img id="nohaydatos" width="100%" src="{{ asset('images/axin/No_hay_datos_disponibles.png') }}">   
                    <div id="Barchart">
                        </div>
                    </div>
                    
                </div>
            </div>
        </div>

    </div>
</div>

@endsection
@section('script')
    <script>
    getStadistics(1,'cliente');
    </script>
@endsection