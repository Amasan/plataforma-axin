@extends('layout')

@section('content')

<div class="container page__container">
    
    <style type="text/css">
        .wrapper {
            display: flex;
            justify-content: center;
      -moz-user-select: none;
      -webkit-user-select: none;
      -ms-user-select: none;
      user-select: none;
    }
    
    .signature-pad {
        border-color: Black;
        border: 1px solid;
        display: flex;
        justify-content: center;
        align-items: center;

    }

    .size {
        width: 50vw;
        height: 120px;
    }

    #cargando {
        display: none;
    }

    .btn-primary {
        border-color: white!important;
    }
    .btn-primary:focus {
        outline: none!important;
        border-color: white!important;
        box-shadow: none!important;
    }
</style>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/signature_pad/1.5.3/signature_pad.min.js"></script>
    <form method="POST" id="firmar_form" enctype="multipart/form-data" action="{{ route('update_firmac') }}">
        <input type="hidden" name="_token" value="{{ csrf_token() }}"></input>
        <div class="row">
            <div class="col-lg-9 pr-lg-0">

                <div class="page-section">
                    <h4>Perfil - Firma de contrato</h4>
                    <br><br><br>
                    <div class="list-group list-group-form">
                        <div class="list-group-item space">
                            <br>
                        
                            <h6 class="text-center"><a class="btn btn-primary" style="background: #c1c127" href="{{ url('ver_contrato') }}" target="_blank">Ver contrato</a></h6>

                            <br><br>
                            <p class="text-left">Firmar dentro del siguiente recuadro</p>
                            
                            <div class="wrapper m-auto">
                                <canvas id="signature-pad" class="signature-pad size"></canvas>
                            </div>
                            <div class="m-auto">
                                <img id="MyPix" style="display:none;" src="">
                            </div>
                            <br>
                            <div class="d-flex justify-content-around">
                                <button class="btn btn-primary" style="background-color: #6a95d8; width:13rem;color:white; height: 2rem!important;" type="button" id="clear"><img width="15" src="{{asset('images/axin/icono_rehacer_firma.png')}}" alt="">&nbsp;Rehacer firma</button>
                                <button hidden class="btn btn-success" type="button"  >Firmar contrato</button>
                        
                                <button class="btn btn-primary botonSiguientePFIRMA" style="background-color: #185bc3; width:13rem;color:white; height: 2rem!important;"  onclick="putImage()" id="boton" type="button">
                                    <span class="spinner-text">Terminar</span>
                                    <span class="spinner-grow spinner-grow-sm" role="status" aria-hidden="true"></span>
                                    <span class="spinner-grow spinner-grow-sm" role="status" aria-hidden="true"></span>
                                    <span class="spinner-grow spinner-grow-sm" role="status" aria-hidden="true"></span>
                                </button>
                            </div>
                            <br>
                            
                        
            

                    
                        <script>
                            
                            var canvas = document.getElementById('signature-pad');

                           /* function clickdraw() {
                                document.getElementById("clear").removeAttribute("disabled");
                            }*/
                        
                            
                              // Adjust canvas coordinate space taking into account pixel ratio,
                              // to make it look crisp on mobile devices.
                              // This also causes canvas to be cleared.
                            function resizeCanvas() {
                                  // When zoomed out to less than 100%, for some very strange reason,
                                  // some browsers report devicePixelRatio as less than 1
                                  // and only part of the canvas is cleared then.
                                var ratio =  Math.max(window.devicePixelRatio || 1, 1);
                                canvas.width = canvas.offsetWidth * ratio;
                                canvas.height = canvas.offsetHeight * ratio;
                                canvas.getContext("2d").scale(ratio, ratio);
                            }
                            
                            window.onresize = resizeCanvas;
                            resizeCanvas();
                            
                            var signaturePad = new SignaturePad(canvas, {
                                backgroundColor: 'rgb(255, 255, 255)' // necessary for saving image as JPEG; can be removed is only saving as PNG or SVG
                            });
                            

                            document.getElementById('clear').addEventListener('click', function () {
                                signaturePad.clear();
                            });
                            
                            

                            function dataURLtoFile(dataurl, filename) {
 
                                var arr = dataurl.split(','),
                                    mime = arr[0].match(/:(.*?);/)[1],
                                    bstr = atob(arr[1]), 
                                    n = bstr.length, 
                                    u8arr = new Uint8Array(n);
                                    
                                while(n--){
                                    u8arr[n] = bstr.charCodeAt(n);
                                }
                                
                                return new File([u8arr], filename, {type:mime});
                            }
                            
                            
                            function putImage()
                            {   
                                setTimeout(() => {
                                        $('.spinner-text').hide();
                                        $('.spinner-grow').show();
                                        $(".botonSiguientePFIRMA").attr('disabled','disabled');
                                    }, 100);
                                var canvas1 = document.getElementById("signature-pad");        
                                if (canvas1.getContext) {
                                    var ctx = canvas1.getContext("2d");                
                                    var myImage = canvas1.toDataURL("image/png");      
                                }
                                var imageElement = document.getElementById("MyPix");  
                            
                                imageElement.src = myImage;
                            

                                var canvas_img_data = canvas.toDataURL('image/png');
                                imageElement.src = canvas_img_data;
                                
                                var link = document.createElement('a');
                                link.href = canvas.toDataURL('image/png');
                                link.click();
                            
                                var image = document.createElement('img');
                                image.src = link.href;

                                const url = link.href;
                                fetch(url)
                                .then(res => res.blob())
                                .then(blob => {
                                    const image = new File([blob], "prueba.png",{ type: "image/png" });
                                    var fd = new FormData();
                                    fd.append('signature',image);
                                    $("#cargando").prop('disabled', true);
                                    $.ajax({
                                        url: 'update_firm',
                                        data: fd,
                                        type: 'post',
                                        contentType: false,
                                        processData: false,
                                        success: function (response) {
                                            console.log(response);
                                            
                                            window.open(response, '_blank');
                                            location.reload();
                                        },
                                        error: function(e) {
                                            console.log(e);
                                            //$("#cargando").removeAttr("disabled");
                                        }
                                    });  
                                });
                            }
                        </script>
                    
                            
                        </div>
                
                    </div>
                </div>

            </div>
            <div class="col-lg-3 page-nav">
                <div class="page-section pt-lg-112pt">
                    <nav class="nav page-nav__menu historialNav">
                        
                        <a class="nav-link disabled" href="{{ url('configuracion/datosp') }}">Datos personales</a>
                        <a class="nav-link disabled" href="{{ url('configuracion/actividade') }}">Actividad económica</a>
                        <a class="nav-link disabled" href="{{ url('configuracion/cargard') }}">Cargar documentos</a>
                        <a class="nav-link disabled active" href="{{ url('configuracion/firmac') }}">Firma de contrato</a>

                
                    </nav>
                </div>
            </div>
        </div>
    </form>
</div>

@endsection