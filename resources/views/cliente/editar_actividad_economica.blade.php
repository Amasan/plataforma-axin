@extends('layout')

@section('content')



<div class="container page__container">
    <style>
        ::placeholder { 
            color: #185bc3!important;
            opacity: 1; 
        }
        .box{
       
            display: inline;
         
        }
        .1{ display: none; }


        select:invalid {
			border-color: #DD2C00!important;
        }

        .check-ok {
            color:#185bc3!important;
            position: absolute; right: 38px; top: 10px!important;
        }

        select:invalid ~ .check-ok {
            display: none!important;
        }

        select:valid ~ .check-ok {
            display: inline!important;
        }
    </style>
    
    <form method="POST" action="{{ route('update_actividade', $client->id) }}" id="frmA">
        <input type="hidden" name="_token" value="{{ csrf_token() }}" id="_token"></input>
        <div class="row">
            <div class="col-lg-9 pr-lg-0">

                <div class="page-section">
                    <h4>Perfil - Actividad económica</h4>
                    <br><br><br>
                    <div class="list-group list-group-form">
                        <div class="list-group-item space">
                            <br>
                            <h6 class="text-center">Cuéntanos un poco de tus ingresos</h6>
                            <div class="form-group mb-0">
                                <div class="form-row">
                                    <div class="col-12 col-md-12 col-lg-12">
                                        <label class="col-form-label form-label col-12 col-md-12 col-lg-12" style="padding:0;">¿ESTADO DE EMPLEO?</label>
                                        <div class="col-12 col-md-12 col-lg-12" style="padding:0;">
                                            <select  name="state_employee" value="{{ $client->state_employee }}" id="state_employee" class="form-control custom-select @error('state_employee') is-invalid @enderror " required>
                                                <option value="{{ $client->state_employee }}"> {{$client->state_employee}}</option>
                                                @if($client->state_employee != 'Empleado')<option value="Empleado">Empleado</option>@endif
                                                @if($client->state_employee != 'Independiente')<option value="Independiente">Independiente</option>@endif
                                                @if($client->state_employee != 'Desempleado')<option value="Desempleado">Desempleado</option>@endif
                                                @if($client->state_employee != 'Jubilado')<option value="Jubilado">Jubilado</option>@endif
                                                @if($client->state_employee != 'Estudiante')<option value="Estudiante">Estudiante</option>@endif
                                                
                                            </select><i class="fa fa-check check-ok"></i>
                                            
                                            @error('state_employee')
                                                <span class="invalid-feedback" role="alert">
                                                    <strong style="color:red!important">{{ $message }}</strong>
                                                </span>
                                            @enderror
                                        </div>
                                    </div>
                                </div>
                            </div>   
                            <div class="form-group mb-0">
                                <div class="form-row">
                                    <div class="col-12 col-md-12 col-lg-12">
                                        <label class="col-form-label form-label col-12 col-md-12 col-lg-12" style="padding:0; font-size:0.85rem!important;">INGRESO ANUAL (USD)</label>
                                        <div class="col-12 col-md-12 col-lg-12" style="padding:0;">
                                            <select  name="anual_activity_income_id" value="{{$client->anual_activity_income_id}}" id="anual_activity_income_id" class="form-control custom-select @error('anual_activity_income_id') is-invalid @enderror " required>
                                                <option value="{{ $client->anual_activity_income_id }}"> {{$client->anual->description}}</option>
                                                @foreach($anualIncomes as $item)
                                                @if($item['id'] != $client->anual->id)<option value="{{$item['id']}}">{{$item['description']}}</option>@endif
                                                
                                                @endforeach
                                            </select><i class="fa fa-check check-ok"></i>
                                            @error('anual_activity_income_id')
                                                <span class="invalid-feedback" role="alert">
                                                    <strong style="color:red!important">{{ $message }}</strong>
                                                </span>
                                            @enderror
                                        </div>
                                    </div>
                                </div>
                            </div>   
                            <div class="form-group mb-0">
                                <div class="form-row">
                                    <div class="col-12 col-md-12 col-lg-12">
                                        <label class="col-form-label form-label col-12 col-md-12 col-lg-12" style="padding:0;">TOTAL DE FONDOS A INVERTIR (USD)</label>
                                        <div class="col-12 col-md-12 col-lg-12" style="padding:0;">
                                            <select  name="total_funds_invest_id" value="{{$client->total_funds_invest_id}}" id="total_funds_invest_id" class="form-control custom-select @error('total_funds_invest_id') is-invalid @enderror " required>
                                                <option value="{{$client->total_funds_invest_id}}" selected>{{$client->total->description}}</option>
                                                @foreach($totalFundInvest as $item)
                                                @if($item['id'] != $client->total->id)<option value="{{$item['id']}}">{{$item['description']}}</option>@endif
                                                
                                                
                                                @endforeach
                                            </select><i class="fa fa-check check-ok"></i>
                                            @error('total_funds_invest_id')
                                                <span class="invalid-feedback" role="alert">
                                                    <strong style="color:red!important">{{ $message }}</strong>
                                                </span>
                                            @enderror
                                        </div>
                                    </div>
                                </div>
                            </div>   
                            <div class="form-group mb-0">
                                <div class="form-row">
                                    <div class="col-12 col-md-12 col-lg-12">
                                        <label class="col-form-label form-label col-12 col-md-12 col-lg-12" style="padding:0;">ORIGEN DE LOS FONDOS</label>
                                        <div class="col-12 col-md-12 col-lg-12" style="padding:0;">
                                            <select  name="family_member_holds_public_office_id" value="{{$client->total_funds_invest_id}}" id="family_member_holds_public_office_id" class="form-control custom-select @error('family_member_holds_public_office_id') is-invalid @enderror " required>
                                                <option value="{{$client->total_funds_invest_id}}" selected>{{$client->public->description}}</option>
                                                @foreach($publicOfficeEmployees as $item)
                                                @if($item['id'] != $client->public->id)<option value="{{$item['id']}}">{{$item['description']}}</option>@endif
                                                
                                                @endforeach
                                            </select><i class="fa fa-check check-ok"></i>
                                            @error('family_member_holds_public_office_id')
                                                <span class="invalid-feedback" role="alert">
                                                    <strong style="color:red!important">{{ $message }}</strong>
                                                </span>
                                            @enderror
                                        </div>
                                    </div>
                                </div>
                            </div>   
                            <div class="form-group mb-0">
                                <div class="form-row">
                                    <div class="col-xs-12 col-md-6 col-lg-6 1 box">
                                        <label class="col-form-label form-label col-xs-12 col-md-12 col-lg-12" style="padding:0; font-size: 11px!important;">¿ERES UNA PERSONA EXPUESTA POLÌTICAMENTE?</label>
                                        <select  name="is_politically_exposed" value="{{$client->is_politically_exposed}}" id="is_politically_exposed" class="form-control custom-select @error('is_politically_exposed') is-invalid @enderror " required>
                                            <option value="{{$client->is_politically_exposed}}" selected>
                                                @if($client->is_politically_exposed) Si
                                                @else No
                                                @endif
                                            </option>
                                            @if($client->is_politically_exposed != '1')<option value="1">Si</option>@endif
                                            @if($client->is_politically_exposed != '0')<option value="0">No</option>@endif
                                            

                                        </select><i class="fa fa-check check-ok" style="top: 33px!important;"></i>
                                        @error('is_politically_exposed')
                                                <span class="invalid-feedback" role="alert">
                                                    <strong style="color:red!important">{{ $message }}</strong>
                                                </span>
                                        @enderror
                                    </div>
                                    <div class="col-xs-12 col-md-6 col-lg-6 1 box">
                                        <label class="col-form-label form-label col-xs-12 col-md-12 col-lg-12" style="padding:0; font-size: 11px!important;">¿ESTÀS DECLARADO EN BANCA ROTA?</label>
                                        <select  name="is_declared_bankrupt" value="{{$client->is_declared_bankrupt}}" id="is_declared_bankrupt" class="form-control custom-select @error('is_declared_bankrupt') is-invalid @enderror " required>
                                            <option value="{{$client->is_declared_bankrupt}}" selected>
                                                @if($client->is_declared_bankrupt) Si
                                                @else No
                                                @endif
                                            </option>
                                            @if($client->is_declared_bankrupt != '1')<option value="1">Si</option>@endif
                                            @if($client->is_declared_bankrupt != '0')<option value="0">No</option>@endif
                                        </select><i class="fa fa-check check-ok" style="top: 33px!important;"></i>
                                        @error('is_declared_bankrupt')
                                                <span class="invalid-feedback" role="alert">
                                                    <strong style="color:red!important">{{ $message }}</strong>
                                                </span>
                                        @enderror
                                    </div>
                            
                                </div>
                            </div>   
            
                            <br>
                            <div class="form-group row align-items-center mb-0">
                                <div class="col-sm-12" style="display: flex; justify-content:center;">
                                    <div class="col-sm-12" style="display: flex; justify-content:center;">
                                        <button class="btn btn-accent botonSiguienteP" style="background-color: #185bc3; width:15rem;color:white;" id="boton" type="submit" @if($client->complete_profile == '5')disabled @endif>
                                            <span class="spinner-text">Actualizar</span>
                                            <span class="spinner-grow spinner-grow-sm" role="status" aria-hidden="true"></span>
                                            <span class="spinner-grow spinner-grow-sm" role="status" aria-hidden="true"></span>
                                            <span class="spinner-grow spinner-grow-sm" role="status" aria-hidden="true"></span>
                                        </button>
                                    </div>
                                </div>

                            </div>
                            
                        </div>
                
                    </div>
                </div>

            </div>
            <div class="col-lg-3 page-nav">
                <div class="page-section pt-lg-112pt">
                    <nav class="nav page-nav__menu historialNav">
                        
                        <a class="nav-link active" href="{{ url('personal/editar_actividade') }}">Actividad económica</a>
                        <a class="nav-link" href="{{ url('personal/editar_cargard') }}">Cargar documentos</a>

                
                    </nav>
                </div>
            </div>
        </div>
    </form>
</div>

@endsection