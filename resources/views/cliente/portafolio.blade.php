@extends('layout')

@section('content')

<div class="container page__container">
    <style>
        .menu {
            background: aliceblue;
        }

        .boton:hover {
            outline: none!important;
            border: none;
            background: #f2f5f7;
            margin: auto!important;
        
        }
        .boton:focus {
            outline: none!important;
            border: none;
            background: #f2f5f7;
            margin: auto!important;
        
        }
        .caretdown {
            position: absolute;
            right: -12px!important;
            top: 45px;
            font-size: 24px;
        }
    </style>
    <div class="page-section">
        <div class="form-row card card-form col-12 col-md-12 col-lg-12 m-auto" style="flex-direction: row!important;">
            <div class="col-12 col-md-6 col-lg-7">

                <div class="form-row dropdown col-12 col-md-12 col-lg-12">

                    <div class="cuentaA form-row dropbtn col-12 col-md-12 col-lg-12">
                        <div class="col-xs-12 col-md-12 col-lg-5">
                            <div class="">
                                <label style="text-transform: none">Tipo de cuenta</label><br>
                                <label class="text-center" style="text-transform: none; color: black; font-size:1.5rem; font-weight:bold;">{{$accountA->typeaccount->name}}</label><br>
                            </div>
                        </div>
                        <div class="col-xs-12 col-md-12 col-lg-7">
                            <div class="">
                                <label style="text-transform:none; font-size:1rem;">Rendimiento promedio: <span style="color: black!important; font-weight:bold; text-transform:none;">1.5%/mes </span> </label><br>
                                <label style="text-transform:none; font-size:1rem;">Inversión minima: <span style="color: black!important; font-weight:bold; text-transform:none;">$100 USD </span> </label><br>
                                <label style="text-transform:none; font-size:1rem;">Plazo: <span style="color: black!important; font-weight:bold; text-transform:none;">Sin plazo</span>  </label><br>
                            </div>
                        </div>
                        <span class="caretdown"><i class="fa fa-caret-down"></i></span>
                    </div>
                    <div class="cuentaI form-row dropbtn col-12 col-md-12 col-lg-12">
                        <div class="col-xs-12 col-md-12 col-lg-5">
                            <div class="">
                                <label style="text-transform: none">Tipo de cuenta</label><br>
                                <label class="text-center" style="text-transform: none; color: black; font-size:1.5rem; font-weight:bold;">{{$accountI->typeaccount->name}}</label><br>
                            </div>
                        </div>
                        <div class="col-xs-12 col-md-12 col-lg-7">
                            <div class="">
                                <label style="text-transform:none; font-size:1rem;">Rendimiento promedio: <span style="color: black!important; font-weight:bold; text-transform:none;">1.5%/mes </span> </label><br>
                                <label style="text-transform:none; font-size:1rem;">Inversión minima: <span style="color: black!important; font-weight:bold; text-transform:none;">$100 USD </span> </label><br>
                                <label style="text-transform:none; font-size:1rem;">Plazo: <span style="color: black!important; font-weight:bold; text-transform:none;">Sin plazo</span>  </label><br>
                            </div>
                        </div>
                        <span class="caretdown"><i class="fa fa-caret-down"></i></span>
                    </div>
                    <div class="cuentaM form-row dropbtn col-12 col-md-12 col-lg-12">
                        <div class="col-xs-12 col-md-12 col-lg-5">
                            <div class="">
                                <label style="text-transform: none">Tipo de cuenta</label><br>
                                <label class="text-center" style="text-transform: none; color: black; font-size:1.5rem; font-weight:bold;">{{$accountM->typeaccount->name}}</label><br>
                            </div>
                        </div>
                        <div class="col-xs-12 col-md-12 col-lg-7">
                            <div class="">
                                <label style="text-transform:none; font-size:1rem;">Rendimiento promedio: <span style="color: black!important; font-weight:bold; text-transform:none;">1.5%/mes </span> </label><br>
                                <label style="text-transform:none; font-size:1rem;">Inversión minima: <span style="color: black!important; font-weight:bold; text-transform:none;">$100 USD </span> </label><br>
                                <label style="text-transform:none; font-size:1rem;">Plazo: <span style="color: black!important; font-weight:bold; text-transform:none;">Sin plazo</span>  </label><br>
                            </div>
                        </div>
                        <span class="caretdown"><i class="fa fa-caret-down"></i></span>
                    </div>

                    <div class="form-row dropdown-content col-12 col-md-12 col-lg-12">
                        <a href="#" class="accountA col-12 col-md-12 col-lg-12">
                            <div class="form-row dropbtn col-12 col-md-12 col-lg-12">
                            
                                    <div class=" col-xs-12 col-md-12 col-lg-5">
                                        <label style="text-transform: none">Tipo de cuenta</label><br>
                                        <label class="text-center" style="text-transform: none; color: black; font-size:1.5rem; font-weight:bold;">{{$accountA->typeaccount->name}}</label><br>
                                    </div>
                                
                            
                                    <div class=" col-xs-12 col-md-12 col-lg-7">
                                        <label style="text-transform:none; font-size:1rem;">Rendimiento promedio: <span style="color: black!important; font-weight:bold; text-transform:none;">1.5%/mes </span> </label><br>
                                        <label style="text-transform:none; font-size:1rem;">Inversión minima: <span style="color: black!important; font-weight:bold; text-transform:none;">$100 USD </span> </label><br>
                                        <label style="text-transform:none; font-size:1rem;">Plazo: <span style="color: black!important; font-weight:bold; text-transform:none;">Sin plazo</span>  </label><br>
                                    </div>
                                
                            </div>
                        </a>
                        <a href="#" class="accountI col-12 col-md-12 col-lg-12">
                            <div class="form-row dropbtn col-12 col-md-12 col-lg-12">
                            
                                    <div class=" col-xs-12 col-md-12 col-lg-5">
                                        <label style="text-transform: none">Tipo de cuenta</label><br>
                                        <label class="text-center" style="text-transform: none; color: black; font-size:1.5rem; font-weight:bold;">{{$accountI->typeaccount->name}}</label><br>
                                    </div>
                                
                            
                                    <div class=" col-xs-12 col-md-12 col-lg-7">
                                        <label style="text-transform:none; font-size:1rem;">Rendimiento promedio: <span style="color: black!important; font-weight:bold; text-transform:none;">1.5%/mes </span> </label><br>
                                        <label style="text-transform:none; font-size:1rem;">Inversión minima: <span style="color: black!important; font-weight:bold; text-transform:none;">$100 USD </span> </label><br>
                                        <label style="text-transform:none; font-size:1rem;">Plazo: <span style="color: black!important; font-weight:bold; text-transform:none;">Sin plazo</span>  </label><br>
                                    </div>
                                
                            </div>
                        </a>
                        <a href="#" class="accountM col-12 col-md-12 col-lg-12">
                            <div class="form-row dropbtn col-12 col-md-12 col-lg-12">
                            
                                    <div class=" col-xs-12 col-md-12 col-lg-5">
                                        <label style="text-transform: none">Tipo de cuenta</label><br>
                                        <label class="text-center" style="text-transform: none; color: black; font-size:1.5rem; font-weight:bold;">{{$accountM->typeaccount->name}}</label><br>
                                    </div>
                                
                            
                                    <div class=" col-xs-12 col-md-12 col-lg-7">
                                        <label style="text-transform:none; font-size:1rem;">Rendimiento promedio: <span style="color: black!important; font-weight:bold; text-transform:none;">1.5%/mes </span> </label><br>
                                        <label style="text-transform:none; font-size:1rem;">Inversión minima: <span style="color: black!important; font-weight:bold; text-transform:none;">$100 USD </span> </label><br>
                                        <label style="text-transform:none; font-size:1rem;">Plazo: <span style="color: black!important; font-weight:bold; text-transform:none;">Sin plazo</span>  </label><br>
                                    </div>
                                
                            </div>
                        </a>
                    </div>
                   
                    
                </div>
            </div>
            <div class="col-12 col-md-6 col-lg-5">
                <br><br>
                <div>
                    @if($complete_profile!=10)
                    <a href="{{ url('banca/depositar_fondos') }}" class="btn btn-accent float-i8-right disabled" style="border-radius:7px; background-color: #185bc3; border:none; width:12rem;">Depositar fondos</a> 
                    @endif
                    @if($complete_profile==10)
                    <a href="{{ url('banca/depositar_fondos') }}" class="btn btn-accent float-i8-right " style="border-radius:7px; background-color: #185bc3; border:none; width:12rem;">Depositar fondos</a> 
                    @endif
                    
                </div>
            </div>
        </div>
        <br><br>
        <div class="row card-group-row mb-lg-8pt">

            <div class="col-lg-4 col-md-6 card-group-row__col">
                <div class="card card-group-row__card">
                    <div class="card-body d-flex align-items-center">
                        <div class="flex d-flex align-items-center">
                            <div class="flex">
                                <p class="mb-0" style="font-size: 12px!important;"><strong>Rendimiento mensual</strong></p>
                                <p class="mb-0 mt-n1 d-flex align-items-center">
                                    0.00%
                                </p>
                            </div>
                        </div>
                        <img width="30" height="auto" src="{{asset('images/axin/tresico.png')}}" alt="">
                    </div>
                </div>
            </div>
            <div class="col-lg-4 col-md-6 card-group-row__col">
                <div class="card card-group-row__card">
                    <div class="card-body d-flex align-items-center">
                        <div class="flex d-flex align-items-center">
                            <div class="flex">
                                <p class="mb-0" style="font-size: 12px!important;"><strong>Mejor mes</strong></p>
                                <p class="mb-0 mt-n1 d-flex align-items-center">
                                    0.00%
                                </p>
                            </div>
                        </div>
                        <img width="30" height="auto" src="{{asset('images/axin/inoico.png')}}" alt="">
                    </div>
                </div>
            </div>
            <div class="col-lg-4 col-md-6 card-group-row__col">
                <div class="card card-group-row__card">
                    <div class="card-body d-flex align-items-center">
                        <div class="flex d-flex align-items-center">
                            <div class="flex">
                                <p class="mb-0" style="font-size: 12px!important;"><strong>Rendimiento acomulado</strong></p>
                                <p class="mb-0 mt-n1 d-flex align-items-center">
                                    0.00%
                                </p>
                            </div>
                        </div>
                        <img width="30" height="auto" src="{{asset('images/axin/Beneficio.png')}}" alt="">
                    </div>
                </div>
            </div>
        </div>
   
        <div class="card mb-32pt">
            <ul class="nav nav-tabs nav-tabs-card">
                <li class="nav-item">
                    <button onclick="Bar()" class="nav-link menu boton" href="#portafolioBar" data-toggle="tab">2020</button>
                </li>
                <li class="nav-item">
                    <button onclick="Area()" class="nav-link menu boton" href="#portafolioArea" data-toggle="tab">2021</button>
                </li>
                <li class="nav-item">
                    <button onclick="Donut()" class="nav-link menu boton" href="#portafolioDonut" data-toggle="tab">2022</button>
                </li>
            </ul>
            <div class="card-body tab-content text-70">
                <div class="tab-pane active" id="portafolioBar"></div>
                <div class="tab-pane" id="portafolioArea"></div>
                <div class="tab-pane" id="portafolioDonut"></div>
            </div>

        </div>
      


        
        
    </div>
</div>

@endsection