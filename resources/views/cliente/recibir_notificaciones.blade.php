@extends('layout')

@section('content')

<div class="container page__container">
    <form id="notification_form" method="POST" action="{{ route('update_notificaciones') }}">
    <input type="hidden" name="_token" value="{{ csrf_token() }}"></input>
        <div class="row">
            <div class="col-lg-9 pr-lg-0">
                <style>
                    :checked{
                        background-color: #2196F3!important;
                    }
                    input, button {
                        height: auto!important; 
                    }
                </style>
                <div class="page-section">
                    <h4>Ajustes - Notificaciones por correo</h4>
                    <br><br><br>
                    <div class="list-group list-group-form">
                        <div class="list-group-item space">
                            <br>
                            <div class="form-group row align-items-center mb-0">
                                <div class="form-check">
                                    <input id="is_active_newsletter" onchange="handleChange(event)" name="is_active_newsletter"  value="{{$is_active_newsletter}}" type="checkbox" class="form-check-input">
                                    <label class="form-check-label" for="exampleCheck1" style="font-weight: bold;">Newsletter</label>
                                </div>
                                <label class="col-form-label form-label col-sm-12">Entérate de las últimas noticias de Axin</label>
                            </div>
                            <hr>
                            <div class="form-group row align-items-center mb-0">
                                <div class="form-check">
                                    <input id="is_active_movements" onchange="handleChange(event)" name="is_active_movements" value="{{$is_active_movements}}" type="checkbox" class="form-check-input">
                                    <label class="form-check-label" for="exampleCheck1" style="font-weight: bold;">Movimientos</label>
                                </div>
                                <label class="col-form-label form-label col-sm-12">Notificarme de movimientos como: depósito, retiro, performance fee, magnament fee, statements</label>
                            </div>
                            <br>
                            <div class="form-group row align-items-center mb-0">
                                <div class="col-sm-12" style="display: flex; justify-content:center;">
                                    <button class="btn btn-accent botonSiguienteP" style="background-color: #185bc3; width:15rem;color:white" id="boton" type="submit">
                                        <span class="spinner-text">Guardar cambios</span>
                                        <span class="spinner-grow spinner-grow-sm" role="status" aria-hidden="true"></span>
                                        <span class="spinner-grow spinner-grow-sm" role="status" aria-hidden="true"></span>
                                        <span class="spinner-grow spinner-grow-sm" role="status" aria-hidden="true"></span>
                                    </button>
                                </div>
                            </div>
                            <script>
                                function handleChange(e){
                                    var checkbox = event.target;
                                    checkbox.value = checkbox.checked?1:0;
                                }
                                var v1 = document.getElementById('is_active_movements');
                                var v2 = document.getElementById('is_active_newsletter');
                                v1.checked = v1.value=='0'?false:true;
                                v2.checked = v2.value=='0'?false:true;
                            </script>
                        </div>
                
                    </div>
                    
                </div>

            </div>
            <div class="col-lg-3 page-nav">
                <div class="page-section pt-lg-112pt">
        
                    <nav class="nav page-nav__menu historialNav">
                        <h5>&nbsp;&nbsp;&nbsp;&nbsp;AJUSTES</h5>
                        <a class="nav-link" href="{{ url('/configuracion/password') }}">Cambiar Contraseña</a>
                        <a class="nav-link active" href="{{ url('/configuracion/notificaciones') }}">Notificaciones por correo</a>

                    </nav>
                </div>
            </div>
        </div>
    </form>
</div>

@endsection
