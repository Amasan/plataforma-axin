@extends('layout')

@section('content')

<div class="container page__container">
    <style>
        ::placeholder { 
            color: #185bc3!important;
            opacity: 1; 
        }
        input:invalid {
			border-color: #DD2C00!important;
        }

        .check-ok {
            color:#185bc3!important;
            position: inherit; right: -20px; top: 10px!important;
        }
        
        .check-No {
            color: red!important;
            position: inherit; right: -20px; top: 10px!important;
        }
    
        input:invalid ~ .check-ok {
            display: none!important;
        }

        input:valid ~ .check-ok {
            display: inline!important;
        }
        input:invalid ~ .check-No {
            display: inline!important;
        }

        input:valid ~ .check-No {
            display: none!important;
        }
    </style>
    <script src="https://code.jquery.com/jquery-1.12.4.min.js"></script>
    <script>
    $(document).ready(function(){
        $("#changeIdentificacion0").click(function(){
       
            $('#file_document_identity_oficial_V1').show();
            $('#file_document_identity_oficial_V2').show();

            $('#file_document_identity_oficial_V1').prop("required", true);
            $('#file_document_identity_oficial_V2').prop("required", true);

        });
        $("#changeIdentificacion1").click(function(){
       
            $('#file_document_identity_oficial_V1').show();
            $('#file_document_identity_oficial_V2').hide();

            $('#file_document_identity_oficial_V1').prop("required", true);
            $('#file_document_identity_oficial_V2').removeAttr("required");

        });
    });
    </script>
    <form id="update_cargard_form" enctype="multipart/form-data" method="POST"  action="{{ route('update_cargard') }}">
        <input type="hidden" name="_token" value="{{ csrf_token() }}"></input>
        <div class="row">
            <div class="col-lg-9 pr-lg-0">

                <div class="page-section">
                    <h4>Perfil - Cargar documentos</h4>
                    <br><br><br>
                    <div class="list-group list-group-form">
                        <div class="list-group-item space">
                            <br>
                            <h6 class="text-center">Estamos a un paso de finalizar, comprueba tu identidad</h6>
                            <div class="form-row col-12 col-md-12 col-lg-12">
                                <div class="col-12 col-md-12 col-lg-12" style="background-color: #eaf6f9; display:flex; height:60px!important">
                                    <label class="col-12 col-md-12 col-lg-12 text-center" style="margin: auto!important;">Te recomendamos que cada archivo sea menor a <span style="color: black; font-weight:bold;">3 MB</span></label>
                                </div>
                            </div><br><br>
                            <div class="form-group mb-0">
                                <div class="form-row @error('type_identity_oficial') is-invalid @enderror">
                                    <div class="col-12 col-md-6 col-lg-6" style="display: inline!important   ">
                                        <div class="form-row">
                                            <img src="{{ asset('images/axin/Identificacion_oficial.png') }}" class="col-form-label form-label col-2 col-md-2 col-lg-2" style="bottom: 13px;" alt="">
                                            <label class="col-form-label form-label col-10 col-md-10 col-lg-10" style="padding:0; color: black!important">IDENTIFICACIÓN OFICIAL</label>
                                        </div>
                                        <label class="col-form-label form-label col-12 col-md-12 col-lg-12" style="padding:0; font-size:0.6rem!important; bottom:20px;">Asegurate que esté vigente. En formato JPG o PNG.</label>

                                    </div>
                                    <div class="col-12 col-md-6 col-lg-6">
                                        <div class="form-row">
                                            <div class="col-6 col-md-6 col-lg-6">
                                                <div class="col-12 col-md-12 col-lg-12" style="padding:0;">
                                                    <button type="button" id="changeIdentificacion0" onclick="changeIdentificacion(0)" class="form-control col-12 col-md-12 col-lg-12" value="IFE/INE" style="color: #185bc3!important">IFE/INE</button>
                                                </div>
                                            </div>
                                            <div class="col-6 col-md-6 col-lg-6">
                                                <div class="col-12 col-md-12 col-lg-12" style="padding:0;">
                                                    <button type="button" id="changeIdentificacion1" onclick="changeIdentificacion(1)" class="form-control col-12 col-md-12 col-lg-12" value="Pasaporte" style="color: #185bc3!important">Pasaporte</button>
                                                </div> 
                                            </div>
                                        </div>
                                    </div>
                                    
                                </div>
                                @error('type_identity_oficial')
                                        <span class="invalid-feedback" role="alert">
                                            <strong style="color:red!important">{{ $message }}</strong>
                                        </span>
                                @enderror
                            </div>
                            <input hidden type="text" value="{{ old('type_identity_oficial') }}" id="type_identity_oficial"  name="type_identity_oficial">
                            <div class="form-group mb-0">
                                <div class="text-center col-12"><h8 class="invalid" style="color:red;"></h8></div>
                                <div class="form-row">
                                    <div class="col-12 col-md-12 col-lg-12">
                                        <div class="file-input @error('file_document_identity_oficial_V1') is-invalid @enderror" id="file_document_identity_oficial_V1">
                                            <input value="{{ old('file_document_identity_oficial_V1') }}" id="file_document_identity_oficial_V1" name="file_document_identity_oficial_V1" type="file" accept="image/jpeg,image/jpg,image/png" required><i class="fa fa-check check-ok"></i><i class="fa fa-times check-No"></i>
                                            <span style="word-wrap: break-word!important;" class='button' data-js-label>+ Selecciónar frente</span>
                                        </div>
                                          
                                        @error('file_document_identity_oficial_V1')
                                        <span class="invalid-feedback" role="alert">
                                            <strong style="color:red!important">{{ $message }}</strong>
                                        </span>
                                        @enderror
                                    </div>
                                </div>
                            </div>   
                            <div class="form-group mb-0">
                                <div class="form-row">
                                    <div class="col-12 col-md-12 col-lg-12">
                                        <div class="file-input @error('file_document_identity_oficial_V2') is-invalid @enderror" id="file_document_identity_oficial_V2">
                                            <input value="{{ old('file_document_identity_oficial_V2') }}" id="file_document_identity_oficial_V2" name="file_document_identity_oficial_V2" type='file' accept="image/jpeg,image/jpg,image/png" required><i class="fa fa-check check-ok"></i><i class="fa fa-times check-No"></i>
                                            <span style="word-wrap: break-word!important;" class='button' data-js-label>+ Selecciónar el reverso</span>
                                        </div>
                                        
                                        @error('file_document_identity_oficial_V2')
                                        <span class="invalid-feedback" role="alert">
                                            <strong style="color:red!important">{{ $message }}</strong>
                                        </span>
                                        @enderror
                                    </div>
                                    
                                </div>
                            </div>

                            <div class="form-group mb-0">
                                <div class="form-row">
                                    <div class="col-12 col-md-6 col-lg-6" style="display: inline!important   ">
                                        <div class="form-row">
                                            <img src="{{ asset('images/axin/Domicilio.png') }}" class="col-form-label form-label col-2 col-md-2 col-lg-2" style="bottom: 13px;" alt="">
                                            <label class="col-form-label form-label col-10 col-md-10 col-lg-10" style="padding:0; color: black!important">DOMICILIO</label>
                                        </div>
                                    </div><BR></BR>
                                    <label class="col-form-label form-label col-12 col-md-12 col-lg-12" style="padding:0; font-size:0.6rem!important; bottom:20px;">Puedes subir recibo de gas, internet, luz, estado de cuenta, no mayor a 3 meses.</label>
                                    <label class="col-form-label form-label col-12 col-md-12 col-lg-12" style="padding:0; font-size:0.6rem!important; bottom:20px;">En formato JPG, PNG o PDF.</label>
                                </div>
                            </div>

                            <div class="form-group mb-0">
                                <div class="form-row">
                                    <div class="col-12 col-md-12 col-lg-12">
                                        <div class="file-input @error('file_document_home') is-invalid @enderror">
                                            <input value="{{ old('file_document_home') }}" name="file_document_home" type='file' accept="image/jpeg,image/jpg,image/png,application/pdf" required><i class="fa fa-check check-ok"></i><i class="fa fa-times check-No"></i>
                                            <span style="word-wrap: break-word!important;" class='button' data-js-label>+ Selecciónar archivo</span>
                                        </div>
                                        
                                        @error('file_document_home')
                                        <span class="invalid-feedback" role="alert">
                                            <strong style="color:red!important">{{ $message }}</strong>
                                        </span>
                                        @enderror
                                    </div>
                                    
                                </div>
                            </div>  
                            <br>
                            <div class="form-group row align-items-center mb-0">
                                <div class="col-sm-12" style="display: flex; justify-content:center;">
                                    <button class="btn btn-accent botonSiguienteP" style="background-color: #185bc3; width:15rem;color:white" id="boton" type="button" onclick="event.preventDefault();
                                                    document.getElementById('update_cargard_form').submit();">
                                        <span class="spinner-text">Siguiente paso</span>
                                        <span class="spinner-grow spinner-grow-sm" role="status" aria-hidden="true"></span>
                                        <span class="spinner-grow spinner-grow-sm" role="status" aria-hidden="true"></span>
                                        <span class="spinner-grow spinner-grow-sm" role="status" aria-hidden="true"></span>
                                    </button>
                                </div>
                            </div>
                            
                        </div>
                
                    </div>
                </div>

            </div>
            <div class="col-lg-3 page-nav">
                <div class="page-section pt-lg-112pt">
                    <nav class="nav page-nav__menu historialNav">
                        
                        <a class="nav-link disabled" href="{{ url('configuracion/datosp') }}">Datos personales</a>
                        <a class="nav-link disabled" href="{{ url('configuracion/actividade') }}">Actividad económica</a>
                        <a class="nav-link disabled active" href="{{ url('configuracion/cargard') }}">Cargar documentos</a>
                        <a class="nav-link disabled" href="{{ url('configuracion/firmac') }}">Firma de contrato</a>
                
                    </nav>
                </div>
            </div>
        </div>
    </form>

</div>

@endsection