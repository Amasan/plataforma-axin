@extends('layout')

@section('content')



<div class="container page__container">
    <style>
        ::placeholder { 
            color: #185bc3!important;
            opacity: 1; 
        }
        .box{
       
            display: inline;
         
        }
        .1{ display: none; }


        select:invalid {
			border-color: #DD2C00!important;
        }

        .check-ok {
            color:#185bc3!important;
            position: absolute; right: 38px; top: 10px!important;
        }

        select:invalid ~ .check-ok {
            display: none!important;
        }

        select:valid ~ .check-ok {
            display: inline!important;
        }
    </style>
    <div class="modal fade" id="mi-modal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
        <div class="modal-dialog col-lg-12" role="document">
            <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title exitoso" id="exampleModalLabel" style="color: #185bc3;"></h5>
            </div>
            <div class="modal-body m-auto">
               <h6 class="text-center">Si envia SI no se le permitira terminar el proceso de registro</h6>
               <h6 class="text-center">¿Desea continuar?</h6>
            </div>
            <div class="modal-footer m-auto">
                <button type="button" class="btn btn-default" id="modal-btn-si">Si</button>
                <button type="button" class="btn btn-primary" id="modal-btn-no">No</button>
            </div>
            </div>
        </div>
    </div>
    
    <form method="POST" action="{{ route('update_actividade') }}" id="frmA">
        <input type="hidden" name="_token" value="{{ csrf_token() }}" id="_token"></input>
        <div class="row">
            <div class="col-lg-9 pr-lg-0">

                <div class="page-section">
                    <h4>Perfil - Actividad económica</h4>
                    <br><br><br>
                    <div class="list-group list-group-form">
                        <div class="list-group-item space">
                            <br>
                            <h6 class="text-center">Cuéntanos un poco de tus ingresos</h6>
                            <div class="form-group mb-0">
                                <div class="form-row">
                                    <div class="col-12 col-md-12 col-lg-12">
                                        <label class="col-form-label form-label col-12 col-md-12 col-lg-12" style="padding:0;">¿ESTADO DE EMPLEO?</label>
                                        <div class="col-12 col-md-12 col-lg-12" style="padding:0;">
                                            <select onchange="validar()" name="state_employee" value="{{ old('state_employee') }}" id="state_employee" class="form-control custom-select @error('state_employee') is-invalid @enderror inputFormu" required>
                                                <option value="" selected>Selecciona una opción</option>
                                                <option value="Empleado">Empleado</option>
                                                <option value="Independiente">Independiente</option>
                                                <option value="Desempleado">Desempleado</option>
                                                <option value="Jubilado">Jubilado</option>
                                                <option value="Estudiante">Estudiante</option>
                                            </select><i class="fa fa-check check-ok"></i>
                                            
                                            @error('state_employee')
                                                <span class="invalid-feedback" role="alert">
                                                    <strong style="color:red!important">{{ $message }}</strong>
                                                </span>
                                            @enderror
                                        </div>
                                    </div>
                                </div>
                            </div>   
                            <div class="form-group mb-0">
                                <div class="form-row">
                                    <div class="col-12 col-md-12 col-lg-12">
                                        <label class="col-form-label form-label col-12 col-md-12 col-lg-12" style="padding:0; font-size:0.85rem!important;">INGRESO ANUAL (USD)</label>
                                        <div class="col-12 col-md-12 col-lg-12" style="padding:0;">
                                            <select onchange="validar()" name="anual_activity_income_id" value="{{ old('anual_activity_income_id') }}" id="anual_activity_income_id" class="form-control custom-select @error('anual_activity_income_id') is-invalid @enderror inputFormu" required>
                                                <option value="" selected>Selecciona una opción</option>
                                                @foreach($anualIncomes as $item)
                                                <option value="{{$item['id']}}">{{$item['description']}}</option>
                                                @endforeach
                                            </select><i class="fa fa-check check-ok"></i>
                                            @error('anual_activity_income_id')
                                                <span class="invalid-feedback" role="alert">
                                                    <strong style="color:red!important">{{ $message }}</strong>
                                                </span>
                                            @enderror
                                        </div>
                                    </div>
                                </div>
                            </div>   
                            <div class="form-group mb-0">
                                <div class="form-row">
                                    <div class="col-12 col-md-12 col-lg-12">
                                        <label class="col-form-label form-label col-12 col-md-12 col-lg-12" style="padding:0;">TOTAL DE FONDOS A INVERTIR (USD)</label>
                                        <div class="col-12 col-md-12 col-lg-12" style="padding:0;">
                                            <select onchange="validar()" name="total_funds_invest_id" value="{{ old('total_funds_invest_id') }}" id="total_funds_invest_id" class="form-control custom-select @error('total_funds_invest_id') is-invalid @enderror inputFormu" required>
                                                <option value="" selected>Selecciona una opción</option>
                                                @foreach($totalFundInvest as $item)
                                                <option value="{{$item['id']}}">{{$item['description']}}</option>
                                                @endforeach
                                            </select><i class="fa fa-check check-ok"></i>
                                            @error('total_funds_invest_id')
                                                <span class="invalid-feedback" role="alert">
                                                    <strong style="color:red!important">{{ $message }}</strong>
                                                </span>
                                            @enderror
                                        </div>
                                    </div>
                                </div>
                            </div>   
                            <div class="form-group mb-0">
                                <div class="form-row">
                                    <div class="col-12 col-md-12 col-lg-12">
                                        <label class="col-form-label form-label col-12 col-md-12 col-lg-12" style="padding:0;">ORIGEN DE LOS FONDOS</label>
                                        <div class="col-12 col-md-12 col-lg-12" style="padding:0;">
                                            <select onchange="validar()" name="family_member_holds_public_office_id" value="{{ old('family_member_holds_public_office_id') }}" id="family_member_holds_public_office_id" class="form-control custom-select @error('family_member_holds_public_office_id') is-invalid @enderror inputFormu" required>
                                                <option value="" selected>Selecciona una opción</option>
                                                @foreach($publicOfficeEmployees as $item)
                                                <option value="{{$item['id']}}">{{$item['description']}}</option>
                                                @endforeach
                                            </select><i class="fa fa-check check-ok"></i>
                                            @error('family_member_holds_public_office_id')
                                                <span class="invalid-feedback" role="alert">
                                                    <strong style="color:red!important">{{ $message }}</strong>
                                                </span>
                                            @enderror
                                        </div>
                                    </div>
                                </div>
                            </div>   
                            <div class="form-group mb-0">
                                <div class="form-row">
                                    <div class="col-xs-12 col-md-6 col-lg-6 1 box">
                                        <label class="col-form-label form-label col-xs-12 col-md-12 col-lg-12" style="padding:0; font-size: 11px!important;">¿ERES UNA PERSONA EXPUESTA POLÌTICAMENTE?</label>
                                        <select onchange="validar()" name="is_politically_exposed" value="{{ old('is_politically_exposed') }}" id="is_politically_exposed" class="form-control custom-select @error('is_politically_exposed') is-invalid @enderror inputFormu" required>
                                            <option value="" selected>Selecciona una opción</option>
                                            <option value="1">Si</option>
                                            <option value="0">No</option>
                                        </select><i class="fa fa-check check-ok" style="top: 33px!important;"></i>
                                        @error('is_politically_exposed')
                                                <span class="invalid-feedback" role="alert">
                                                    <strong style="color:red!important">{{ $message }}</strong>
                                                </span>
                                        @enderror
                                    </div>
                                    <div class="col-xs-12 col-md-6 col-lg-6 1 box">
                                        <label class="col-form-label form-label col-xs-12 col-md-12 col-lg-12" style="padding:0; font-size: 11px!important;">¿ESTÀS DECLARADO EN BANCA ROTA?</label>
                                        <select onchange="validar()" name="is_declared_bankrupt" value="{{ old('is_declared_bankrupt') }}" id="is_declared_bankrupt" class="form-control custom-select @error('is_declared_bankrupt') is-invalid @enderror inputFormu" required>
                                            <option value="" selected>Selecciona una opción</option>
                                            <option value="1">Si</option>
                                            <option value="0">No</option>
                                        </select><i class="fa fa-check check-ok" style="top: 33px!important;"></i>
                                        @error('is_declared_bankrupt')
                                                <span class="invalid-feedback" role="alert">
                                                    <strong style="color:red!important">{{ $message }}</strong>
                                                </span>
                                        @enderror
                                    </div>
                            
                                </div>
                            </div>   
            
                            <br>
                            <div class="form-group row align-items-center mb-0">
                                <div class="col-sm-12" style="display: flex; justify-content:center;">
                                    <button class="btn btn-accent botonSiguienteP" style="background-color: #185bc3; width:15rem;color:white" id="boton" type="submit" disabled>
                                        <span class="spinner-text">Siguiente paso</span>
                                        <span class="spinner-grow spinner-grow-sm" role="status" aria-hidden="true"></span>
                                        <span class="spinner-grow spinner-grow-sm" role="status" aria-hidden="true"></span>
                                        <span class="spinner-grow spinner-grow-sm" role="status" aria-hidden="true"></span>
                                    </button>
                                </div>
                                <div hidden class="col-sm-12" style="display: flex; justify-content:center; margin-top:20px;">
                                    <a href="{{ url('configuracion/datosp') }}" style="text-decoration: none; font-weight:bold; color:black;"><img width="25px" height="auto" src="{{ asset('images/axin/Regresar.png')}}" alt=""> Regresar</a>
                                </div>
                            </div>
                            
                        </div>
                
                    </div>
                </div>

            </div>
            <div class="col-lg-3 page-nav">
                <div class="page-section pt-lg-112pt">
                    <nav class="nav page-nav__menu historialNav">
                        
                        <a class="nav-link disabled" href="{{ url('configuracion/datosp') }}">Datos personales</a>
                        <a class="nav-link disabled active" href="{{ url('configuracion/actividade') }}">Actividad económica</a>
                        <a class="nav-link disabled" href="{{ url('configuracion/cargard') }}">Cargar documentos</a>
                        <a class="nav-link disabled" href="{{ url('configuracion/firmac') }}">Firma de contrato</a>

                
                    </nav>
                </div>
            </div>
        </div>
    </form>
</div>

@endsection