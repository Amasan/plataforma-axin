@extends('layout')

@section('content')

<div class="container page__container">
    <style>
        input:invalid {
			border-color: #ff8800!important;
        }

        .check-ok {
            color:#185bc3!important;
            position: absolute; right: -20px; top: 10px!important;
        }
    
        input:invalid ~ .check-ok {
            display: none!important;
        }

        input:valid ~ .check-ok {
            display: inline!important;
        }
    </style>
    <form method="POST" id="password_form" action="{{ route('update_password') }}">
    <input type="hidden" name="_token" value="{{ csrf_token() }}"></input>
        <div class="row">
            <div class="col-lg-9 pr-lg-0">

                <div class="page-section">
                    <h4>Ajustes - Cambiar contraseña</h4>
                    <br><br><br>
                    <div class="list-group list-group-form">
                        <div class="list-group-item space">

                            <div class="form-group row align-items-center mb-0">
                                <label class="col-form-label form-label col-sm-12">Nueva contraseña</label>
                                <div class="col-sm-12">
                                    <input name="password" value="{{ old('password') }}" onkeyup="validar()" type="password" class="form-control inputFormu" minlength="8" placeholder="Ingresa la nueva contraseña" required/><i class="fa fa-check check-ok"></i>
                                    @error('password')
                                        <span class="invalid-feedback" role="alert">
                                            <strong style="color:red!important">{{ $message }}</strong>
                                        </span>
                                    @enderror
                                </div>
                            </div>
                            <div class="form-group row align-items-center mb-0">
                                <label class="col-form-label form-label col-sm-12">Confirmar contraseña</label>
                                <div class="col-sm-12">
                                    <input name="confirm_password" value="{{ old('confirm_password') }}" onkeyup="validar()" type="password" class="form-control inputFormu @error('confirm_password') is-invalid @enderror" minlength="8" placeholder="Re-ingresa tu nueva contraseña" required/><i class="fa fa-check check-ok"></i>
                                    @error('confirm_password')
                                        <span class="invalid-feedback" role="alert">
                                            <strong style="color:red!important">{{ $message }}</strong>
                                        </span>
                                    @enderror
                                </div>
                            </div>
                            <br><br>
                            <div class="form-group row align-items-center mb-0">
                                <div class="col-sm-12" style="display: flex; justify-content:center;">
                                    <button class="btn btn-accent botonSiguienteP" style="background-color: #185bc3; width:15rem;color:white" id="boton" type="submit" disabled>
                                        <span class="spinner-text">Guardar Cambios</span>
                                        <span class="spinner-grow spinner-grow-sm" role="status" aria-hidden="true"></span>
                                        <span class="spinner-grow spinner-grow-sm" role="status" aria-hidden="true"></span>
                                        <span class="spinner-grow spinner-grow-sm" role="status" aria-hidden="true"></span>
                                    </button>
                                </div>
                            </div>
                            
                        </div>
                
                    </div>
                </div>

            </div>
            <div class="col-lg-3 page-nav">
                <div class="page-section pt-lg-112pt">
        
                    <nav class="nav page-nav__menu historialNav">
                        <h5>&nbsp;&nbsp;&nbsp;&nbsp;AJUSTES</h5>
                        <a class="nav-link active" href="{{ url('/configuracion/password') }}">Cambiar Contraseña</a>
                        <a class="nav-link" href="{{ url('/configuracion/notificaciones') }}">Notificaciones por correo</a>

                    </nav>
                </div>
            </div>
        </div>
    </form>
</div>

@endsection