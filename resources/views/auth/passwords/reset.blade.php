<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Portal axin</title>
    <link type="text/css" href="{{ asset('css/resetPassword.css') }}" rel="stylesheet">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css" integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm" crossorigin="anonymous">
</head>
<body>
    <section id="cpassword">
        <div class="container">
            <div class="row">
                <div class="col-12 col-sm-12 col-md-12 text-center">
                    <a><img class="logo" src="{{ asset('images/axin/Logo-Axin.png') }}"/></a>
                </div>
                <div class="col-12 col-sm-12 col-md-12 col-lg-6 col-xl-6  content">
                    <h1>Bienvenido.</h1>
                    <div class="row" style="padding-left: 14px;"><h3>Cambiar la contraseña.</h3><img class="flechab" src="assets/img/flechab.png" alt=""></div>
                    <img class="flecha" src="assets/img/flecha.png" alt="">
                </div>
                <div class="col-12 col-sm-12 col-md-12 col-lg-6 col-xl-6">
                    <form method="POST" action="{{ url('password/reset') }}">
                        @csrf

                        <input type="hidden" name="token" value="{{ $token }}">

                        <div class="form-group row"  style="justify-content: flex-end;">
                            <label for="staticEmail" class="col-sm-12 col-form-label">Nueva contraseña</label>
                            <div class="input-group col-sm-12">
                                <input class="form-control py-2 border-right-0 border formatexep @error('password') is-invalid @enderror" id="password" type="password" name="password" required autocomplete="new-password">
                                @error('password')
                                <span class="invalid-feedback" role="alert">
                                    <strong>{{ $message }}</strong>
                                </span>
                                @enderror
                                <span class="input-group-append">
                                    <div class="input-group-text bg-transparent formatcod"><img width="20px" src="{{ asset('images/axin/Candado.png') }}" /></div>
                                </span>
                            </div>
                        </div>
                        <div class="form-group row"  style="justify-content: flex-end;">
                            <label for="staticEmail" class="col-sm-12 col-form-label">Volver a escribir la contraseña</label>
                            <div class="input-group col-sm-12">
                                <input class="form-control py-2 border-right-0 border formatexep" id="password-confirm" type="password" name="password_confirmation" required autocomplete="new-password">
                                <span class="input-group-append">
                                    <div class="input-group-text bg-transparent formatcod"><img width="20px" src="{{ asset('images/axin/Candado.png') }}" /></div>
                                </span>
                            </div>
                        </div>
    
                        <div class="form-group row text-center">
                        
                            <label class="col-12 col-sm-12" for="" style="font-size: .8rem;">8 caracteres como minimo, distigue mayúsculas de minúsculas</label>
                      
                        </div>
                        <div class="form-group row text-center">
                            <div class="col-12 col-sm-12 col-md-12">
                                <button type="submit" class="btn btn-primary col-6 col-sm-8 col-md-8 boton">
                                    {{ __('Restaurar contrasenia') }}
                                </button>
                            </div>
                           
                        </div>
                     
    
                    </form>
                </div>
            </div>
        </div>
        <div class="col-md-12 text-center" style="color: #fff; padding-top: 8rem;">
            <p>&copy; 2020 secure.axin.mx. All rights reserved.</p>
        </div>
    </section>
    

    <script src="https://code.jquery.com/jquery-3.2.1.slim.min.js" integrity="sha384-KJ3o2DKtIkvYIK3UENzmM7KCkRr/rE9/Qpg6aAZGJwFDMVNA/GpGFF93hXpG5KkN" crossorigin="anonymous"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.9/umd/popper.min.js" integrity="sha384-ApNbgh9B+Y1QKtv3Rn7W3mgPxhU9K/ScQsAP7hUibX39j7fakFPskvXusvfa0b4Q" crossorigin="anonymous"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js" integrity="sha384-JZR6Spejh4U02d8jOt6vLEHfe/JQGiRRSQQxSfFWpi1MquVdAyjUar5+76PVCmYl" crossorigin="anonymous"></script>
</body>
</html>