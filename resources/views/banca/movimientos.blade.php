@extends('layout')

@section('content')

<div class="container page__container">
    <form action="compact-edit-account.html">
        <div class="row">
            <div class="col-lg-9 pr-lg-0">

                <div class="page-section">
                    <h4>Banca - Movimientos</h4>
                    <br><br><br>
                    <div class="card table-responsive">
                        <table class="table table-flush table-nowrap">
                            <thead>
                                <tr>
                                    <th class="text-center" style="font-size: 0.8rem; color: #185bc3; font-weight:100;">CUENTA</th>
                                    <th class="text-center" style="font-size: 0.8rem; color: #185bc3; font-weight:100;">CONCEPTO</th>
                                    <th class="text-center" style="font-size: 0.8rem; color: #185bc3; font-weight:100;">FECHA</th>
                                    <th class="text-center" style="font-size: 0.8rem; color: #185bc3; font-weight:100;">MONTO</th>
                                    <th class="text-center" style="font-size: 0.8rem; color: #185bc3; font-weight:100;">ESTADO</th>
                                </tr>
                            </thead>
                            <tbody>
                                @foreach ($items as $item) 
                                    <tr>
                                        <td class="text-center" style="font-size: 0.8rem; background-color: #f2f5f7;">{{$item['name']}} - {{$item['account_number']}}</td>
                                        <td class="text-center" style="font-size: 0.8rem;">{{$item['concept']}}</td>
                                        <td class="text-center" style="font-size: 0.8rem; background-color: #f2f5f7;">{{$item['date_deposite']}}</td>
                                        <td class="text-center" style="font-size: 0.8rem;">
                                            @if ($item['amount'] == 0)
                                            ---------
                                            @else
                                            ${{number_format($item['amount'], 2, '.', ',')}} USD
                                            @endif
                                        </td>
                                        <td class="text-center" style="font-size: 0.8rem; background-color: #f2f5f7;">{{$item['status']}}</td>
                                    </tr>
                                @endforeach
                                <tr>
                                    <td class="text-center" style="font-size: 0.8rem; background-color: #f2f5f7;"></td>
                                    <td class="text-center" style="font-size: 0.8rem;"></td>
                                    <td class="text-center" style="font-size: 0.8rem; background-color: #f2f5f7;"></td>
                                    <td class="text-center" style="font-size: 0.8rem;"></td>
                                    <td class="text-center" style="font-size: 0.8rem; background-color: #f2f5f7;"></td>
                                </tr>
                                <tr>
                                    <td class="text-center" style="font-size: 0.8rem; background-color: #f2f5f7;"></td>
                                    <td class="text-center" style="font-size: 0.8rem;"></td>
                                    <td class="text-center" style="font-size: 0.8rem; background-color: #f2f5f7;"></td>
                                    <td class="text-center" style="font-size: 0.8rem;"></td>
                                    <td class="text-center" style="font-size: 0.8rem; background-color: #f2f5f7;"></td>
                                </tr>

                            </tbody>
                        </table>
                        {{ $items->links() }}
                    </div>
                </div>

            </div>
            <div class="col-lg-3 page-nav">
                <div class="page-section pt-lg-112pt">
                    <nav class="nav page-nav__menu historialNav">
                        
                        <a class="nav-link" href="{{ url('banca/depositar_fondos') }}">Depositar fondos</a>
                        <a class="nav-link" href="{{ url('banca/comprobante') }}">Comprobar deposito</a>
                        <a class="nav-link" href="{{ url('banca/retirar_fondos') }}">Retirar fondos</a>
                        <a class="nav-link active" href="{{ url('banca/movimientos') }}">Movimientos</a>
                
                    </nav>
                </div>
            </div>
        </div>
    </form>
</div>


@endsection