@extends('layout')

@section('content')

<div class="container page__container">
   
        <div class="row">
            <div class="col-lg-9 pr-lg-0">

                <div class="page-section">
                    <h4>Banca - Depositar fondos</h4>
                    <br><br><br>
                    <div class="card table-responsive">
                        <table class="table table-flush table-nowrap">

                            <tbody>
                                @php
                                $status = true;
                                $index = 1;
                                @endphp
                                @foreach($settings as $setting)
                                <tr>
                                
                                @if($status)
                                <td class="text-right" style="color:#185bc3; font-size: 0.8rem;">{{$setting['key']}}</td>
                                <td class="text-left" style="font-size: 0.8rem">{{$setting['value']}}</td>
                                @else
                                <td class="text-right" style="color:#185bc3; font-size: 0.8rem;background-color: #f2f5f7;">{{$setting['key']}}</td>
                                <td class="text-left" style="font-size: 0.8rem; background-color: #f2f5f7; display:flex;"><p class="text-copy" id="p{{$index}}">{{$setting['value']}}</p>  <button class="copy-button1" type="button" onclick="copyToClipboard('#p{{$index}}')" data-toggle="popover" data-content="Copiado"><span class="copy-icon"><i class="far fa-copy"></i></span></button></td>
                                @php
                                $index = $index + 1;
                                @endphp
                                @endif
                                @php
                                $status = !$status;
                                @endphp
                                </tr>
                                @endforeach
                            </tbody>
                        </table>
                    </div>
                </div>

            </div>
            
            <div class="col-lg-3 page-nav">
                <div class="page-section pt-lg-112pt">
                    <nav class="nav page-nav__menu transferenciaNav">
                        
                        <a class="nav-link active" href="{{ url('banca/depositar_fondos') }}">Depositar fondos</a>
                        <a class="nav-link" href="{{ url('banca/comprobante') }}">Comprobar deposito</a>
                        <a class="nav-link" href="{{ url('banca/retirar_fondos') }}">Retirar fondos</a>
                        <a class="nav-link" href="{{ url('banca/movimientos') }}">Movimientos</a>
                
                    </nav>
                
                </div>
            </div>
            
        </div>
  
</div>

@endsection