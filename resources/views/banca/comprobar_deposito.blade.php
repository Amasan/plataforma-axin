@extends('layout')

@section('content')

<div class="container page__container">
    <style>
        ::placeholder { 
            color: #185bc3!important;
            opacity: 1; 
        }
        input:invalid {
			border-color: #DD2C00!important;
        }

        select:invalid {
			border-color: #DD2C00!important;
        }

        .check-ok {
            color:#185bc3!important;
            position: inherit; right: -20px; top: 10px!important;
        }
        
        .check-No {
            color: red!important;
            position: inherit; right: -20px; top: 10px!important;
        }
    
        input:invalid ~ .check-ok {
            display: none!important;
        }

        input:valid ~ .check-ok {
            display: inline!important;
        }
        input:invalid ~ .check-No {
            display: inline!important;
        }

        input:valid ~ .check-No {
            display: none!important;
        }

        select:invalid ~ .check-ok {
            display: none!important;
        }

        select:valid ~ .check-ok {
            display: inline!important;
        }
        
    </style>
    
    <form id="update_comprobante" enctype="multipart/form-data" method="POST"  action="{{route('update_comprobante')}}">
        <input type="hidden" name="_token" value="{{ csrf_token() }}"></input>
        <div class="row">
            <div class="col-lg-9 pr-lg-0">

                <div class="page-section">
                    <h4>Banca - Comprobar depositos</h4>
                    <br><br><br>
                    <div class="list-group list-group-form">
                        <div class="list-group-item space">
                            <br>
                            <h6 class="text-center">Sube tus comprobantes de deposito para revisión</h6>
                            <div class="form-row col-12 col-md-12 col-lg-12">
                                {{-- <div class="col-2 col-md-2 col-lg-2">
                                    <img class="col-12 col-md-12 col-lg-12" src="{{ asset('images/axin/Newsletter.png') }}" alt="" style="padding: 0; bottom: 6px; left: 7px;">
                                </div> --}}
                                <div class="col-12 col-md-12 col-lg-12" style="background-color: #eaf6f9; display:flex; height:60px!important">
                                    <label class="col-12 col-md-12 col-lg-12 text-center" style="margin: auto!important;">Te recomendamos que cada archivo sea menor a <span style="color: black; font-weight:bold;">3 MB</span> en formato <span style="color: black; font-weight:bold;">JPG, PNG O PDF</span></label>
                                </div>
                            </div><br><br>  
                            <div class="form-group row align-items-center mb-0">
                                <label class="col-form-label form-label col-sm-12">NOMBRE</label>
                                <div class="col-sm-12">
                                    <input name="name" value="{{ $name }}" onkeyup="validar()" type="text" class="form-control inputFormu @error('name') is-invalid @enderror" placeholder="" required readonly /><i class="fa fa-check check-ok" style="color: #185bc3!important;position: absolute;right: 43px;top: 10px!important;"></i>
                                    @error('name')
                                        <span class="invalid-feedback" role="alert">
                                            <strong style="color:red!important">{{ $message }}</strong>
                                        </span>
                                    @enderror
                                </div>
                            </div>
                            <div class="form-group row align-items-center mb-0">
                                <label class="col-form-label form-label col-sm-12">CORREO ELÉCTRONICO</label>
                                <div class="col-sm-12">
                                    <input name="email" value="{{ $email }}" onkeyup="validar()" type="email" class="form-control inputFormu @error('email') is-invalid @enderror" placeholder=""  required readonly /><i class="fa fa-check check-ok" style="color: #185bc3!important;position: absolute;right: 43px;top: 10px!important;"></i>
                                    @error('email')
                                        <span class="invalid-feedback" role="alert">
                                            <strong style="color:red!important">{{ $message }}</strong>
                                        </span>
                                    @enderror
                                </div>
                            </div>
                            <div class="form-group row align-items-center mb-0">
                                <label class="col-form-label form-label col-sm-12">CUENTA DE INVERSIÓN</label>
                                <div class="col-sm-12">
                                    <select name="account_client_id" value="{{ old('account_client_id') }}" onclick="validar()" id="account_client_id" class="form-control custom-select inputFormu @error('account_client_id') is-invalid @enderror" required>
                                        <option value="" selected>Selecciona la cuenta del deposito</option>
                                        @foreach($accounts as $account)
                                        <option value="{{$account->id}}">{{$account->typeaccount->name}} - {{$account->account_number}}</option>  
                                        @endforeach 
                                    </select><i class="fa fa-check check-ok" style="color: #185bc3!important;position: absolute;right: 43px;top: 10px!important;"></i>
                                    @error('account_client_id')
                                        <span class="invalid-feedback" role="alert">
                                            <strong style="color:red!important">{{ $message }}</strong>
                                        </span>
                                    @enderror
                                </div>
                            </div>
                            <div class="form-group mb-0">
                                <div class="form-row">
                                    <label class="col-form-label form-label col-sm-12">COMPROBANTE</label>
                                    <div class="col-12 col-md-12 col-lg-12">
                                        <div class="file-input @error('file_document_home') is-invalid @enderror">
                                            <input class="inputFormu" onchange="validar()" value="{{ old('file_document_home') }}" name="file_url" id="file_url" type='file' accept="image/jpeg,image/jpg,image/png,application/pdf" required><i class="fa fa-check check-ok"></i><i class="fa fa-times check-No"></i>
                                            <span class='button' data-js-label>+ Selecciónar archivo</span>
                                        </div>
                                         
                                        @error('file_document_home')
                                        <span class="invalid-feedback" role="alert">
                                            <strong style="color:red!important">{{ $message }}</strong>
                                        </span>
                                        @enderror
                                    </div>
                                    <div class="text-center col-12"><h8 class="invalid" style="color:red;"></h8></div>
                                </div>
                            </div>  
                            <br>
                            <div class="form-group row align-items-center mb-0">
                                <div class="col-sm-12" style="display: flex; justify-content:center;">
                                    <button class="btn btn-accent botonSiguienteP" style="background-color: #185bc3; width:15rem;color:white" id="boton" type="submit" disabled>
                                        <span class="spinner-text">Enviar</span>
                                        <span class="spinner-grow spinner-grow-sm" role="status" aria-hidden="true"></span>
                                        <span class="spinner-grow spinner-grow-sm" role="status" aria-hidden="true"></span>
                                        <span class="spinner-grow spinner-grow-sm" role="status" aria-hidden="true"></span>
                                    </button>
                                </div>
                            </div>
                            
                        </div>
                
                    </div>
                </div>

            </div>
            <div class="col-lg-3 page-nav">
                <div class="page-section pt-lg-112pt">
                    <nav class="nav page-nav__menu historialNav">
                        
                        <a class="nav-link" href="{{ url('banca/depositar_fondos') }}">Depositar fondos</a>
                        <a class="nav-link active" href="{{ url('banca/comprobante') }}">Comprobar deposito</a>
                        <a class="nav-link" href="{{ url('banca/retirar_fondos') }}">Retirar fondos</a>
                        <a class="nav-link" href="{{ url('banca/movimientos') }}">Movimientos</a>
                
                    </nav>
                </div>
            </div>
        </div>
    </form>

</div>

@endsection