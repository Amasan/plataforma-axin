@extends('layout')

@section('content')

<div class="container page__container">
    <style>
        .box{
            display: none;
        }
        .Bitcoin{ display: block;}
        .Transferencia{ display: block;}

        .mayus{
            text-transform: uppercase;
        }

        input:invalid {
			border-color: #DD2C00!important;
        }
        select:invalid {
			border-color: #DD2C00!important;
        }

        .check-ok {
            color:#185bc3!important;
            position: absolute; right: 38px; top: 10px!important;
        }
    
        input:invalid ~ .check-ok {
            display: none;
        }

        input:valid ~ .check-ok {
            display: inline;
        }

        select:invalid ~ .check-ok {
            display: none!important;
        }

        select:valid ~ .check-ok {
            display: inline!important;
        }
        
    
    </style>
    <script src="https://code.jquery.com/jquery-1.12.4.min.js"></script>
    <script>
    $(document).ready(function(){
        $("#method").change(function(){
            $(this).find("option:selected").each(function(){
                var optionValue = $(this).attr("value");
             
                if(optionValue){
                    $(".box").not("." + optionValue).hide();
                    $("." + optionValue).show();
                } else{
                    $(".box").hide();

                }

                if ($('.Bitcoin').is(':hidden')) {
                    $('#bit').removeAttr("required");
                    {{--  alert("BITCOIN NO REQUERIDO");  --}}
                } else if ($('.Bitcoin').is(':visible')) {
                    {{--  alert("BITCOIN REQUERIDO");  --}}
                    $('#bit').prop("required", true);
                } 
                
                if ($('.Transferencia').is(':hidden')) {
                    $('#trans1').removeAttr("required");
                    $('#trans2').removeAttr("required");
                    $('#trans3').removeAttr("required");
                    {{--  alert("TRANSFERENCIA NO REQUERIDA");  --}}
                } else if ($('.Transferencia').is(':visible')){
                    {{--  alert("TRANSFERENCIA REQUERIDA");  --}}
                    $('#trans1').prop("required", true);
                    $('#trans2').prop("required", true);
                    $('#trans3').prop("required", true);
                }
                

                
            });
        }).change();
    });
    </script>
    <form id="update_retirar_form" method="POST" action="{{ route('update_retirar_fondos') }}">
        <input type="hidden" name="_token" value="{{ csrf_token() }}"></input>
        <div class="row">
            <div class="col-lg-9 pr-lg-0">

                <div class="page-section">
                    <h4>Banca - Retirar fondos</h4>
                    <br><br><br>
                    <div class="list-group list-group-form">
                        <div class="list-group-item space">
                            <div class="form-group row align-items-center mb-0">
                                <label class="col-form-label form-label col-sm-12">NOMBRE</label>
                                <div class="col-sm-12">
                                    <input name="name" value="{{ $name }}" onkeyup="validar()" type="text" class="form-control inputFormu @error('name') is-invalid @enderror" placeholder="" required readonly /><i class="fa fa-check check-ok"></i>
                                    @error('name')
                                        <span class="invalid-feedback" role="alert">
                                            <strong style="color:red!important">{{ $message }}</strong>
                                        </span>
                                    @enderror
                                </div>
                            </div>
                            <div class="form-group row align-items-center mb-0">
                                <label class="col-form-label form-label col-sm-12">CORREO ELÉCTRONICO</label>
                                <div class="col-sm-12">
                                    <input name="email" value="{{ $email }}" onkeyup="validar()" type="email" class="form-control inputFormu @error('email') is-invalid @enderror" placeholder=""  required readonly /><i class="fa fa-check check-ok"></i>
                                    @error('email')
                                        <span class="invalid-feedback" role="alert">
                                            <strong style="color:red!important">{{ $message }}</strong>
                                        </span>
                                    @enderror
                                </div>
                            </div>
                            <div class="form-group row align-items-center mb-0">
                                <label class="col-form-label form-label col-sm-12">CUENTA DE INVERSIÓN</label>
                                <div class="col-sm-12">
                                    <select name="account_client_id" value="{{ old('account_client_id') }}" onchange="validar()" id="expire_month" class="form-control custom-select inputFormu @error('account_client_id') is-invalid @enderror" required>
                                        <option disabled value="" selected>Selecciona una cuenta</option>
                                        @foreach ($accounts as $account) 
                                            <option value="{{ $account['id'] }}">{{ $account['name'] }} - {{ $account['account_number'] }}</option>
                                        @endforeach
                                    </select><i class="fa fa-check check-ok"></i>
                                    @error('account_client_id')
                                        <span class="invalid-feedback" role="alert">
                                            <strong style="color:red!important">{{ $message }}</strong>
                                        </span>
                                    @enderror
                                </div>
                            </div>

                    
                            <div class="form-group mb-0">
                                <div class="form-row">
                                    <div class="col-xs-12 col-md-12 col-lg-12">
                                        <label class="col-form-label form-label col-xs-12 col-md-12 col-lg-12" style="padding:0;">CANTIDAD DE RETIRO (USD)</label>
                                        <div class="col-xs-12 col-md-12 col-lg-12" style="padding:0;">
                                            <input name="amount" onkeyup="validar()" onkeypress="return validarNumeros(event)" value="{{ old('amount') }}" type="number" class="form-control col-xs-12 col-md-12 col-lg-12 inputFormu @error('amount') is-invalid @enderror" min="0" placeholder="Cantidad en dolares" required /><i style="right: 25px!important;" class="fa fa-check check-ok"></i>
                                            @error('amount')
                                                <span class="invalid-feedback" role="alert">
                                                    <strong style="color:red !important">{{ $message }}</strong>
                                                </span>
                                            @enderror
                                        </div>
                                    </div>
                                </div>
                            </div>

                            <div class="form-group row align-items-center mb-0">
                                <label class="col-form-label form-label col-sm-12">MÉTODO DE RETIRO</label>
                                <div class="col-sm-12">
                                    <select name="method" value="{{ old('method') }}" onchange="validar()" id="method" class="form-control custom-select inputFormu @error('method') is-invalid @enderror" required />
                                        <option value="">Selecciona un método</option>
                                        <option value="Bitcoin" {{ old('method') == 'Bitcoin' ? 'selected' : '' }}>Bitcoin</option>
                                        <option value="Transferencia" {{ old('method') == 'Transferencia' ? 'selected' : '' }}>Transferencia bancaria</option>
                                    </select><i class="fa fa-check check-ok"></i>
                                    @error('method')
                                        <span class="invalid-feedback" role="alert">
                                            <strong style="color:red!important">{{ $message }}</strong>
                                        </span>
                                    @enderror
                                </div>
                            </div>

                            <div class="form-group mb-0 Bitcoin box">
                                <div class="form-row">
                                    <div class="col-xs-12 col-md-12 col-lg-12">
                                        <label class="col-form-label form-label col-xs-12 col-md-12 col-lg-12" style="padding:0;">DIRECCIÓN DE DEPOSITO</label>
                                        <div class="col-xs-12 col-md-12 col-lg-12" style="padding:0;">
                                            <input name="bitcoin_address" value="{{ old('bitcoin_address') }}" id="bit" type="text" class="form-control col-xs-12 col-md-12 col-lg-12 @error('bitcoin_address') is-invalid @enderror" onkeypress="return validarNumerosLetras(event)" maxlength="34" pattern="^1[A-Za-z0-9]{27,34}$|^3[A-Za-z-0-9]{27,34}$" placeholder="Dirección de deposito" /><i style="right: 25px!important;" class="fa fa-check check-ok"></i>
                                            @error('bitcoin_address')
                                                <span class="invalid-feedback" role="alert">
                                                    <strong style="color:red!important">{{ $message }}</strong>
                                                </span>
                                            @enderror
                                        </div>
                                    </div>
                                </div>
                            </div>
                            
                            <div class="form-group row align-items-center mb-0 Transferencia box">
                                <label class="col-form-label form-label col-sm-12">*NOMBRE DEL BANCO</label>
                                <div class="col-sm-12">
                                    <input id="trans1" name="bank_name" value="{{ old('bank_name') }}" type="text" onkeypress="return validarCaracteres(event)" class="form-control mayus @error('bank_name') is-invalid @enderror" placeholder="Nombre del banco"  /><i class="fa fa-check check-ok"></i>
                                    @error('bank_name')
                                        <span class="invalid-feedback" role="alert">
                                            <strong style="color:red!important">{{ $message }}</strong>
                                        </span>
                                    @enderror
                                </div>
                            </div>
                            
                            <div class="form-group row align-items-center mb-0 Transferencia box">
                                <label class="col-form-label form-label col-sm-12">*NÚMERO DE CUENTA BANCARIA</label>
                                <div class="col-sm-12">
                                    <input id="trans2" onkeypress="return validarNumeros(event)" name="bank_number" value="{{ old('bank_number') }}" type="text" class="form-control @error('bank_number') is-invalid @enderror" placeholder="Número de cuenta" /><i class="fa fa-check check-ok"></i>
                                    @error('bank_number')
                                        <span class="invalid-feedback" role="alert">
                                            <strong style="color:red!important">{{ $message }}</strong>
                                        </span>
                                    @enderror
                                </div>
                            </div>
                            
                            <div class="form-group row align-items-center mb-0 Transferencia box">
                                <label class="col-form-label form-label col-sm-12">CÓDIGO SWIFT/ABA/NÚMERO DE RUTA</label>
                                <div class="col-sm-12">
                                    <input type="text" onkeypress="return validarNumerosLetras(event)" name="swift_code" value="{{ old('swift_code') }}" class="form-control mayus @error('swift_code') is-invalid @enderror" placeholder="Código SWIFT" />
                                    @error('swift_code')
                                        <span class="invalid-feedback" role="alert">
                                            <strong style="color:red!important">{{ $message }}</strong>
                                        </span>
                                    @enderror
                                </div>
                            </div>
                            
                            <div class="form-group row align-items-center mb-0 Transferencia box">
                                <label class="col-form-label form-label col-sm-12">IBAN (SOLO BANCOS EUROPEOS)</label>
                                <div class="col-sm-12">
                                    <input type="text"  onkeypress="return validarNumerosLetras(event)" name="ivan_code" value="{{ old('ivan_code') }}" class="form-control mayus @error('ivan_code') is-invalid @enderror" placeholder="Código IBAN" />
                                    @error('ivan_code')
                                        <span class="invalid-feedback" role="alert">
                                            <strong style="color:red!important">{{ $message }}</strong>
                                        </span>
                                    @enderror
                                </div>
                            </div>
                            <div class="form-group row align-items-center mb-0 Transferencia box">
                                <label class="col-form-label form-label col-sm-12">*EN CASO DE REALIZAR UNA TRANSFERENCIA AL EXTRAJERO, COMPLETA LOS CAMPOS SWIFT Y ABA.</label>
                            </div>
                            <br>
                            <div class="form-group row align-items-center mb-0">
                                <div class="col-sm-12" style="display: flex; justify-content:center;">
                                    <button class="btn btn-accent botonSiguienteP" style="background-color: #185bc3; width:15rem;color:white" id="boton" type="submit" disabled>
                                        <span class="spinner-text">Solicitar retiro</span>
                                        <span class="spinner-grow spinner-grow-sm" role="status" aria-hidden="true"></span>
                                        <span class="spinner-grow spinner-grow-sm" role="status" aria-hidden="true"></span>
                                        <span class="spinner-grow spinner-grow-sm" role="status" aria-hidden="true"></span>
                                    </button>
                                </div>
                            </div>
                            
                        </div>
                
                    </div>
                </div>

            </div>
            <div class="col-lg-3 page-nav">
                <div class="page-section pt-lg-112pt">
                    <nav class="nav page-nav__menu historialNav">
                        
                        <a class="nav-link" href="{{ url('banca/depositar_fondos') }}">Depositar fondos</a>
                        <a class="nav-link" href="{{ url('banca/comprobante') }}">Comprobar deposito</a>
                        <a class="nav-link active" href="{{ url('banca/retirar_fondos') }}">Retirar fondos</a>
                        <a class="nav-link" href="{{ url('banca/movimientos') }}">Movimientos</a>
                
                    </nav>
                </div>
            </div>
        </div>
    </form>
</div>

@endsection