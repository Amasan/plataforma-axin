@extends('layout')

@section('content')

<div class="container page__container">
    <style>
        input:invalid {
			border-color: #ff6600!important;
        }
        select:invalid {
			border-color: #ff6600!important;
        }

        .check-ok {
            color:#185bc3!important;
            position: absolute; right: 34px; top: 42px!important;
        }
    
        input:invalid ~ .check-ok {
            display: none!important;
        }

        input:valid ~ .check-ok {
            display: inline!important;
        }
        select:invalid ~ .check-ok {
            display: none!important;
        }

        select:valid ~ .check-ok {
            display: inline!important;
        }
        .modal .modal-dialog {
            width: 80%!important;
        }
        #transfiriendo {
            display: none;
        }
        .caretdown {
            position: absolute!important;
            right: -20px!important;
            top: 13px!important;
            font-size: 24px!important;
        }
        .text1 {
            font-size:13px;
            color: gray;
        }
        .text2 {
            font-size:13px;
            text-align: right;
            font-weight: bold;
        }

    </style>
    <style>
    .collapsible {
    background-color: #777;
    color: white;
    cursor: pointer;
    padding: 18px;
    width: 100%;
    border: none;
    text-align: left;
    outline: none;
    font-size: 15px;
    transition: width 600ms ease-out, height 600ms ease-out;
    }

    .collapsible .active, .collapsible:hover {
    background-color: red;
    }

    .content-collapsible {
    padding: 0 18px;
    display: none;
    overflow: hidden;
    transition: height 1000ms ease-out;
    background-color: #f1f1f1;
    }
    </style>
    <div class="page-section">

        <div class="card card-form d-flex flex-column flex-sm-row mb-lg-32pt">
            <div class="dropdown">
                <div class="cuentaA row dropbtn" id="accounts_div">
                    @php
                        $index = false;
                    @endphp
                    @foreach($accounts as $account)
                        @if($account->is_active)
                            <div class="col-sm-auto">
                                <div class="form-group">
                                    <div class="form-row">
                                        <div class="col-xs-6 col-md-6 col-lg-6">
                                            <label for="filter_category">Tipo de Cuenta</label><br>
                                            <label for="filter_category" style="font-size: 1rem; color:black; font-weight: bold;">{{$account->nameaccount}} - {{$account->account_number}}</label>
                                        </div>
                                        <div class="col-xs-6 col-md-6 col-lg-6">
                                            <br>
                                            @if($account->amount_money >= 1000)
                                                <p  class="statusActiva">Activa</p>
                                            @else
                                                <p  class="statusInactiva">Inactiva</p>
                                            @endif
                                            @if(!$index)
                                                <span class="caretdown"><i class="fa fa-caret-down"></i></span>
                                                @php
                                                    $index = true;
                                                @endphp
                                            @endif
                                            
                                        </div>
                                    </div>
                                </div>
                            </div>
                        @endif
                    @endforeach
                   
                    
                </div>

            </div>

            <div class="card-form__body card-body-form-group flex" style="background-color: white;">
                <div class="row" style="float: right;">
                    <div class="col-sm-auto">
                        <div class="form-group">
                            <div class="form-row">
                                <div class="col-sm-auto">
                                    <div class="form-group">
                                        <div class="form-row">
                                            {{--  <p class="m-auto text-center p-4"><span style="font-weight: bold;">20%</span><br><span>P.FEE</span></p>  --}}
                                            <div>
                                                <div>
                                                    @if($complete_profile!=10)
                                                    <a href="{{ url('banca/depositar_fondos') }}" class="btn btn-accent disabled" style="border-radius:7px; background-color: #185bc3; border:none; width:12rem;"><img width="20" src="{{ asset('images/axin/Icono_2.png') }}" alt="">&nbsp;Depositar fondos</a><br><br>
                                                    <a href="" hidden class="btn btn-accent disabled" data-toggle="modal" data-target="#transferir" data-backdrop="static" data-keyboard="false" style="border-radius:7px; background-color: #c1c127; border:none; width:12rem;"><img width="20" src="{{ asset('images/axin/Icono_1.png') }}" alt="">&nbsp;Transferir fondos</a>
                                                    @endif
                                                    @if($complete_profile==10)
                                                    <a href="{{ url('banca/depositar_fondos') }}" class="btn btn-accent" style="border-radius:7px; background-color: #185bc3; border:none; width:12rem;"><img width="20" src="{{ asset('images/axin/Icono_2.png') }}" alt="">&nbsp;Depositar fondos</a><br><br>
                                                    <a href="" hidden class="btn btn-accent" data-toggle="modal" data-target="#transferir" data-backdrop="static" data-keyboard="false" style="border-radius:7px; background-color: #c1c127; border:none; width:12rem;"><img width="20" src="{{ asset('images/axin/Icono_1.png') }}" alt="">&nbsp;Transferir fondos</a>
                                                    @endif
                                            
                                                </div>
                                            </div>
                                            
                                        </div>
                                    </div>
                                </div>
                                
                            </div>
                        </div>
                    </div>

                </div>
            </div>

        </div>

        
        <div class="row card-group-row mb-lg-8pt">
            <div class="col-lg-4 col-md-6 card-group-row__col">
                <div class="card card-group-row__card">
                    <div class="card-header p-0 nav">
                        <div class="row no-gutters flex" role="tablist">
                            <div class="col-auto">
                                <div class="p-card-header">
                                    <p class="mb-0"><strong>Estadísticas</strong></p>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="card-body">
                        <div class="row">
                            <div class="col-6 text1">
                                Rendimiento
                            </div>
                            <div class="col-6 text2" id="rendimientoAcumulado">
                                
                            </div>
                            <hr>
                            <div class="col-6 text1">
                                Depósitos
                            </div>
                            <div class="col-6 text2" id="inversionAcumulada">
                                
                            </div>
                            <div class="col-6 text1">
                                Ganancia
                            </div>
                            <div class="col-6 text2" id="gananciaAcumulada">
                            
                            </div>
                            <div class="col-6 text1">
                                Retiros
                            </div>
                            <div class="col-6 text2" id="retiros">
                            
                            </div>
                            <div class="col-6 text1">
                                Comisiones
                            </div>
                            <div class="col-6 text2" id="comisiones">
                            
                            </div>
                            <div class="col-6 text1">
                                Saldo Actual
                            </div>
                            <div class="col-6 text2"  id="saldoAcumulada">
                                
                            </div>
                        </div>
                        <hr>
                        <div class="row">
                            <div class="col-6 text1">
                                Performance fee
                            </div>
                            <div class="col-6 text2" id="performance_fee">
                            
                            </div>
                            <div class="col-6 text1">
                                Managment fee
                            </div>
                            <div class="col-6 text2" id="management_fee">
                            
                            </div>
                        </div>
                        <hr>
                        <div class="row">
                            <div class="col-6 text1">
                                Inicio de contrato
                            </div>
                            <div class="col-6 text2" id="inicioContrato">
                            
                            </div>
                            <div class="col-6 text1">
                                Fin de contrato
                            </div>
                            <div class="col-6 text2" id="finContrato">
                            
                            </div>
                        </div>
                        <hr>
                        <div class="row">
                            <div class="col-6 text1">
                                Actualizado
                            </div>
                            <div class="col-6 text2" id="actualizacion">
                            
                            </div>
                        </div>

                    </div>
                </div>
            </div>
            <div class="col-lg-8 col-md-6 card-group-row__col">
                <div class="card card-group-row__card">
                    <div class="card-header p-0 nav">
                        <div class="row no-gutters flex" role="tablist">
                            <div class="col-auto">
                                <div class="p-card-header">
                                    <p class="mb-0"><strong>Gráfica</strong></p>
                                </div>
                            </div>
                            <div class="col-auto ml-sm-auto p-2">
                      
                                <div hidden class="flatpickr-wrapper">
                                    <div id="recent_orders_date" data-toggle="flatpickr" data-flatpickr-wrap="true" data-flatpickr-mode="range" data-flatpickr-alt-format="d/m/Y" data-flatpickr-date-format="d/m/Y">
                                        <a href="javascript:void(0)" class="link-date" data-toggle>13/03/2018 to 20/03/2018</a>
                                        <input class="flatpickr-hidden-input" type="hidden" value="13/03/2018 to 20/03/2018" data-input>
                                    </div>
                                </div>
                    
                            </div>
                        </div>
                    </div>
                    
                    <div class="card-body">
                    <img id="nohaydatos" width="100%" src="{{ asset('images/axin/No_hay_datos_disponibles.png') }}">   
                    <div id="Barchart">
                        </div>
                    </div>
                    
                </div>
            </div>
        </div>

    </div>
</div>

@endsection
@section('script')
    <script>
    var array = {!! json_encode($accounts->toArray()) !!};
    console.log(array);
    getStadistics(array[0].idaccount,'socio');
    </script>
@endsection