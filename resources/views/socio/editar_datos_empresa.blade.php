@extends('layout')

@section('content')

<div class="container page__container">
    <style>
        ::placeholder { 
            color: #185bc3!important;
            opacity: 1; 
        }
        input:invalid {
			border-color: #DD2C00!important;
        }

        .check-ok {
            color:#185bc3!important;
            position: inherit; right: -20px; top: 10px!important;
        }
        
        .check-No {
            color: red!important;
            position: inherit; right: -20px; top: 10px!important;
        }
    
        input:invalid ~ .check-ok {
            display: none!important;
        }

        input:valid ~ .check-ok {
            display: inline!important;
        }
        input:invalid ~ .check-No {
            display: inline!important;
        }

        input:valid ~ .check-No {
            display: none!important;
        }

        .invalidDocument {
            background: rgba(255, 0, 0, 0.6)!important; 
            color:white!important;
            display: inline-block;
            cursor: pointer;
            padding: 8px 16px;
            border-radius: 2px;
            margin-right: 8px;
            border: 2px dashed red;
            width:300px;
            text-align:center;
        }
    </style>
    <form id="update_cargard_form" enctype="multipart/form-data" method="POST"  action="{{ route('update_cargard2') }}">
        <div class="row">
            <div class="col-lg-9 pr-lg-0">

                <div class="page-section">
                    <h4>Perfil - Datos de la empresa</h4>
                    <br><br><br>
                    <div class="list-group list-group-form">
                        <div class="list-group-item space">
                            <div class="form-group row align-items-center mb-0">
                                <label class="col-form-label form-label col-sm-12">NOMBRE DE LA EMPRESA</label>
                                <div class="col-sm-12">
                                    
                                    <input disabled onkeyup="validar()" onkeypress="return validarCaracteres(event)" minlength="3" maxlength="80" type="text" value="{{ $partner->name_business }}" name="name_business" id="name_business" class="form-control" placeholder="" readonly required/>
                                    
                                </div>
                            </div>
                            <div class="form-group row align-items-center mb-0">
                                <label class="col-form-label form-label col-sm-12">RFC</label>
                                <div class="col-sm-12">
                                    
                                    <input disabled onkeyup="validar()" onkeypress="return validarNumerosLetras(event)" minlength="10" maxlength="80" type="text" value="{{ $partner->rfc_business }}" name="rfc_business" id="rfc_business" class="form-control" placeholder="" required/>
                                    
                                </div>
                            </div>
                            <div class="form-group mb-0">
                                <div class="form-row">
                                    <div class="col-xs-12 col-md-6 col-lg-6">
                                        <label class="col-form-label form-label col-xs-12 col-md-12 col-lg-12" style="padding:0;">PAIS</label>
                                        <select onchange="validar()" id="birth_date_country" name="country_business" value="{{ old('country_business') }}" class="form-control custom-select inputFormu" required>
                                            <option disabled value="" selected>{{$partner->country_business}}</option>
                                        </select>
                                        
                                    </div>
                                    <div class="col-xs-12 col-md-6 col-lg-6">
                                        <div class="form-row">
                                            <div class="col-xs-12 col-md-6 col-lg-6">
                                                <label class="col-form-label form-label col-xs-12 col-md-12 col-lg-12" style="padding:0; font-size:0.8rem!important;">CIUDAD</label>
                                                <div class="col-xs-12 col-md-12 col-lg-12" style="padding:0;">
                                                    <input disabled onkeyup="validar()" onkeypress="return validarCaracteres(event)" type="text" name="city_business" value="{{ $partner->city_business}}" class="form-control" placeholder="" required/>
                                                    
                                                </div>
                                            </div>
                                            <div class="col-xs-12 col-md-6 col-lg-6">
                                                <label class="col-form-label form-label col-xs-12 col-md-12 col-lg-12" style="padding:0; font-size:0.8rem!important;">CÓDIGO POSTAL</label>
                                                <div class="col-xs-12 col-md-12 col-lg-12" style="padding:0;">
                                                    <input disabled onkeyup="validar()" onkeypress="return validarNumeros(event)" type="text" pattern="^[0-9]\d{2,10}$" name="code_postal_business" value="{{ $partner->code_postal_business}}" class="form-control col-xs-12 col-md-12 col-lg-12" placeholder="" required/>
                                                    
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>   
                            <div class="form-group mb-0">
                                <div class="form-row">
                                    <div class="col-xs-12 col-md-12 col-lg-12">
                                        <label class="col-form-label form-label col-xs-12 col-md-12 col-lg-12" style="padding:0;">DOMICILIO FISCAL</label>
                                        <div class="col-xs-12 col-md-12 col-lg-12" style="padding:0;">
                                            <input onkeyup="validar()" type="text" name="direction_company" value="{{ $partner->direction_company }}" class="form-control col-xs-12 col-md-12 col-lg-12" pattern="^[A-Za-z0-9äÄëËïÏöÖüÜáéíóúáéíóúÁÉÍÓÚÂÊÎÔÛâêîôûàèìòùÀÈÌÒÙñÑ, ]{10,100}$" placeholder="" required/>
                                            
                                        </div>
                                    </div>
                                </div>
                            </div>   
                            <br>
                            <div class="form-group row align-items-center mb-0">
                                <div class="col-sm-12" style="display: flex; justify-content:center;">
                                    <button class="btn btn-accent" style="background-color: #185bc3; width:15rem;color:white" id="boton" type="submit" disabled>
                                        <span class="spinner-text">Actualizar</span>
                                    </button>
                                </div>
                            </div>
                        </div>
                
                    </div>
                </div>

            </div>
            <div class="col-lg-3 page-nav">
                <div class="page-section pt-lg-112pt">
                    <nav class="nav page-nav__menu historialNav">
                        
                        <a class="nav-link active" href="{{ url('socio/editar_empresa') }}">Actividad Empresarial</a>
                        <a class="nav-link" href="{{ url('socio/editar_documentos') }}">Cargar documentos</a>

                    </nav>
                </div>
            </div>
        </div>
    </form>

</div>

@endsection