<!doctype html>
<html lang="es">
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">

        <title>Contrato</title>

        <style>

            h1,h2,h3,h4,,h5,p {
                font-family: Arial!important;
            }
            .negrita {
                font-weight: bold;
            }
            .justificar {
                text-align: justify;
            }
            .subrayar {
                text-decoration-line: underline;
            }
            .rellenar {
                background: yellow;
            }
            .firma {
                display: flex; justify-content:center;
            }
        </style>

    </head>
    <body>
        <div style="margin: 5rem 10rem 5rem 10rem">
            <div>
                <div>
                    <h5 class="negrita">CONTRATO INDIVIDUAL DE TRABAJO POR TIEMPO DETERMINADO</h5>
                </div>
                <br>
                <div>
                    <p class="justificar">
                        &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;En la ciudad de Xalapa,Veracruz a <span class="negrita subrayar">{{$date}}</span>, los
                    que suscribimos el presente, a saber representada por el C. Jhonatan
                    Chávez Castillo como patrón, que en lo  sucesivo  se denominará “<span class="negrita">el
                        patrón</span> ”, y, por la otra, el C. Carlos Enrique Juárez Jiménez por su propio
                    derecho, como trabajador, que en adelante se denominará “ <span class="negrita">el
                        trabajador</span> ”, hacemos constar que hemos convenido en celebrar <span class="negrita">un
                            contrato individual de trabajo por tiempo determinado</span>, bajo las
                    siguientes:
                    </p>
                </div>
                <div>
                    <h3>CLÁUSULAS</h3>
                </div>
                <div>
                    <p class="justificar">
                        <span class="negrita">Primera.</span>  El C. Jhonatan Chávez Castillo declara que es una persona física
                        con domicilio en Calle Octavio Blancas núm. 20, Fraccionamiento Lucas
                        Martin, CP:91100, Xalapa,Ver y que acredita su personalidad con credencial
                        de elector.
                        <br><br>
                        El trabajador declara:
                        <br><br>
                        Llamarse Carlos Enrique Juárez Jiménez edad 30 años estado
                        civil <span class="negrita subrayar">casado</span> nacionalidad Mexicana y con domicilio en
                        Calle Cuauhtemoc núm.23, Colonia 21 de Marzo, CP:91010, Xalapa,Ver.
                        <br><br>
                        <span class="negrita">Segunda.</span> Este contrato se celebra por un tiempo <span class="rellenar">determinado de 90 días,
                        con vencimiento en la fecha 21 de enero del 2020</span> en <span class="rellenar">virtud de que el trabajador se encontrará en periodo de prueba.</span>
                        <br><br>
                        <span class="negrita">Tercera.</span> El trabajador se obliga a prestar los servicios personales que se
                        especifican en la cláusula anterior, subordinado jurídicamente al patrón,
                        <br><br>
                        con esmero y eficiencia, en las oficinas (o talleres) del patrón, y en
                        cualquier lugar de esta ciudad donde el patrón desempeñe actividades.
                        <br><br>
                        Queda expresamente convenido que el trabajador acatará en el desempeño
                        de su trabajo todas las disposiciones del Reglamento Interior de Trabajo,
                        todas las órdenes, circulares y disposiciones que dicte el patrón y todos los
                        ordenamientos legales que le sean aplicables.
                        <br><br>
                        <span class="negrita">Cuarta.</span> La duración de la jornada será de 8 horas diarias, de lunes a
                        Viernes con el siguiente horario: <span class="rellenar">(9am a 6pm)</span> por lo que constituye una
                        jornada semanal <span class="rellenar">de 40 horas</span>.
                        <br><br>
                        <span class="negrita">Quinta.</span> El trabajador está obligado a checar su tarjeta o a firmar las listas
                        de asistencia, a la entrada y salida de sus labores, por lo que e
                        incumplimiento de ese requisito indicará la falta injustificada a sus labores,
                        para todos los efectos legales.

                        &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<span class="negrita">Septima.</span> El trabajador percibirá, por la prestación de los servicios
                        a que se refiere este contrato, un salario de $6,000 (doce mil pesos MN)
                        pesos mensuales. El salario se le cubrirá cada quince días, en moneda de
                        curso legal y en las oficinas del patrón, estando obligado el trabajador a
                        firmar las constancias de pago respectivas.
                        <br><br>
                        &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<span class="negrita">Octava.</span> <span class="rellenar">Por cada 5 días de trabajo el trabajador tendrá un descanso
                            semanal de 2 días.</span>
                        <br><br>
                        <span class="negrita">Novena.</span> En caso de faltas injustificadas de asistencia al trabajo, se podrán
                        deducir dichas faltas del periodo de prestación de servicios computables
                        para fijar las vacaciones, reduciéndose éstas proporcionalmente,
                        reduciéndose el pago proporcionalmente.
                        <br><br>
                        &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<span class="negrita">Decima.</span> El trabajador será capacitado o adiestrado en los términos de
                        los planes y programas establecidos (o que se establezcan), por el patrón.
                        <br><br>
                        <span class="negrita">Decimoprimera.</span> Ambas partes convienen en que, al vencimiento del
                        término estipulado, este contrato quedará terminado automáticamente.
                        <br><br>
                        <span class="negrita">Decimosegundo.</span> Ambas partes declaran que, respecto a las obligaciones
                        y derechos que mutuamente les corresponden y que no hayan sido motivo
                        de cláusula expresa en el presente contrato.
                        <br><br><br><br>
                        &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Leído que fue por ambas partes este contrato, e impuestas de su
                        contenido, lo firmaron en acuerdo quedando una copia en poder de cada una de ellas.
                        <br><br><br><br><br>

                        <span class="firma negrita">EL PATRÓN                                         &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;EL TRABAJADOR</span>  
                        <br><br><br>
                        <span class="firma">C. Jhonatan Chávez Castillo                                         {{$nombre}}</span>             
                        <br><br>
                        <span class="firma">                                       <img src="{{$url_firm}}" width="100" alt=""></span>   
                                 
                        <br><br>

                        
                        

                         

                                       
                    </p>
                </div>
            </div>
        </div>
    </body>
</html>