@extends('layout')

@section('content')

<div class="container page__container">
    <style>
        #phone { width: 100%; }
        .iti{ width: 100%;}
        .mayus{
            text-transform: uppercase;
        }
        input:invalid {
			border-color: #DD2C00!important;
        }
        select:invalid {
			border-color: #DD2C00!important;
        }

        .check-ok {
            color:#185bc3!important;
            position: absolute; right: 9px; top: 10px!important;
        }
    
        input:invalid ~ .check-ok {
            display: none!important;
        }

        input:valid ~ .check-ok {
            display: inline!important;
        }
        select:invalid ~ .check-ok {
            display: none!important;
        }

        select:valid ~ .check-ok {
            display: inline!important;
        }

    </style>

    <form method="POST" action="{{ route('socio.datos_empresa_store') }}">
    <input type="hidden" name="_token" value="{{ csrf_token() }}"></input>
        <div class="row">
            <div class="col-lg-9 pr-lg-0">

                <div class="page-section">
                    <h4>Perfil - Datos de la empresa</h4>
                    <br><br><br>
                    <div class="list-group list-group-form">
                        <div class="list-group-item space">
                            <div class="form-group row align-items-center mb-0">
                                <label class="col-form-label form-label col-sm-12">NOMBRE DE LA EMPRESA</label>
                                <div class="col-sm-12">
                                    
                                    <input onkeyup="validar()" onkeypress="return validarCaracteres(event)" minlength="3" maxlength="80" type="text" value="{{ $name_business }}" name="name_business" id="name_business" class="form-control @error('name_business') is-invalid @enderror inputFormu mayus" placeholder="" readonly required/><i style="right: 20px!important;" class="fa fa-check check-ok"></i>
                                    @error('name_business')
                                        <span class="invalid-feedback" role="alert">
                                            <strong style="color:red!important">{{ $message }}</strong>
                                        </span>
                                    @enderror
                                </div>
                            </div>
                            <div class="form-group row align-items-center mb-0">
                                <label class="col-form-label form-label col-sm-12">RFC</label>
                                <div class="col-sm-12">
                                    
                                    <input onkeyup="validar()" onkeypress="return validarNumerosLetras(event)" minlength="10" maxlength="80" type="text" value="{{ old('rfc_business') }}" name="rfc_business" id="rfc_business" class="form-control @error('rfc_business') is-invalid @enderror inputFormu mayus" placeholder="" required/><i style="right: 20px!important;" class="fa fa-check check-ok"></i>
                                    @error('rfc_business')
                                        <span class="invalid-feedback" role="alert">
                                            <strong style="color:red!important">{{ $message }}</strong>
                                        </span>
                                    @enderror
                                </div>
                            </div>
                            <div class="form-group mb-0" style="display: none">
                                <select  hidden id="country_code"></select>
                                <input hidden id="phone">               
                            </div>
                            <div class="form-group mb-0">
                                <div class="form-row">
                                    <div class="col-xs-12 col-md-6 col-lg-6">
                                        <label class="col-form-label form-label col-xs-12 col-md-12 col-lg-12" style="padding:0;">PAÍS</label>
                                        <select onchange="validar()" id="birth_date_country" name="country_business" value="{{ old('country_business') }}" class="form-control custom-select @error('country_business') is-invalid @enderror inputFormu" required>
                                        </select><i style="top: 35px!important; right: 47px!important;" class="fa fa-check check-ok"></i>
                                        @error('country_business')
                                            <span class="invalid-feedback" role="alert">
                                                <strong style="color:red!important">{{ $message }}</strong>
                                            </span>
                                        @enderror
                                    </div>
                                    <div class="col-xs-12 col-md-6 col-lg-6">
                                        <div class="form-row">
                                            <div class="col-xs-12 col-md-6 col-lg-6">
                                                <label class="col-form-label form-label col-xs-12 col-md-12 col-lg-12" style="padding:0; font-size:0.8rem!important;">CIUDAD</label>
                                                <div class="col-xs-12 col-md-12 col-lg-12" style="padding:0;">
                                                    <input onkeyup="validar()" onkeypress="return validarCaracteres(event)" type="text" name="city_business" value="{{ old('city_business') }}" class="form-control col-xs-12 col-md-12 col-lg-12 @error('city_business') is-invalid @enderror inputFormu mayus" placeholder="" required/><i class="fa fa-check check-ok"></i>
                                                    @error('city_business')
                                                        <span class="invalid-feedback" role="alert">
                                                            <strong style="color:red!important">{{ $message }}</strong>
                                                        </span>
                                                    @enderror
                                                </div>
                                            </div>
                                            <div class="col-xs-12 col-md-6 col-lg-6">
                                                <label class="col-form-label form-label col-xs-12 col-md-12 col-lg-12" style="padding:0; font-size:0.8rem!important;">CÓDIGO POSTAL</label>
                                                <div class="col-xs-12 col-md-12 col-lg-12" style="padding:0;">
                                                    <input onkeyup="validar()" onkeypress="return validarNumeros(event)" type="text" pattern="^[0-9]\d{2,10}$" name="code_postal_business" value="{{ old('code_postal_business') }}" class="form-control col-xs-12 col-md-12 col-lg-12 @error('code_postal_business') is-invalid @enderror inputFormu" placeholder="" required/><i class="fa fa-check check-ok"></i>
                                                    @error('code_postal_business')
                                                        <span class="invalid-feedback" role="alert">
                                                            <strong style="color:red!important">{{ $message }}</strong>
                                                        </span>
                                                    @enderror
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>   
                            <div class="form-group mb-0">
                                <div class="form-row">
                                    <div class="col-xs-12 col-md-12 col-lg-12">
                                        <label class="col-form-label form-label col-xs-12 col-md-12 col-lg-12" style="padding:0;">DOMICILIO FISCAL</label>
                                        <div class="col-xs-12 col-md-12 col-lg-12" style="padding:0;">
                                            <input onkeyup="validar()" type="text" name="direction_company" value="{{ old('direction_company') }}" class="form-control col-xs-12 col-md-12 col-lg-12 @error('direction_company') is-invalid @enderror inputFormu mayus" pattern="^[A-Za-z0-9äÄëËïÏöÖüÜáéíóúáéíóúÁÉÍÓÚÂÊÎÔÛâêîôûàèìòùÀÈÌÒÙñÑ, ]{10,100}$" placeholder="" required/> <i class="fa fa-check check-ok"></i>
                                            @error('direction_company')
                                                <span class="invalid-feedback" role="alert">
                                                    <strong style="color:red!important">{{ $message }}</strong>
                                                </span>
                                            @enderror
                                        </div>
                                    </div>
                                </div>
                            </div>   
    
                            <br>
                            <div class="form-group row align-items-center mb-0">
                                <div class="col-sm-12" style="display: flex; justify-content:center;">
                                    <button class="btn btn-accent botonSiguienteP" style="background-color: #185bc3; width:15rem;color:white" id="boton" type="submit" disabled>
                                        <span class="spinner-text">Siguiente paso</span>
                                        <span class="spinner-grow spinner-grow-sm" role="status" aria-hidden="true"></span>
                                        <span class="spinner-grow spinner-grow-sm" role="status" aria-hidden="true"></span>
                                        <span class="spinner-grow spinner-grow-sm" role="status" aria-hidden="true"></span>
                                    </button>
                                </div>
                            </div>
                            
                        </div>
                
                    </div>
                </div>

            </div>
            <div class="col-lg-3 page-nav">
                <div class="page-section pt-lg-112pt">
                    <nav class="nav page-nav__menu historialNav">
    
                        <a class="nav-link disabled active">Datos de la empresa</a>
                        <a class="nav-link disabled">Representante legal</a>
                        <a class="nav-link disabled">Documentos representante legal</a>
                        <a class="nav-link disabled">Firma de contrato</a>

                
                    </nav>
                </div>
            </div>
        </div>
    </form>
</div>

@endsection