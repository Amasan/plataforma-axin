@extends('layout')

@section('content')

<div class="container page__container">
    <style>
        #phone { width: 100%; }
        .iti{ width: 100%;}
        .mayus{
            text-transform: uppercase;
        }
        input:invalid {
			border-color: #DD2C00!important;
        }
        select:invalid {
			border-color: #DD2C00!important;
        }

        .check-ok {
            color:#185bc3!important;
            position: absolute; right: 9px; top: 10px!important;
        }
    
        input:invalid ~ .check-ok {
            display: none!important;
        }

        input:valid ~ .check-ok {
            display: inline!important;
        }
        select:invalid ~ .check-ok {
            display: none!important;
        }

        select:valid ~ .check-ok {
            display: inline!important;
        }

    </style>

    <form method="POST" action="{{ route('socio.representante_legal_store') }}">
    <input type="hidden" name="_token" value="{{ csrf_token() }}"></input>
        <div class="row">
            <div class="col-lg-9 pr-lg-0">

                <div class="page-section">
                    <h4>Perfil - Representante legal</h4>
                    <br><br><br>
                    <div class="list-group list-group-form">
                        <div class="list-group-item space">
                            <div class="form-group row align-items-center mb-0">
                                <label class="col-form-label form-label col-sm-12">NOMBRE(S)</label>
                                <div class="col-sm-12">
                                    
                                    <input onkeyup="validar()" onkeypress="return validarCaracteres(event)" minlength="3" maxlength="80" type="text" value="{{ old('name') }}" name="name" id="name" class="form-control @error('name') is-invalid @enderror inputFormu mayus" placeholder="" required/><i style="right: 20px!important;" class="fa fa-check check-ok"></i>
                                    @error('name')
                                        <span class="invalid-feedback" role="alert">
                                            <strong style="color:red!important">{{ $message }}</strong>
                                        </span>
                                    @enderror
                                </div>
                            </div>
                            
                            <div class="form-group mb-0">
                                <div class="form-row">
                                    <div class="col-xs-12 col-md-6 col-lg-6">
                                        <label class="col-form-label form-label col-xs-12 col-md-12 col-lg-12" style="padding:0;">APELLIDO PATERNO</label>
                                        <div class="col-xs-12 col-md-12 col-lg-12" style="padding:0;">
                                            <input onkeyup="validar()" onkeypress="return validarCaracteres(event)" minlength="4" maxlength="80" type="text" name="first_last_name" value="{{ old('first_last_name') }}" class="form-control col-xs-12 col-md-12 col-lg-12  @error('first_last_name') is-invalid @enderror inputFormu mayus" placeholder="" required /><i class="fa fa-check check-ok"></i>
                                            @error('first_last_name')
                                                <span class="invalid-feedback" role="alert">
                                                    <strong style="color:red!important">{{ $message }}</strong>
                                                </span>
                                            @enderror
                                        </div>
                                    </div>
                                    <div class="col-xs-12 col-md-6 col-lg-6">
                                        <label class="col-form-label form-label col-xs-12 col-md-12 col-lg-12" style="padding:0;">APELLIDO MATERNO</label>
                                        <div class="col-xs-12 col-md-12 col-lg-12" style="padding:0;">
                                            <input onkeyup="validar()" onkeypress="return validarCaracteres(event)" minlength="4" maxlength="80" type="text" name="second_last_name" value="{{ old('second_last_name') }}" class="form-control col-xs-12 col-md-12 col-lg-12 @error('second_last_name') is-invalid @enderror inputFormu mayus" placeholder="" required /><i class="fa fa-check check-ok"></i>
                                            @error('second_last_name')
                                                <span class="invalid-feedback" role="alert">
                                                    <strong style="color:red!important">{{ $message }}</strong>
                                                </span>
                                            @enderror
                                        </div>
                                    </div>
                                </div>
                            </div>   
                            <div class="form-group mb-0">
                                <div class="form-row">
                                    <div class="col-xs-12 col-md-6 col-lg-6">
                                        <label class="col-form-label form-label col-xs-12 col-md-12 col-lg-12" style="padding:0;">FECHA DE NACIMIENTO</label>
                                        <div class="col-xs-12 col-md-12 col-lg-12" style="padding:0;">
                                            <input onchange="validar()" id="date" type="date" name="birth_date" value="{{ old('birth_date') }}" class="form-control col-xs-12 col-md-12 col-lg-12 @error('birth_date') is-invalid @enderror inputFormu" required/><i style="right: 41px!important;" class="fa fa-check check-ok"></i>
                                            @error('birth_date')
                                                <span class="invalid-feedback" role="alert">
                                                    <strong style="color:red!important">{{ $message }}</strong>
                                                </span>
                                            @enderror
                                        </div>
                                    </div>
                                    <div class="col-xs-12 col-md-6 col-lg-6">
                                        <label class="col-form-label form-label col-xs-12 col-md-12 col-lg-12" style="padding:0;">TÉLEFONO CELULAR</label>
                                        <div class="col-xs-12 col-md-12 col-lg-12" style="padding:0;">
                                            <div class="col-xs-12 col-md-12 col-lg-12" style="padding:0;">
                                                <select hidden id="country_code" class="form-control @error('country_code') is-invalid @enderror" name="country_code" value="{{ old('country_code') }}" required></select>
                                                <input onkeyup="validar()" onkeypress="return validarNumeros(event)" class="form-control iti @error('number_phone') is-invalid @enderror inputFormu" id="phone" name="number_phone" value="{{ old('number_phone') }}" pattern="^[0-9]\d{5,10}$" minlength="5" maxlength="10" type="tel" required><i class="fa fa-check check-ok"></i>
                                                @error('number_phone')
                                                    <span class="invalid-feedback" role="alert">
                                                        <strong style="color:red!important">{{ $message }}</strong>
                                                    </span>
                                                @enderror                    

                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>   
                            <div class="form-group mb-0">
                                <div class="form-row">
                                    <div class="col-xs-12 col-md-6 col-lg-6">
                                        <label class="col-form-label form-label col-xs-12 col-md-12 col-lg-12" style="padding:0;">PAÍS DE NACIMIENTO</label>
                                        <select onchange="validar()" id="birth_date_country" name="birth_date_country" value="{{ old('birth_date_country') }}" class="form-control custom-select @error('birth_date_country') is-invalid @enderror inputFormu" required>
                                        </select><i style="top: 35px!important; right: 47px!important;" class="fa fa-check check-ok"></i>
                                        @error('birth_date_country')
                                            <span class="invalid-feedback" role="alert">
                                                <strong style="color:red!important">{{ $message }}</strong>
                                            </span>
                                        @enderror
                                    </div>
                                    <div class="col-xs-12 col-md-6 col-lg-6">
                                        <div class="form-row">
                                            <div class="col-xs-12 col-md-6 col-lg-6">
                                                <label class="col-form-label form-label col-xs-12 col-md-12 col-lg-12" style="padding:0; font-size:0.8rem!important;">CIUDAD</label>
                                                <div class="col-xs-12 col-md-12 col-lg-12" style="padding:0;">
                                                    <input onkeyup="validar()" onkeypress="return validarCaracteres(event)" type="text" name="city" value="{{ old('city') }}" class="form-control col-xs-12 col-md-12 col-lg-12 @error('city') is-invalid @enderror inputFormu mayus" placeholder="" required/><i class="fa fa-check check-ok"></i>
                                                    @error('city')
                                                        <span class="invalid-feedback" role="alert">
                                                            <strong style="color:red!important">{{ $message }}</strong>
                                                        </span>
                                                    @enderror
                                                </div>
                                            </div>
                                            <div class="col-xs-12 col-md-6 col-lg-6">
                                                <label class="col-form-label form-label col-xs-12 col-md-12 col-lg-12" style="padding:0; font-size:0.8rem!important;">CÓDIGO POSTAL</label>
                                                <div class="col-xs-12 col-md-12 col-lg-12" style="padding:0;">
                                                    <input onkeyup="validar()" onkeypress="return validarNumeros(event)" type="text" pattern="^[0-9]\d{2,10}$" name="code_postal" value="{{ old('code_postal') }}" class="form-control col-xs-12 col-md-12 col-lg-12 @error('code_postal') is-invalid @enderror inputFormu" placeholder="" required/><i class="fa fa-check check-ok"></i>
                                                    @error('code_postal')
                                                        <span class="invalid-feedback" role="alert">
                                                            <strong style="color:red!important">{{ $message }}</strong>
                                                        </span>
                                                    @enderror
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>   
                            <div class="form-group mb-0">
                                <div class="form-row">
                                    <div class="col-xs-12 col-md-12 col-lg-12">
                                        <label class="col-form-label form-label col-xs-12 col-md-12 col-lg-12" style="padding:0;">DIRECCIÓN</label>
                                        <div class="col-xs-12 col-md-12 col-lg-12" style="padding:0;">
                                            <input onkeyup="validar()" type="text" name="direction" value="{{ old('direction') }}" class="form-control col-xs-12 col-md-12 col-lg-12 @error('direction') is-invalid @enderror inputFormu mayus" pattern="^[A-Za-z0-9äÄëËïÏöÖüÜáéíóúáéíóúÁÉÍÓÚÂÊÎÔÛâêîôûàèìòùÀÈÌÒÙñÑ, ]{10,100}$" placeholder="" required/> <i class="fa fa-check check-ok"></i>
                                            @error('direction')
                                                <span class="invalid-feedback" role="alert">
                                                    <strong style="color:red!important">{{ $message }}</strong>
                                                </span>
                                            @enderror
                                        </div>
                                    </div>
                                </div>
                            </div>   
    
                            <br>

                            <div class="form-group row align-items-center mb-0">
                                <div class="col-sm-12" style="display: flex; justify-content:center;">
                                    <button class="btn btn-accent botonSiguienteP" style="background-color: #185bc3; width:15rem;color:white" id="boton" type="submit" disabled>
                                        <span class="spinner-text">Siguiente paso</span>
                                        <span class="spinner-grow spinner-grow-sm" role="status" aria-hidden="true"></span>
                                        <span class="spinner-grow spinner-grow-sm" role="status" aria-hidden="true"></span>
                                        <span class="spinner-grow spinner-grow-sm" role="status" aria-hidden="true"></span>
                                    </button>
                                </div>
                            </div>
                            
                        </div>
                
                    </div>
                </div>

            </div>
            <div class="col-lg-3 page-nav">
                <div class="page-section pt-lg-112pt">
                    <nav class="nav page-nav__menu historialNav">
    
                        <a class="nav-link disabled">Datos de la empresa</a>
                        <a class="nav-link disabled active">Representante legal</a>
                        <a class="nav-link disabled">Documentos representante legal</a>
                        <a class="nav-link disabled">Firma de contrato</a>

                
                    </nav>
                </div>
            </div>
        </div>
    </form>
</div>

@endsection