@extends('layoutAdmins')

@section('content')
    <style>
        .activa {
            background-color: #185bc3!important;
            color: white!important;
        }
        strong {
            font-weight: bold!important;
            color: black!important;
        }
    </style>
    <div class="m-4">
    <ul class="nav nav-tabs nav-tabs-card">
        <li class="nav-item">
            <a class="nav-link" href="{{ route('rendimiento.clientes.depositos') }}">Depositos</a>
        </li>
        <li class="nav-item">
            <a class="nav-link activa" href="{{ route('rendimiento.clientes.retirarFondos') }}">Retiros</a>
        </li>
        <li class="nav-item">
            <a class="nav-link" href="{{ route('rendimiento.clientes.movimientos') }}">Movimientos</a>
        </li>
        @foreach (Auth::user()->roles()->get() as $rol)
            @if($rol->id == 1)
                <li class="nav-item">
                    <a class="nav-link" href="{{ route('rendimiento.clientes.operaciones') }}">Operaciones</a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" href="{{ route('admin.clientes.clientes') }}">+ Depositos</a>
                </li>
            @endif
        @endforeach
    </ul>
    <br>
    <br>
    <form action="{{ route('rendimiento.clientes.retirarFondos') }}" method="GET" class="form-inline float-left">
        <div class="form-group">
            <input type="text" name="name" class="form-control" placeholder="Nombre">
        </div>
        <div class="form-group">
            <input type="text" name="email" class="form-control" placeholder="Correo electronico">
        </div>
        <div class="form-group">
            <button type="submit" class="btn btn-default"><span class="fas fa-search"></span></button>
        </div>
    </form>
    <div class="table-responsive">
        <table class="table">
            <thead>
                <tr>
                    <th>Cliente</th>
                    <th>Correo</th>
                    <th>Cuenta de retiro</th>
                    <th>Cantidad</th>
                    <th>Tipo de Retiro</th>
                    <th>Datos</th>
                    <th>Fecha</th>
                    <th>Fin de contrato</th>
                    <th>Estado</th>
                    <th>Notificación</th>
                </tr>
            </thead>
            <tbody>
                @foreach ($clientWithDrawalRequests as $clientWithDrawalRequest)
                <tr>
                    <td>{{$clientWithDrawalRequest->accountClient->client->user->name}} {{$clientWithDrawalRequest->accountClient->client->user->first_last_name}} {{$clientWithDrawalRequest->accountClient->client->user->second_last_name}}</td>
                    <td>{{$clientWithDrawalRequest->accountClient->client->user->email}}</td>
                    <td>{{$clientWithDrawalRequest->accountClient->typeaccount->name}}</td>
                    <td>$ {{$clientWithDrawalRequest->amount}} USD</td>
                    <td>{{$clientWithDrawalRequest->method}}</td>
                    <td>
                    @if ($clientWithDrawalRequest->bitcoin_address)
                        {{$clientWithDrawalRequest->bitcoin_address}}.
                    @endif
                    @if($clientWithDrawalRequest->bank_number)
                    <strong>TARJETA:</strong> {{$clientWithDrawalRequest->bank_number}}
                    @endif
                    @if($clientWithDrawalRequest->swift_code)
                        <br><strong>SWIFT:</strong> {{$clientWithDrawalRequest->swift_code}}
                    @endif
                    @if($clientWithDrawalRequest->ivan_code)
                        <br><strong>IBAN:</strong> {{$clientWithDrawalRequest->ivan_code}}
                    @endif
                    </td>
                    <td>{{$clientWithDrawalRequest->accountClient->date_contract_fin}}</td>
                    <td>{{$clientWithDrawalRequest->created_at->format('Y-m-d')}}</td>  
                    @if ($clientWithDrawalRequest->status == 'Realizado')
                    <td>Realizado</td>
                    @elseif($clientWithDrawalRequest->status == 'Rechazado')
                    <td>Rechazado</td>
                    @else
                    <form method="POST" action="{{ route('rendimiento.clientes.retiros_cliente_reviewed') }}">
                    <input type="hidden" name="_token" value="{{ csrf_token() }}"></input>
                    <input type="hidden" name="id" value="{{ $clientWithDrawalRequest->id }}"></input>
                    <td class="m-0">
                        <select id="status{{ $clientWithDrawalRequest->id }}" onchange="retiro({{ $clientWithDrawalRequest->id }})" name="status" class="form-control" required>
                            <option value="{{$clientWithDrawalRequest->status}}" selected>
                                @if($clientWithDrawalRequest->status == 'Realizado') Realizado
                                @elseif($clientWithDrawalRequest->status == 'Pendiente') Pendiente
                                @elseif($clientWithDrawalRequest->status == 'Rechazado') Rechazado
                                @endif
                            </option>
                            @if($clientWithDrawalRequest->status != 'Realizado')<option value="Realizado">Realizado</option>@endif
                            @if($clientWithDrawalRequest->status != 'Pendiente')<option value="Pendiente">Pendiente</option>@endif
                            @if($clientWithDrawalRequest->status != 'Rechazado')<option value="Rechazado">Rechazado</option>@endif
                        </select>
                        <button class="btn btn-primary btn-lg btn-block" id="retirarfondosbutton{{ $clientWithDrawalRequest->id }}" type="submit" disabled>Guardar</button>
                    </td>

                    </form>
                    @endif 
                    <td><a href="{{ route('notificaciones.transacciones.mensajeRetiro',$clientWithDrawalRequest->id ) }}" class="btn btn-primary">Notificar</a></td>             
                </tr>
            @endforeach
            </tbody>
        </table>
        {{ $clientWithDrawalRequests->links() }}
    </div>
</div>
@endsection