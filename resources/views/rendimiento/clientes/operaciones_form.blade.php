@extends('layoutAdmins')

@section('content')
    <style>
        .activa {
            background-color: #185bc3!important;
            color: white!important;
        }
    </style>
<div class="m-4">
    <ul class="nav nav-tabs nav-tabs-card">
        <li class="nav-item">
            <a class="nav-link" href="{{ route('rendimiento.clientes.depositos') }}">Depositos</a>
        </li>
        <li class="nav-item">
            <a class="nav-link" href="{{ route('rendimiento.clientes.retirarFondos') }}">Retiros</a>
        </li>
        <li class="nav-item">
            <a class="nav-link" href="{{ route('rendimiento.clientes.movimientos') }}">Movimientos</a>
        </li>
        @foreach (Auth::user()->roles()->get() as $rol)
            @if($rol->id == 1)
                <li class="nav-item">
                    <a class="nav-link activa" href="{{ route('rendimiento.clientes.operaciones') }}">Operaciones</a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" href="{{ route('admin.clientes.clientes') }}">+ Depositos</a>
                </li>
            @endif
        @endforeach
    </ul>
</div>
<div class="container page__container">
    <div class="page-section">
 
        <div class="card card-form d-flex flex-column flex-sm-row mb-lg-32pt">
            <div class="card-form__body card-body-form-group flex text-center" style="background-color: white;">
                <form id="update_operacion_form" method="POST" action="{{ route('operations_update') }}">
                    <input type="hidden" name="_token" value="{{ csrf_token() }}"></input>
                    <div class="card-header"><h4>{{$client->user->name}} {{$client->user->first_last_name}} {{$client->user->second_last_name}}</h4></div>
                    <div class="card-body">

                        <div class="form-group row align-items-center mb-0">
                            <div class="col-sm-4">
                                <label class="col-form-label form-label">AÑO</label>
                                <select id="year" name="year" class="form-control" required>
                                    <option value="2020" selected>2020</option>
                                    @foreach($years as $year)
                                        <option value="{{$year}}">{{$year}}</option>
                                    @endforeach
                                </select>
                            </div>
                            
                            <div class="col-sm-4">
                                <label class="col-form-label form-label">MES</label>
                                <select id="month" name="month" class="form-control" required>
                                    <option value="01" selected>01</option>
                                    <option value="02">02</option>
                                    <option value="03">03</option>
                                    <option value="04">04</option>
                                    <option value="05">05</option>
                                    <option value="06">06</option>
                                    <option value="07">07</option>
                                    <option value="08">08</option>
                                    <option value="09">09</option>
                                    <option value="10">10</option>
                                    <option value="11">11</option>
                                    <option value="12">12</option>
                                </select>
                            </div>
                            
                            <div class="col-sm-4">
                                <label class="col-form-label form-label">DIA</label>
                                <select id="day" name="day" class="form-control" required>
                                    <option value="1" selected>1</option>
                                    @foreach($days as $day)
                                        <option value="{{$day}}">{{$day}}</option>
                                    @endforeach
                                </select>
                            </div>
                        </div>
                        <div class="form-group row align-items-center mb-0">
                            <div class="col-sm-12">
                            <input name="client_id" value="{{$client->id}}" hidden>
                            </div>
                        </div>
                        <div class="form-group row align-items-center mb-0">
                            <label class="col-form-label form-label col-sm-12">CUENTA</label>
                            <div class="col-sm-12">
                                <select name="type_account_id" value="{{ old('type_account_id') }}" onchange="validar()" id="expire_month" class="form-control custom-select" required>
                                    <option disabled value="" selected>Selecciona una cuenta</option>
                                    @foreach ($accounts as $account) 
                                        <option value="{{ $account['id'] }}">{{ $account['name'] }}</option>
                                    @endforeach
                                </select>
                                
                            </div>
                        </div>

                                
                        <div class="form-group mb-0">
                            <div class="form-row">
                                <div class="col-xs-12 col-md-12 col-lg-12">
                                    <label class="col-form-label form-label col-xs-12 col-md-12 col-lg-12" style="padding:0;">GANANCIA ACUMULADA MENSUAL (USD)</label>
                                    <div class="col-xs-12 col-md-12 col-lg-12" style="padding:0;">
                                        <input name="amount"  value="" type="text" onchange="validar()" class="form-control col-xs-12 col-md-12 col-lg-12" placeholder="Cantidad en dolares" required />
                                        
                                    </div>
                                </div>
                            </div>
                        </div>     
                        <br>
                        <div class="form-group row align-items-center mb-0">
                            <div class="col-sm-12" style="display: flex; justify-content:center;">
                                <button class="btn btn-accent" style="background-color: #185bc3; width:15rem;" id="boton" type="submit">Procesar</button>
                            </div>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>
@endsection