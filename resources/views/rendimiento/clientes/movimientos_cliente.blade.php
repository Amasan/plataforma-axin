@extends('layoutAdmins')

@section('content')
    <style>
        .activa {
            background-color: #185bc3!important;
            color: white!important;
        }
    </style>
<div class="m-4"> 
    <ul class="nav nav-tabs nav-tabs-card">
        <li class="nav-item">
            <a class="nav-link" href="{{ route('rendimiento.clientes.depositos') }}">Depositos</a>
        </li>
        <li class="nav-item">
            <a class="nav-link" href="{{ route('rendimiento.clientes.retirarFondos') }}">Retiros</a>
        </li>
        <li class="nav-item">
            <a class="nav-link activa" href="{{ route('rendimiento.clientes.movimientos') }}">Movimientos</a>
        </li>
        @foreach (Auth::user()->roles()->get() as $rol)
            @if($rol->id == 1)
                <li class="nav-item">
                    <a class="nav-link" href="{{ route('rendimiento.clientes.operaciones') }}">Operaciones</a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" href="{{ route('admin.clientes.clientes') }}">+ Depositos</a>
                </li>
            @endif
        @endforeach
    </ul>
    <br>
    <br>
        <div class="table-responsive">
        <table class="table">
            <thead>
                <tr>
                    <th>CUENTA</th>
                    <th>CONCEPTO</th>
                    <th>FECHA</th>
                    <th>MONTO</th>
                    <th>ESTADO</th>                     
                </tr>
            </thead>
            <tbody>
            @foreach ( $items as $item )
                 <tr>
                    <td>{{$item['name']}} - {{$item['account_number']}}</td>
                    <td>{{$item['concept']}}</td>
                    <td>{{$item['date_deposite']}}</td>
                    <td>
                        @if ($item['amount'] == 0)
                        ---------
                        @else
                        ${{$item['amount']}} USD
                        @endif
                    </td>
                    <td>{{$item['status']}}</td>
                </tr>
            @endforeach
               

            </tbody>
        </table>
       {{ $items->links() }}
    </div>
</div>
@endsection