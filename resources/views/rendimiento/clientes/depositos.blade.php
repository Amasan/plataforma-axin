@extends('layoutAdmins')

@section('content')
    <style>
        .activa {
            background-color: #185bc3!important;
            color: white!important;
        }
    </style>
    <div class="m-4">
        <ul class="nav nav-tabs nav-tabs-card">
            <li class="nav-item">
                <a class="nav-link activa" href="{{ route('rendimiento.clientes.depositos') }}">Depositos</a>
            </li>
            <li class="nav-item">
                <a class="nav-link" href="{{ route('rendimiento.clientes.retirarFondos') }}">Retiros</a>
            </li>
            <li class="nav-item">
                <a class="nav-link" href="{{ route('rendimiento.clientes.movimientos') }}">Movimientos</a>
            </li>
            @foreach (Auth::user()->roles()->get() as $rol)
                @if($rol->id == 1)
                    <li class="nav-item">
                        <a class="nav-link" href="{{ route('rendimiento.clientes.operaciones') }}">Operaciones</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" href="{{ route('admin.clientes.clientes') }}">+ Depositos</a>
                    </li>
                @endif
            @endforeach
            
        </ul>
        <br>
        <br>
        <form action="{{ route('rendimiento.clientes.depositos') }}" method="GET" class="form-inline float-left">
            <div class="form-group">
                <input type="text" name="name" class="form-control" placeholder="Nombre">
            </div>
            <div class="form-group">
                <input type="text" name="email" class="form-control" placeholder="Correo electronico">
            </div>
            <div class="form-group">
                <button type="submit" class="btn btn-default"><span class="fas fa-search"></span></button>
            </div>
        </form>
        <div class="table-responsive">
        <table class="table">
            <thead>
                <tr>
                    <th>Cliente</th>
                    <th>Correo</th>
                    <th>Cuenta</th>
                    <th>Comprobante</th>
                    <th>Fecha de envio</th>
                    <th>Estatus</th>
                    <th>Monto de deposito</th>
                    <th>Notificación</th>
                </tr>
            </thead>
            <tbody>
                @foreach ($clientReceipts as $clientReceipt)
                    <tr>
                        <td>{{$clientReceipt->name}} {{$clientReceipt->first_last_name}} {{$clientReceipt->second_last_name}}</td>
                        <td>{{$clientReceipt->email}}</td>
                        <td>{{$clientReceipt->accountName}}</td>
                        <td>
                            <a type="button" onclick="getcomprobante('{{$clientReceipt->clientId}}','{{$clientReceipt->file_name}}','file')">
                                @if($clientReceipt->file_name && $clientReceipt->reviewed == 'NULL')
                                <img src="{{asset('images/axin/doc3.png')}}" width="40" height="auto">
                                @elseif($clientReceipt->file_name && $clientReceipt->reviewed == '1')
                                <img src="{{asset('images/axin/doc2.png')}}" width="40" height="auto">
                                @elseif ($clientReceipt->file_name && $clientReceipt->reviewed == '0')
                                <img src="{{asset('images/axin/doc1.png')}}" width="40" height="auto">
                                @endif
                            </a>
                        </td>
                        <td>{{ $clientReceipt->created_at->format('Y-m-d')}}</td>
                        <form method="POST" action="{{ route('rendimiento.clientes.depositos_cliente_reviewed') }}">
                        <input type="hidden" name="_token" value="{{ csrf_token() }}"></input>
                        <input type="hidden" name="id" value="{{ $clientReceipt->id }}"></input>
                        <input type="hidden" name="account_client_id" value="{{ $clientReceipt->account_client_id }}"></input>
                        <td>
                        @if ($clientReceipt->reviewed == '1')
                        Aceptado
                        @elseif($clientReceipt->reviewed == '0')
                        Rechazado
                        @else
                            <select onchange="revisado({{$clientReceipt->id}})" id="reviewed{{$clientReceipt->id}}" name="reviewed" class="form-control" required>
                                <option value="{{$clientReceipt->reviewed}}" selected>
                                    @if($clientReceipt->reviewed == 'NULL') Pendiente
                                    @elseif($clientReceipt->reviewed == '1') Aceptado
                                    @elseif($clientReceipt->reviewed == '0') Rechazado
                                    @endif
                                </option>
                                @if($clientReceipt->reviewed != '1')<option value="1">Aceptado</option>@endif
                                @if($clientReceipt->reviewed != '0')<option value="0">Rechazado</option>@endif
                            </select>
                        @endif
                        </td>
                        
                        <td>
                            @if ($clientReceipt->reviewed == 'NULL')
                            <input class="form-control" placeholder="Monto de deposito" type="number" min='0'  onkeypress="return validarNumeros(event)" id="amount{{$clientReceipt->id}}" name="amount" required disabled>
                            <button class="btn btn-primary btn-lg btn-block" id="receiptbutton{{$clientReceipt->id}}" type="submit" disabled>Guardar</button>
                            @elseif($clientReceipt->reviewed == '1')
                            Monto enviado
                            @elseif($clientReceipt->reviewed == '0')
                            Monto rechazado
                            @endif
                        </td>

                        </form>
                        <td><a href="{{ route('notificaciones.transacciones.mensajeDeposito', $clientReceipt->id ) }}" class="btn btn-primary">Notificar</a></td>
                        
                    </tr>
                @endforeach
            </tbody>
        </table>
        {{ $clientReceipts->links() }}
    </div>
</div>
@endsection

