@extends('layoutAdmins')

@section('content')
    <style>
        .activa {
            background-color: #185bc3!important;
            color: white!important;
        }
    </style>
    <div class="m-4">
        <ul class="nav nav-tabs nav-tabs-card">
            <li class="nav-item">
                <a class="nav-link activa" href="{{ route('rendimiento.socios.depositos') }}">Depositos</a>
            </li>
            <li class="nav-item">
                <a class="nav-link" href="{{ route('rendimiento.socios.retirarFondos') }}">Retiros</a>
            </li>
            <li class="nav-item">
                <a class="nav-link" href="{{ route('rendimiento.socios.movimientos') }}">Movimientos</a>
            </li>
            @foreach (Auth::user()->roles()->get() as $rol)
                @if($rol->id == 1)
                    <li class="nav-item">
                        <a class="nav-link" href="{{ route('rendimiento.socios.operaciones') }}">Operaciones</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" href="{{ route('admin.socios.socios') }}">+ Depositos</a>
                    </li>
                @endif
            @endforeach
            
        </ul>
        <br>
        <br>
        <form action="{{ route('rendimiento.socios.depositos') }}" method="GET" class="form-inline float-left">
            <div class="form-group">
                <input type="text" name="name" class="form-control" placeholder="Nombre">
            </div>
            <div class="form-group">
                <input type="text" name="email" class="form-control" placeholder="Correo electronico">
            </div>
            <div class="form-group">
                <button type="submit" class="btn btn-default"><span class="fas fa-search"></span></button>
            </div>
        </form>
        <div class="table-responsive">
        <table class="table">
            <thead>
                <tr>
                    <th>Socio</th>
                    <th>Correo</th>
                    <th>Cuenta</th>
                    <th>Comprobante</th>
                    <th>Fecha de envio</th>
                    <th>Estatus</th>
                    <th>Monto de deposito</th>
                    <th>Notificación</th>
                </tr>
            </thead>
            <tbody>
                @foreach ($partnerReceipts as $partnerReceipt)
                    <tr>
                        <td>{{$partnerReceipt->name}} {{$partnerReceipt->first_last_name}} {{$partnerReceipt->second_last_name}}</td>
                        <td>{{$partnerReceipt->email}}</td>
                        <td>{{$partnerReceipt->accountName}}</td>
                        <td>
                            <a type="button" onclick="getcomprobanteSocio('{{$partnerReceipt->partnerId}}','{{$partnerReceipt->file_name}}','file')">
                                @if($partnerReceipt->file_name && $partnerReceipt->reviewed == 'NULL')
                                <img src="{{asset('images/axin/doc3.png')}}" width="40" height="auto">
                                @elseif($partnerReceipt->file_name && $partnerReceipt->reviewed == '1')
                                <img src="{{asset('images/axin/doc2.png')}}" width="40" height="auto">
                                @elseif ($partnerReceipt->file_name && $partnerReceipt->reviewed == '0')
                                <img src="{{asset('images/axin/doc1.png')}}" width="40" height="auto">
                                @endif
                            </a>
                        </td>
                        <td>{{ $partnerReceipt->created_at->format('Y-m-d')}}</td>
                        <form method="POST" action="{{ route('rendimiento.socios.depositos_socio_reviewed') }}">
                        <input type="hidden" name="_token" value="{{ csrf_token() }}"></input>
                        <input type="hidden" name="id" value="{{ $partnerReceipt->id }}"></input>
                        <input type="hidden" name="account_partner_id" value="{{ $partnerReceipt->account_partner_id }}"></input>
                        <td>
                        @if ($partnerReceipt->reviewed == '1')
                        Aceptado
                        @elseif($partnerReceipt->reviewed == '0')
                        Rechazado
                        @else
                            <select onchange="revisado({{$partnerReceipt->id}})" id="reviewed{{$partnerReceipt->id}}" name="reviewed" class="form-control" required>
                                <option value="{{$partnerReceipt->reviewed}}" selected>
                                    @if($partnerReceipt->reviewed == 'NULL') Pendiente
                                    @elseif($partnerReceipt->reviewed == '1') Aceptado
                                    @elseif($partnerReceipt->reviewed == '0') Rechazado
                                    @endif
                                </option>
                                @if($partnerReceipt->reviewed != '1')<option value="1">Aceptado</option>@endif
                                @if($partnerReceipt->reviewed != '0')<option value="0">Rechazado</option>@endif
                            </select>
                        @endif
                        </td>
                        
                        <td>
                            @if ($partnerReceipt->reviewed == 'NULL')
                            <input class="form-control" placeholder="Monto de deposito" type="number" min='0'  onkeypress="return validarNumeros(event)" id="amount{{$partnerReceipt->id}}" name="amount" required disabled>
                            <button class="btn btn-primary btn-lg btn-block" id="receiptbutton{{$partnerReceipt->id}}" type="submit" disabled>Guardar</button>
                            @elseif($partnerReceipt->reviewed == '1')
                            Monto enviado
                            @elseif($partnerReceipt->reviewed == '0')
                            Monto rechazado
                            @endif
                        </td>

                        </form>
                        <td><a href="{{ route('notificaciones.transacciones.mensajeDepositoSocio', $partnerReceipt->id ) }}" class="btn btn-primary">Notificar</a></td>
                        
                    </tr>
                @endforeach
            </tbody>
        </table>
        {{ $partnerReceipts->links() }}
    </div>
</div>
@endsection

