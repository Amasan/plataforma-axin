@extends('layoutAdmins')

@section('content')
    <style>
        .activa {
            background-color: #185bc3!important;
            color: white!important;
        }
    </style>
<div class="m-4">
        <ul class="nav nav-tabs nav-tabs-card">
            <li class="nav-item">
                <a class="nav-link" href="{{ route('rendimiento.socios.depositos') }}">Depositos</a>
            </li>
            <li class="nav-item">
                <a class="nav-link" href="{{ route('rendimiento.socios.retirarFondos') }}">Retiros</a>
            </li>
            <li class="nav-item">
                <a class="nav-link" href="{{ route('rendimiento.socios.movimientos') }}">Movimientos</a>
            </li>
            @foreach (Auth::user()->roles()->get() as $rol)
                @if($rol->id == 1)
                    <li class="nav-item">
                        <a class="nav-link activa" href="{{ route('rendimiento.socios.operaciones') }}">Operaciones</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" href="{{ route('admin.socios.socios') }}">+ Depositos</a>
                    </li>
                @endif
            @endforeach
            
        </ul>
        <br><br>
        <a type="button" href="{{route('rendimiento.socios.operaciones_form', $partner)}}" class="btn btn-success">Nueva operación</a>
        <br>
        <h1 class="text-center">Historial de operaciones</h1>
        <h2 class="text-center">{{$client_account->user->name}} {{$client_account->user->first_last_name}} {{$client_account->user->second_last_name}}</h2>
        <br>
        <div class="table-responsive">
        <table class="table">
            <thead>
                <tr>
                    <th>Nombre de Cuenta</th>
                    <th>Numero de cuenta</th>
                    <th>Monto USD</th>
                    <th>Operacion</th>
                    <th>Creado</th>
                    <th>Actualizado</th>
                </tr>
            </thead>
            <tbody>
            @foreach ( $accumulateds as $accumulated )
                 <tr>
                    <td>{{$accumulated->name}}</td>
                    <td>{{$accumulated->account_number}}</td>
                    <td>${{$accumulated->amount}} USD</td>
                    <td>{{$accumulated->date}}</td>
                    <td>{{$accumulated->created_at}}</td>
                    <td>{{$accumulated->updated_at}}</td>
                    <td>
                        <a href="{{route('historyOperationsPartnerEdit',$accumulated->id)}}" class="btn btn-warnning btn-block" style="background-color: #185bc3; width:10rem;color:white" type="submit">
                            <span class="spinner-text">Actualizar</span>
                        </a>
                    </td>   
                </tr>
            @endforeach
            </tbody>
        </table>
       {{ $accumulateds->links() }}
    </div>
</div>
@endsection
