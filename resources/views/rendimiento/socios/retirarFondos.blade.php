@extends('layoutAdmins')

@section('content')
    <style>
        .activa {
            background-color: #185bc3!important;
            color: white!important;
        }
        strong {
            font-weight: bold!important;
            color: black!important;
        }
    </style>
    <div class="m-4">
    <ul class="nav nav-tabs nav-tabs-card">
        <li class="nav-item">
            <a class="nav-link" href="{{ route('rendimiento.socios.depositos') }}">Depositos</a>
        </li>
        <li class="nav-item">
            <a class="nav-link activa" href="{{ route('rendimiento.socios.retirarFondos') }}">Retiros</a>
        </li>
        <li class="nav-item">
            <a class="nav-link" href="{{ route('rendimiento.socios.movimientos') }}">Movimientos</a>
        </li>
        @foreach (Auth::user()->roles()->get() as $rol)
            @if($rol->id == 1)
                <li class="nav-item">
                    <a class="nav-link" href="{{ route('rendimiento.socios.operaciones') }}">Operaciones</a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" href="{{ route('admin.socios.socios') }}">+ Depositos</a>
                </li>
            @endif
        @endforeach
        
    </ul>
    <br>
    <br>
    <form action="{{ route('rendimiento.socios.retirarFondos') }}" method="GET" class="form-inline float-left">
        <div class="form-group">
            <input type="text" name="name" class="form-control" placeholder="Nombre">
        </div>
        <div class="form-group">
            <input type="text" name="email" class="form-control" placeholder="Correo electronico">
        </div>
        <div class="form-group">
            <button type="submit" class="btn btn-default"><span class="fas fa-search"></span></button>
        </div>
    </form>
    <div class="table-responsive">
        <table class="table">
            <thead>
                <tr>
                    <th>Socio</th>
                    <th>Correo</th>
                    <th>Cuenta de retiro</th>
                    <th>Cantidad</th>
                    <th>Tipo de Retiro</th>
                    <th>Datos</th>
                    <th>Fecha</th>
                    <th>Fin de Contrato</th>
                    <th>Estado</th>
                    <th>Notificación</th>
                </tr>
            </thead>
            <tbody>
                @foreach ($partnerWithDrawalRequests as $partnerWithDrawalRequest)
                <tr>
                    <td>{{$partnerWithDrawalRequest->accountPartner->partner->user->name}} {{$partnerWithDrawalRequest->accountPartner->partner->user->first_last_name}} {{$partnerWithDrawalRequest->accountPartner->partner->user->second_last_name}}</td>
                    <td>{{$partnerWithDrawalRequest->accountPartner->partner->user->email}}</td>
                    <td>{{$partnerWithDrawalRequest->accountPartner->typeaccount->name}}</td>
                    <td>$ {{$partnerWithDrawalRequest->amount}} USD</td>
                    <td>{{$partnerWithDrawalRequest->method}}</td>
                    <td>
                    @if ($partnerWithDrawalRequest->bitcoin_address)
                        {{$partnerWithDrawalRequest->bitcoin_address}}.
                    @endif
                    @if($partnerWithDrawalRequest->bank_number)
                        <strong>TARJETA:</strong> {{$partnerWithDrawalRequest->bank_number}}
                    @endif
                    @if($partnerWithDrawalRequest->swift_code)
                        <br><strong>SWIFT:</strong> {{$partnerWithDrawalRequest->swift_code}}
                    @endif
                    @if($partnerWithDrawalRequest->ivan_code)
                        <br><strong>IBAN:</strong> {{$partnerWithDrawalRequest->ivan_code}}
                    @endif
                    </td>
                    <td>{{$partnerWithDrawalRequest->created_at->format('Y-m-d')}}</td>  
                    <td>{{$partnerWithDrawalRequest->accountPartner->date_contract_fin}}</td>
                    @if ($partnerWithDrawalRequest->status == 'Realizado')
                    <td>Realizado</td>
                    @elseif($partnerWithDrawalRequest->status == 'Rechazado')
                    <td>Rechazado</td>
                    @else
                    <form method="POST" action="{{ route('rendimiento.socios.retiros_socio_reviewed') }}">
                    <input type="hidden" name="_token" value="{{ csrf_token() }}"></input>
                    <input type="hidden" name="id" value="{{ $partnerWithDrawalRequest->id }}"></input>
                    <td class="m-0">
                        <select id="status{{ $partnerWithDrawalRequest->id }}" onchange="retiro({{ $partnerWithDrawalRequest->id }})" name="status" class="form-control" required>
                            <option value="{{$partnerWithDrawalRequest->status}}" selected>
                                @if($partnerWithDrawalRequest->status == 'Realizado') Realizado
                                @elseif($partnerWithDrawalRequest->status == 'Pendiente') Pendiente
                                @elseif($partnerWithDrawalRequest->status == 'Rechazado') Rechazado
                                @endif
                            </option>
                            @if($partnerWithDrawalRequest->status != 'Realizado')<option value="Realizado">Realizado</option>@endif
                            @if($partnerWithDrawalRequest->status != 'Pendiente')<option value="Pendiente">Pendiente</option>@endif
                            @if($partnerWithDrawalRequest->status != 'Rechazado')<option value="Rechazado">Rechazado</option>@endif
                        </select>
                        <button class="btn btn-primary btn-lg btn-block" id="retirarfondosbutton{{ $partnerWithDrawalRequest->id }}" type="submit" disabled>Guardar</button>
                    </td>

                    </form>
                    @endif 
                    <td><a href="{{ route('notificaciones.transacciones.mensajeRetiroSocio',$partnerWithDrawalRequest->id ) }}" class="btn btn-primary">Notificar</a></td>             
                </tr>
            @endforeach
            </tbody>
        </table>
        {{ $partnerWithDrawalRequests->links() }}
    </div>
</div>
@endsection