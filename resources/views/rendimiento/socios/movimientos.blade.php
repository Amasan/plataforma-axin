@extends('layoutAdmins')

@section('content')
    <style>
        .activa {
            background-color: #185bc3!important;
            color: white!important;
        }
    </style>
    <div class="m-4">
        <ul class="nav nav-tabs nav-tabs-card">
            <li class="nav-item">
                <a class="nav-link" href="{{ route('rendimiento.socios.depositos') }}">Depositos</a>
            </li>
            <li class="nav-item">
                <a class="nav-link" href="{{ route('rendimiento.socios.retirarFondos') }}">Retiros</a>
            </li>
            <li class="nav-item">
                <a class="nav-link activa" href="{{ route('rendimiento.socios.movimientos') }}">Movimientos</a>
            </li>
            @foreach (Auth::user()->roles()->get() as $rol)
                @if($rol->id == 1)
                    <li class="nav-item">
                        <a class="nav-link" href="{{ route('rendimiento.socios.operaciones') }}">Operaciones</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" href="{{ route('admin.socios.socios') }}">+ Depositos</a>
                    </li>
                @endif
            @endforeach
            
        </ul>
        <br>
        <br>
        <form action="{{ route('rendimiento.socios.movimientos') }}" method="GET" class="form-inline float-left">
            <div class="form-group">
                <input type="text" name="name" class="form-control" placeholder="Nombre">
            </div>
            <div class="form-group">
                <input type="text" name="email" class="form-control" placeholder="Correo electronico">
            </div>
            <div class="form-group">
                <button type="submit" class="btn btn-default"><span class="fas fa-search"></span></button>
            </div>
        </form>
        <div class="table-responsive">
        <table class="table">
            <thead>
                <tr>
                    <th>Socio</th>
                    <th>Pais</th>
                    <th>Correo</th>   
                    <th>Telefono</th>
                    <th></th>
                </tr>
            </thead>
            <tbody>
            @foreach ( $partners as $partner )
                 <tr>
                    <td>{{$partner->name}} {{$partner->first_last_name}} {{$partner->second_last_name}}</td>
                    <td>{{$partner->birth_date_country}}</td>
                    <td>{{$partner->email}}</td>
                    <td>{{$partner->number_phone}}</td>
                    <td><a class="btn btn-primary" href="{{route('rendimiento.socios.movimientos_socio', $partner->id)}}">Ver</a></td>
                </tr>
            @endforeach
            </tbody>
        </table>
       {{ $partners->links() }}
    </div>
</div>
@endsection