@extends('layoutAdmins')

@section('content')
    <style>
        .activa {
            background-color: #185bc3!important;
            color: white!important;
        }
    </style>
<div class="m-4">
    <ul class="nav nav-tabs nav-tabs-card">
        <li class="nav-item">
            <a class="nav-link" href="{{ route('rendimiento.socios.depositos') }}">Depositos</a>
        </li>
        <li class="nav-item">
            <a class="nav-link" href="{{ route('rendimiento.socios.retirarFondos') }}">Retiros</a>
        </li>
        <li class="nav-item">
            <a class="nav-link" href="{{ route('rendimiento.socios.movimientos') }}">Movimientos</a>
        </li>
        @foreach (Auth::user()->roles()->get() as $rol)
            @if($rol->id == 1)
                <li class="nav-item">
                    <a class="nav-link activa" href="{{ route('rendimiento.socios.operaciones') }}">Operaciones</a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" href="{{ route('admin.socios.socios') }}">+ Depositos</a>
                </li>
            @endif
        @endforeach
        
    </ul>
</div>
<div class="container page__container">
    <div class="page-section">
 
        <div class="card card-form d-flex flex-column flex-sm-row mb-lg-32pt">
            <div class="card-form__body card-body-form-group flex text-center" style="background-color: white;">
                <form id="update_operacion_form" method="POST" action="{{ route('operations_update_partner_edit') }}">
                    <input type="hidden" name="_token" value="{{ csrf_token() }}"></input>
                    <input type="hidden" name="id" value="{{$accumulated->id}}"></input>
                    <div class="card-header"><h4>{{$partner->user->name}} {{$partner->user->first_last_name}} {{$partner->user->second_last_name}}</h4></div>
                    <div class="card-body">
                        <div class="form-group row align-items-center mb-0">
                            <label class="col-form-label form-label col-sm-12">TIPO DE OPERACION</label>
                            <div class="col-sm-12">
                                <select name="is_commision" id="is_commision" class="form-control custom-select" onchange="updateCommision();" required>
                                    <option value="{{$accumulated->is_commision}}" selected>@php echo $accumulated->is_commision=='1'?"Comisión":"Operación"; @endphp</option>
                                    @if($accumulated->is_commision == '1')
                                        <option value="0">Operación</option>
                                    @else
                                        <option value="1">Comisión</option>
                                    @endif
                                </select>
                                
                            </div>
                        </div>

                        <div class="form-group row align-items-center mb-0">
                        <label class="col-form-label form-label col-sm-12">FECHA DE INICIO</label>
                            <div class="col-sm-4">
                                <label class="col-form-label form-label" style="color:#77c13a !important;">AÑO</label>
                                <select id="year" name="year" class="form-control" required>
                                    <option value="{{$date->year}}" selected>{{$date->year}}</option>
                                    @foreach($years as $year)
                                        <option value="{{$year}}">{{$year}}</option>
                                    @endforeach
                                </select>
                            </div>
                            
                            <div class="col-sm-4">
                                <label class="col-form-label form-label" style="color:#77c13a !important;">MES</label>
                                <select id="month" name="month" class="form-control" required>
                                    <option value="{{$date->month}}" selected>{{$date->month}}</option>
                                    <option value="01">01</option>
                                    <option value="02">02</option>
                                    <option value="03">03</option>
                                    <option value="04">04</option>
                                    <option value="05">05</option>
                                    <option value="06">06</option>
                                    <option value="07">07</option>
                                    <option value="08">08</option>
                                    <option value="09">09</option>
                                    <option value="10">10</option>
                                    <option value="11">11</option>
                                    <option value="12">12</option>
                                    
                                </select>
                            </div>
                            
                            <div class="col-sm-4">
                                <label class="col-form-label form-label" style="color:#77c13a !important;">DIA</label>
                                <select id="day" name="day" class="form-control" required>
                                    <option value="{{$date->day}}" selected>{{$date->day}}</option>
                                    @foreach($days as $day)
                                        <option value="{{$day}}">{{$day}}</option>
                                    @endforeach
                                </select>
                            </div>
                        </div>
                        
                        <div class="form-group row align-items-center mb-0" id="date_end" style="visibility:hidden">
                        <label class="col-form-label form-label col-sm-12">FECHA FIN</label>
                            <div class="col-sm-4">
                                <label class="col-form-label form-label" style="color:#77c13a !important;">AÑO</label>
                                <select id="year" name="year_end" class="form-control">
                                    <option value="{{$date_end->year}}" selected>{{$date_end->year}}</option>
                                    @foreach($years as $year)
                                        <option value="{{$year}}">{{$year}}</option>
                                    @endforeach
                                </select>
                            </div>
                            
                            <div class="col-sm-4">
                                <label class="col-form-label form-label" style="color:#77c13a !important;">MES</label>
                                <select id="month" name="month_end" class="form-control">
                                    <option value="{{$date_end->month}}" selected>{{$date_end->month}}</option>
                                    <option value="01">01</option>
                                    <option value="02">02</option>
                                    <option value="03">03</option>
                                    <option value="04">04</option>
                                    <option value="05">05</option>
                                    <option value="06">06</option>
                                    <option value="07">07</option>
                                    <option value="08">08</option>
                                    <option value="09">09</option>
                                    <option value="10">10</option>
                                    <option value="11">11</option>
                                    <option value="12">12</option>
                                </select>
                            </div>
                            
                            <div class="col-sm-4">
                                <label class="col-form-label form-label" style="color:#77c13a !important;">DIA</label>
                                <select id="day" name="day_end" class="form-control">
                                    <option value="{{$date_end->day}}" selected>{{$date_end->day}}</option>
                                    @foreach($days as $day)
                                        <option value="{{$day}}">{{$day}}</option>
                                    @endforeach
                                </select>
                            </div>
                        </div>
                        
                        <div class="form-group row align-items-center mb-0">
                            <div class="col-sm-12">
                            <input name="partner_id" value="{{$partner->id}}" hidden>
                            </div>
                        </div>
                        <div class="form-group row align-items-center mb-0">
                            <label class="col-form-label form-label col-sm-12">CUENTA</label>
                            <div class="col-sm-12">
                                <select name="type_account_id" onchange="validar()" id="expire_month" class="form-control custom-select" required>
                                    <option value="{{$accounts->id}}" selected>{{$accounts->name}} - {{$accounts->account_number}}</option>
                                    @foreach ($accounts2 as $account)
                                        @if($account->id != $accounts->id)
                                            <option value="{{ $account->id }}">{{ $account->name }} - {{$account->account_number}}</option>
                                        @endif
                                    @endforeach
                                </select>
                                
                            </div>
                        </div>

                                
                        <div class="form-group mb-0">
                            <div class="form-row">
                                <div class="col-xs-12 col-md-12 col-lg-12">
                                    <label class="col-form-label form-label col-xs-12 col-md-12 col-lg-12" style="padding:0;">GANANCIA O PERDIDA ACUMULADA (USD)</label>
                                    <div class="col-xs-12 col-md-12 col-lg-12" style="padding:0;">
                                        <input name="amount" id="amount_data"  value="{{$accumulated->amount}}" type="number" step="any" onchange="validar()" class="form-control col-xs-12 col-md-12 col-lg-12" placeholder="Cantidad en dolares" required />
                                        
                                    </div>
                                </div>
                            </div>
                        </div>     
                        <br>
                        <div class="form-group row align-items-center mb-0">
                            <div class="col-sm-12" style="display: flex; justify-content:center;">
                                <button class="btn btn-accent" style="background-color: #185bc3; width:15rem;" id="boton" type="submit">Actualizar</button>
                            </div>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>
@endsection
@section('script')
<script>
function updateCommision(){
    var x = document.getElementById("is_commision");
    var date_end = document.getElementById("date_end");
    var amountData = document.getElementById("amount_data");
    if(x.value=='1'){
        date_end.setAttribute('style','visibility:visible');
        amountData.setAttribute("min", "0");
    }
    else{
        date_end.setAttribute('style','visibility:hidden');
        amountData.setAttribute("min", "-10000000000");
    }
    
}
updateCommision();
</script>

@endsection