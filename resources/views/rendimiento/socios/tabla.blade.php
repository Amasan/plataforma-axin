@extends('layoutAdmins')

@section('content')
<div class="m-4"> 
        <br>
        <h1 class="text-center">Tabla</h1>
        <br>
        <div class="table-responsive">
        <table class="table">
            <thead>
                <tr>
                    <th>Número</th>
                    <th>Estrategia</th>
                    <th>Fecha abierta</th>
                    <th>Fecha cerrada</th>
                    <th>Instrumento</th>
                    <th>Acción</th>
                    <th>Lotes</th>
                    <th>SL (Price)</th>
                    <th>TC (Price)</th>
                    <th>Open price</th>
                    <th>Closed Price</th>
                    <th>Pips</th>
                    <th>Net profit</th>
                    <th>Gain</th>
                </tr>
            </thead>
            <tbody>

                <tr>
                    <td>1</td>
                    <td>Upimore</td>
                    <td>07/15/20 18:08</td>
                    <td>07/16/20 18:08</td>
                    <td>EURUSD</td>
                    <td>Sell</td>
                    <td>0,02</td>
                    <td>1,1480</td>
                    <td>1,1242</td>
                    <td>1,14197</td>
                    <td>1,13746</td>
                    <td>45,1</td>
                    <td>4,51</td>
                    <td>0.39%</td>
                </tr>

            </tbody>
        </table>

    </div>
</div>
@endsection

