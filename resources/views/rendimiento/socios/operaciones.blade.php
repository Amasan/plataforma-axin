@extends('layoutAdmins')

@section('content')
    <style>
        .activa {
            background-color: #185bc3!important;
            color: white!important;
        }
    </style>
    <div class="m-4">
        <ul class="nav nav-tabs nav-tabs-card">
            <li class="nav-item">
                <a class="nav-link" href="{{ route('rendimiento.socios.depositos') }}">Depositos</a>
            </li>
            <li class="nav-item">
                <a class="nav-link" href="{{ route('rendimiento.socios.retirarFondos') }}">Retiros</a>
            </li>
            <li class="nav-item">
                <a class="nav-link" href="{{ route('rendimiento.socios.movimientos') }}">Movimientos</a>
            </li>
            @foreach (Auth::user()->roles()->get() as $rol)
                @if($rol->id == 1)
                    <li class="nav-item">
                        <a class="nav-link activa" href="{{ route('rendimiento.socios.operaciones') }}">Operaciones</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" href="{{ route('admin.socios.socios') }}">+ Depositos</a>
                    </li>
                @endif
            @endforeach
            
        </ul>
        <br>
        <br>
        <form action="{{ route('rendimiento.socios.operaciones') }}" method="GET" class="form-inline float-left">
            <div class="form-group">
                <input type="text" name="name" class="form-control" placeholder="Nombre">
            </div>
            <div class="form-group">
                <input type="text" name="email" class="form-control" placeholder="Correo electronico">
            </div>
            <div class="form-group">
                <button type="submit" class="btn btn-default"><span class="fas fa-search"></span></button>
            </div>
        </form>
        <div class="table-responsive">
        <table class="table">
            <thead>
                <tr>
                    <th>Nombre completo</th>
                    <th>Pais</th>
                    <th>Correo</th>
                    <th>Telefono</th>
                    <th></th>
                    <th></th>
                    <th></th>
                </tr>
            </thead>
            <tbody>
            @foreach ( $partners as $partner )
                 <tr>
                    <td>{{$partner->name}} {{$partner->first_last_name}} {{$partner->second_last_name}}</td>
                    <td>{{$partner->birth_date_country}}</td>
                    <td>{{$partner->email}}</td>
                    <td>{{$partner->number_phone}}</td>
                    <td>
                        <a href="{{route('rendimiento.socios.operaciones_form', $partner->id)}}" class="btn btn-primary btn-block" style="background-color: #185bc3; width:10rem;color:white" type="submit">
                            <span class="spinner-text">Operacion</span>
                        </a>
                    </td>
                    <td>
                        <a href="{{route('historyOperationsPartner', $partner->id)}}" class="btn btn-primary btn-block" style="background-color: #185bc3; width:10rem;color:white" type="submit">
                            <span class="spinner-text">Historial</span>
                        </a>
                    </td>             
                </tr>
                {{--  Modal  --}}
                <div class="modal fade " id="transferir" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
                    <div class="modal-dialog col-lg-12" role="document">
                        <div class="modal-content">
                            <div class="modal-header">
                                <h5 class="modal-title" id="exampleModalLabel">Comisiones</h5>
                            </div>
                            <div class="modal-body">
                                <form id="operacion" method="POST" action="{{route('updateComissionPartner')}}">
                                <input type="hidden" name="_token" value="{{ csrf_token() }}" id="_token"></input>
                                    
                                    <div class="form-row">
                                        <div class="form-group col-12 row">
                                            <div class="col-sm-6">
                                                <label class="col-form-label form-label">AÑO</label>
                                                <select id="year" name="year" class="form-control" required>
                                                    <option value="2020" selected>2020</option>
                                                    @foreach($years as $year)
                                                        <option value="{{$year}}">{{$year}}</option>
                                                    @endforeach
                                                </select>
                                            </div>
                                            
                                            <div class="col-sm-6">
                                                <label class="col-form-label form-label">MES</label>
                                                <select id="month" name="month" class="form-control" required>
                                                    <option value="01" selected>01</option>
                                                    <option value="02">02</option>
                                                    <option value="03">03</option>
                                                    <option value="04">04</option>
                                                    <option value="05">05</option>
                                                    <option value="06">06</option>
                                                    <option value="07">07</option>
                                                    <option value="08">08</option>
                                                    <option value="09">09</option>
                                                    <option value="10">10</option>
                                                    <option value="11">11</option>
                                                    <option value="12">12</option>
                                                </select>
                                            </div>
                                            
                                            <div hidden class="col-sm-4">
                                                <label class="col-form-label form-label">DIA</label>
                                                <select id="day" name="day" class="form-control" required>
                                                    <option value="1" selected>1</option>
                                                    @foreach($days as $day)
                                                        <option value="{{$day}}">{{$day}}</option>
                                                    @endforeach
                                                </select>
                                            </div>
                                        </div>
                                        <div class="col-12 form-group row">
                                                <label class="col-form-label form-label col-sm-12">CUENTA</label>
                                                <div class="col-sm-12">
                                                    <select name="type_account_id" value="{{ old('type_account_id') }}" id="type_account_id" class="form-control custom-select" required>
                                                        <option disabled value="" selected>Selecciona una cuenta</option>
                                                        @foreach ($accounts as $account) 
                                                            <option value="{{ $account['id'] }}">{{ $account['name'] }}</option>
                                                        @endforeach
                                                    </select>
                                                    
                                                </div>
                                        </div>
                                        <input type="number" hidden id="commision_usd" name="commision_usd">
                                        <input type="number" hidden id="partner_id" value="{{$partner->id}}" name="partner_id">
                                        <div class="col-12 form-group row">
                                            <div class="col-sm-8">
                                                <label for="">Porcentaje de comision</label>
                                                <input id="commision" name="commision" type="number" class="form-control inputFormuT" placeholder="%" required>
                                            </div>
                                            <div class="col-sm-3">
                                                <label for="">&nbsp;</label>
                                                <button type="button" class="btn btn-success" onclick="getComisionStatusPartner('{{$partner->id}}')">Calcular</button>
                                            </div>
                                        </div>
                                        <div class="col-12 form-group" align="center" id="resultadoComision">
                                        </div>
                                    </div>
                                </form>
                            </div>
                            <div class="modal-footer">
                                <button type="button" class="btn btn-secondary" data-dismiss="modal">Cancelar</button>
                                <button id="realizar" type="submit" onclick="event.preventDefault();
                                                document.getElementById('operacion').submit();" class="btn btn-primary" data-toggle="modal" data-backdrop="static" data-keyboard="false" data-dismiss="modal" disabled>Realizar</button>
                                
                            </div>
                        </div>
                    </div>
                </div>
            @endforeach
            </tbody>
        </table>
        {{ $partners->links() }}
        
    </div>
</div>
@endsection

